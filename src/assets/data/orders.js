export const orderItemActionEnum = [
  { id: 'ADD', name: 'ADD' },
  { id: 'MODIFY', name: 'MODIFY' },
  { id: 'DELETE', name: 'DELETE' }
];
