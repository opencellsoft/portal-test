export const billingCycle = [
  { id: 'BC_EIR_DAILY', name: 'BC_EIR_DAILY' },
  { id: 'BC_EIR_MONTHLY_1ST', name: 'BC_EIR_MONTHLY_1ST' },
  { id: 'CYC_INV_MT_1', name: 'CYC_INV_MT_1' },
  { id: 'CYC_INV_MT_2', name: 'CYC_INV_MT_2' },
  { id: 'BC_FD_MONTHLY_1ST', name: 'BC_FD_MONTHLY_1ST' }
];
