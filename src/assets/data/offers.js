export const lifeCycleStatus = [
  { id: 'LAUNCHED', name: 'LAUNCHED' },
  { id: 'ACTIVE', name: 'ACTIVE' },
  { id: 'IN_DESIGN', name: 'IN_DESIGN' },
  { id: 'RETIRED', name: 'RETIRED' },
  { id: 'IN_TEST', name: 'IN_TEST' },
  { id: 'REJECTED', name: 'REJECTED' },
  { id: 'IN_STUDY', name: 'IN_STUDY' },
  { id: 'OBSOLETE', name: 'OBSOLETE' }
];
