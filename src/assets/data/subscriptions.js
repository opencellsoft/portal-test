export const statChoices = [
  { id: 'ACTIVE', name: 'ACTIVE' },
  { id: 'TERMINATED', name: 'TERMINATED' },
  { id: 'CANCELED', name: 'CANCELED' },
  { id: 'INACTIVE', name: 'INACTIVE' },
  { id: 'CLOSED', name: 'CLOSED' },
  { id: 'SUSPENDED', name: 'SUSPENDED' }
];

export const servicesChoices = [
  { id: 'SE_FD_REC_MAIN', name: 'SE_FD_REC_MAIN' },
  { id: 'SE_FD_TRY', name: 'SE_FD_TRY' }
];

export const offerTemplateChoices = [
  { id: 'OF_FD_FNAC_PLUS', name: 'OF_FD_FNAC_PLUS' },
  { id: 'OF_FD_FNAC_SERENITE_M', name: 'OF_FD_FNAC_SERENITE_M' },
  { id: 'OF_FD_FNAC_SERENITE_Y', name: 'OF_FD_FNAC_SERENITE_Y' }
];

export const offerCode = [
  { id: '9083949', name: '9083949' },
  { id: '5311209', name: '5311209' },
  { id: '14993318', name: '14993318' }
];

export const CFCode = [
  { id: 'CF_SU_FD_OFFER_CODE', name: 'CF_SU_FD_OFFER_CODE' }
];
