export const countries = [{ id: 'FR', name: 'FR' }, { id: 'US', name: 'US' }];

export const languages = [
  { id: 'FRA', name: 'FRA' },
  { id: 'ENG', name: 'ENG' }
];
