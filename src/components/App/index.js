import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Admin } from 'react-admin';
import { Route } from 'react-router-dom';
import { isEmpty, get } from 'lodash';
import { MuiPickersUtilsProvider } from 'material-ui-pickers';
import MomentUtils from '@date-io/moment';

import { signIn as signInAction } from '../../actions/user';
import { getPermissions, isAllowed } from '../../utils/authorization';

import GenericPage from '../GenericRenderer/Layout';
import Resource from '../Globals/Resource';

import Layout from '../Layout';
import Directory from '../Layout/Directory';
import LoginPage from '../Auth/LoginPage';
import getCustomRoutes from '../../routes';
import { getDirectoryRoutes } from '../../utils/routing';

import {
  CRUD_LIST,
  CRUD_SHOW,
  CRUD_CREATE,
  CRUD_EDIT
} from '../../constants/generic';

export class App extends Component {
  static propTypes = {
    config: PropTypes.shape({
      menu: PropTypes.object,
      modules: PropTypes.object
    }).isRequired
  };

  componentWillMount = () => this.handleUser();

  componentDidUpdate = () => this.handleUser();

  handleUser = () => {
    const { currentUser, signInAction } = this.props;

    if (isEmpty(currentUser)) {
      signInAction();
    }
  };

  renderDynamicResources = () => {
    const { config, currentUser } = this.props;
    const { modules = {} } = config || {};

    return Object.values(modules).reduce(
      (
        res,
        {
          resource,
          roles,
          entity,
          subItems,
          pages: { list, show, edit, create, ...customPages } = {},
          drawer = {}
        }
      ) => {
        const userPermissions = getPermissions(currentUser);

        const isAllowedEdit = isAllowed(roles, userPermissions, 'edit');
        const isAllowedCreate = isAllowed(roles, userPermissions, 'create');

        res.push(
          <Resource
            key={resource}
            name={resource}
            list={
              list
                ? (props) => (
                    <GenericPage
                      {...props}
                      entity={entity}
                      page={CRUD_LIST}
                      config={list}
                      drawer={drawer}
                      isAllowedEdit={isAllowedEdit}
                      isAllowedCreate={isAllowedCreate}
                    />
                  )
                : undefined
            }
            show={
              show
                ? (props) => (
                    <GenericPage
                      {...props}
                      entity={entity}
                      page={CRUD_SHOW}
                      config={show}
                    />
                  )
                : undefined
            }
            edit={
              edit && isAllowedEdit
                ? (props) => (
                    <GenericPage
                      {...props}
                      entity={entity}
                      page={CRUD_EDIT}
                      config={edit}
                      isEdit
                    />
                  )
                : undefined
            }
            create={
              create && isAllowedCreate
                ? (props) => (
                    <GenericPage
                      {...props}
                      page={CRUD_CREATE}
                      config={create}
                    />
                  )
                : undefined
            }
            customPages={
              !isEmpty(customPages)
                ? Object.keys(customPages).map((page) => {
                    const { path, ...config } = customPages[page];

                    return {
                      path: path || page,
                      component: (props) => (
                        <GenericPage {...props} page={page} config={config} />
                      )
                    };
                  })
                : undefined
            }
          />
        );

        return res;
      },
      []
    );
  };

  renderDirectoryRoutes = () =>
    getDirectoryRoutes(this.props.config).map(({ resource, subItems }) => (
      <Route
        key={resource}
        path={`/${resource}`}
        exact
        render={() => <Directory subItems={subItems} />}
      />
    ));

  render = () => (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <Admin
        {...this.props}
        appLayout={Layout}
        loginPage={LoginPage}
        customRoutes={[
          ...getCustomRoutes(get(this.props, 'config.options.routes', {})),
          ...this.renderDirectoryRoutes(this.props.config)
        ]}
      >
        {this.renderDynamicResources()}
      </Admin>
    </MuiPickersUtilsProvider>
  );
}

export default connect(
  ({
    config: { config },
    user,
    admin,
    theme: {
      theme: { currentTheme: theme }
    }
  }) => ({
    config,
    currentUser: user,
    isLoggedIn: admin.auth.isLoggedIn,
    theme
  }),
  { signInAction }
)(App);
