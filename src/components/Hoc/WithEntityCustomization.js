import { cloneElement, useCallback } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { withDataProvider, GET_ONE, CRUD_GET_ONE_SUCCESS } from 'react-admin';
import { isEmpty, get } from 'lodash';

import { RESOURCE_ENTITY_CUSTOMIZATION } from '../../config/resources';
import KeyCache from '../../utils/key-cache';

const ENTITY_CUSTOMIZATION_TIMEOUT = 3000;
const staticCache = new KeyCache(ENTITY_CUSTOMIZATION_TIMEOUT);

const WithEntityCustomization = (props) => {
  const {
    children,
    dataProvider,
    verb,
    entityIdentifier,
    dispatch,
    data,
    ...rest
  } = props;
  const { entity } = rest;

  const fetchSchema = useCallback(
    (entity) => {
      const cacheID = {
        namespace: RESOURCE_ENTITY_CUSTOMIZATION,
        key: entityIdentifier
      };
      if (staticCache.hasCache(cacheID)) {
        return;
      }

      staticCache.setCache(cacheID);
      const payload = {
        id: entityIdentifier
      };

      dataProvider(verb, RESOURCE_ENTITY_CUSTOMIZATION, payload)
        .then((response) =>
          dispatch({
            type: CRUD_GET_ONE_SUCCESS,
            payload: response,
            requestPayload: payload,
            meta: {
              resource: RESOURCE_ENTITY_CUSTOMIZATION,
              basePath: `/${RESOURCE_ENTITY_CUSTOMIZATION}`,
              fetchResponse: 'GET_ONE',
              fetchStatus: 'RA/FETCH_END'
            }
          })
        )
        .catch((error) => {
          staticCache.removeCache(cacheID);
        });
    },
    [entityIdentifier, verb]
  );

  if (isEmpty(entity)) {
    return children;
  }

  if (data) {
    return cloneElement(children, { fieldsDefinition: data });
  }

  fetchSchema(entity);

  return null;
};

WithEntityCustomization.propTypes = {
  entity: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
  data: PropTypes.array,
  verb: PropTypes.string,
  entityIdentifier: PropTypes.string,
  dataProvider: PropTypes.func.isRequired,
  dispatch: PropTypes.func.isRequired,
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.array])
};

export default compose(
  connect(
    (
      {
        admin: {
          resources: {
            [RESOURCE_ENTITY_CUSTOMIZATION]: { data }
          }
        },
        config: { config }
      },
      { resource }
    ) => {
      const entity = get(config.modules, `${resource}.entity`);
      const isDefinitionObject = typeof entity === 'object';
      const entityIdentifier = isDefinitionObject ? entity.name : entity;
      const verb =
        isDefinitionObject && entity.isCustom ? 'GET_ONE_CUSTOM' : GET_ONE;

      return {
        data: get(data, entityIdentifier),
        entity,
        entityIdentifier,
        verb
      };
    }
  ),
  withDataProvider
)(WithEntityCustomization);
