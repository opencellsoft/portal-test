import PropTypes from 'prop-types';
import { isEmpty, isEqual } from 'lodash';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { getPermissions } from '../../utils/authorization';

const WithRoles = ({
  roles = [],
  permissions = [],
  userPermissions = [],
  children
}) => {
  const allowed = !isEmpty(roles)
    ? roles.every((role) => userPermissions.includes(role))
    : !isEmpty(permissions)
    ? isEqual(
        userPermissions.filter((value) => permissions.includes(value)),
        permissions
      )
    : true;

  return allowed ? children : null;
};

WithRoles.propTypes = {
  roles: PropTypes.array.isRequired,
  permissions: PropTypes.array,
  userPermissions: PropTypes.array,
  children: PropTypes.object
};

export default compose(
  connect(
    ({ user }) => ({
      userPermissions: getPermissions(user)
    }),
    {}
  )
)(WithRoles);
