import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';

/**
 * Extract specified props to use
 * as references for cache
 *
 * @param {object} props
 * @param {array} dependencies
 */
const extractDependentProps = (props, dependencies) => {
  if (!dependencies) {
    return null;
  }

  return dependencies.reduce((res, propKey) => {
    res[propKey] = props[propKey];

    return res;
  }, {});
};

// Using variable outside of scope for default value to preserve reference
const defaultDependencies = [];

/**
 * Allows to generate styles from theme + props
 * If no dependencies are specified (props that should trigger re-renders)
 * WrappedComponent will be memoized the first time
 *
 * @param {object} styles
 * @param {array} dependencies
 */
const withStylesProps = (styles, dependencies = defaultDependencies) => (
  Component
) => (props) => {
  const dependentProps = useMemo(
    () => extractDependentProps(props, dependencies),
    dependencies
  );
  const WrappedComponent = useMemo(
    () => compose(withStyles((theme) => styles(theme, props)))(Component),
    dependentProps
  );

  return <WrappedComponent {...props} />;
};

withStylesProps.propTypes = {
  styles: PropTypes.object.isRequired,
  dependencies: PropTypes.arrayOf(PropTypes.string)
};

export default withStylesProps;
