import { cloneElement, useMemo } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { getFormValues } from 'redux-form';
import { get, isEmpty, has } from 'lodash';
import { mergeCfsWithDefinitions } from '../../utils/custom-fields';

const WithDependentValueCfs = ({
  children,
  fieldsDefinition,
  formValues,
  item
}) => {
  const fields = useMemo(
    () => {
      const { confirm } = item || {};

      const { customFields: confirmCustomFields = {} } = confirm || {};

      const customFieldsFromFormValues =
        !isEmpty(confirmCustomFields) &&
        has(formValues, confirmCustomFields.referenceField)
          ? get(
              formValues,
              `${confirmCustomFields.referenceField}.customFields.customField`,
              []
            )
          : [];

      const fieldsWithMeta = !isEmpty(customFieldsFromFormValues)
        ? mergeCfsWithDefinitions(customFieldsFromFormValues, fieldsDefinition)
        : [];

      return fieldsWithMeta;
    },
    [fieldsDefinition, formValues, item]
  );

  return cloneElement(children, { fields });
};

WithDependentValueCfs.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.array]).isRequired,
  form: PropTypes.string.isRequired,
  fieldsDefinition: PropTypes.object.isRequired,
  item: PropTypes.object.isRequired,
  formValues: PropTypes.object
};

export default compose(
  connect((state, props) => ({
    formValues: getFormValues(props.form)(state)
  }))
)(WithDependentValueCfs);
