import React, { cloneElement, useState } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { isEmpty, get } from 'lodash';
import cx from 'classnames';
import { REDUX_FORM_NAME } from 'react-admin';
import {
  Dialog,
  DialogContent,
  DialogContentText,
  DialogActions,
  DialogTitle,
  Button
} from '@material-ui/core';
import { getFormValues, isInvalid } from 'redux-form';
import { withStyles } from '@material-ui/core/styles';

import withTranslate from './withTranslate';

const styles = ({ spacing }) => ({
  title: {
    '&::first-letter': {
      textTransform: 'capitalize'
    }
  },
  contentText: {
    minWidth: 400
  },
  overflow: {
    overflowY: 'visible'
  }
});

const getChildrenCFsFromParentCF = (
  targetContainer,
  index,
  formValues,
  record,
  cfParent,
  cfParentKey,
  source
) => {
  targetContainer[index] = {
    ...targetContainer[index],
    ...formValues
  };

  const parentCfItems = get(record, cfParent, []);
  const parentCf = parentCfItems.find(
    (elm) => elm[cfParentKey] === targetContainer[index][cfParentKey]
  );
  const parentIndex = parentCfItems.indexOf(parentCf);
  parentCf[source][+index] = targetContainer[+index];
  parentCfItems[parentIndex] = parentCf;

  return { ...record, [cfParent]: parentCfItems };
};

const WithExtendedSave = ({
  children,
  save,
  classes,
  fieldsDefinition,
  config,
  record,
  isFormInvalid,
  formValues,
  withParentRecord,
  cfParent,
  cfParentKey,
  ...props
}) => {
  const [notify, setNotification] = useState(false);
  const { afterSubmitActions } = config;
  const { label, content, dependsOn } =
    (!isEmpty(afterSubmitActions) &&
      afterSubmitActions.find(({ action }) => action === 'modal')) ||
    {};
  const enhancedSave = !isEmpty(fieldsDefinition)
    ? (data, redirect) => {
        if (isFormInvalid) return false;
        const hasDepends =
          !isEmpty(dependsOn) &&
          Object.keys(dependsOn).reduce((res, key) => {
            if (!isEmpty(record) && get(data, key) !== get(record, key)) {
              return true;
            }

            return false;
          }, false);
        if (withParentRecord) {
          const { source, index } = props;
          const targetContainer = get(record, source, []);
          // check if there is another level of data
          if (isEmpty(targetContainer)) {
            data = getChildrenCFsFromParentCF(
              targetContainer,
              index,
              formValues,
              record,
              cfParent,
              cfParentKey,
              source
            );
          } else {
            targetContainer[index] = {
              ...targetContainer[index],
              ...formValues
            };
            data = { ...record, [source]: targetContainer };
          }
        }

        return save(
          {
            ...data,
            __cfDefinition: fieldsDefinition
          },
          !hasDepends
            ? redirect
            : () => {
                setNotification(!notify);
              }
        );
      }
    : save;

  //TODO can we use Globals/Dialogs here?
  return (
    <React.Fragment>
      {cloneElement(children, {
        save: enhancedSave,
        ...props
      })}
      <>
        <Dialog open={notify} onClose={(notify) => setNotification(!notify)}>
          <DialogTitle className={classes.title}>{label}</DialogTitle>
          <DialogContent className={cx(classes.contentText, classes.overflow)}>
            <DialogContentText>{content}</DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={(notify) => setNotification(!notify)}
              color="primary"
            >
              OK
            </Button>
          </DialogActions>
        </Dialog>
      </>
    </React.Fragment>
  );
};

WithExtendedSave.propTypes = {
  record: PropTypes.object,
  config: PropTypes.object,
  isFormInvalid: PropTypes.bool,
  fieldsDefinition: PropTypes.object,
  classes: PropTypes.object.isRequired,
  save: PropTypes.func,
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.array]),
  source: PropTypes.string,
  index: PropTypes.string,
  withParentRecord: PropTypes.bool,
  formValues: PropTypes.object,
  cfParent: PropTypes.string,
  cfParentKey: PropTypes.string
};
const mapStateToProps = (state, { config, form }) => ({
  formValues: getFormValues(form || REDUX_FORM_NAME)(state),
  isFormInvalid: isInvalid(form || REDUX_FORM_NAME)(state)
});
export default compose(
  connect(mapStateToProps),
  withTranslate,
  withStyles(styles)
)(WithExtendedSave);
