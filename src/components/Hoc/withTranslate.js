import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import {
  translate,
  getFieldLabelTranslationArgs as originGetFieldLabelTranslationArgs
} from 'react-admin';
import { connect } from 'react-redux';
import { compose } from 'recompose';

import { getFullReference } from '../../utils/routing';

/**
 *
 * @param {string} key
 * @param {object} modules
 *
 * @return string
 */
const transformKey = (key = '', modules) =>
  typeof key === 'string'
    ? key.replace(
        /^resources\.([^.]+)\.(.+)$/i,
        (match, resource, restPath) =>
          `resources.${getFullReference(resource, modules)}.${restPath}`
      )
    : '';

export const getFieldLabelTranslationArgs = (
  { label, resource, ...restProps },
  modules
) =>
  originGetFieldLabelTranslationArgs({
    label: transformKey(label, modules),
    resource: transformKey(resource, modules),
    ...restProps
  });

const WithTranslate = (WrappedComponent) => ({
  translate: translateOrigin,
  ...restProps
}) => {
  const translate = useCallback(
    (key, options) =>
      translateOrigin(transformKey(key, restProps.modules), options),
    [translateOrigin]
  );

  return <WrappedComponent translate={translate} {...restProps} />;
};

WithTranslate.propTypes = {
  translate: PropTypes.func.isRequired
};

export default (WrappedComponent) =>
  compose(
    connect(({ config: { config: { modules } } }) => ({
      modules
    })),
    translate
  )(WithTranslate(WrappedComponent));
