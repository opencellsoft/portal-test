import React from 'react';
import PropTypes from 'prop-types';
import { Query } from 'react-admin';
import { isEmpty } from 'lodash';

const withQueryProps = () => (Component) => (props) => {
  const { verb, resource } = props;

  return !isEmpty(verb) && !isEmpty(resource) ? (
    <Query type={verb} resource={resource}>
      {(queryProps) => <Component {...props} {...queryProps} />}
    </Query>
  ) : (
    <Component {...props} />
  );
};

withQueryProps.propTypes = {
  verb: PropTypes.string.isRequired,
  resource: PropTypes.string.isRequired
};

export default withQueryProps;
