import React, { cloneElement } from 'react';
import PropTypes from 'prop-types';
import { isEmpty, get } from 'lodash';
import { onlyUpdateForKeys } from 'recompose';

import {
  CreateButton,
  ExportButton,
  CardActions,
  sanitizeListRestProps
} from 'react-admin';

import WithRoles from '../../Hoc/WithRoles';

import UploadButton from '../Buttons/UploadButton';

const ListActions = ({
  bulkActions,
  currentSort,
  className,
  resource,
  filters,
  withUpload,
  displayedFilters,
  exporter,
  filterValues,
  permanentFilter,
  withCreate,
  basePath,
  selectedIds,
  onUnselectItems,
  showFilter,
  total,
  config,
  ...rest
}) => {
  if (
    !withUpload &&
    isEmpty(bulkActions) &&
    isEmpty(filters) &&
    !withCreate &&
    isEmpty(exporter)
  ) {
    return null;
  }

  const { uploadedFileFormat } = config;

  return (
    <CardActions className={className} {...sanitizeListRestProps(rest)}>
      {filters &&
        cloneElement(filters, {
          resource,
          showFilter,
          displayedFilters,
          filterValues,
          context: 'button'
        })}

      {withCreate && (
        <WithRoles roles={get(withCreate, 'roles')}>
          <CreateButton basePath={basePath} />
        </WithRoles>
      )}

      {withUpload && (
        <WithRoles roles={get(withUpload, 'roles')}>
          <UploadButton
            verb="UPLOAD_FILE"
            resource={resource}
            uploadedFileFormat={uploadedFileFormat}
          />
        </WithRoles>
      )}

      {bulkActions &&
        cloneElement(bulkActions, {
          basePath,
          filterValues,
          resource,
          selectedIds,
          onUnselectItems
        })}

      <ExportButton
        disabled={total === 0}
        resource={resource}
        sort={currentSort}
        filter={{ ...filterValues, ...permanentFilter }}
        exporter={exporter}
      />
    </CardActions>
  );
};

ListActions.propTypes = {
  bulkActions: PropTypes.oneOfType([PropTypes.element, PropTypes.bool]),
  basePath: PropTypes.string,
  className: PropTypes.string,
  currentSort: PropTypes.object,
  displayedFilters: PropTypes.object,
  exporter: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  filters: PropTypes.element,
  filterValues: PropTypes.object,
  hasCreate: PropTypes.bool,
  withUpload: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  resource: PropTypes.string,
  onUnselectItems: PropTypes.func.isRequired,
  selectedIds: PropTypes.arrayOf(PropTypes.any),
  showFilter: PropTypes.func,
  total: PropTypes.number.isRequired
};

ListActions.defaultProps = {
  selectedIds: []
};

export default onlyUpdateForKeys([
  'resource',
  'filters',
  'displayedFilters',
  'filterValues',
  'selectedIds'
])(ListActions);
