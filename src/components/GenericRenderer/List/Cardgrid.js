/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
// import cx from 'classnames';
import { withStyles } from '@material-ui/core/styles';
// import { isEmpty } from 'lodash';
import { compose } from 'recompose';

import withTranslate from '../../Hoc/withTranslate';

// import Field from '../Fields';
// import Expand from '../Expands';

// import DatagridMain from './DatagridMain';
// import DatagridBody from './DatagridBody';
import CardItem from './CardItem';

const styles = ({ palette, spacing }) => ({
  datagridWrapper: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  bold: {
    fontWeight: 'bold',
    fontSize: '16px'
  },
  centeredLabel: {
    textAlign: 'center'
  }
});

const Cardgrid = ({
  classes,
  ids,
  data,
  basePath,
  className,
  translate,
  ...props
  // eslint-disable-next-line arrow-body-style
}) => {
  // const { resource: distinctResource } = expand || {};
  // const currentResource = distinctResource || resource;

  return (
    <div className={classes.datagridWrapper}>
      {ids.map((id) => (
        <CardItem key={id} />
      ))}

      {/* <Card className={classes.card}>
      <CardContent className={classes.wrapper}>
        <AddIcon className={cx(classes.icon, classes.addInstance)} />
        <Typography gutterBottom component="div" className={classes.title}>
          Add
        </Typography>
      </CardContent>
    </Card> */}
    </div>
  );
};

Cardgrid.propTypes = {
  classes: PropTypes.object.isRequired,
  onRowClick: PropTypes.string,
  fields: PropTypes.array.isRequired,
  expand: PropTypes.object,
  className: PropTypes.string,
  translate: PropTypes.func.isRequired
};

export default compose(
  withTranslate,
  withStyles(styles)
)(Cardgrid);
