/* eslint-disable jsx-a11y/aria-role */
import React, { Component, isValidElement } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { linkToRecord } from 'react-admin';
import { Checkbox, TableCell, TableRow } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';

import IconButton from '@material-ui/core/IconButton';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import DatagridCell from './DatagridCell';

const styles = ({ spacing, palette }) => ({
  rowCell: {
    maxWidth: 250
  },
  bulkActionsCell: {},
  expandWrapper: {
    padding: spacing.unit,
    background: palette.background.default,
    '&:last-child': {
      paddingRight: spacing.unit
    }
  }
});

const sanitizeRestProps = ({
  basePath,
  children,
  classes,
  className,
  rowClick,
  id,
  isLoading,
  onToggleItem,
  push,
  record,
  resource,
  selected,
  style,
  styles,
  ...rest
}) => rest;

class CustomDatagridRow extends Component {
  static propTypes = {
    basePath: PropTypes.string,
    children: PropTypes.node,
    classes: PropTypes.object,
    className: PropTypes.string,
    expand: PropTypes.node,
    hasBulkActions: PropTypes.bool,
    hover: PropTypes.bool,
    id: PropTypes.any,
    onToggleItem: PropTypes.func,
    push: PropTypes.func,
    record: PropTypes.object,
    resource: PropTypes.string,
    rowClick: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
    selected: PropTypes.bool,
    style: PropTypes.object,
    styles: PropTypes.object
  };

  static defaultProps = {
    hasBulkActions: false,
    hover: true,
    record: {},
    selected: false
  };

  constructor(props) {
    super(props);

    this.state = {
      expanded: false,
      colSpan: this.computeColSpan(props)
    };
  }

  componentDidUpdate = (prevProps, prevState) => {
    const colSpan = this.computeColSpan(this.props);
    if (colSpan !== prevState.colSpan) {
      this.setState({ colSpan });
    }
  };

  handleToggleExpanded = (event) => {
    this.setState((state) => ({ expanded: !state.expanded }));
    event.stopPropagation();
  };

  handleToggle = (event) => {
    this.props.onToggleItem(this.props.id);
    event.stopPropagation();
  };

  handleClick = async (event) => {
    const { basePath, rowClick, id, record } = this.props;

    if (!rowClick) return;

    if (typeof rowClick === 'function') {
      const path = await rowClick(id, basePath, record);
      this.handleRedirection(path, event);

      return;
    }

    this.handleRedirection(rowClick, event);
  };

  handleRedirection = (path, event) => {
    const { basePath, id, push } = this.props;

    if (path === 'edit') {
      push(linkToRecord(basePath, id));

      return;
    }
    if (path === 'show') {
      push(linkToRecord(basePath, id, 'show'));

      return;
    }
    if (path === 'expand') {
      this.handleToggleExpanded(event);

      return;
    }
    if (!path) return;

    push(path);
  };

  computeColSpan = (props) => {
    const { children, hasBulkActions } = props;

    return (
      1 + // show expand button
      (hasBulkActions ? 1 : 0) + // checkbox column
      React.Children.toArray(children).filter((child) => !!child).length // non-null children
    );
  };

  render = () => {
    const {
      basePath,
      children,
      classes,
      className,
      expand,
      hasBulkActions,
      hover,
      id,
      record,
      resource,
      selected,
      fields,
      style,
      styles,
      ...rest
    } = this.props;
    const { expanded, colSpan } = this.state;

    return (
      <>
        <TableRow
          className={className}
          key={id}
          style={style}
          hover={hover}
          onClick={this.handleClick}
          {...sanitizeRestProps(rest)}
        >
          {expand && (
            <TableCell padding="none" className={cx(classes.expandIconCell)}>
              <IconButton
                className={cx(classes.expandIcon, {
                  [classes.expanded]: expanded
                })}
                component="div"
                tabIndex={-1}
                aria-hidden="true"
                role="expand"
                onClick={this.handleToggleExpanded}
              >
                <ExpandMoreIcon />
              </IconButton>
            </TableCell>
          )}

          {hasBulkActions && (
            <TableCell
              padding="none"
              className={cx(hasBulkActions && classes.bulkActionsCell)}
            >
              <Checkbox
                color="primary"
                className={`select-item ${classes.checkbox}`}
                checked={selected}
                onClick={this.handleToggle}
              />
            </TableCell>
          )}

          {React.Children.map(children, (field, index) =>
            isValidElement(field) ? (
              <DatagridCell
                key={`${id}-${field.props.source || index}`}
                className={cx(`column-${field.props.source}`, classes.rowCell)}
                record={record}
                {...{ field, basePath, resource, rowId: id }}
              />
            ) : null
          )}
        </TableRow>

        {expand && expanded && (
          <TableRow key={`${id}-expand`}>
            <TableCell
              className={classes.expandWrapper}
              colSpan={colSpan}
              role="expand-content"
            >
              {React.cloneElement(expand, {
                record,
                basePath,
                resource,
                id: String(id)
              })}
            </TableCell>
          </TableRow>
        )}
      </>
    );
  };
}

export default compose(
  connect(
    null,
    { push }
  ),
  withStyles(styles)
)(CustomDatagridRow);
