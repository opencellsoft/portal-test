import React, { memo } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { TableCell } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { get } from 'lodash';

const styles = () => ({
  highlightCell: {
    backgroundColor: '#ef9a9a'
  }
});

const sanitizeRestProps = ({
  cellClassName,
  className,
  field,
  formClassName,
  headerClassName,
  record,
  basePath,
  resource,
  ...rest
}) => rest;

export const CustomDatagridCell = memo(
  ({
    className,
    field,
    record,
    basePath,
    resource,
    rowId,
    classes,
    ...rest
  }) => {
    const hasValue = get(record, field.props.source);

    return (
      <TableCell
        className={cx(
          className,
          field.props.cellClassName,
          !hasValue && field.props.highlightEmptyCell && classes.highlightCell
        )}
        numeric={field.props.textAlign === 'right'}
        padding="none"
        {...sanitizeRestProps(rest)}
      >
        {React.cloneElement(field, {
          record,
          basePath: field.props.basePath || basePath,
          resource,
          rowId
        })}
      </TableCell>
    );
  }
);

CustomDatagridCell.propTypes = {
  className: PropTypes.string,
  field: PropTypes.element,
  record: PropTypes.object,
  basePath: PropTypes.string,
  resource: PropTypes.string,
  rowId: PropTypes.number
};

export default withStyles(styles)(CustomDatagridCell);
