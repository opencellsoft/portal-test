import React from 'react';
import { List } from 'react-admin';
import { withStyles, createStyles } from '@material-ui/core/styles';

import Cardgrid from './Cardgrid';

const styles = createStyles({
  card: {
    boxShadow: 'none',
    background: 'transparent'
  }
});

const CardList = ({
  config = {},
  hasList,
  hasShow,
  hasCreate,
  hasEdit,
  ...props
}) => (
  <List
    {...props}
    pagination={false}
    toolbar={false}
    actions={false}
    filters={false}
  >
    <Cardgrid {...props} config={config} />
  </List>
);

export default withStyles(styles)(CardList);
