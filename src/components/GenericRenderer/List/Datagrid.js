import React, { memo } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { isEmpty, get } from 'lodash';
import { compose } from 'recompose';

import withTranslate from '../../Hoc/withTranslate';

import Field from '../Fields';
import Expand from '../Expands';

import DatagridMain from './DatagridMain';
import DatagridBody from './DatagridBody';

const styles = ({ palette, spacing }) => ({
  datagridWrapper: {
    overflowX: 'auto',
    '& td[role=expand-content]': {
      padding: spacing.unit,
      background: palette.background.default
    }
  },
  bold: {
    fontWeight: 'bold',
    fontSize: '16px'
  },
  centeredLabel: {
    textAlign: 'center'
  }
});

const CustomDatagrid = memo(
  ({
    classes,
    onRowClick,
    resource,
    relatedResource,
    pageResource,
    index: recordIndex,
    parentRecord,
    fields = [],
    expand,
    isEdit,
    className,
    translate,
    ...props
  }) => {
    const { record } = props;
    const { resource: distinctResource } = expand || {};
    const currentResource = distinctResource || resource;

    return (
      <div className={classes.datagridWrapper}>
        <DatagridMain
          {...props}
          resource={currentResource}
          rowClick={onRowClick}
          expand={
            !isEmpty(expand) ? (
              <Expand config={expand} parentRecord={record} />
            ) : (
              undefined
            )
          }
          className={cx(classes.datagrid, className)}
          isEdit={isEdit}
          body={<DatagridBody fields={fields} />}
        >
          {fields.map(({ source, dependsOn, label, ...config }, index) => {
            const isDepend =
              !isEmpty(dependsOn) &&
              Object.keys(dependsOn).reduce((res, key) => {
                if (dependsOn[key] === get(record, key)) {
                  return true;
                }

                return false;
              }, false);

            if (!isDepend && !isEmpty(dependsOn)) return null;

            return (
              <Field
                key={`${source}_${index}`}
                source={source}
                isEdit={isEdit}
                pageResource={pageResource}
                parentRecord={parentRecord}
                index={recordIndex}
                label={
                  label ||
                  ((currentResource || pageResource) && source
                    ? translate(
                        `resources.${currentResource ||
                          pageResource}.fields.${source}`
                      )
                    : source || null)
                }
                {...config}
              />
            );
          })}
        </DatagridMain>
      </div>
    );
  }
);

CustomDatagrid.propTypes = {
  classes: PropTypes.object.isRequired,
  onRowClick: PropTypes.string,
  fields: PropTypes.array.isRequired,
  expand: PropTypes.object,
  className: PropTypes.string,
  translate: PropTypes.func.isRequired
};

export default compose(
  withTranslate,
  withStyles(styles)
)(CustomDatagrid);
