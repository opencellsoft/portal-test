import React, { cloneElement } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import {
  ListToolbar,
  Pagination,
  BulkActionsToolbar,
  BulkDeleteButton,
  ListActions,
  getListControllerProps
} from 'react-admin';
import compose from 'recompose/compose';

import CustomActions from '../Table/CustomActions';

import withTranslate from '../../Hoc/withTranslate';

export const styles = {
  root: {
    display: 'flex'
  },
  mainWrapper: {
    position: 'relative',
    flex: '1 1 auto',
    maxWidth: '100%'
  },
  actions: {
    zIndex: 2,
    display: 'flex',
    justifyContent: 'flex-end',
    flexWrap: 'wrap'
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    alignSelf: 'flex-start'
  },
  noResults: { padding: 20 }
};

const sanitizeRestProps = ({
  actions,
  basePath,
  bulkActions,
  changeListParams,
  children,
  classes,
  className,
  crudGetList,
  currentSort,
  data,
  defaultTitle,
  displayedFilters,
  exporter,
  filter,
  filterDefaultValues,
  filters,
  filterValues,
  hasCreate,
  hasEdit,
  hasList,
  hasShow,
  hideFilter,
  history,
  ids,
  initialTab,
  isLoading,
  loadedOnce,
  locale,
  location,
  match,
  onSelect,
  onToggleItem,
  onUnselectItems,
  options,
  page,
  pagination,
  params,
  permissions,
  perPage,
  push,
  query,
  refresh,
  resource,
  selectedIds,
  selectedTab,
  setFilters,
  setPage,
  setPerPage,
  setSelectedIds,
  setSort,
  showFilter,
  sort,
  tabField,
  tabs,
  theme,
  title,
  toggleItem,
  total,
  translate,
  version,
  ...rest
}) => rest;

const validate = (bulkActions, selectedIds) => {
  if (!bulkActions) {
    return [];
  }

  return bulkActions.filter((action) => {
    if (!action.dependsOn) {
      return true;
    }
    const {
      minSelectedItems = 0,
      maxSelectedItems = Number.MAX_VALUE
    } = action.dependsOn;

    return (
      selectedIds.length >= minSelectedItems &&
      selectedIds.length <= maxSelectedItems
    );
  });
};
export const ListView = ({
  // component props
  actions = <ListActions />,
  aside,
  filters,
  bulkActions,
  bulkActionButtons = (
    <BulkDeleteButton
      undoable={
        false // deprecated
      }
    />
  ),
  pagination = <Pagination />,
  children,
  className,
  classes,
  exporter,
  config,
  tabs,
  ...rest
}) => {
  // overridable by user
  const { version, data: record, selectedIds, resource, total } = rest;

  const {
    withDelete = true,
    withPaginationTop = false,
    bulkCustomActions = []
  } = config;
  const controllerProps = getListControllerProps(rest);
  const validBulkActions = validate(bulkCustomActions, selectedIds);
  const selectedItems = record ? selectedIds.map((id) => record[id]) : [];

  return (
    <div
      className={cx('list-page', classes.root, className)}
      {...sanitizeRestProps(rest)}
    >
      <div className={classes.mainWrapper}>
        {bulkActions !== false && bulkActionButtons !== false && (
          <BulkActionsToolbar
            {...rest}
            selectedIds={selectedIds}
            resource={resource}
          >
            <CustomActions
              record={record}
              actions={validBulkActions}
              selectedItems={selectedItems}
            />
            {withDelete ? bulkActionButtons : <></>}
          </BulkActionsToolbar>
        )}

        {(filters || actions) && (
          <ListToolbar
            filters={filters}
            {...controllerProps}
            actions={actions}
            bulkActions={bulkActions}
            exporter={exporter}
          />
        )}

        {tabs}

        <div key={version}>
          {withPaginationTop &&
            total > 0 &&
            pagination &&
            cloneElement(pagination, controllerProps)}

          {children &&
            cloneElement(children, {
              ...controllerProps,
              hasBulkActions:
                bulkActions !== false && bulkActionButtons !== false
            })}

          {pagination && cloneElement(pagination, controllerProps)}
        </div>
      </div>
    </div>
  );
};

ListView.propTypes = {
  actions: PropTypes.element,
  aside: PropTypes.node,
  basePath: PropTypes.string,
  bulkActions: PropTypes.oneOfType([PropTypes.bool, PropTypes.element]),
  bulkActionButtons: PropTypes.oneOfType([PropTypes.bool, PropTypes.element]),
  children: PropTypes.element,
  className: PropTypes.string,
  classes: PropTypes.object,
  currentSort: PropTypes.shape({
    field: PropTypes.string,
    order: PropTypes.string
  }),
  data: PropTypes.object,
  defaultTitle: PropTypes.string,
  displayedFilters: PropTypes.object,
  exporter: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  filterDefaultValues: PropTypes.object,
  filters: PropTypes.element,
  filterValues: PropTypes.object,
  hasCreate: PropTypes.bool,
  hideFilter: PropTypes.func,
  ids: PropTypes.array,
  isLoading: PropTypes.bool,
  onSelect: PropTypes.func,
  onToggleItem: PropTypes.func,
  onUnselectItems: PropTypes.func,
  page: PropTypes.number,
  pagination: PropTypes.oneOfType([PropTypes.bool, PropTypes.element]),
  perPage: PropTypes.number,
  refresh: PropTypes.func,
  resource: PropTypes.string,
  selectedIds: PropTypes.array,
  setFilters: PropTypes.func,
  setPage: PropTypes.func,
  setPerPage: PropTypes.func,
  setSort: PropTypes.func,
  showFilter: PropTypes.func,
  title: PropTypes.any,
  total: PropTypes.number,
  translate: PropTypes.func,
  version: PropTypes.number
};

ListView.defaultProps = {
  classes: {}
};

export default compose(
  withStyles(styles),
  withTranslate
)(ListView);
