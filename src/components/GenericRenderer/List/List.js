import React from 'react';
import PropTypes from 'prop-types';
import { ListController } from 'react-admin';
import { Card } from '@material-ui/core';
import { createStyles } from '@material-ui/core/styles';

import defaultTheme from '../../../styles/defaultTheme';

import ListView from './ListView';

export const styles = ({ spacing }) =>
  createStyles({
    root: {
      display: 'flex'
    },
    card: {
      position: 'relative',
      flex: '1 1 auto',
      margin: `0 ${spacing.unit}px`
    },
    actions: {
      zIndex: 2,
      display: 'flex',
      justifyContent: 'flex-end',
      flexWrap: 'wrap'
    },
    header: {
      display: 'flex',
      justifyContent: 'space-between',
      alignSelf: 'flex-start'
    },
    noResults: { padding: 20 }
  });

/**
 * List page component
 *
 * The <List> component renders the list layout (title, buttons, filters, pagination),
 * and fetches the list of records from the REST API.
 * It then delegates the rendering of the list of records to its child component.
 * Usually, it's a <Datagrid>, responsible for displaying a table with one row for each post.
 *
 * In Redux terms, <List> is a connected component, and <Datagrid> is a dumb component.
 *
 * Props:
 *   - title
 *   - perPage
 *   - sort
 *   - filter (the permanent filter to apply to the query)
 *   - actions
 *   - filters (a React Element used to display the filter form)
 *   - pagination
 *
 * @example
 *     const PostFilter = (props) => (
 *         <Filter {...props}>
 *             <TextInput label="Search" source="q" alwaysOn />
 *             <TextInput label="Title" source="title" />
 *         </Filter>
 *     );
 *     export const PostList = (props) => (
 *         <List {...props}
 *             title="List of posts"
 *             sort={{ field: 'published_at' }}
 *             filter={{ is_published: true }}
 *             filters={<PostFilter />}
 *         >
 *             <Datagrid>
 *                 <TextField source="id" />
 *                 <TextField source="title" />
 *                 <EditButton />
 *             </Datagrid>
 *         </List>
 *     );
 */
const List = ({ classes, config, ...props }) => {
  const { bulkActionButtons } = config || {};

  return (
    <ListController {...props}>
      {(controllerProps) => (
        <Card className={classes.card}>
          <ListView
            {...props}
            {...controllerProps}
            bulkActionButtons={bulkActionButtons}
            config={config}
          />
        </Card>
      )}
    </ListController>
  );
};

List.propTypes = {
  // the props you can change
  actions: PropTypes.element,
  aside: PropTypes.node,
  bulkActions: PropTypes.oneOfType([PropTypes.element, PropTypes.bool]),
  bulkActionButtons: PropTypes.oneOfType([PropTypes.element, PropTypes.bool]),
  children: PropTypes.node,
  classes: PropTypes.object,
  className: PropTypes.string,
  filter: PropTypes.object,
  filterDefaultValues: PropTypes.object,
  filters: PropTypes.element,
  pagination: PropTypes.element,
  perPage: PropTypes.number,
  sort: PropTypes.shape({
    field: PropTypes.string,
    order: PropTypes.string
  }),
  title: PropTypes.any,
  // the props managed by react-admin
  authProvider: PropTypes.func,
  hasCreate: PropTypes.bool.isRequired,
  hasEdit: PropTypes.bool.isRequired,
  hasList: PropTypes.bool.isRequired,
  hasShow: PropTypes.bool.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  path: PropTypes.string,
  resource: PropTypes.string.isRequired,
  theme: PropTypes.object
};

List.defaultProps = {
  filter: {},
  perPage: 10,
  theme: defaultTheme
};

export default List;
