import React, { PureComponent, cloneElement } from 'react';
import PropTypes from 'prop-types';
import { Card } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { get } from 'lodash';

import Tabs from '../../Globals/Tabs';

import ListView from './ListView';
import Datagrid from './Datagrid';

const styles = () => ({
  root: {
    display: 'flex'
  },
  card: {
    position: 'relative',
    flex: '1 1 auto'
  }
});

class ListWrapper extends PureComponent {
  static propTypes = {
    tabs: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired
      })
    ).isRequired,
    selectedTab: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.array]).isRequired,
    filterValues: PropTypes.object.isRequired,
    setFilters: PropTypes.func.isRequired,
    tabField: PropTypes.string.isRequired,
    aside: PropTypes.node,
    classes: PropTypes.object.isRequired
  };

  state = {
    tabs: this.props.config.tabs,
    selectedTab: get(
      this.props,
      'config.initialTab',
      get(this.props, 'config.tabs.0.id')
    )
  };

  componentDidMount = () => {
    const { config, filterValues, setFilters } = this.props;
    const { tabField, initialTab } = config || {};

    setFilters({ ...filterValues, [tabField]: initialTab });
  };

  handleTab = (e, selectedTab) => {
    const { config, filterValues, setFilters } = this.props;
    const { tabField } = config || {};

    return this.setState({ selectedTab }, () =>
      setFilters({ ...filterValues, [tabField]: selectedTab })
    );
  };

  render = () => {
    const { tabs, selectedTab } = this.state;
    const { config, aside, classes, ...rest } = this.props;
    const currentTabConfig = config.tabs.find(({ id }) => id === selectedTab);
    const {
      fields,
      onRowClick = config.onRowClick,
      expand,
      bulkCustomActions
    } = currentTabConfig || {};

    return (
      <div className={classes.root}>
        <Card className={classes.card}>
          <ListView
            {...rest}
            config={config}
            bulkCustomActions={bulkCustomActions}
            tabs={
              <Tabs
                tabs={tabs}
                selectedTab={selectedTab}
                onChange={this.handleTab}
              />
            }
          >
            <Datagrid
              {...this.props}
              config={config}
              onRowClick={onRowClick}
              fields={fields}
              expand={expand}
            />
          </ListView>
        </Card>

        {aside && cloneElement(aside, rest)}
      </div>
    );
  };
}

export default withStyles(styles)(ListWrapper);
