import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { Filter } from 'react-admin';
import compose from 'recompose/compose';
import { isEmpty, get } from 'lodash';

import TextInput from '../Form/Inputs/TextInput';

import { getFilters } from '../../../utils/generic-render';

import withTranslate from '../../Hoc/withTranslate';
import InputFilterRenderer from '../Form/InputFilterRenderer';

const renderForm = (filters, resource, translate, setFilters) => {
  const field = filters.map(({ source, reference, ...rest }) => {
    const label = translate(
      `resources.${resource || reference}.fields.${source}`
    );

    return (
      <InputFilterRenderer
        source={source}
        resource={resource}
        reference={reference}
        label={label}
        {...rest}
      />
    );
  });

  return field;
};

const ListFilters = ({
  config,
  resource,
  localFilter,
  filtersForm,
  translate,
  ...props
}) => {
  const { setFilters, ...restProps } = props;
  const { withSearch } = config || '';

  const filters = getFilters(config);

  const setFiltersAndConfig = useCallback(
    (values) => {
      const filtersConfiguration = filters.reduce((res, { source, filter }) => {
        res[source] = filter;

        return res;
      }, {});

      if (setFilters) {
        setFilters({ ...values, __filterParams: filtersConfiguration });
      }
    },
    [filters]
  );

  if (!withSearch && isEmpty(filtersForm) && isEmpty(filters)) {
    return null;
  }
  const source =
    typeof withSearch === 'object'
      ? `wildcardOr ${get(withSearch, 'source')} `
      : 'q';

  return !!localFilter && !isEmpty(filtersForm) ? (
    <>
      {withSearch && (
        <TextInput label={translate('search')} source={source} alwaysOn />
      )}
      {renderForm(filtersForm, resource, translate)}
    </>
  ) : (
    <Filter {...restProps} setFilters={setFiltersAndConfig} resource={resource}>
      {withSearch && (
        <TextInput label={translate('search')} source={source} alwaysOn />
      )}
      {!isEmpty(filters) && renderForm(filters, resource, translate)}
    </Filter>
  );
};

ListFilters.propTypes = {
  config: PropTypes.object.isRequired,
  translate: PropTypes.func.isRequired,
  resource: PropTypes.string,
  localFilter: PropTypes.bool,
  filtersForm: PropTypes.object
};

export default compose(withTranslate)(ListFilters);
