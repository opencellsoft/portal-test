import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Drawer } from '@material-ui/core';
import { withStyles, createStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { refreshView as refreshViewAction, REDUX_FORM_NAME } from 'react-admin';
import { destroy } from 'redux-form';
import { Switch, Route } from 'react-router-dom';
import compose from 'recompose/compose';
import { isEmpty, get } from 'lodash';

import Title from '../../Layout/Title';
import SimpleList from './SimpleList';
import ListWithTabs from './ListWithTabs';
import CardList from './CardList';

import ListFilters from './ListFilters';
import ListActions from './ListActions';
import Form from '../Form';

import { CARD_LIST } from '../../../constants/generic';

import withTranslate from '../../Hoc/withTranslate';

const styles = createStyles({
  root: {
    margin: '0 8px'
  },
  card: {},
  drawerWrapper: {
    margin: 0
  },
  drawerRow: {
    gridTemplateColumns: 'repeat(auto-fit, minmax(220px, auto))'
  }
});

const getWrapper = (config) => {
  const { type, tabs } = config;

  switch (true) {
    case type === CARD_LIST:
      return CardList;
    case !!tabs:
      return ListWithTabs;
    default:
      return SimpleList;
  }
};

class GenericList extends PureComponent {
  static propTypes = {
    resource: PropTypes.string,
    config: PropTypes.object.isRequired,
    drawer: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired,
    title: PropTypes.object.isRequired,
    defaultTitle: PropTypes.object.isRequired,
    refreshView: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
    isAllowedCreate: PropTypes.bool,
    isAllowedEdit: PropTypes.bool
  };

  resetForm = () => {
    const { dispatch, form } = this.props;

    if (dispatch) {
      dispatch(destroy(form || REDUX_FORM_NAME));
    }
  };

  handleClose = () => {
    const { history, refreshView } = this.props;

    this.resetForm();

    history.push(`/${this.props.resource}`);

    return refreshView();
  };

  render = () => {
    const {
      config,
      resource,
      drawer,
      classes,
      className,
      listData,
      translate,
      isAllowedCreate,
      isAllowedEdit,
      ...rest
    } = this.props;
    const Wrapper = getWrapper(config);
    const { create = {}, edit = {} } = drawer || {};

    const defaultTitle = '';

    return (
      <>
        <Wrapper
          {...this.props}
          config={config}
          filters={<ListFilters config={config} />}
          exporter={!config.withExport ? false : undefined}
          hasCreate={isAllowedCreate && config.withCreate}
          className={classes.wrapper}
          isAllowedEdit={isAllowedEdit}
          isAllowedCreate={isAllowedCreate}
          actions={
            <ListActions
              withUpload={config.withUpload}
              config={config}
              resource={resource}
              drawer={drawer}
              withCreate={isAllowedCreate && config.withCreate}
              {...rest}
            />
          }
        />

        <Title defaultTitle={defaultTitle} />

        {!isEmpty(drawer) && (
          <Switch>
            <Route exact path={`/${resource}/create`}>
              {({ match }) => {
                const isMatch = !isEmpty(create);
                const { redirectAfterSubmit = 'list' } = create;

                return (
                  <Drawer
                    open={isMatch}
                    anchor="right"
                    onClose={this.handleClose}
                  >
                    {isMatch ? (
                      <Form
                        onCancel={this.handleClose}
                        {...this.props}
                        hasShow={false}
                        redirectAfterSubmit={redirectAfterSubmit}
                        config={create.main}
                        rowClassname={classes.drawerRow}
                        className={classes.drawerWrapper}
                        inDrawer
                      />
                    ) : null}
                  </Drawer>
                );
              }}
            </Route>

            <Route path={`/${resource}/:id`}>
              {({ match }) => {
                const isMatch =
                  !isEmpty(edit) && get(match, 'params.id') !== 'create';
                const { redirectAfterSubmit = 'list' } = edit;
                const record = get(listData, match.params.id);

                return (
                  <Drawer
                    open={isMatch}
                    anchor="right"
                    onClose={this.handleClose}
                  >
                    {isMatch ? (
                      <Form
                        id={match.params.id}
                        onCancel={this.handleClose}
                        {...this.props}
                        record={record}
                        hasShow={false}
                        redirectAfterSubmit={redirectAfterSubmit}
                        config={edit.main}
                        isEdit
                        inDrawer
                        undoable={false}
                        rowClassname={classes.drawerRow}
                        className={classes.drawerWrapper}
                      />
                    ) : null}
                  </Drawer>
                );
              }}
            </Route>
          </Switch>
        )}
      </>
    );
  };
}

const mapStateToProps = ({ admin }, props) => ({
  listData: admin.resources[props.resource]
    ? admin.resources[props.resource].data
    : null
});

export default compose(
  connect(
    mapStateToProps,
    (dispatch) => ({ refreshView: refreshViewAction, dispatch })
  ),
  withTranslate,
  withStyles(styles)
)(GenericList);
