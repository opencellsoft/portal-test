import React, { memo, useCallback } from 'react';
import PropTypes from 'prop-types';
import { shouldUpdate } from 'recompose';
import { TableBody } from '@material-ui/core';
import cx from 'classnames';

import DatagridRow from './DatagridRow';

const DatagridBody = memo(
  ({
    basePath,
    children,
    classes,
    className,
    data,
    fields,
    expand,
    hasBulkActions,
    hover,
    ids,
    isLoading,
    onToggleItem,
    resource,
    row,
    rowClick,
    rowStyle,
    selectedIds,
    styles,
    version,
    withSelectOnlyOne,
    ...rest
  }) => {
    const toggleItemHandler = useCallback(
      (id) => {
        //unselect all selected rows
        if (withSelectOnlyOne) {
          selectedIds
            .filter((itemID) => itemID !== id)
            .forEach((itemID) => {
              onToggleItem(itemID);
            });
        }

        return onToggleItem(id);
      },
      [selectedIds, withSelectOnlyOne]
    );

    return (
      <TableBody className={cx('datagrid-body', className)} {...rest}>
        {ids.map((id, rowIndex) =>
          React.cloneElement(
            row,
            {
              basePath,
              classes,
              className: cx(classes.row, {
                [classes.rowEven]: rowIndex % 2 === 0,
                [classes.rowOdd]: rowIndex % 2 !== 0,
                [classes.clickableRow]: rowClick
              }),
              expand,
              hasBulkActions,
              hover,
              id,
              key: id,
              onToggleItem: toggleItemHandler,
              record: data[id],
              resource,
              rowClick,
              selected: selectedIds.includes(id),
              style: rowStyle ? rowStyle(data[id], rowIndex) : null,
              fields
            },
            children
          )
        )}
      </TableBody>
    );
  }
);

DatagridBody.propTypes = {
  basePath: PropTypes.string,
  classes: PropTypes.object,
  className: PropTypes.string,
  children: PropTypes.node,
  data: PropTypes.object,
  expand: PropTypes.node,
  hasBulkActions: PropTypes.bool,
  hover: PropTypes.bool,
  ids: PropTypes.arrayOf(PropTypes.any),
  isLoading: PropTypes.bool,
  onToggleItem: PropTypes.func,
  resource: PropTypes.string,
  row: PropTypes.element,
  rowClick: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  rowStyle: PropTypes.func,
  selectedIds: PropTypes.arrayOf(PropTypes.any).isRequired,
  styles: PropTypes.object,
  version: PropTypes.number
};

DatagridBody.defaultProps = {
  data: {},
  hasBulkActions: false,
  ids: [],
  row: <DatagridRow />
};

const areArraysEqual = (arr1, arr2) =>
  arr1.length === arr2.length && arr1.every((v, i) => v === arr2[i]);

const PureDatagridBody = shouldUpdate(
  (props, nextProps) =>
    props.version !== nextProps.version ||
    nextProps.isLoading === false ||
    !areArraysEqual(props.ids, nextProps.ids) ||
    props.data !== nextProps.data
)(DatagridBody);

// trick material-ui Table into thinking this is one of the child type it supports
// @ts-ignore
PureDatagridBody.muiName = 'TableBody';

export default PureDatagridBody;
