import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { ListController } from 'react-admin';

import ListWrapper from './ListWrapper';

const ListWithTabs = memo(({ defaultTitle, ...props }) => (
  <ListController {...props}>
    {(controllerProps) => (
      <ListWrapper
        {...props}
        {...controllerProps}
        defaultTitle={defaultTitle}
      />
    )}
  </ListController>
));

ListWithTabs.propTypes = {
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired
    })
  ).isRequired,
  initialTab: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  tabField: PropTypes.string.isRequired
};

export default ListWithTabs;
