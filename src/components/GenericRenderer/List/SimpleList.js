import React from 'react';
import { Pagination } from 'react-admin';
import { withStyles, createStyles } from '@material-ui/core/styles';

import List from './List';
import Datagrid from './Datagrid';
import BulkDeleteButton from '../Buttons/BulkDeleteButton';

const styles = ({ spacing }) =>
  createStyles({
    card: {
      boxShadow:
        '0 7px 14px rgba(50, 50, 93, 0.1), 0 3px 6px rgba(0, 0, 0, 0.08)',
      margin: `0 ${spacing.unit}px`
    }
  });

const PostBulkActionButtons = (props) => (
  <>
    <BulkDeleteButton {...props} undoable={false} />
  </>
);

const SimpleList = ({
  config = {},
  hasList,
  hasShow,
  hasCreate,
  hasEdit,
  isAllowedEdit,
  ...props
}) => (
  <List
    hasList={hasList}
    hasShow={hasShow}
    hasCreate={hasCreate}
    hasEdit={hasEdit}
    bulkActionButtons={
      typeof bulkActionButtons !== 'undefined' ? (
        config.bulkActionButtons
      ) : (
        <PostBulkActionButtons {...props} />
      )
    }
    perPage={config.rowsPerPage}
    rowsPerPageOptions={config.rowsPerPageOptions}
    pagination={
      <Pagination
        perPage={config.rowsPerPage}
        rowsPerPageOptions={config.rowsPerPageOptions}
      />
    }
    config={config}
    {...props}
  >
    <Datagrid
      {...props}
      config={config}
      onRowClick={isAllowedEdit ? config.onRowClick : null}
      fields={config.fields}
      expand={config.expand}
    />
  </List>
);

SimpleList.defaultProps = {
  rowsPerPage: 10,
  rowsPerPageOptions: [10, 25, 50]
};

export default withStyles(styles)(SimpleList);
