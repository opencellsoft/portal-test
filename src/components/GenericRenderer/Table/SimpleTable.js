import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import {
  LinearProgress,
  crudGetList as crudGetListAction,
  setListSelectedIds as setListSelectedIdsAction,
  toggleListItem as toggleListItemAction
} from 'react-admin';
import { connect } from 'react-redux';
import { Divider, Typography } from '@material-ui/core';
import { withTheme, withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';
import { get, isEmpty, isEqual, orderBy } from 'lodash';
import moment from 'moment';
import { ACTION_UPDATE_SERVICE } from '../../../constants/generic';
import { hydrateUrlFromParams } from '../../../utils/generic-render';

import Pagination from '../../Globals/Tables/Pagination';
import Datagrid from '../List/Datagrid';
import WidgetTitle from '../Widgets/WidgetTitle';
import CustomEditRow from './CustomEditRow';
import Expansion from './Expansion';
import Toolbar from './Toolbar';

import WithEntityCustomization from '../../Hoc/WithEntityCustomization';
import withTranslate from '../../Hoc/withTranslate';

const styles = ({ palette, spacing, shadows }) => ({
  wrapper: {
    position: 'relative',
    flex: 1
  },
  datagridWrapper: {
    overflowX: 'auto'
  },
  datagridExpandWrapper: {
    overflowX: 'initial'
  },
  nestedDatagridExpandWrapper: {
    maxWidth: `calc(100vw - 75px - ${spacing.unit * 6}px - ${spacing.unit *
      4}px)`,
    overflow: 'auto'
  },
  nestedDatagridExpandWrapperOpen: {
    maxWidth: `calc(100vw - ${spacing.unit * 30}px - ${spacing.unit *
      8}px - ${spacing.unit * 4}px)`,
    overflow: 'auto'
  },
  inExpand: {
    overflowX: 'auto',
    boxShadow: shadows[1],
    borderRadius: 5,
    background: 'white'
  },
  expandTitle: {
    margin: `${spacing.unit * 2}px 0 ${spacing.unit}px 0`
  },
  loader: {
    width: '95%',
    margin: `${spacing.unit * 3}px auto ${spacing.unit * 2}px`
  },
  noDataWrapper: {
    padding: 16
  },
  noData: {
    textAlign: 'center',
    flex: 1
  },
  noDataExpand: {
    borderRadius: 5,
    background: 'white'
  }
});

class SimpleTable extends PureComponent {
  static propTypes = {
    data: PropTypes.array,
    fields: PropTypes.array,
    title: PropTypes.string,
    record: PropTypes.object,
    source: PropTypes.string,
    cfCode: PropTypes.string,
    localSort: PropTypes.bool,
    basePath: PropTypes.string,
    activeRow: PropTypes.number,
    theme: PropTypes.object.isRequired,
    withEdit: PropTypes.bool,
    inExpand: PropTypes.bool,
    classes: PropTypes.object.isRequired,
    selectedIds: PropTypes.array,
    setSelectedIds: PropTypes.func,
    translate: PropTypes.func.isRequired,
    localFilter: PropTypes.bool,
    bulkActionButtons: PropTypes.oneOfType([PropTypes.node, PropTypes.array])
  };

  initialLoad = false;

  state = {
    sortParsed: false,
    page: 0,
    rowsPerPage: 5,
    localData: this.props.data,
    currentSort: { field: '', order: 'ASC' },
    open: false,
    rowRecord: {}
  };

  handleOpenDialog = (record) => {
    this.setState({ open: true, rowRecord: record });
  };

  handleCloseDialog = () => {
    this.setState({ open: false, rowRecord: [] });
  };

  filterByConfig = (data, filter, exclude = false) => {
    const filterProperties = Object.keys(filter);

    return data.filter((el) =>
      filterProperties.reduce(
        (res, filterKey) =>
          res ||
          (exclude
            ? el[filterKey] !== filter[filterKey]
            : el[filterKey] === filter[filterKey]),
        false
      )
    );
  };

  filterData = (data) => {
    const { rowsPerPage, page } = this.state;
    const {
      localFilterForm,
      nestedDataFilter,
      excludedDataFilter
    } = this.props;
    const { values } = localFilterForm || {};

    if (nestedDataFilter) {
      data = this.filterByConfig(data, nestedDataFilter);
    }
    if (excludedDataFilter) {
      data = this.filterByConfig(data, excludedDataFilter, true);
    }

    if (!isEmpty(values)) {
      const filterProperties = Object.keys(values);
      const { sortParsed, localData } = this.state;
      const dataToFilter = sortParsed ? localData : data;
      data =
        !isEmpty(filterProperties) &&
        dataToFilter.filter((el) => {
          const hasData = filterProperties.reduce((res, filterKey) => {
            const value = this.transformData(el, filterKey);
            switch (true) {
              case filterKey.includes('Date'):
                return (
                  res &&
                  value === moment(values[filterKey]).format('YYYY-MM-DD')
                );
              case typeof value === 'number':
                return res && value.toString() === values[filterKey];

              default:
                return (
                  (res && values[filterKey] === null) ||
                  (res &&
                    value.includes(
                      values[filterKey] && values[filterKey].toLowerCase()
                    ))
                );
            }
          }, true);

          return hasData && el;
        });
    }

    const sliceData = data
      .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
      .reduce(
        (res, data, index) => ({
          ...res,
          [page * rowsPerPage + index]: data
        }),
        {}
      );

    return {
      total: data.length,
      data: isEmpty(sliceData) && data.length > 1 ? data : sliceData
    };
  };

  aggregateData = (record, source = '') => {
    const { inExpand } = this.props;
    const sources = source.split('+').filter((el) => el);

    if (inExpand && isEmpty(sources)) {
      return [record];
    }

    return sources
      .reduce((res, source) => {
        const value = get(record, source);

        if (!value) {
          return res;
        }

        return Array.isArray(value) ? [...res, ...value] : [...res, value];
      }, [])
      .filter((el) => el);
  };

  handleData = () => {
    const {
      record,
      resource,
      source,
      data: injectedData = [],
      resourceData = []
    } = this.props;

    const rawData = !isEmpty(resource)
      ? Object.values(get(resourceData, 'data') || {})
      : !isEmpty(injectedData)
      ? injectedData
      : this.aggregateData(record, source) || [];

    const data = rawData ? (!Array.isArray(rawData) ? [rawData] : rawData) : [];

    return this.filterData(data);
  };

  handleChangePage = (e, page) => this.setState({ page });

  handleChangeRowsPerPage = ({ target: { value: rowsPerPage } }) =>
    this.setState({ rowsPerPage, page: 0 });

  handleSelect = (ids) => {
    const {
      setSelectedIds,
      resource,
      relatedResource,
      isCFInputTable,
      setSelectedIdsCustom
    } = this.props;
    const identifier = resource || relatedResource;

    if (!identifier && !isCFInputTable) {
      return false;
    }

    return isCFInputTable
      ? setSelectedIdsCustom(this.state.localData, ids)
      : setSelectedIds(identifier, ids);
  };

  handleUnselectItems = () => {
    const { setSelectedIds, resource, relatedResource } = this.props;
    const identifier = resource || relatedResource;

    if (!identifier) {
      return false;
    }

    return setSelectedIds(identifier, []);
  };

  handleToggleItem = (id) => {
    const { localData } = this.state;
    const {
      toggleItem,
      resource,
      relatedResource,
      isCFInputTable,
      handleToggleItemCustom
    } = this.props;
    const identifier = resource || relatedResource;

    if (!identifier && !isCFInputTable) {
      return false;
    }

    return isCFInputTable
      ? handleToggleItemCustom(localData, id)
      : toggleItem(identifier, id);
  };

  transformData = (data, field) => {
    const { fields } = this.props;
    const selectedField = fields.find(({ source }) => source === field);

    switch (true) {
      case get(selectedField, 'type') === 'number':
        return parseFloat(get(data, field)) || 0;
      case typeof get(data, field) === 'string':
        return get(data, field).toLowerCase();
      default:
        return get(data, field) || '';
    }
  };

  getRowClickHandler = () => {
    const {
      isCFInputTable,
      customRowClick,
      onRowClick,
      record,
      reference
    } = this.props;
    const { localData } = this.state;

    if (isCFInputTable) {
      return (id) => onRowClick(localData, id);
    }

    if (!isEmpty(customRowClick)) {
      switch (customRowClick.action) {
        case ACTION_UPDATE_SERVICE: {
          return (id) => this.handleOpenDialog(localData[id]);
        }
        default:
          return onRowClick;
      }
    }

    if (!onRowClick) {
      return null;
    }

    if (typeof onRowClick === 'string' && onRowClick.includes('@record')) {
      return hydrateUrlFromParams({
        url: onRowClick,
        params: { record }
      });
    }

    if (!!reference && onRowClick) {
      return (id, basePath, record) => {
        switch (onRowClick) {
          case 'edit':
            return `/${reference}/${record.code}`;
          default:
            return `/${reference}/${record.code}/show`;
        }
      };
    }

    return onRowClick;
  };

  updateSort = (field) => {
    const { data: propsData, localFilterForm } = this.props;
    const { values } = localFilterForm || {};
    const { data } = this.filterData(propsData);
    const currentData = !isEmpty(values) ? data : propsData;
    const { currentSort } = this.state;
    const order = currentSort.order === 'ASC' ? 'DESC' : 'ASC';
    const localData = orderBy(
      currentData,
      [(currentData) => this.transformData(currentData, field)],
      order.toLowerCase()
    );

    return this.setState({
      sortParsed: true,
      localData,
      currentSort: { field, order }
    });
  };

  setSort = (value) => this.setState({ sortParsed: value });

  componentDidUpdate = ({
    activeRow: prevActiveRow,
    data: prevData,
    isLoading: prevIsLoading
  }) => {
    const {
      activeRow,
      data,
      localSort,
      localFilterForm,
      isLoading
    } = this.props;
    const { rowsPerPage, sortParsed, localData } = this.state;
    const { values, registeredFields } = localFilterForm || {};

    if (isEmpty(values) && !isEmpty(registeredFields) && sortParsed) {
      this.setState({
        localData
      });
    }

    //@todo
    //comparing data is not enough to reset page
    if (data && !isEqual(data, prevData)) {
      this.setState({
        page: 0
      });

      if (localSort)
        this.setState({
          localData: data,
          currentSort: { field: '', order: 'ASC' }
        });
    }

    if (activeRow && prevActiveRow < 0 && activeRow > rowsPerPage) {
      this.setState({ page: Math.floor(activeRow / rowsPerPage) });
    }

    if (!isLoading && prevIsLoading) {
      this.initialLoad = true;
    }
  };

  renderExpansion = () => {
    const { classes, fields } = this.props;
    const { data } = this.handleData();
    const hasData = !isEmpty(data);

    return (
      <>
        {hasData &&
          Object.values(data).map((key) => (
            <Expansion fields={fields} data={key} classes={classes} />
          ))}
      </>
    );
  };

  getDependsFields = () => {
    const { dependsFields, fields, dependsRecord } = this.props;
    const customFields = get(dependsRecord, dependsFields)[0] || {};
    const data = get(customFields, 'value') || [];
    const filterFields = [];
    fields.map(
      (field) =>
        Array.isArray(data) &&
        data.filter(
          (cf) => cf.value === field.source && filterFields.push(field)
        )
    );

    return filterFields;
  };

  renderTable = () => {
    const {
      bulkActions,
      withPagination,
      resourceData,
      basePath,
      dependsFields,
      dependsRecord,
      reference,
      toolbarBottom,
      activeRow,
      roles,
      title,
      fields,
      theme,
      classes,
      isLoading,
      rowStyleHandler,
      inExpand,
      noDataLabel,
      isNested,
      localSort,
      sidebarOpen,
      isCFInputTable,
      getSelectedIdsCustom,
      handleDeleteCustom,
      selectedIdsCustom,
      nestedDataField,
      customRowClick = {},
      record = {},
      translate,
      ...rest
    } = this.props;

    const {
      page,
      rowsPerPage,
      localData,
      sortParsed,
      currentSort
    } = this.state;

    const { data, total } = this.handleData();
    const hasData = !isEmpty(data);
    const filteredFields =
      (dependsRecord && dependsFields && this.getDependsFields()) || fields;

    const selectedIds =
      (isCFInputTable
        ? selectedIdsCustom
        : get(resourceData, 'list.selectedIds')) || [];

    const datagridClasses = {
      ...classes,
      datagridWrapper: inExpand
        ? cx(
            classes.datagridExpandWrapper,
            isNested &&
              (sidebarOpen
                ? classes.nestedDatagridExpandWrapperOpen
                : classes.nestedDatagridExpandWrapper)
          )
        : classes.datagridWrapper
    };
    const finalBasePath = reference ? `/${reference}` : basePath;

    return (
      <>
        {inExpand && title && (
          <WidgetTitle title={title} className={classes.expandTitle} />
        )}
        {!toolbarBottom && (
          <Toolbar
            {...rest}
            record={record}
            fields={filteredFields}
            setSort={this.setSort}
            roles={roles}
            basePath={finalBasePath}
            bulkActions={bulkActions}
            selectedIds={isCFInputTable ? selectedIdsCustom : selectedIds}
            data={sortParsed ? localData : data}
            isCFInputTable={isCFInputTable}
            handleDeleteCustom={handleDeleteCustom}
          />
        )}
        {hasData ? (
          <>
            <div className={!inExpand && classes.datagridWrapper}>
              <Datagrid
                {...this.props}
                fields={filteredFields}
                basePath={finalBasePath}
                data={sortParsed ? localData : data}
                classes={datagridClasses}
                currentSort={localSort ? currentSort : {}}
                setSort={(event) => localSort && this.updateSort(event)}
                rowStyle={(record, index) =>
                  rowStyleHandler
                    ? rowStyleHandler(record, index, page, rowsPerPage)
                    : {
                        backgroundColor:
                          page * rowsPerPage + index === activeRow
                            ? theme.palette.action.selected
                            : 'initial'
                      }
                }
                ids={Object.keys(data)}
                selectedIds={selectedIds}
                hasBulkActions={!!bulkActions}
                onSelect={this.handleSelect}
                onToggleItem={this.handleToggleItem}
                onUnselectItems={this.handleUnselectItems}
                className={inExpand && classes.inExpand}
                total={total}
                onRowClick={this.getRowClickHandler()}
              />
            </div>

            {withPagination &&
              (!inExpand || (inExpand && total > rowsPerPage)) && (
                <Pagination
                  count={total}
                  page={page}
                  rowsPerPage={rowsPerPage}
                  onChangePage={this.handleChangePage}
                  onChangeRowsPerPage={this.handleChangeRowsPerPage}
                />
              )}
          </>
        ) : !noDataLabel ? (
          <>
            {!inExpand && <Divider />}

            {!this.initialLoad && isLoading ? (
              <LinearProgress className={classes.loader} />
            ) : (
              <div
                className={cx(
                  classes.noDataWrapper,
                  inExpand && classes.noDataExpand
                )}
              >
                <Typography variant="subheading" className={classes.noData}>
                  {translate('message.no_result_found')}
                </Typography>
              </div>
            )}
          </>
        ) : null}
        {toolbarBottom && (
          <>
            <Divider />
            <Toolbar
              {...rest}
              data={data}
              record={record}
              bulkActions={bulkActions}
              selectedIds={selectedIds}
            />
          </>
        )}
        {!isEmpty(customRowClick) && (
          <WithEntityCustomization resource={get(customRowClick, 'resource')}>
            <CustomEditRow
              customRowClick={customRowClick}
              nestedDataField={nestedDataField}
              handleCloseDialog={this.handleCloseDialog}
              open={this.state.open}
              rowRecord={this.state.rowRecord}
              parentCode={record.code}
            />
          </WithEntityCustomization>
        )}
      </>
    );
  };

  render = () => {
    const { verticalTable, classes } = this.props;

    return (
      <div className={classes.wrapper}>
        {verticalTable ? this.renderExpansion() : this.renderTable()}
      </div>
    );
  };
}

export default compose(
  connect(
    (
      {
        admin: {
          resources,
          loading,
          ui: { sidebarOpen }
        },
        form: { simpleTableForm }
      },
      { resource, relatedResource, setSelectedIds }
    ) => ({
      resourceData: resources[resource || relatedResource],
      isLoading: loading > 0,
      sidebarOpen,
      localFilterForm: simpleTableForm
    }),

    {
      crudGetList: crudGetListAction,
      setSelectedIds: setListSelectedIdsAction,
      toggleItem: toggleListItemAction
    }
  ),
  withTranslate,
  withTheme(),
  withStyles(styles)
)(SimpleTable);
