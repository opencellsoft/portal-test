import React from 'react';
import PropTypes from 'prop-types';
import {
  SaveButton,
  Toolbar,
  withDataProvider,
  refreshView as refreshViewAction,
  showNotification as showNotificationAction
} from 'react-admin';
import { connect } from 'react-redux';
import {
  reduxForm,
  getFormValues,
  isValid,
  touch as touchAction
} from 'redux-form';
import { compose } from 'recompose';
import { capitalize, pick, isEmpty, get } from 'lodash';

import { Button, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import Row from '../../Globals/Row';
import Dialog from '../../Globals/Dialogs/Dialog';
import InputRenderer from '../Form/InputRenderer';

import { MODAL_REDUX_FORM_NAME } from '../../../constants/generic';
import { transformCustomFields } from '../../../providers/genericProvider/transformer';

import { mergeCfsWithDefinitions } from '../../../utils/custom-fields';
import { collectErrors } from '../../../utils/form';
import CustomFieldsSection from '../Form/CustomFieldsSection';
import { flattenKeys } from '../../../utils/general';

import withTranslate from '../../Hoc/withTranslate';

const styles = ({ spacing }) => ({
  dialogToolbar: {
    display: 'flex',
    justifyContent: 'space-between',
    margin: 0,
    background: 'none'
  },
  dialogButton: {
    margin: spacing.unit
  },
  row: {
    gridTemplateColumns: `repeat(${3}, minmax(220px, auto))`
  },
  subgroup: {
    gridColumn: `span 3`
  },
  subgroupRow: {
    gridTemplateColumns: `repeat(${3}, minmax(220px, 450px))`
  },
  formGroupTitle: {
    padding: spacing.unit * 2,
    textTransform: ' uppercase',
    letterSpacing: '.0875rem',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    fontSize: '0.875rem',
    fontWeight: '500',
    lineHheight: '1.375em'
  },
  cfWrapper: {
    marginTop: '20px'
  }
});

const CustomEditRow = ({
  classes,
  parentCode,
  customRowClick = {},
  nestedDataField,
  translate,
  handleCloseDialog,
  open,
  rowRecord,
  dataProvider,
  formValues,
  refreshView,
  showNotification,
  form,
  fieldsDefinition,
  isRejectFormValid,
  formErrors,
  touch
}) => {
  const {
    verb,
    rows,
    resource,
    includeProperties = [],
    customFields: confirmCustomFields = {}
  } = customRowClick;

  const { referenceField } = confirmCustomFields;

  const handleEdit = () => {
    const restProps = pick(formValues, includeProperties);

    const updatedRecord = {
      subscriptionCode: parentCode,
      serviceToUpdate: [
        {
          ...restProps,
          ...pick(
            { ...transformCustomFields(formValues, fieldsDefinition) },
            includeProperties
          )
        }
      ]
    };

    if (isRejectFormValid) {
      dataProvider(verb, resource, { data: updatedRecord })
        .then(() => {
          refreshView();
          showNotification(translate('ra.notification.updated'));
        })
        .then(handleCloseDialog)
        .catch((error) => showNotification(capitalize(error.message)))
        .finally(() => {
          handleCloseDialog();
          refreshView();
        });
    } else {
      touch(...Object.keys(flattenKeys(formErrors)));
      showNotification('ra.message.invalid_form');
    }
  };

  const CustomToolbar = (props) => (
    <Toolbar {...props} className={classes.dialogToolbar}>
      <Button
        onClick={handleCloseDialog}
        color="primary"
        className={classes.dialogButton}
      >
        {translate('ra.action.cancel')}
      </Button>
      <SaveButton className={classes.dialogButton} onClick={handleEdit} />
    </Toolbar>
  );

  const customFields = get(rowRecord, 'customFields.customField', []);

  const fieldsWithMeta = !isEmpty(customFields)
    ? mergeCfsWithDefinitions(customFields, fieldsDefinition)
    : [];

  return (
    <Dialog
      open={open}
      onClose={handleCloseDialog}
      title={translate(`ra.action.edit`)}
      actions={[<CustomToolbar />]}
      maxWidth="lg"
    >
      <form>
        {rows.map((row, index) => (
          <Row className={classes.row} key={index} grid>
            {Object.values(row).map((field, key) =>
              field.hasOwnProperty('title') ? (
                <div className={classes.subgroup}>
                  <h2>{}</h2>
                  <Typography
                    variant="subheading"
                    noWrap
                    component="h2"
                    classes={{
                      root: classes.formGroupTitle
                    }}
                  >
                    {field.title}
                  </Typography>
                  {Object.values(field.rows).map((row, key) => (
                    <Row className={classes.subgroupRow} key={key} grid>
                      {Object.values(row).map((field, key) => (
                        <InputRenderer
                          key={key}
                          className={classes.dialogButton}
                          {...field}
                        />
                      ))}
                    </Row>
                  ))}
                </div>
              ) : (
                <InputRenderer
                  key={key}
                  className={classes.dialogButton}
                  {...field}
                />
              )
            )}
          </Row>
        ))}
        {!isEmpty(customFields) && (
          <CustomFieldsSection
            fields={fieldsWithMeta}
            cfPath={referenceField}
            formId={form}
            classes={classes}
          />
        )}
      </form>
    </Dialog>
  );
};

CustomEditRow.propTypes = {
  classes: PropTypes.object.isRequired,
  customRowClick: PropTypes.object.isRequired,
  nestedDataField: PropTypes.string,
  translate: PropTypes.func,
  handleCloseDialog: PropTypes.func,
  open: PropTypes.bool,
  rowRecord: PropTypes.object,
  dataProvider: PropTypes.func,
  formValues: PropTypes.object,
  refreshView: PropTypes.func,
  showNotification: PropTypes.func,
  parentCode: PropTypes.string.isRequired,
  form: PropTypes.string.isRequired,
  fieldsDefinition: PropTypes.object.isRequired,
  isRejectFormValid: PropTypes.bool,
  formErrors: PropTypes.object,
  touch: PropTypes.func
};

const mapStateToProps = (state, ownProps) => ({
  initialValues: ownProps.rowRecord,
  formValues: getFormValues(MODAL_REDUX_FORM_NAME)(state),
  isRejectFormValid: isValid(MODAL_REDUX_FORM_NAME)(state),
  formErrors: collectErrors(state, MODAL_REDUX_FORM_NAME)
});

export default compose(
  connect(
    mapStateToProps,
    {
      refreshView: refreshViewAction,
      showNotification: showNotificationAction,
      touch: touchAction
    }
  ),
  withTranslate,
  reduxForm({
    form: MODAL_REDUX_FORM_NAME,
    destroyOnUnmount: true,
    enableReinitialize: true,
    keepDirtyOnReinitialize: true
  }),
  withStyles(styles),
  withDataProvider
)(CustomEditRow);
