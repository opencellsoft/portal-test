import React, { PureComponent, cloneElement } from 'react';
import PropTypes from 'prop-types';
import {
  withDataProvider,
  showNotification as showNotificationAction,
  refreshView as refreshViewAction,
  setListSelectedIds as setListSelectedIdsAction,
  crudGetList as crudGetListAction,
  getDefaultValues,
  downloadCSV
} from 'react-admin';
import cx from 'classnames';
import inflection from 'inflection';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import {
  reduxForm,
  getFormValues,
  isValid,
  reset,
  touch as touchAction,
  change,
  destroy
} from 'redux-form';
import { compose } from 'recompose';
import { get, isEmpty, pick, omit, isEqual } from 'lodash';

import { Drawer, Button as MuiButton, IconButton } from '@material-ui/core';
import { HighlightOff as ActionHide, Add as AddIcon } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';

import moment from 'moment-timezone';
import { unparse as convertToCSV } from 'papaparse/papaparse.min';
import {
  transformCustomFields,
  configureCustomActionAPICall,
  updatePrevVersionData
} from '../../../providers/genericProvider/transformer';

import {
  processParams,
  hydrateStringFromParams,
  transformBySource
} from '../../../utils/generic-render';
import { collectErrors } from '../../../utils/form';
import WithDependentValueCfs from '../../Hoc/WithDependentValueCfs';
import withTranslate from '../../Hoc/withTranslate';

import {
  ACTION_API,
  ACTION_EXPORT,
  ACTION_MODAL,
  ACTION_INSTANTIATE_SERVICE,
  ACTION_ACTIVATE_SERVICE,
  ACTION_SUSPEND_SERVICE,
  ACTION_TERMINATE_SERVICE,
  ACTION_RESUME_SERVICE,
  FILE_TYPES,
  MODAL_REDUX_FORM_NAME,
  DYNAMIC_INPUT
} from '../../../constants/generic';

import DynamicIcon from '../../Globals/DynamicIcon';
import Dialog from '../../Globals/Dialogs/Dialog';
import ConfirmDialog from '../../Globals/Dialogs/ConfirmDialog';
import Row from '../../Globals/Row';
import Form from '../Form';
import InputRenderer from '../Form/InputRenderer';
import CustomRouterButton from '../Buttons/CustomRouterButton';
import CustomFieldsSection from '../Form/CustomFieldsSection';
import { flattenKeys } from '../../../utils/general';
import ActionsButton from '../../Globals/Widgets/ActionsButton';

const styles = ({ palette }) => ({
  label: {
    paddingLeft: '0.5em'
  },
  icon: {
    fontSize: 20
  },
  cfWrapper: {
    marginTop: '20px'
  },
  row: {
    gridTemplateColumns: `repeat(3, minmax(220px, auto))`
  },
  dynamicInputRow: {
    marginLeft: '16px'
  },
  body: { display: 'flex', alignItems: 'flex-end' },
  root: {
    color: palette.primary.main
  },
  rootPrimary: {
    color: 'white'
  }
});

class CustomActionItem extends PureComponent {
  static propTypes = {
    actions: PropTypes.array,
    classes: PropTypes.object.isRequired,
    classesOverride: PropTypes.object,
    history: PropTypes.func.isRequired,
    dataProvider: PropTypes.func.isRequired
  };

  state = {
    dialog: false,
    drawer: false,
    shouldConfirm: null,
    itemId: 0,
    dynamicFields: [],
    updatedData: false
  };

  componentWillUnmount = () => this.resetForm();

  componentDidUpdate = (prevProps, prevState) => {
    const { dynamicFields } = this.state;
    const { formValues, selectedItems } = this.props;
    const { item } = this.props;
    const { confirm } = item || {};
    const fields = get(confirm, 'fields', {});

    Object.values(fields).forEach(({ type, fields: field }) => {
      if (type === DYNAMIC_INPUT) {
        let initialValue;

        field.forEach(({ source, defaultValue, onAdd }) => {
          if (onAdd && defaultValue) {
            if (!isEmpty(selectedItems)) {
              selectedItems.forEach((selectedItem) => {
                initialValue = processParams({
                  params: { defaultValue },
                  source: { selectedItem }
                });

                return initialValue;
              });
            }
            const data = get(formValues, source) || [];
            const size = dynamicFields.length - 1;
            const formSize = data.length - 1;
            const values =
              !isEmpty(data) &&
              data.reduce((res, val, index) => {
                if (!val) return res;
                if (index === formSize) return res;

                return res + val;
              }, 0);
            const someValues =
              !isEmpty(data) &&
              data.reduce((res, val, index) => {
                if (!val) return res;

                return res + val;
              }, 0);
            const lastDynamicField = dynamicFields[size] || {};
            const sourceField = Object.values(lastDynamicField).reduce(
              (res, { source: sourceField }) => {
                const [sourcef] = sourceField.split('[');
                if (sourcef === source) return sourceField;

                return res;
              },
              ''
            );
            if (
              (onAdd.operator === '-' && values > initialValue.defaultValue) ||
              someValues > initialValue.defaultValue
            ) {
              this.props.dispatch(
                change(MODAL_REDUX_FORM_NAME, sourceField, 0)
              );
            }
            if (formSize > size) {
              if (
                onAdd.operator === '-' &&
                someValues < initialValue.defaultValue
              ) {
                const val = initialValue.defaultValue - values;
                this.props.dispatch(
                  change(MODAL_REDUX_FORM_NAME, sourceField, val)
                );
              }
            } else if (
              (values <= initialValue.defaultValue && formSize === size) ||
              (values === 0 && data[size] < initialValue.defaultValue)
            ) {
              if (onAdd.operator === '-') {
                const val = initialValue.defaultValue - values;
                this.props.dispatch(
                  change(MODAL_REDUX_FORM_NAME, sourceField, val)
                );
              }
            }
          }
        });
      }
    });
  };

  handleHide = (event, field) => {
    event.preventDefault();
    const { dynamicFields } = this.state;
    const { formValues, selectedItems } = this.props;
    const fields = dynamicFields.filter(
      (dynamicField) => !isEqual(field, dynamicField)
    );
    let initialValue;

    Object.values(field).forEach(
      ({ onAdd, defaultValue, source: sourceField }) => {
        if (onAdd) {
          const [source] = sourceField.split('[');
          const size = fields.length - 1;
          const popSource = Object.values(fields[size]).reduce(
            (res, { source: stateSource }) => {
              if (stateSource.split('[')[0] === source) return stateSource;

              return res;
            },
            ''
          );
          selectedItems.forEach((selectedItem) => {
            initialValue = processParams({
              params: { defaultValue },
              source: { selectedItem }
            });

            return initialValue;
          });
          const form = parseFloat(get(formValues, popSource));
          const dataToRest = parseFloat(get(formValues, sourceField));
          const newVal = dataToRest + form;
          if (fields.length === 1)
            this.props.dispatch(
              change(
                MODAL_REDUX_FORM_NAME,
                popSource,
                initialValue.defaultValue
              )
            );
          if (newVal <= initialValue.defaultValue) {
            this.props.dispatch(
              change(MODAL_REDUX_FORM_NAME, popSource, newVal)
            );
          }
        }
        this.props.dispatch(change(MODAL_REDUX_FORM_NAME, sourceField, ''));
      }
    );
    this.setState({ dynamicFields: fields });
  };

  toggleDialog = (nextDialog) => {
    const { onClose } = this.props;
    const { dialog } = this.state;

    if (typeof nextDialog !== 'undefined' && dialog === !!nextDialog) {
      return false;
    }

    const nextState =
      typeof nextDialog !== 'undefined'
        ? { dialog: !!nextDialog }
        : { dialog: !dialog };

    return this.setState(nextState, () => {
      if (dialog && onClose) {
        onClose();
      }
    });
  };

  toggleDrawer = () =>
    this.setState({ drawer: !this.state.drawer }, this.resetForm);

  handleLocalExport = (e) => {
    const { record, item, listData, showNotification } = this.props;
    const { title, resource, fields } = item;

    e.preventDefault();

    if (isEmpty(listData)) {
      return showNotification('export.empty');
    }

    const iterableData = Array.isArray(listData)
      ? listData
      : Object.values(listData);

    const finalData = Array.isArray(fields)
      ? iterableData.map((dataEl) =>
          fields.reduce((res, field) => {
            const { source, label = source } = field;

            res[label] = get(dataEl, source);

            return res;
          }, {})
        )
      : iterableData;

    return downloadCSV(
      convertToCSV(finalData),
      hydrateStringFromParams({
        string: title || resource,
        params: { pageRecord: record }
      })
    );
  };

  resetForm = (event) => {
    this.props.dispatch(reset(MODAL_REDUX_FORM_NAME));

    if (event && isEmpty(this.state.dynamicFields)) {
      this.props.dispatch(destroy(MODAL_REDUX_FORM_NAME));
      const { item } = this.props;
      const { confirm } = item || {};
      const fields = get(confirm, 'fields', {});
      Object.values(fields).forEach((field) => {
        const { type, fields } = field || '';
        if (type === DYNAMIC_INPUT) {
          return this.dynamicInput(event, fields);
        }

        return false;
      });
    }
  };

  handleFile = (raw) => {
    const { formValues = {}, item, resource, showNotification } = this.props;
    const { isFile, fileFormat } = item || {};

    if (!isFile) {
      return raw;
    }
    const fileType =
      Object.keys(FILE_TYPES).find(
        (key) => key === formValues.fileType || key === fileFormat
      ) || 'pdf';

    if (fileType) {
      const link = document.createElement('a');
      link.href = `data:${get(
        FILE_TYPES,
        fileType
      )};base64,${encodeURIComponent(raw)}`;
      const currentDate = moment().format('L');
      link.download = `${resource}_${currentDate}.${fileType}`;

      return link.click();
    }

    return showNotification('file type is undefined');
  };

  // eslint-disable-next-line consistent-return
  handleAPICall = () => {
    const {
      item,
      resource: pageResource,
      record,
      parentRecord: pageRecord,
      onClose,
      formValues,
      dataProvider,
      showNotification,
      refreshView,
      translate,
      fieldsDefinition,
      setListSelectedIds,
      history,
      isRejectFormValid,
      touch,
      formErrors,
      selectedData,
      crudGetList,
      pagination,
      sort = { order: 'DESC' },
      selectedIds,
      cfCode,
      cfIndex
    } = this.props;

    const {
      resource,
      verb,
      dependsOn,
      params,
      transformSource,
      confirm,
      forwardRecord,
      redirectAfterSubmit,
      accumulate = false,
      excludeProperties,
      parentResource,
      deleteCf = false,
      updateVersion = {}
    } = item || {};
    const { shouldConfirm, dynamicFields } = this.state;

    if (!confirm && onClose) {
      onClose();
    }
    let { selectedItems } = this.props;
    if (Array.isArray(excludeProperties)) {
      selectedItems = selectedItems.map((selectedItem) =>
        omit(selectedItem, excludeProperties)
      );
    }

    const confirmValues = (get(confirm, 'fields') || []).reduce(
      (res, { source, type, fields }) => {
        if (type === DYNAMIC_INPUT) {
          const dynamicForm = [];
          dynamicFields.forEach((dynamicField) =>
            dynamicForm.push(
              Object.values(dynamicField).reduce((res, field) => {
                const [source] = field.source.split('[');
                const data = get(formValues, field.source);

                return { ...res, [source]: data };
              }, {})
            )
          );

          return { ...res, [source]: dynamicForm };
        }

        return { ...res, [source]: formValues[source] };
      },
      {}
    );

    const selectedParams =
      (dependsOn && {
        ...selectedData,
        [dependsOn.source]: !this.getSwitchValue()
      }) ||
      {};
    const paramsToProcess = {
      ...params,
      ...(shouldConfirm ? confirmValues : {}),
      ...(dependsOn ? selectedParams : {})
    };
    let queries;
    let requestData;

    try {
      requestData =
        !accumulate &&
        processParams({
          params: paramsToProcess,
          source: {
            record: !isEmpty(selectedData) ? selectedData : record,
            pageRecord,
            form: formValues,
            selectedItems,
            selectedData
          }
        });

      requestData = transformSource
        ? transformBySource(transformSource, requestData)
        : { ...requestData };

      const getAPICallPromise = configureCustomActionAPICall(dataProvider, {
        resource,
        deleteCf,
        record,
        fieldsDefinition,
        selectedIds,
        cfCode,
        cfIndex
      });

      queries = accumulate
        ? selectedItems.map((selectedItem) => {
            const requestData = processParams({
              params: paramsToProcess,
              source: { record, pageRecord, form: formValues, selectedItem }
            });

            return getAPICallPromise(requestData, verb);
          })
        : !isEmpty(updateVersion)
        ? getAPICallPromise(requestData, verb).then(() => {
            const { verb: updateVerb } = updateVersion;
            const prevVersionData = updatePrevVersionData(
              selectedItems[0],
              requestData,
              updateVersion
            );

            return getAPICallPromise(prevVersionData, updateVerb);
          })
        : getAPICallPromise(requestData, verb);
    } catch (error) {
      return showNotification(error.message, 'error');
    }

    const provider = accumulate ? Promise.all(queries) : queries;

    if (isRejectFormValid) {
      provider
        .then(({ data }) => this.handleFile(data))
        .then(() => showNotification(translate('input.action.success')))
        .then((data) => {
          if (
            isEmpty(redirectAfterSubmit) ||
            typeof redirectAfterSubmit !== 'string'
          ) {
            return false;
          }
          const params = {
            pathname: redirectAfterSubmit,
            data: forwardRecord ? requestData : undefined
          };

          return history.push(params);
        })
        .then(() => {
          if (!isEmpty(dynamicFields)) {
            this.handleClose();
          }
          if (shouldConfirm) {
            this.toggleDialog(false);
          }

          if (onClose) {
            onClose();
          }

          setListSelectedIds(pageResource, []);
        })
        .catch((error) => showNotification(error.message, 'error'))
        .finally(() => {
          if (parentResource) {
            const filters = processParams({
              params,
              source: {
                record: pageRecord
              }
            });
            crudGetList(resource, pagination, sort, filters);
            crudGetList(parentResource, pagination, sort, filters);
          }
          this.setState({ updatedData: true });
          refreshView();
          this.resetForm();
        });
    } else {
      touch(...Object.keys(flattenKeys(formErrors)));
      showNotification('ra.message.invalid_form');
    }
  };

  setShouldConfirm = (event, shouldConfirm) => {
    this.setState(
      {
        shouldConfirm,
        dialog: !!shouldConfirm,
        dynamicFields: !shouldConfirm ? [] : this.state.dynamicFields
      },
      () => {
        const { onClose } = this.props;

        return !shouldConfirm && onClose ? onClose() : null;
      }
    );
  };

  handleConfirm = (event, cb) => {
    const { item } = this.props;
    const { confirm } = item || {};

    event.preventDefault();
    event.stopPropagation();

    return confirm ? this.setShouldConfirm(event, cb) : cb();
  };

  getServiceRequestData = () => {
    const {
      item,
      record = {},
      formValues,
      selectedItems = [],
      fieldsDefinition
    } = this.props;

    const { includeProperties, action } = item || {};

    const updatedServices = selectedItems.map((item) =>
      pick(item, includeProperties)
    );

    switch (true) {
      case action === ACTION_INSTANTIATE_SERVICE:
        const service = get(formValues, 'service', {});
        const restProps = pick(formValues, includeProperties);

        return {
          subscription: record.code,
          servicesToInstantiate: {
            service: [
              {
                ...restProps,
                ...pick(
                  { ...transformCustomFields(service, fieldsDefinition) },
                  includeProperties
                )
              }
            ]
          }
        };
      case action === ACTION_ACTIVATE_SERVICE: {
        return {
          subscription: record.code,
          servicesToActivate: {
            service: [...updatedServices]
          }
        };
      }
      case action === ACTION_RESUME_SERVICE:
      case action === ACTION_SUSPEND_SERVICE: {
        return {
          subscriptionCode: record.code,
          serviceToUpdate: [...updatedServices]
        };
      }
      case action === ACTION_TERMINATE_SERVICE: {
        return {
          subscriptionCode: record.code,
          serviceIds: selectedItems.map((item) => item.id),
          ...pick(formValues, includeProperties)
        };
      }
      default:
        return {};
    }
  };

  handleServiceAction = () => {
    const {
      item,
      onClose,
      dataProvider,
      showNotification,
      refreshView,
      translate,
      fieldsDefinition,
      setListSelectedIds,
      isRejectFormValid,
      formErrors,
      touch
    } = this.props;

    const { resource, verb, confirm } = item || {};
    const { shouldConfirm } = this.state;

    if (!confirm && onClose) {
      onClose();
    }

    const requestData = this.getServiceRequestData();

    if (isRejectFormValid) {
      dataProvider(verb, resource, {
        data: { ...requestData, __cfDefinition: fieldsDefinition }
      })
        .then(() => {
          showNotification(translate('input.action.success'));
          refreshView();

          if (shouldConfirm) {
            this.toggleDialog(false);
          }
        })
        .then(() => setListSelectedIds(resource, []))
        .catch((error) => showNotification(error.message, 'error'))
        .finally(() => {
          refreshView();
          this.resetForm();
          this.toggleDialog(false);
        });
    } else {
      touch(...Object.keys(flattenKeys(formErrors)));
      showNotification('ra.message.invalid_form');
    }
  };

  getSwitchValue = () => {
    const { item, selectedData, resourceData, record, formSource } = this.props;
    const { source } = item;
    const { data, list } = resourceData || {};
    const { ids = [] } = list || {};
    const displayData = this.state.updatedData ? data : record;
    const { id } = selectedData || '';

    return (
      !isEmpty(displayData) &&
      source &&
      Object.values(displayData).reduce((res, el) => {
        const value =
          (!isEmpty(ids) && ids.filter((id) => el.id === id)) || false;
        if (
          (!isEmpty(value) || value === true) &&
          el[source] === null &&
          id === get(el, formSource)
        ) {
          return true;
        }

        return res;
      }, false)
    );
  };

  determineItemComponent = () => {
    const { item, record, classes, classesOverride } = this.props;
    const { href, action, forwardRecord, variant } = item;
    const data = forwardRecord && !isEmpty(record) ? record : null;
    const finalClasses = cx(
      classesOverride || classes,
      variant && { root: classes.rootPrimary }
    );

    switch (true) {
      case !!href:
        return (
          <MuiButton
            component={(props) => (
              <CustomRouterButton
                to={{
                  pathname: href,
                  data
                }}
                {...props}
              />
            )}
            classes={finalClasses}
            variant={variant}
            color="primary"
            size="small"
          />
        );
      case action === 'edit':
        return (
          <MuiButton
            onClick={(e) => this.handleConfirm(e, this.toggleDrawer)}
            size="small"
            component="div"
            classes={finalClasses}
            variant={variant}
            color="primary"
          />
        );
      case action === ACTION_API:
        return (
          <ActionsButton
            finalClasses={finalClasses}
            onClick={(e) => this.handleConfirm(e, this.handleAPICall)}
            item={item}
            getSwitchValue={this.getSwitchValue()}
          />
        );

      case action === ACTION_MODAL:
        return (
          <MuiButton
            onClick={(e) => this.handleConfirm(e, this.toggleDialog)}
            size="small"
            component="div"
            classes={finalClasses}
            variant={variant}
            color="primary"
          />
        );
      case action === ACTION_EXPORT:
        return (
          <MuiButton
            onClick={this.handleLocalExport}
            size="small"
            component="div"
            classes={finalClasses}
            variant={variant}
            color="primary"
          />
        );
      case action === ACTION_ACTIVATE_SERVICE:
      case action === ACTION_SUSPEND_SERVICE:
      case action === ACTION_TERMINATE_SERVICE:
      case action === ACTION_RESUME_SERVICE:
      case action === ACTION_INSTANTIATE_SERVICE:
        return (
          <MuiButton
            onClick={(e) => this.handleConfirm(e, this.handleServiceAction)}
            classes={finalClasses}
            variant={variant}
            color="primary"
            component="div"
          />
        );
      default:
        return (
          <MuiButton
            component="div"
            classes={finalClasses}
            variant={variant}
            color="primary"
          />
        );
    }
  };

  calcValue = (source, defaultValue, onAdd) => {
    const { formValues, selectedItems } = this.props;
    const { operator } = onAdd;
    const { dynamicFields } = this.state;

    let params;
    selectedItems.map((selectedItem) => {
      params = processParams({
        params: { defaultValue },
        source: { selectedItem }
      });

      return params;
    });
    const data = (!!formValues && get(formValues, source.split('[')[0])) || [];
    const values =
      !isEmpty(data) &&
      data.reduce((res, val) => {
        if (val && operator === '-') return res - val;

        return res;
      }, params.defaultValue);
    if (values < 0) return 0;

    return dynamicFields.length === 1 ? params.defaultValue : values;
  };

  dynamicInput = (event, fields) => {
    event.preventDefault();
    const { dynamicFields, itemId } = this.state;
    const dynamicInput = [];
    fields.map((field) =>
      dynamicInput.push({
        ...field,
        source: `${field.source}[${itemId}]`
      })
    );
    let data;
    dynamicFields.push({ ...dynamicInput });
    Object.values(dynamicInput).forEach((field) => {
      const { defaultValue, source, onAdd } = field;
      if (onAdd) {
        data = this.calcValue(source, defaultValue, onAdd);

        this.props.dispatch(change(MODAL_REDUX_FORM_NAME, source, data));
      }
    });

    return this.setState({
      itemId: itemId + 1,
      dynamicFields
    });
  };

  handleClose = () => {
    this.props.dispatch(destroy(MODAL_REDUX_FORM_NAME));
    this.setState({
      dialog: false,
      dynamicFields: [],
      itemId: 0
    });
  };

  renderDynamicInput = (fields, label) => {
    const { classes, classesOverride, record, selectedItems } = this.props;
    const { dynamicFields } = this.state;
    const finalClasses = classesOverride || classes;

    return (
      <>
        {dynamicFields.map((field, index) => (
          <div
            className={
              dynamicFields.length > 1
                ? cx('filter-field', finalClasses.body)
                : null
            }
          >
            {dynamicFields.length > 1 && (
              <IconButton
                className="hide-filter"
                onClick={(e) => this.handleHide(e, field)}
                filterElement={field}
              >
                <ActionHide />
              </IconButton>
            )}
            {Object.values(field).map((element) => {
              const { defaultValue } = element;
              let value;
              selectedItems.map((selectedItem) => {
                value = processParams({
                  params: { defaultValue },
                  source: { selectedItem }
                });

                return value;
              });

              return (
                <Row
                  key={index}
                  className={
                    dynamicFields.length > 1
                      ? finalClasses.dynamicInputRow
                      : null
                  }
                  grid
                >
                  <InputRenderer
                    {...element}
                    defaultValue={value.defaultValue}
                    classes={finalClasses.body}
                    record={record}
                  />
                </Row>
              );
            })}
          </div>
        ))}

        <MuiButton
          component="div"
          onClick={(e) => this.dynamicInput(e, fields)}
        >
          <AddIcon />
          {label}
        </MuiButton>
      </>
    );
  };

  renderContextualElements = () => {
    const {
      item,
      fields,
      pageResource,
      parentRecord,
      record,
      form,
      translate,
      classes,
      classesOverride,
      fieldsDefinition
    } = this.props;
    const { resource, label, action, confirm, confirmMessage = true } =
      item || {};
    const { dialog, drawer, shouldConfirm } = this.state;

    const relatedResource = pageResource || resource;
    const relatedRecord = parentRecord || record || {};
    const {
      fields: confirmFields = [],
      label: confirmLabel,
      customFields = {}
    } = confirm || {};
    const { referenceField } = customFields;
    const finalConfirmMessage =
      typeof confirmMessage === 'string'
        ? confirmMessage
        : confirmMessage !== false
        ? 'confirm_content'
        : false;

    const finalClasses = classesOverride || classes;

    switch (true) {
      case !!shouldConfirm:
        return (
          <ConfirmDialog
            open={dialog}
            title="confirm"
            content={finalConfirmMessage}
            translateOptions={{
              label: inflection.humanize(
                inflection.singularize(translate(confirmLabel || label)),
                true
              )
            }}
            onConfirm={shouldConfirm}
            onClose={() => this.handleClose()}
          >
            {confirmFields.map((field, key) => {
              const { type, fields, label } = field;
              if (type === DYNAMIC_INPUT) {
                return <>{this.renderDynamicInput(fields, label)}</>;
              }

              return (
                <Row
                  key={key}
                  grid
                  className={!isEmpty(customFields) ? finalClasses.row : ''}
                >
                  <InputRenderer
                    key={`${field.source}_key`}
                    {...field}
                    record={record}
                    form={form || MODAL_REDUX_FORM_NAME}
                  />
                </Row>
              );
            })}
            {!isEmpty(customFields) && (
              <WithDependentValueCfs
                form={form || MODAL_REDUX_FORM_NAME}
                fieldsDefinition={fieldsDefinition}
                item={item}
              >
                <CustomFieldsSection
                  cfPath={referenceField}
                  formId={form || MODAL_REDUX_FORM_NAME}
                  classes={finalClasses}
                />
              </WithDependentValueCfs>
            )}
          </ConfirmDialog>
        );
      case action === ACTION_MODAL:
        return (
          <Dialog
            open={dialog}
            title={label}
            onClose={this.toggleDialog}
            actions={
              <MuiButton
                onClick={this.toggleDialog}
                component="div"
                color="primary"
              >
                {translate('ra.action.back')}
              </MuiButton>
            }
          >
            {this.renderDialog()}
          </Dialog>
        );
      case action === 'edit':
        return (
          <Drawer open={drawer} anchor="right" onClose={this.toggleDrawer}>
            {drawer ? (
              <Form
                {...this.props}
                id={relatedRecord.id}
                resource={relatedResource}
                basePath={`/${relatedResource}`}
                config={{ rows: [fields] }}
                save={this.handleSave}
                redirectAfterSubmit={this.toggleDrawer}
                isEdit
                inDrawer
                rowClassname={finalClasses.drawerRow}
              />
            ) : null}
          </Drawer>
        );
      default:
        return null;
    }
  };

  render = () => {
    const { record, item, classes, classesOverride, translate } = this.props;
    const { label, icon } = item || {};

    const finalClasses = classesOverride || classes;
    const translatedLabel = translate(label);

    const finalLabel = hydrateStringFromParams({
      string: translatedLabel,
      params: { record }
    });

    return (
      <>
        {cloneElement(this.determineItemComponent(), {
          children: (
            <>
              <DynamicIcon icon={icon} className={finalClasses.icon} />
              <div className={finalClasses.label}>{finalLabel}</div>
            </>
          )
        })}

        {this.renderContextualElements()}
      </>
    );
  };
}

export default compose(
  connect(
    (state, props) => {
      const initialValues =
        props.item.injectPageRecord !== false
          ? get(props, 'item.withSelectedItemRecord', false)
            ? props.selectedItems[0]
            : getDefaultValues(state, props)
          : {};

      return {
        form: MODAL_REDUX_FORM_NAME,
        saving: props.saving || state.admin.saving,
        isRejectFormValid: isValid(MODAL_REDUX_FORM_NAME)(state),
        formValues: getFormValues(MODAL_REDUX_FORM_NAME)(state),
        formErrors: collectErrors(state, MODAL_REDUX_FORM_NAME),
        initialValues
      };
    },
    {
      showNotification: showNotificationAction,
      crudGetList: crudGetListAction,
      refreshView: refreshViewAction,
      setListSelectedIds: setListSelectedIdsAction,
      touch: touchAction
    }
  ),

  withTranslate,
  withDataProvider,
  reduxForm({
    form: MODAL_REDUX_FORM_NAME,
    destroyOnUnmount: false,
    enableReinitialize: true,
    keepDirtyOnReinitialize: true
  }),
  withRouter,
  withStyles(styles)
)(CustomActionItem);
