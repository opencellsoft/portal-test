import React, { memo } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { CardActions, ExportButton, sanitizeListRestProps } from 'react-admin';
import { withStyles } from '@material-ui/core/styles';
import { compose, onlyUpdateForKeys } from 'recompose';

import CreateButton from '../Buttons/CreateButton';
import ImportButton from '../Buttons/ImportButton';
import CustomActions from './CustomActions';
import FilterButton from '../Buttons/FilterButton';
import UploadButton from '../Buttons/UploadButton';

const styles = () => ({
  wrapper: {
    alignItems: 'center'
  }
});

const Actions = memo(
  ({
    bulkActions,
    currentSort,
    className,
    record,
    resource,
    listData,
    filters,
    displayedFilters,
    exporter,
    filterValues,
    permanentFilter,
    withCreate,
    withImport,
    withMappedInsert,
    customActions,
    withUpload,
    basePath,
    selectedIds,
    onUnselectItems,
    showFilter,
    total,
    children,
    createLabel,
    classes,
    fields,
    localFilter,
    selectedData,
    uploadedFileFormat,
    ...rest
  }) => (
    <CardActions
      className={cx(classes.wrapper, className)}
      {...sanitizeListRestProps(rest)}
    >
      <CustomActions
        record={record}
        selectedData={selectedData}
        actions={customActions}
        filters={filters}
        listData={listData}
      />

      {children}
      <FilterButton {...rest} fields={fields} localFilter={localFilter} />
      {withCreate && <CreateButton label={createLabel} basePath={basePath} />}

      {withImport && (
        <ImportButton
          record={record}
          resource={resource}
          basePath={basePath}
          {...rest}
        />
      )}

      {withUpload && (
        <UploadButton
          verb="UPLOAD_FILE"
          resource={resource}
          uploadedFileFormat={uploadedFileFormat}
        />
      )}
      {exporter !== false && (
        <ExportButton
          disabled={total === 0}
          resource={resource}
          sort={currentSort}
          filter={{ ...filterValues, ...permanentFilter }}
          exporter={exporter}
        />
      )}
    </CardActions>
  )
);

Actions.propTypes = {
  bulkActions: PropTypes.oneOfType([PropTypes.element, PropTypes.bool]),
  basePath: PropTypes.string,
  className: PropTypes.string,
  currentSort: PropTypes.object,
  displayedFilters: PropTypes.object,
  exporter: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  filters: PropTypes.element,
  filterValues: PropTypes.object,
  withCreate: PropTypes.bool,
  resource: PropTypes.string,
  onUnselectItems: PropTypes.func.isRequired,
  selectedIds: PropTypes.arrayOf(PropTypes.any),
  showFilter: PropTypes.func,
  fields: PropTypes.object,
  localFilter: PropTypes.bool,
  total: PropTypes.number.isRequired
};

Actions.defaultProps = {
  selectedIds: []
};

export default compose(
  onlyUpdateForKeys([
    'resource',
    'filters',
    'basePath',
    'displayedFilters',
    'filterValues',
    'selectedIds',
    'children'
  ]),
  withStyles(styles)
)(Actions);
