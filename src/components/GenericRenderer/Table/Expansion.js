import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Typography
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { get } from 'lodash';

class Expansion extends PureComponent {
  static propTypes = {
    data: PropTypes.array,
    fields: PropTypes.array
  };

  render = () => {
    const { fields, data } = this.props;

    return (
      <>
        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography variant="body1" component="span">
              {get(data, fields[0].source)}
            </Typography>
          </ExpansionPanelSummary>
          {fields.map(({ source }) => {
            const value = get(data, source);

            return (
              <ExpansionPanelDetails>
                <Typography>
                  <b> {source}</b> :
                </Typography>
                <Typography>{value} </Typography>
              </ExpansionPanelDetails>
            );
          })}
        </ExpansionPanel>
      </>
    );
  };
}

export default Expansion;
