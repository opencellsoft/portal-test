import React, { useContext, memo } from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';

import PageContext from '../Layout/PageContext';
import WithEntityCustomization from '../../Hoc/WithEntityCustomization';
import WithRoles from '../../Hoc/WithRoles';
import CustomActionItem from './CustomActionItem';

const CustomActions = memo((props) => {
  const {
    selectedData,
    actions = [],
    resource,
    record: injectedRecord,
    ...rest
  } = props;
  const { record, resource: pageResource } = useContext(PageContext);
  const currentRecord = injectedRecord || record;
  if (isEmpty(actions)) {
    return false;
  }

  return actions.map((item, index) => (
    <WithRoles roles={item.roles}>
      <WithEntityCustomization resource={item.resource || resource}>
        <CustomActionItem
          {...rest}
          item={item}
          resource={pageResource}
          record={currentRecord}
          selectedData={selectedData}
          parentRecord={record}
          key={index}
        />
      </WithEntityCustomization>
    </WithRoles>
  ));
});

CustomActions.propTypes = {
  actions: PropTypes.array
};

export default CustomActions;
