import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { ListToolbar, BulkActionsToolbar, BulkActions } from 'react-admin';
import { isEmpty, get } from 'lodash';

import ListActions from './ListActions';
import BulkDeleteButton from '../Buttons/BulkDeleteButton';
import CustomActions from './CustomActions';
import { restDataByHashTable } from '../../../utils/data-transformer';

const validate = (bulkActions, selectedIds) => {
  if (!bulkActions) {
    return [];
  }

  return bulkActions.filter((action) => {
    if (!action.dependsOn) {
      return true;
    }
    const {
      minSelectedItems = 0,
      maxSelectedItems = Number.MAX_VALUE,
      ...rest
    } = action.dependsOn;

    if (isEmpty(rest))
      return (
        selectedIds.length >= minSelectedItems &&
        selectedIds.length <= maxSelectedItems
      );

    const hasDependsOn = Object.keys(rest).reduce((result, key) => {
      const rawValue = rest[key];
      const match =
        typeof rawValue === 'string' && rawValue.match(/@(.*?)(\.|$)(.*)/);
      const dynamicContainer = match && match.pop();

      const propertyAliasMatch =
        typeof rawValue === 'string' && rawValue.match(/!/);

      switch (true) {
        case !!dynamicContainer:
          const TabDynamicContainer = selectedIds.filter(
            (selectedId) =>
              get(selectedIds[0], dynamicContainer) !==
              get(selectedId, dynamicContainer)
          );

          if (!isEmpty(TabDynamicContainer)) return true;

          return false;

        case !!propertyAliasMatch:
          const tabPropertyAliasMatch = selectedIds.filter(
            (selectedId) => get(selectedId, key) === rest[key].split('!')[1]
          );

          if (result && isEmpty(tabPropertyAliasMatch)) return true;

          return false;

        default:
          return false;
      }
    }, false);

    return (
      selectedIds.length >= minSelectedItems &&
      selectedIds.length <= maxSelectedItems &&
      hasDependsOn
    );
  });
};

const Toolbar = memo(
  ({
    filters,
    actions = [],
    customActions = [],
    bulkActions,
    withDelete,
    bulkActionButtons,
    exporter = false,
    resource,
    relatedResource,
    cfCode,
    cfIndex,
    selectedIds,
    fields,
    localFilter,
    withCreate,
    withImport,
    record,
    field,
    className,
    bulkCustomActions,
    data,
    roles,
    selectedData,
    nestedDataField,
    handleDeleteCustom,
    ...props
  }) => {
    const hasActions =
      (actions && (typeof actions === 'boolean' || !isEmpty(actions))) ||
      (customActions && typeof customActions === 'boolean') ||
      !isEmpty(customActions) ||
      withCreate ||
      withImport;
    let selectedItems =
      record && record[nestedDataField]
        ? selectedIds.map((id) => record[nestedDataField][id])
        : !isEmpty(data)
        ? selectedIds.map((id) => data[id])
        : [];
    selectedItems = restDataByHashTable(selectedItems, fields);
    const validBulkCustomActions = validate(bulkCustomActions, selectedItems);

    return (
      <>
        {validBulkCustomActions.length > 0 && (
          <BulkActionsToolbar
            {...props}
            selectedIds={selectedIds}
            resource={resource}
            relatedResource={relatedResource}
          >
            <CustomActions
              {...props}
              record={record}
              selectedData={selectedData}
              resource={resource}
              actions={validBulkCustomActions}
              selectedItems={selectedItems}
              cfCode={cfCode}
              cfIndex={cfIndex}
            />
          </BulkActionsToolbar>
        )}

        {bulkActions !== false &&
          withDelete &&
          validBulkCustomActions.length === 0 && (
            <BulkActionsToolbar
              {...props}
              selectedIds={selectedIds}
              resource={resource}
              relatedResource={relatedResource}
            >
              <BulkDeleteButton
                {...props}
                resource={resource}
                relatedResource={relatedResource}
                cfCode={cfCode}
                cfIndex={cfIndex}
                record={record}
                data={!Array.isArray(data) ? Object.values(data) : data}
                handleDeleteCustom={handleDeleteCustom}
              />
            </BulkActionsToolbar>
          )}

        {(filters || hasActions || (bulkActions && !isEmpty(selectedIds))) && (
          <ListToolbar
            filters={filters}
            actions={
              actions || !isEmpty(customActions) ? (
                <ListActions
                  {...props}
                  fields={fields}
                  localFilter={localFilter}
                  record={record}
                  customActions={customActions}
                  listData={data}
                  selectedData={selectedData}
                  filters={filters}
                  withCreate={withCreate}
                  withImport={withImport}
                  exporter={exporter}
                >
                  {typeof actions === 'boolean' ? null : actions}
                </ListActions>
              ) : (
                actions
              )
            }
            bulkActions={
              bulkActions && typeof bulkActions === 'boolean' ? (
                <BulkActions />
              ) : (
                bulkActions
              )
            }
            exporter={exporter}
          />
        )}
      </>
    );
  }
);

Toolbar.defaultProps = {
  selectedIds: []
};

Toolbar.propTypes = {
  data: PropTypes.array,
  fields: PropTypes.array.isRequired,
  record: PropTypes.object,
  source: PropTypes.string,
  cfCode: PropTypes.string,
  cfIndex: PropTypes.number,
  basePath: PropTypes.string,
  withEdit: PropTypes.bool,
  selectedIds: PropTypes.array,
  bulkActionButtons: PropTypes.oneOfType([PropTypes.node, PropTypes.array])
};

export default Toolbar;
