import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import { MAIN } from '../../../constants/generic';

import Title from '../../Layout/Title';
import SectionRenderer from '../Layout/SectionRenderer';

const styles = ({ spacing }) => ({
  row: {
    display: 'flex',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    margin: 0,
    padding: `0 ${spacing.unit}px ${spacing.unit * 2}px`
  }
});

const CustomPage = ({ classes, ...props }) => {
  const { title, record, page } = props;
  const defaultTitle = (page ? [page] : [])
    .reduce((res, el) => [...res, ...el.split('-')], [])
    .join(' ');

  return (
    <>
      <Title title={title} record={record} defaultTitle={defaultTitle} />
      <SectionRenderer section={MAIN} rowClassname={classes.row} {...props} />
    </>
  );
};

CustomPage.propTypes = {
  config: PropTypes.object.isRequired,
  title: PropTypes.string,
  record: PropTypes.object,
  resource: PropTypes.string.isRequired,
  basePath: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CustomPage);
