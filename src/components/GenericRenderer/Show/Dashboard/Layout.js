import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { isEmpty } from 'lodash';
import { compose } from 'recompose';

import withTranslate from '../../../Hoc/withTranslate';

import { MAIN } from '../../../../constants/generic';

import SectionRenderer from '../../Layout/SectionRenderer';
import Title from '../../../Layout/Title';

const styles = ({ spacing }) => ({
  row: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
    flexWrap: 'wrap',
    margin: 0,
    padding: 0
  }
});

const Layout = ({ classes, translate, ...props }) => {
  const { record = {} } = props;

  const defaultTitle = translate('ra.page.show', {
    name: '',
    id: record.id
  });

  return (
    <>
      <Title defaultTitle={defaultTitle} />

      {!isEmpty(record) && (
        <SectionRenderer section={MAIN} rowClassname={classes.row} {...props} />
      )}
    </>
  );
};

Layout.propTypes = {
  config: PropTypes.object.isRequired,
  title: PropTypes.string,
  record: PropTypes.object,
  resource: PropTypes.string.isRequired,
  defaultTitle: PropTypes.string,
  basePath: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
  translate: PropTypes.func.isRequired
};

export default compose(
  withTranslate,
  withStyles(styles)
)(Layout);
