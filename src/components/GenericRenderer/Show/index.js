import React from 'react';
import { ShowController } from 'react-admin';

import { DASHBOARD } from '../../../constants/generic';
import Dashboard from './Dashboard';
import withTranslate from '../../Hoc/withTranslate';

const Components = {
  [DASHBOARD]: Dashboard
};

const GenericShow = ({ config, defaultTitle, ...props }) => {
  const { type } = config;
  const Component = Components[type];

  return (
    <ShowController {...props}>
      {(controllerProps) => (
        <Component {...props} {...controllerProps} config={config} />
      )}
    </ShowController>
  );
};

export default withTranslate(GenericShow);
