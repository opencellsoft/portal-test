import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button as RAButton } from 'react-admin';
import UpdateIcon from '@material-ui/icons/Autorenew';
import { compose } from 'recompose';
import { get } from 'lodash';

import WithFileUpload from './WithFileUpload';
import withTranslate from '../../Hoc/withTranslate';

const UpdateVersionButton = ({
  label = 'input.file.update_version',
  onClick,
  translate
}) => (
  <RAButton
    to={undefined}
    label={translate(label, { cf: '' })}
    onClick={onClick}
    component="button"
  >
    <UpdateIcon />
  </RAButton>
);

UpdateVersionButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired,
  label: PropTypes.string
};

export default compose(
  connect(({ admin: { resources } }, { recordId, resource }) => ({
    record: get(resources, `${resource}.data[${recordId}]`)
  })),
  WithFileUpload,
  withTranslate
)(UpdateVersionButton);
