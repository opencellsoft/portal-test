import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  refreshView as refreshViewAction,
  showNotification as showNotificationAction,
  Button as RAButton,
  withDataProvider,
  REDUX_FORM_NAME
} from 'react-admin';
import { reduxForm, getFormValues } from 'redux-form';
import { Button, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import UploadIcon from '@material-ui/icons/Publish';
import { get } from 'lodash';
import { compose } from 'recompose';

import InputRenderer from '../Form/InputRenderer';
import Dialog from '../../Globals/Dialogs/Dialog';

import { getFullReference } from '../../../utils/routing';

import withTranslate from '../../Hoc/withTranslate';

const styles = ({ spacing, breakpoints, palette, typography }) => ({
  importButton: {
    marginLeft: spacing.unit
  },
  row: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    marginBottom: spacing.unit * 3
  },
  centered: {
    display: 'flex',
    justifyContent: 'center'
  },
  error: {
    color: palette.error.main,
    marginTop: 5,
    fontWeight: typography.fontWeightMedium
  }
});

class UploadButton extends PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    translate: PropTypes.func.isRequired,
    resource: PropTypes.string.isRequired,
    resourceFileFormats: PropTypes.string.isRequired,
    selectedFileFormat: PropTypes.object,
    refreshView: PropTypes.func.isRequired,
    dataProvider: PropTypes.func.isRequired,
    showNotification: PropTypes.func.isRequired
  };

  state = {
    dialog: false
  };

  toggleDialog = () => this.setState({ dialog: !this.state.dialog });

  importFile = () => {
    const { path } = this.state;
    const {
      refreshView,
      showNotification,
      resource,
      dataProvider,
      selectedFileFormat,
      formValues,
      verb,
      uploadedFileFormat
    } = this.props;

    const { code: fileFormatCode } = selectedFileFormat || '';
    const { file } = formValues;

    return dataProvider(verb, resource, {
      record: selectedFileFormat || {},
      data: {
        file: file.rawFile,
        path,
        fileFormat: uploadedFileFormat || fileFormatCode
      }
    })
      .then(() => {
        refreshView();
        showNotification('input.action.success');
        this.toggleDialog();
      })
      .catch(() => showNotification('error'));
  };

  render = () => {
    const {
      classes,
      translate,
      formValues,
      resourceFileFormats,
      uploadedFileFormat
    } = this.props;
    const { dialog } = this.state;
    const { rawFile } = formValues.file || '';
    const fields = uploadedFileFormat
      ? [{ type: 'file', source: 'file', label: 'File' }]
      : [
          { source: 'Format', reference: resourceFileFormats, label: 'Format' },
          { type: 'file', source: 'file', label: 'File' }
        ];

    return (
      <>
        <RAButton
          to={undefined}
          label="Upload"
          onClick={this.toggleDialog}
          component="button"
        >
          <UploadIcon />
        </RAButton>
        <Dialog
          open={dialog}
          onClose={this.toggleDialog}
          actions={
            <>
              <Button onClick={this.toggleDialog} color="primary">
                {translate('ra.action.cancel')}
              </Button>

              <Button
                color="primary"
                variant="contained"
                className={classes.importButton}
                onClick={this.importFile}
                disabled={!rawFile}
              >
                {translate('input.file.upload')}
              </Button>
            </>
          }
        >
          <div className={classes.row}>
            <Typography variant="headline" gutterBottom>
              {translate('input.file.upload_file')}
            </Typography>
          </div>

          <div className={classes.row}>
            {fields.map((field) => (
              <InputRenderer {...field} />
            ))}
          </div>
        </Dialog>
      </>
    );
  };
}

const mapStateToProps = (state, props) => {
  const formValues = getFormValues(props.form || REDUX_FORM_NAME)(state) || {};
  const fileFormatId = formValues.Format || {};
  const resourceFileFormats = getFullReference(
    'file-formats',
    state.config.config.modules
  );

  return {
    form: props.form || REDUX_FORM_NAME,
    formValues,
    saving: props.saving || state.admin.saving,
    resourceFileFormats,
    selectedFileFormat: get(
      state,
      `admin.resources.${resourceFileFormats}.data.${fileFormatId}`
    )
  };
};

export default compose(
  connect(
    mapStateToProps,
    {
      showNotification: showNotificationAction,
      refreshView: refreshViewAction
    }
  ),
  withTranslate, // Must be before reduxForm so that it can be used in validation
  withDataProvider,
  reduxForm({
    destroyOnUnmount: false,
    enableReinitialize: true,
    keepDirtyOnReinitialize: true
  }),
  withStyles(styles)
)(UploadButton);
