import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import cx from 'classnames';
import {
  Button,
  Responsive,
  withDataProvider,
  refreshView as refreshViewAction,
  showNotification as showNotificationAction
} from 'react-admin';
import { Button as MuiButton } from '@material-ui/core';
import { withStyles, createStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';
import { capitalize } from 'lodash';

import { processParams } from '../../../utils/generic-render';

import DynamicIcon from '../../Globals/DynamicIcon';

import withTranslate from '../../Hoc/withTranslate';

const styles = ({ palette }) =>
  createStyles({
    floating: {
      color: palette.getContrastText(palette.primary.main),
      margin: 0,
      top: 'auto',
      right: 20,
      bottom: 60,
      left: 'auto',
      position: 'fixed',
      zIndex: 1000
    },
    floatingLink: {
      color: 'inherit'
    }
  });

class APIButton extends PureComponent {
  static propTypes = {
    basePath: PropTypes.string,
    className: PropTypes.string,
    classes: PropTypes.object,
    label: PropTypes.string,
    size: PropTypes.string,
    dataProvider: PropTypes.func.isRequired,
    translate: PropTypes.func.isRequired,
    icon: PropTypes.element
  };

  handleClick = () => {
    const {
      dataProvider,
      resource,
      record,
      verb,
      params = {},
      refreshView,
      showNotification
    } = this.props;

    return dataProvider(verb, resource, {
      data: { ...processParams({ params, source: { record } }) }
    })
      .then(refreshView)
      .catch((error) => showNotification(capitalize(error.message)));
  };

  render = () => {
    const {
      className,
      classes = {},
      translate,
      label = 'ra.action.create',
      icon,
      ...rest
    } = this.props;

    return (
      <Responsive
        small={
          <MuiButton
            variant="fab"
            color="primary"
            className={cx(classes.floating, className)}
            aria-label={label && translate(label)}
            onClick={this.handleClick}
            {...rest}
          >
            {icon && <DynamicIcon icon={icon} />}
          </MuiButton>
        }
        medium={
          <Button
            className={className}
            label={label}
            onClick={this.handleClick}
            {...rest}
          >
            {icon && <DynamicIcon icon={icon} />}
          </Button>
        }
      />
    );
  };
}

const enhance = compose(
  withTranslate,
  connect(
    undefined,
    {
      refreshView: refreshViewAction,
      showNotification: showNotificationAction
    }
  ),
  withDataProvider,
  withStyles(styles)
);

export default enhance(APIButton);
