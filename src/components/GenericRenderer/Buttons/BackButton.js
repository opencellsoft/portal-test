import React from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import { withStyles, createStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import ArrowBack from '@material-ui/icons/ArrowBack';
import classnames from 'classnames';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { withRouter } from 'react-router-dom';
import withTranslate from '../../Hoc/withTranslate';

const styles = (theme) =>
  createStyles({
    deleteButton: {
      color: theme.palette.error.main,
      '&:hover': {
        backgroundColor: fade(theme.palette.error.main, 0.12),
        '@media (hover: none)': {
          backgroundColor: 'transparent'
        }
      }
    }
  });

const sanitizeRestProps = ({
  basePath,
  className,
  classes,
  label,
  invalid,
  variant,
  translate,
  handleSubmit,
  handleSubmitWithRedirect,
  submitOnEnter,
  record,
  redirect,
  resource,
  locale,
  ...rest
}) => rest;

const BackButton = ({
  className,
  classes = {},
  invalid,
  label = 'ra.action.back',
  pristine,
  redirect,
  saving,
  submitOnEnter,
  translate,
  icon,
  onClick,
  history,
  ...rest
}) => (
  <Button
    onClick={history.goBack}
    label={label}
    className={classnames('ra-back-button', classes.backButton, className)}
    key="button"
    {...sanitizeRestProps(rest)}
  >
    {icon} {label && translate(label, { _: label })}
  </Button>
);

BackButton.propTypes = {
  label: PropTypes.string,
  refreshView: PropTypes.func.isRequired,
  icon: PropTypes.element,
  history: PropTypes.object.isRequired
};

BackButton.defaultProps = {
  label: 'ra.action.back',
  icon: <ArrowBack />
};

const enhance = compose(
  withTranslate,
  withRouter,
  withStyles(styles)
);

export default enhance(BackButton);
