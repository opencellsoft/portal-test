import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  crudUpdate as crudUpdateAction,
  Button as RAButton
} from 'react-admin';
import { Button, Typography, TextField } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import UploadIcon from '@material-ui/icons/Publish';
import { parse as parseCSV } from 'papaparse';
import { compose } from 'recompose';
import { isEmpty } from 'lodash';
import moment from 'moment-timezone';

import Dialog from '../../Globals/Dialogs/Dialog';

import withTranslate from '../../Hoc/withTranslate';

const styles = ({ spacing, breakpoints, palette, typography }) => ({
  importButton: {
    marginLeft: spacing.unit
  },
  row: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    marginBottom: spacing.unit * 3
  },
  centered: {
    display: 'flex',
    justifyContent: 'center'
  },
  creationCard: {
    display: 'none',
    [breakpoints.up('md')]: {
      display: 'block'
    }
  },
  floatingCreation: {
    display: 'block',
    [breakpoints.up('md')]: {
      display: 'none'
    }
  },
  error: {
    color: palette.error.main,
    marginTop: 5,
    fontWeight: typography.fontWeightMedium
  }
});

class ImportButton extends PureComponent {
  static propTypes = {
    record: PropTypes.object.isRequired,
    selectedOffer: PropTypes.string.isRequired,
    offerData: PropTypes.object.isRequired,
    isLoading: PropTypes.bool,
    classes: PropTypes.object.isRequired,
    translate: PropTypes.func.isRequired
  };

  now = moment()
    .startOf('day')
    .format('YYYY-MM-DD');

  state = {
    dialog: false,
    date: this.now,
    parsedFile: null,
    error: null,
    submit: false
  };

  componentDidUpdate = ({ isLoading: prevIsLoading }) => {
    const { isLoading } = this.props;
    const { dialog, submit } = this.state;

    if (dialog && submit && isLoading !== prevIsLoading) {
      this.toggleDialog();
    }
  };

  toggleDialog = () =>
    this.setState({
      dialog: !this.state.dialog,
      date: this.now,
      file: null,
      parsedFile: null,
      error: null,
      submit: false
    });

  handleDateChange = ({ target: { value } }) => {
    const { translate } = this.props;
    const newDate = moment.utc(value);
    const now = moment.utc(this.now);

    if (newDate.isBefore(now, 'day')) {
      return this.setState({
        error: translate('input.date.before_now_error'),
        date: this.now
      });
    }

    return this.setState({
      date: newDate.startOf('day').format('YYYY-MM-DD'),
      error: null
    });
  };

  onChange = (field) => ({ target: { value } }) =>
    this.setState({ [field]: value });

  handleFileError = () =>
    this.setState({
      parsedFile: null,
      error: this.props.translate('input.file.file_error')
    });

  validateFile = ({ data, errors }) => {
    if (isEmpty(data)) {
      return this.handleFileError();
    }

    return this.setState({ parsedFile: data, error: null });
  };

  handleFile = ({ target: { files } }) => {
    try {
      parseCSV(files[0], {
        complete: (results) =>
          this.setState({ file: files[0], parsedFile: null, error: null }, () =>
            this.validateFile(results)
          )
      });
    } catch (error) {
      this.handleFileError();
    }
  };

  formatRecord = () => {
    const { parsedFile, date } = this.state;
    const { record, selectedOffer, offerData } = this.props;
    const { customFields = {} } = record;
    const { customField = [], inheritedCustomField = [] } = customFields;

    const {
      id: code,
      description,
      fieldType,
      languageDescriptions
    } = offerData;

    const fieldLatestVersion = customField.reduce((res, cf) => {
      const { code, valuePeriodPriority } = cf;

      if (code !== selectedOffer) {
        return res;
      }

      if (!res || res.valuePeriodPriority < valuePeriodPriority) {
        return cf;
      }

      return res;
    }, null);
    const nextVersion = fieldLatestVersion
      ? fieldLatestVersion.valuePeriodPriority + 1
      : 1;

    const formattedData = {
      code,
      description,
      fieldType,
      languageDescriptions,
      valuePeriodPriority: nextVersion,
      valuePeriodStartDate: moment
        .utc(date)
        .local()
        .startOf('day')
        .valueOf(),
      mapValue: parsedFile.reduce(
        (res, row) => {
          const [code, ...rest] = row;

          return { ...res, [code]: { value: rest.join('|') } };
        },
        {
          key: {
            value: offerData.matrixColumn.map(({ code }) => code).join('/')
          }
        }
      )
    };

    return {
      ...record,
      customFields: {
        customField: [...customField, formattedData],
        inheritedCustomField
      }
    };
  };

  submit = () => {
    const { parsedFile, date } = this.state;

    if (!parsedFile || !date) {
      return false;
    }

    const updatedRecord = this.formatRecord();

    return updatedRecord;
  };

  render = () => {
    const { classes, translate } = this.props;
    const { dialog, date, file, parsedFile, error } = this.state;

    return (
      <>
        <RAButton
          to={undefined}
          label={translate('input.file.import')}
          onClick={this.toggleDialog}
          component="button"
        >
          <UploadIcon />
        </RAButton>

        <Dialog
          open={dialog}
          onClose={this.toggleDialog}
          actions={
            <>
              <Button onClick={this.toggleDialog} color="primary">
                {translate('ra.action.cancel')}
              </Button>

              <Button
                color="primary"
                variant="contained"
                className={classes.importButton}
                onClick={this.submit}
                disabled={!parsedFile}
              >
                {translate('input.file.import')}
              </Button>
            </>
          }
        >
          <div className={classes.row}>
            <Typography variant="headline" gutterBottom>
              Import
            </Typography>
          </div>

          <div className={classes.row}>
            <TextField
              label="Starting date"
              type="date"
              onChange={this.handleDateChange}
              value={date}
              InputLabelProps={{
                shrink: true
              }}
              inputProps={{
                min: date,
                required: true
              }}
            />
          </div>

          <div className={classes.row}>
            <Button variant="contained" component="label">
              {translate('input.file.choose_file')}
              <input
                type="file"
                onChange={this.handleFile}
                style={{ display: 'none' }}
                accept=".csv"
              />
            </Button>
          </div>

          {file && (
            <div className={classes.row}>
              <div className={classes.centered}>
                <Typography variant="caption">{file.name}</Typography>
              </div>
            </div>
          )}

          {error && (
            <div className={classes.row}>
              <div className={classes.centered}>
                <Typography variant="caption" className={classes.error}>
                  {error}
                </Typography>
              </div>
            </div>
          )}
        </Dialog>
      </>
    );
  };
}

export default compose(
  connect(
    ({ admin: { resources, loading } }, { resource, selectedOffer }) => ({
      offerData: resources[resource] ? resources.data[selectedOffer] : null,
      isLoading: loading > 0
    }),
    { crudUpdate: crudUpdateAction }
  ),
  withStyles(styles),
  withTranslate
)(ImportButton);
