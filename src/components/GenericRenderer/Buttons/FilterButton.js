import React, { PureComponent } from 'react';
import { findDOMNode } from 'react-dom';
import PropTypes from 'prop-types';
import Menu from '@material-ui/core/Menu';
import { withStyles, createStyles } from '@material-ui/core/styles';
import ContentFilter from '@material-ui/icons/FilterList';
import classnames from 'classnames';

import compose from 'recompose/compose';
import { reduxForm, change } from 'redux-form';
import { connect } from 'react-redux';
import IconButton from '@material-ui/core/IconButton';
import ActionHide from '@material-ui/icons/HighlightOff';
import { Button } from 'react-admin';
import { isEmpty, get } from 'lodash';
import FilterButtonMenuItem from './FilterButtonMenuItem';
import ListFilters from '../List/ListFilters';

import withTranslate from '../../Hoc/withTranslate';

const styles = (theme) =>
  createStyles({
    form: {
      marginTop: '-10px',
      paddingTop: 0,
      display: 'flex',
      alignItems: 'flex-end',
      flexWrap: 'wrap'
    },
    body: { display: 'flex', alignItems: 'flex-end' },
    spacer: { width: '1em' },
    icon: {
      color: theme.palette.primary1Color || '#00bcd4',
      paddingBottom: 0
    },
    clearFix: { clear: 'right' }
  });

class FilterButton extends PureComponent {
  static propTypes = {
    resource: PropTypes.string.isRequired,
    filters: PropTypes.arrayOf(PropTypes.node).isRequired,
    displayedFilters: PropTypes.object.isRequired,
    filterValues: PropTypes.object.isRequired,
    showFilter: PropTypes.func.isRequired,
    translate: PropTypes.func.isRequired,
    classes: PropTypes.object,
    className: PropTypes.string,
    setSort: PropTypes.func.isRequired
  };

  state = {
    open: false,
    form: false,
    sourceFiltered: {}
  };

  getHiddenFilters = () => {
    const { sourceFiltered } = this.state;
    const { fields, localeFilterForm, setSort } = this.props;

    if (setSort) setSort(false);
    const filters =
      Array.isArray(fields) && fields.filter(({ filter }) => filter);
    const formProperties =
      !isEmpty(localeFilterForm) && localeFilterForm.registeredFields;

    const localeFilters =
      !isEmpty(filters) &&
      filters.filter((element) => {
        const { source } = element;
        const filteredProperties = get(sourceFiltered, source) || {};
        const filteredform = get(formProperties, source) || {};

        if (
          isEmpty(sourceFiltered) ||
          filteredProperties.source !== source ||
          (formProperties && filteredform.name !== source)
        )
          return element;

        return null;
      });

    return localeFilters;
  };

  handleClickButton = (event) => {
    event.preventDefault();

    this.setState({
      open: true,
      anchorEl: findDOMNode(this.button) // eslint-disable-line react/no-find-dom-node
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false
    });
  };

  handleShow = (data) => {
    const { sourceFiltered } = this.state || {};
    this.props.setSort(false);
    this.setState({
      sourceFiltered: { ...sourceFiltered, ...data },
      open: false,
      form: true
    });
  };

  handleHide = (el, element) => {
    const { dispatch, setSort } = this.props;
    const data = Object.keys(this.state.sourceFiltered).reduce((res, key) => {
      if (element !== key) {
        return { ...res, [key]: this.state.sourceFiltered[key] };
      }

      return res;
    }, {});

    dispatch(change('simpleTableForm', element, ''));

    return this.setState(
      {
        sourceFiltered: { ...data }
      },
      setSort && setSort(false)
    );
  };

  renderButton = (hiddenFilters) => {
    const {
      classes = {},
      className,
      resource,
      filterValues,
      fields,
      ...rest
    } = this.props;
    const { open, anchorEl, sourceFiltered, form } = this.state;

    return (
      <div className={classnames(classes, className)} {...rest}>
        <Button
          ref={(node) => {
            this.button = node;
          }}
          className="add-filter"
          label="ra.action.add_filter"
          onClick={this.handleClickButton}
        >
          <ContentFilter />
        </Button>

        <Menu open={open} anchorEl={anchorEl} onClose={this.handleRequestClose}>
          {hiddenFilters.map((filterElement) => (
            <FilterButtonMenuItem
              key={filterElement.source}
              sourceFiltered={sourceFiltered}
              filter={filterElement}
              form={form}
              resource={resource}
              onShow={this.handleShow}
              {...this.props}
            />
          ))}
        </Menu>
      </div>
    );
  };

  renderForm = (filterElement) => {
    const { classes = {}, localFilter, resource, translate } = this.props;
    const { sourceFiltered } = this.state;
    const filteredForm = [get(sourceFiltered, filterElement)];

    return (
      <div
        data-source={filterElement.source}
        className={classnames('filter-field', classes.body)}
      >
        {!filterElement.alwaysOn && (
          <IconButton
            className="hide-filter"
            onClick={(e) => this.handleHide(e, filterElement)}
            filterElement={filterElement}
            data-key={filterElement.source}
            title={translate('ra.action.remove_filter')}
          >
            <ActionHide />
          </IconButton>
        )}
        <ListFilters
          resource={resource}
          filtersForm={filteredForm}
          localFilter={localFilter}
        />
        <div className={classes.spacer}>&nbsp;</div>
      </div>
    );
  };

  render() {
    const hiddenFilters = this.getHiddenFilters();
    const { classes = {}, className } = this.props;

    const { sourceFiltered, form } = this.state;

    return (
      <>
        <div className={classnames(className, classes.form)}>
          {form &&
            Object.keys(sourceFiltered).map((filterElement) =>
              this.renderForm(filterElement)
            )}
        </div>
        {hiddenFilters &&
          hiddenFilters.length > 0 &&
          this.renderButton(hiddenFilters)}
      </>
    );
  }
}

export default compose(
  connect(
    ({ form: { simpleTableForm } }) => ({
      localeFilterForm: simpleTableForm
    }),
    null
  ),
  reduxForm({
    form: 'simpleTableForm'
  }),
  withTranslate,
  withStyles(styles)
)(FilterButton);
