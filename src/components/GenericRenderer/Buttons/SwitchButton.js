import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Button as RAButton } from 'react-admin';
import { Switch } from '@material-ui/core';
import { compose } from 'recompose';
import withTranslate from '../../Hoc/withTranslate';

class SwitchButton extends PureComponent {
  static propTypes = {
    onClick: PropTypes.func.isRequired,
    translate: PropTypes.func.isRequired,
    label: PropTypes.string,
    value: PropTypes.bool
  };

  render = () => {
    const { label, onClick, value, translate } = this.props;

    return (
      <RAButton
        color="primary"
        to={undefined}
        label={translate(label)}
        component="button"
        onClick={onClick}
      >
        <Switch checked={value} />
      </RAButton>
    );
  };
}

export default compose(withTranslate)(SwitchButton);
