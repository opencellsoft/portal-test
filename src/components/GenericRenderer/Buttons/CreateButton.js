import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Button, Responsive } from 'react-admin';
import cx from 'classnames';
import MuiButton from '@material-ui/core/Button';
import { withStyles, createStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import { compose, onlyUpdateForKeys } from 'recompose';

import withTranslate from '../../Hoc/withTranslate';

const styles = ({ palette }) =>
  createStyles({
    floating: {
      color: palette.getContrastText(palette.primary.main),
      margin: 0,
      top: 'auto',
      right: 20,
      bottom: 60,
      left: 'auto',
      position: 'fixed',
      zIndex: 1000
    },
    floatingLink: {
      color: 'inherit'
    }
  });

const CreateButton = ({
  basePath = '',
  className,
  classes = {},
  translate,
  label = 'ra.action.create',
  data,
  icon = <AddIcon />,
  ...rest
}) => (
  <Responsive
    small={
      <MuiButton
        component={Link}
        variant="fab"
        color="primary"
        className={cx(classes.floating, className)}
        to={{
          pathname: `${basePath}/create`,
          data
        }}
        aria-label={label && translate(label)}
        {...rest}
      >
        {icon}
      </MuiButton>
    }
    medium={
      <Button
        component={Link}
        to={{
          pathname: `${basePath}/create`,
          data
        }}
        className={className}
        label={label}
        {...rest}
      >
        {icon}
      </Button>
    }
  />
);

CreateButton.propTypes = {
  basePath: PropTypes.string,
  className: PropTypes.string,
  classes: PropTypes.object,
  label: PropTypes.string,
  size: PropTypes.string,
  translate: PropTypes.func.isRequired,
  icon: PropTypes.element
};

const enhance = compose(
  withTranslate,
  onlyUpdateForKeys(['basePath', 'label', 'translate']),
  withStyles(styles)
);

export default enhance(CreateButton);
