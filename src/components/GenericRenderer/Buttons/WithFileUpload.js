import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  FileInput,
  FileField,
  REDUX_FORM_NAME,
  crudUpdate as crudUpdateAction,
  withDataProvider,
  refreshView as refreshViewAction,
  showNotification as showNotificationAction,
  required
} from 'react-admin';
import { reduxForm, destroy, getFormValues } from 'redux-form';
import { Button, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { parse as parseCSV } from 'papaparse';
import { compose } from 'recompose';
import { isEmpty, sortBy, capitalize, get, isEqual, cloneDeep } from 'lodash';

import { RESOURCE_ENTITY_CUSTOMIZATION } from '../../../config/resources';
import {
  createMatrix,
  getLatestCFVersion,
  extractMatrixDefinition
} from '../../../utils/custom-fields';

import Dialog from '../../Globals/Dialogs/Dialog';
import DateInput from '../Form/Inputs/DateInput';
import {
  setPreviousVersion,
  setFieldsValueToNull
} from '../../../utils/record';

import withTranslate from '../../Hoc/withTranslate';

const styles = ({ spacing, breakpoints, palette, typography }) => ({
  importButton: {
    marginLeft: spacing.unit
  },
  row: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    marginBottom: spacing.unit * 3
  },
  centered: {
    display: 'flex',
    justifyContent: 'center'
  },
  creationCard: {
    display: 'none',
    [breakpoints.up('md')]: {
      display: 'block'
    }
  },
  floatingCreation: {
    display: 'block',
    [breakpoints.up('md')]: {
      display: 'none'
    }
  },
  error: {
    color: palette.error.main,
    marginTop: 5,
    fontWeight: typography.fontWeightMedium
  }
});

const withFileUpload = (WrappedComponent) => {
  class WithFileUpload extends Component {
    static propTypes = {
      resource: PropTypes.string.isRequired,
      record: PropTypes.object.isRequired,
      basePath: PropTypes.string.isRequired,
      crudUpdate: PropTypes.func.isRequired,
      cfCode: PropTypes.string,
      cfDefinition: PropTypes.object,
      isLoading: PropTypes.bool,
      fileType: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
      classes: PropTypes.object.isRequired,
      translate: PropTypes.func.isRequired,
      dispatch: PropTypes.func.isRequired,
      dataProvider: PropTypes.func.isRequired,
      refreshView: PropTypes.func.isRequired,
      showNotification: PropTypes.func.isRequired
    };

    state = {
      dialog: false,
      selectedFileType: 'csv',
      parsedFile: null,
      error: null,
      submit: false
    };

    componentDidUpdate = ({
      isLoading: prevIsLoading,
      formValues: prevFormValues
    }) => {
      const { isLoading, formValues } = this.props;
      const { dialog, submit } = this.state;

      if (dialog && submit && isLoading !== prevIsLoading) {
        this.toggleDialog();
      }

      if (
        formValues &&
        formValues.file &&
        (!prevFormValues || !prevFormValues.file)
      ) {
        this.handleFile();
      }
    };

    resetForm = () =>
      this.props.dispatch(destroy(this.props.form || REDUX_FORM_NAME));

    toggleDialog = () =>
      this.setState(
        {
          dialog: !this.state.dialog,
          error: null,
          submit: false
        },
        this.resetForm
      );

    handleFileError = () =>
      this.setState({
        error: this.props.translate('input.file.file_error')
      });

    validateFile = (data) => {
      if (isEmpty(data)) {
        return this.handleFileError();
      }

      return this.setState({ parsedFile: data, error: null });
    };

    getFileParser = () => {
      const { selectedFileType } = this.state;

      switch (selectedFileType) {
        case 'csv':
          return parseCSV;
        default:
          return null;
      }
    };

    getDataToParse = (dataToParse) => {
      const { cfDefinition, cfCode } = this.props;

      const { keyColumns, valueColumns } = extractMatrixDefinition(
        cfDefinition,
        cfCode
      );
      const columns = Object.values(keyColumns.concat(valueColumns));
      const firstData = dataToParse.shift();

      const hasColumns = isEqual(
        Object.values(columns),
        Object.values(firstData)
      );

      return { dataToParse, hasColumns };
    };

    handleFile = () => {
      const { formValues } = this.props;
      const { file } = formValues;
      const { rawFile } = file || {};
      const parser = this.getFileParser();

      try {
        parser(rawFile, {
          complete: ({ data }) =>
            this.setState({ parsedFile: null, error: null }, () => {
              const { dataToParse, hasColumns } = this.getDataToParse(data);
              if (hasColumns) {
                this.validateFile(dataToParse);
              } else {
                this.validateFile(data);
              }
            })
        });
      } catch (error) {
        this.handleFileError();
      }
    };

    formatRecord = (transformer) => {
      const { parsedFile } = this.state;
      const { record, cfCode, cfDefinition, formValues, isUpdate } = this.props;
      const { date } = formValues;

      const { customFields = {} } = record || {};
      const { customField = [], inheritedCustomField = [] } = customFields;

      const { field = [] } = cfDefinition || {};
      const relatedCfDefinition = field.find(({ code }) => code === cfCode);

      if (isEmpty(relatedCfDefinition)) {
        return record;
      }

      const currentLatestVersion = getLatestCFVersion(record, cfCode);
      const currentCFIndex = currentLatestVersion
        ? customField.findIndex(
            ({ code, valuePeriodPriority }) =>
              code === cfCode &&
              valuePeriodPriority === currentLatestVersion.valuePeriodPriority
          )
        : -1;

      const formattedData = createMatrix(
        customField,
        relatedCfDefinition,
        parsedFile,
        date
      );

      if (isUpdate) {
        formattedData.valuePeriodPriority =
          currentLatestVersion.valuePeriodPriority;
        formattedData.valuePeriodStartDate =
          currentLatestVersion.valuePeriodStartDate;

        customField[currentCFIndex] = formattedData;
      }

      let customFieldsUpdated = [
        ...customField,
        ...(isUpdate ? [] : [formattedData])
      ];

      if (!isUpdate) {
        customFieldsUpdated = transformer(customFieldsUpdated, cfCode);
      }

      return {
        ...record,
        customFields: {
          customField: sortBy(customFieldsUpdated, [
            'code',
            'valuePeriodPriority'
          ]),
          inheritedCustomField
        }
      };
    };

    handleSubmit = () => {
      const { parsedFile } = this.state;
      const {
        crudUpdate,
        resource,
        record,
        basePath,
        cfDefinition,
        isUpdate,
        dataProvider,
        refreshView,
        showNotification
      } = this.props;

      if (isEmpty(cfDefinition)) {
        return false;
      }

      if (!parsedFile) {
        return false;
      }

      const updatedRecord = isUpdate
        ? this.formatRecord()
        : cloneDeep(this.formatRecord(setPreviousVersion));
      const nullableRecord = isUpdate
        ? null
        : this.formatRecord(setFieldsValueToNull);

      const { id } = record || {};

      return this.setState({ submit: true }, () =>
        isUpdate
          ? crudUpdate(
              resource,
              id,
              { ...updatedRecord, __cfDefinition: cfDefinition },
              record,
              basePath,
              null
            )
          : dataProvider('UPDATE', resource, {
              data: nullableRecord
            })
              .then(() =>
                dataProvider('UPDATE', resource, {
                  data: updatedRecord
                })
                  .then(refreshView)
                  .catch((error) => showNotification(capitalize(error.message)))
              )
              .catch((error) => showNotification(capitalize(error.message)))
      );
    };

    render = () => {
      const {
        cfCode,
        classes,
        label,
        translate,
        isUpdate,
        invalid
      } = this.props;
      const { dialog, selectedFileType, error } = this.state;

      const cfKey =
        cfCode
          .trim()
          .split('_')
          .map((word) => capitalize(word))
          .join(' ') || '';

      return (
        <>
          <WrappedComponent {...this.props} onClick={this.toggleDialog} />

          {dialog && (
            <Dialog
              open
              onClose={this.toggleDialog}
              actions={
                <>
                  <Button onClick={this.toggleDialog} color="primary">
                    {translate('ra.action.cancel')}
                  </Button>

                  <Button
                    color="primary"
                    variant="contained"
                    className={classes.importButton}
                    onClick={this.handleSubmit}
                    disabled={invalid}
                  >
                    {translate(label || 'input.file.import_version')}
                  </Button>
                </>
              }
            >
              <div className={classes.row}>
                <Typography variant="headline" gutterBottom>
                  {translate(
                    label ||
                      (cfCode
                        ? 'input.file.import_version'
                        : 'input.file.import'),
                    {
                      cf: cfKey
                    }
                  ).replace(/(\b\S.+\b)(?=.*\1)/g, '')}
                </Typography>
              </div>

              {!isUpdate && (
                <div className={classes.row}>
                  <DateInput
                    source="date"
                    label="Starting date"
                    validate={[required()]}
                  />
                </div>
              )}

              <div className={classes.row}>
                <FileInput
                  source="file"
                  label="File"
                  accept={`.${selectedFileType}`}
                  validate={[required()]}
                >
                  <FileField source="src" title="title" />
                </FileInput>
              </div>

              {error && (
                <div className={classes.row}>
                  <div className={classes.centered}>
                    <Typography variant="caption" className={classes.error}>
                      {error}
                    </Typography>
                  </div>
                </div>
              )}
            </Dialog>
          )}
        </>
      );
    };
  }

  return compose(
    connect(
      (state, { form, saving, resource }) => {
        const {
          admin: { resources, saving: adminSaving, loading },
          config: { config }
        } = state;
        const entity = get(config.modules, `${resource}.entity`);

        return {
          form: form || REDUX_FORM_NAME,
          formValues: getFormValues(form || REDUX_FORM_NAME)(state),
          saving: saving || adminSaving,
          cfDefinition:
            resources[RESOURCE_ENTITY_CUSTOMIZATION].data[entity] || {},
          isLoading: loading > 0
        };
      },
      {
        crudUpdate: crudUpdateAction,
        refreshView: refreshViewAction,
        showNotification: showNotificationAction
      }
    ),
    withTranslate, // Must be before reduxForm so that it can be used in validation
    reduxForm({
      destroyOnUnmount: false,
      enableReinitialize: true,
      keepDirtyOnReinitialize: true
    }),
    withDataProvider,
    withStyles(styles)
  )(WithFileUpload);
};

export default withFileUpload;
