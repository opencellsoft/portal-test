import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import {
  Confirm,
  Button,
  withDataProvider,
  refreshView as refreshViewAction,
  showNotification as showNotificationAction,
  setListSelectedIds as setListSelectedIdsAction,
  DELETE_MANY,
  UPDATE
} from 'react-admin';
import inflection from 'inflection';
import ActionDelete from '@material-ui/icons/Delete';
import { withStyles, createStyles } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { isNil, isFunction } from 'lodash';

import withTranslate from '../../Hoc/withTranslate';

const sanitizeRestProps = ({
  basePath,
  classes,
  dataProvider,
  filterValues,
  label,
  resource,
  selectedIds,
  ...rest
}) => rest;

const styles = (theme) =>
  createStyles({
    deleteButton: {
      color: theme.palette.error.main,
      '&:hover': {
        backgroundColor: fade(theme.palette.error.main, 0.12),
        // Reset on mouse devices
        '@media (hover: none)': {
          backgroundColor: 'transparent'
        }
      }
    }
  });

class BulkDeleteButton extends PureComponent {
  static propTypes = {
    basePath: PropTypes.string,
    classes: PropTypes.object,
    dataProvider: PropTypes.func.isRequired,
    label: PropTypes.string,
    cfCode: PropTypes.string,
    resource: PropTypes.string.isRequired,
    selectedIds: PropTypes.arrayOf(PropTypes.any).isRequired,
    icon: PropTypes.element
  };

  static defaultProps = {
    label: 'ra.action.delete',
    icon: <ActionDelete />
  };

  state = { isOpen: false };

  handleClick = (e) => {
    this.setState({ isOpen: true });
    e.stopPropagation();
  };

  handleDialogClose = () => this.setState({ isOpen: false });

  handleDelete = () => {
    const {
      dataProvider,
      resource,
      relatedResource,
      record,
      cfCode,
      cfIndex,
      selectedIds,
      setListSelectedIds,
      fieldsDefinition,
      refreshView,
      showNotification,
      translate,
      data,
      handleDeleteCustom
    } = this.props;
    const identifier = resource || relatedResource;

    const handler = isNil(cfCode)
      ? dataProvider(DELETE_MANY, identifier, {
          ids: selectedIds
        })
      : dataProvider(UPDATE, identifier, {
          data: {
            id: record.id,
            ...record,
            __cf: {
              [cfCode]: {
                [cfIndex]: data.filter(
                  (cf, index) => !selectedIds.find((id) => Number(id) === index)
                )
              }
            },
            __cfDefinition: fieldsDefinition,
            __isDelete: true
          }
        });

    return isFunction(handleDeleteCustom)
      ? handleDeleteCustom(selectedIds, data, [this.handleDialogClose])
      : handler
          .then(this.handleDialogClose)
          .then(() => setListSelectedIds(identifier, []))
          .then(() => {
            refreshView();
            showNotification(translate('input.action.success'));
          })

          .catch((error) => {
            this.handleDialogClose();
            showNotification(error.message);
          })
          .finally(() => refreshView());
  };

  render = () => {
    const {
      classes,
      label,
      icon,
      resource,
      relatedResource,
      selectedIds,
      translate,
      ...rest
    } = this.props;
    const { isOpen } = this.state;

    return (
      <>
        <Button
          onClick={this.handleClick}
          label={label}
          className={classes.deleteButton}
          {...sanitizeRestProps(rest)}
        >
          {icon}
        </Button>

        <Confirm
          isOpen={isOpen}
          title="message.bulk_delete_title"
          content="message.bulk_delete_content"
          translateOptions={{
            smart_count: selectedIds.length,
            name: inflection.humanize(
              translate('item', {
                smart_count: selectedIds.length,
                _: inflection.inflect('item', selectedIds.length)
              }),
              true
            )
          }}
          onConfirm={this.handleDelete}
          onClose={this.handleDialogClose}
        />
      </>
    );
  };
}

const EnhancedBulkDeleteButton = compose(
  connect(
    undefined,
    {
      refreshView: refreshViewAction,
      showNotification: showNotificationAction,
      setListSelectedIds: setListSelectedIdsAction
    }
  ),
  withDataProvider,
  withTranslate,
  withStyles(styles)
)(BulkDeleteButton);

export default EnhancedBulkDeleteButton;
