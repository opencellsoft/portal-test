import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button as RAButton } from 'react-admin';
import UploadIcon from '@material-ui/icons/CloudUpload';
import { compose } from 'recompose';
import { get } from 'lodash';

import withTranslate from '../../Hoc/withTranslate';
import WithFileUpload from './WithFileUpload';

const ImportVersionButton = ({
  label = 'input.file.import_version',
  onClick,
  translate
}) => (
  <RAButton
    to={undefined}
    label={translate(label, { cf: '' })}
    onClick={onClick}
    component="button"
  >
    <UploadIcon />
  </RAButton>
);

ImportVersionButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired,
  label: PropTypes.string
};

export default compose(
  connect(({ admin: { resources } }, { recordId, resource }) => ({
    record: get(resources, `${resource}.data[${recordId}]`)
  })),
  WithFileUpload,
  withTranslate
)(ImportVersionButton);
