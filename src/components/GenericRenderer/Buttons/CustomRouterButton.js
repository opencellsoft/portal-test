import React from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

const CustomRouterButton = ({ history, to, children, ...rest }) => (
  <button
    type="button"
    onClick={() => {
      history.push(to);
    }}
    {...rest}
  >
    {children}
  </button>
);

CustomRouterButton.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
  to: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

export default withRouter(CustomRouterButton);
