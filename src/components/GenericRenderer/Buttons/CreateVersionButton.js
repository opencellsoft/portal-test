import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Button as RAButton,
  withDataProvider,
  refreshView as refreshViewAction,
  showNotification as showNotificationAction,
  REDUX_FORM_NAME,
  required
} from 'react-admin';
import { reduxForm, getFormValues, destroy } from 'redux-form';
import { compose } from 'recompose';
import { capitalize, get, isEmpty, sortBy, cloneDeep, noop } from 'lodash';
import { Button, Typography } from '@material-ui/core';
import CreateIcon from '@material-ui/icons/Add';
import { withStyles } from '@material-ui/core/styles';

import Dialog from '../../Globals/Dialogs/Dialog';
import DateInput from '../Form/Inputs/DateInput';

import { RESOURCE_ENTITY_CUSTOMIZATION } from '../../../config/resources';

import {
  setPreviousVersion,
  setFieldsValueToNull
} from '../../../utils/record';
import { createMatrix } from '../../../utils/custom-fields';

import withTranslate from '../../Hoc/withTranslate';

const styles = ({ spacing, breakpoints, palette, typography }) => ({
  importButton: {
    marginLeft: spacing.unit
  },
  row: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    marginBottom: spacing.unit * 3
  },
  centered: {
    display: 'flex',
    justifyContent: 'center'
  },
  error: {
    color: palette.error.main,
    marginTop: 5,
    fontWeight: typography.fontWeightMedium
  }
});

class CreateVersionButton extends PureComponent {
  static propTypes = {
    resource: PropTypes.string.isRequired,
    record: PropTypes.object.isRequired,
    formValues: PropTypes.object.isRequired,
    cfCode: PropTypes.string.isRequired,
    cfDefinition: PropTypes.object.isRequired,
    dataProvider: PropTypes.func.isRequired,
    refreshView: PropTypes.func.isRequired,
    showNotification: PropTypes.func.isRequired,
    invalid: PropTypes.bool,
    translate: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
    label: PropTypes.string,
    form: PropTypes.string,
    dispatch: PropTypes.func.isRequired
  };

  state = {
    dialog: false,
    error: null
  };

  resetForm = () =>
    this.props.dispatch(destroy(this.props.form || REDUX_FORM_NAME));

  getCustomFields = () => {
    const { record, cfCode, cfDefinition, formValues } = this.props;
    const { valuePeriodStartDate, valuePeriodEndDate } = formValues;

    const { customFields = {} } = record || {};
    const { customField = [] } = customFields;

    const { field = [] } = cfDefinition || {};
    const relatedCfDefinition = field.find(({ code }) => code === cfCode);

    const { matrixColumn = {} } = relatedCfDefinition;
    const data = Object.values(matrixColumn).reduce((res, { code }) => {
      res[code] = '';

      return res;
    }, {});

    let formattedData = createMatrix(
      customField,
      relatedCfDefinition,
      [data],
      valuePeriodStartDate,
      valuePeriodEndDate
    );

    formattedData = {
      ...formattedData,
      mapValue: {
        key: get(formattedData, 'mapValue.key')
      }
    };

    const sortedCustomField = sortBy(
      [...customField, ...[formattedData]],
      ['code', 'valuePeriodPriority']
    );

    return sortedCustomField;
  };

  formatRecord = (transformer = noop) => {
    const { record, cfCode, cfDefinition } = this.props;

    const { customFields = {} } = record || {};
    const { inheritedCustomField = [] } = customFields;

    const { field = [] } = cfDefinition || {};
    const relatedCfDefinition = field.find(({ code }) => code === cfCode);

    if (isEmpty(relatedCfDefinition)) {
      return record;
    }

    const sortedCustomFields = this.getCustomFields();

    const withUpdatedPrevious =
      transformer(sortedCustomFields, cfCode) || sortedCustomFields;

    return {
      ...record,
      customFields: {
        customField: [...withUpdatedPrevious],
        inheritedCustomField
      }
    };
  };

  handleSubmit = () => {
    const {
      dataProvider,
      resource,
      cfDefinition,
      refreshView,
      showNotification
    } = this.props;

    if (isEmpty(cfDefinition)) {
      return false;
    }

    const formatedRecord = cloneDeep(this.formatRecord(setPreviousVersion));

    const nullableRecord = this.formatRecord(setFieldsValueToNull);

    return (
      this.toggleDialog(),
      dataProvider('UPDATE', resource, {
        data: nullableRecord
      }).then(() =>
        dataProvider('UPDATE', resource, {
          data: formatedRecord
        })
          .then(refreshView)
          .catch((error) => showNotification(capitalize(error.message)))

          .catch((error) => showNotification(capitalize(error.message)))
      )
    );
  };

  toggleDialog = () =>
    this.setState({ dialog: !this.state.dialog }, this.resetForm);

  render = () => {
    const { label, translate, classes, invalid, cfCode } = this.props;
    const { dialog, error } = this.state;

    const cfKey =
      cfCode
        .trim()
        .split('_')
        .map((word) => capitalize(word))
        .join(' ') || '';

    return (
      <>
        <RAButton
          to={undefined}
          label={translate(label, { cf: '' })}
          onClick={this.toggleDialog}
          component="button"
        >
          <CreateIcon />
        </RAButton>

        {dialog && (
          <Dialog
            open
            onClose={this.toggleDialog}
            actions={
              <>
                <Button onClick={this.toggleDialog} color="primary">
                  {translate('ra.action.cancel')}
                </Button>

                <Button
                  color="primary"
                  variant="contained"
                  className={classes.importButton}
                  onClick={this.handleSubmit}
                  disabled={invalid}
                >
                  {translate(label || 'input.file.import_version')}
                </Button>
              </>
            }
          >
            <div className={classes.row}>
              <Typography variant="headline" gutterBottom>
                {translate(
                  label ||
                    (cfCode
                      ? 'input.file.import_version'
                      : 'input.file.import'),
                  {
                    cf: cfKey
                  }
                ).replace(/(\b\S.+\b)(?=.*\1)/g, '')}
              </Typography>
            </div>

            <div className={classes.row}>
              <DateInput
                source="valuePeriodStartDate"
                label="Starting date"
                validate={[required()]}
              />
            </div>
            <div className={classes.row}>
              <DateInput source="valuePeriodEndDate" label="End date" />
            </div>

            {error && (
              <div className={classes.row}>
                <div className={classes.centered}>
                  <Typography variant="caption" className={classes.error}>
                    {error}
                  </Typography>
                </div>
              </div>
            )}
          </Dialog>
        )}
      </>
    );
  };
}

export default compose(
  connect(
    (state, { recordId, resource, form }) => {
      const {
        admin: { resources },
        config: { config }
      } = state;
      const entity = get(config.modules, `${resource}.entity`);

      return {
        record: get(resources, `${resource}.data[${recordId}]`),
        form: form || REDUX_FORM_NAME,
        formValues: getFormValues(form || REDUX_FORM_NAME)(state),
        cfDefinition:
          resources[RESOURCE_ENTITY_CUSTOMIZATION].data[entity] || {}
      };
    },
    { refreshView: refreshViewAction, showNotification: showNotificationAction }
  ),
  withTranslate, // Must be before reduxForm so that it can be used in validation
  reduxForm({
    destroyOnUnmount: false,
    enableReinitialize: true,
    keepDirtyOnReinitialize: true
  }),
  withDataProvider,
  withStyles(styles)
)(CreateVersionButton);
