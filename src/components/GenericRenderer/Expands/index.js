import React, { Fragment, memo } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import {
  SIMPLE_TABLE,
  FIELD_TEXT,
  DETAILED_LIST_WIDGET
} from '../../../constants/generic';

import OneReferenceExpand from './OneReferenceExpand';
import SimpleTable from '../Table/SimpleTable';
import TextFieldExpand from './TextFieldExpand';
import DetailedListWidget from '../Widgets/DetailedListWidget';

const styles = ({ spacing }) => ({
  divider: {
    marginBottom: spacing.unit
  }
});

const determineExpandComponent = (type) => {
  switch (type) {
    case SIMPLE_TABLE:
      return SimpleTable;
    case FIELD_TEXT:
      return TextFieldExpand;
    case DETAILED_LIST_WIDGET:
      return DetailedListWidget;
    default:
      return null;
  }
};

const Expands = memo(
  ({ config, resource, record, parentRecord, classes, ...rest }) => {
    const currentConfig = !Array.isArray(config) ? [config] : config;

    return currentConfig.reduce((res, elementConfig, index) => {
      const {
        type = 'text',
        reference,
        source,
        resource: configResource,
        onRowClick = false,
        ...remainingConfig
      } = elementConfig;
      const ExpandComponent = determineExpandComponent(type);
      const currentResource = configResource || resource;

      const Wrapper = reference ? OneReferenceExpand : Fragment;

      if (ExpandComponent) {
        res.push(
          <>
            {res.length > 0 && <div className={classes.divider} />}

            <Wrapper
              reference={reference}
              source={source}
              record={record}
              parentRecord={parentRecord}
            >
              <ExpandComponent
                {...rest}
                inExpand
                key={index}
                source={source}
                record={reference ? 'undefined' : record}
                pageResource={currentResource}
                parentRecord={record}
                onRowClick={onRowClick}
                {...remainingConfig}
              />
            </Wrapper>
          </>
        );
      }

      return res;
    }, []);
  }
);

Expands.propTypes = {
  config: PropTypes.object.isRequired,
  resource: PropTypes.string,
  record: PropTypes.object,
  parentRecord: PropTypes.object,
  classes: PropTypes.object
};

export default withStyles(styles)(Expands);
