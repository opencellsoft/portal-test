import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { compose, pure } from 'recompose';

import withTranslate from '../../Hoc/withTranslate';
import TextField from '../Fields/TextField';

const styles = ({ spacing, shadows, typography }) => ({
  wrapper: {
    padding: spacing.unit * 2,
    boxShadow: shadows[1],
    borderRadius: 5,
    background: 'white'
  },
  label: {
    fontWeight: typography.fontWeightMedium,
    marginBottom: spacing.unit
  },
  content: {
    whiteSpace: 'normal'
  }
});

const TextFieldExpand = memo((props) => {
  const { source, resource, pageResource, label, classes, translate } = props;
  const currentResource = resource || pageResource;

  return (
    <div className={classes.wrapper}>
      <Typography
        component="div"
        variant="caption"
        className={classes.label}
        noWrap
      >
        {label ||
          (currentResource && source
            ? translate(`resources.${currentResource}.fields.${source}`)
            : source || null)}
      </Typography>

      <TextField {...props} className={classes.content} />
    </div>
  );
});

TextFieldExpand.propTypes = {
  basePath: PropTypes.string,
  className: PropTypes.string,
  record: PropTypes.object,
  source: PropTypes.string.isRequired,
  noEmptyLabel: PropTypes.string,
  error: PropTypes.string,
  classes: PropTypes.object
};

const PureTextField = pure(TextFieldExpand);

export default compose(
  withStyles(styles),
  withTranslate
)(PureTextField);
