import React, { cloneElement } from 'react';
import PropTypes from 'prop-types';
import { LinearProgress } from 'react-admin';
import { withStyles } from '@material-ui/core/styles';
import { get } from 'lodash';

const styles = () => ({
  progress: {
    width: 42
  },
  link: {
    '&:hover': {
      textDecoration: 'underline'
    }
  }
});

export const OneReferenceExpandView = ({
  allowEmpty,
  basePath,
  children,
  className,
  classes = {},
  record,
  reference,
  referenceRecord,
  referenceField,
  resource,
  resourceLinkPath,
  source,
  translateChoice = false,
  fetchedOnce,
  isLoading,
  ...rest
}) => {
  const value = get(referenceRecord, referenceField) || get(record, source);
  const isWildcard = value === '*';

  if (!isWildcard && !fetchedOnce && isLoading) {
    return <LinearProgress className={classes.progress} />;
  }

  return children ? cloneElement(children, { record: referenceRecord }) : null;
};

OneReferenceExpandView.propTypes = {
  allowEmpty: PropTypes.bool,
  basePath: PropTypes.string,
  children: PropTypes.element,
  className: PropTypes.string,
  classes: PropTypes.object,
  record: PropTypes.object,
  reference: PropTypes.string,
  referenceRecord: PropTypes.object,
  resource: PropTypes.string,
  resourceLinkPath: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  source: PropTypes.string,
  fetchedOnce: PropTypes.bool,
  isLoading: PropTypes.bool,
  translateChoice: PropTypes.bool
};

export default withStyles(styles)(OneReferenceExpandView);
