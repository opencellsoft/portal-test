import React, { memo } from 'react';
import PropTypes from 'prop-types';
import pure from 'recompose/pure';

import withTranslate from '../../../Hoc/withTranslate';
import OneReferenceExpandController from './OneReferenceExpandController';
import OneReferenceExpandView from './OneReferenceExpandView';

const OneReferenceExpand = memo(
  ({
    record,
    parentRecord,
    source,
    reference,
    referenceField,
    children,
    ...props
  }) => (
    <OneReferenceExpandController
      record={record}
      parentRecord={parentRecord}
      source={source}
      reference={reference}
      basePath={`/${reference}`}
    >
      {(controllerProps) => (
        <OneReferenceExpandView
          {...controllerProps}
          referenceField={referenceField || 'description'}
        >
          {children}
        </OneReferenceExpandView>
      )}
    </OneReferenceExpandController>
  )
);

OneReferenceExpand.propTypes = {
  record: PropTypes.object,
  source: PropTypes.string.isRequired
};

OneReferenceExpand.displayName = 'OneReferenceExpand';
const PureResourceListField = pure(OneReferenceExpand);
export default withTranslate(PureResourceListField);
