import { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { has } from 'lodash';
import { crudGetManyAccumulate as crudGetManyAccumulateAction } from 'react-admin';

import { processParams } from '../../../../utils/generic-render';

export class UnconnectedOneReferenceExpandController extends Component {
  static defaultProps = {
    referenceRecord: null,
    record: {}
  };

  static propTypes = {
    record: PropTypes.object,
    parentRecord: PropTypes.object,
    referenceRecord: PropTypes.any,
    crudGetManyAccumulate: PropTypes.func.isRequired,
    children: PropTypes.any,
    referenceField: PropTypes.string,
    source: PropTypes.string,
    isLoading: PropTypes.bool
  };

  state = {
    fetchedOnce: false,
    source: processParams({
      params: this.props.source,
      source: {
        record: this.props.record,
        parentRecord: this.props.parentRecord
      }
    })
  };

  componentDidMount = () => this.fetchReference(this.props);

  componentWillReceiveProps = (nextProps) => {
    const { record } = this.props;
    const { record: nextRecord } = nextProps;

    if (record.id !== nextRecord.id && nextRecord.id) {
      this.fetchReference(nextProps);
    }
  };

  hasSourceProperty = () => {
    const { source } = this.props;
    const sourceArray = !Array.isArray(source) ? [source] : source;

    return sourceArray.reduce((res, key) => {
      const propertyMatch = key.match(/@(.*?)\.(.*)/);
      const containerKey = propertyMatch ? propertyMatch[1] : 'record';

      const { [containerKey]: container } = this.props;
      const property = propertyMatch ? propertyMatch[2] : key;

      return res && has(container, property);
    }, true);
  };

  fetchReference = (props) => {
    const { crudGetManyAccumulate } = this.props;
    const { source } = this.state;

    const hasSourceProperty = this.hasSourceProperty(props);

    if (
      hasSourceProperty &&
      source !== null &&
      source !== '*' &&
      typeof source !== 'undefined'
    ) {
      crudGetManyAccumulate(props.reference, [source]);

      setTimeout(() => this.setFetched());
    }
  };

  setFetched = () => this.setState({ fetchedOnce: true });

  render = () => {
    const {
      children,
      record,
      referenceRecord,
      referenceField,
      isLoading
    } = this.props;
    const { fetchedOnce } = this.state;

    return children({
      record,
      fetchedOnce,
      referenceRecord,
      referenceField,
      isLoading
    });
  };
}

const mapStateToProps = (
  { admin, loading },
  { reference, record, source }
) => ({
  admin,
  referenceRecord:
    admin.resources[reference] && admin.resources[reference].data[record.code],
  isLoading: loading > 0
});

const OneReferenceExpandController = connect(
  mapStateToProps,
  {
    crudGetManyAccumulate: crudGetManyAccumulateAction
  }
)(UnconnectedOneReferenceExpandController);

export default OneReferenceExpandController;
