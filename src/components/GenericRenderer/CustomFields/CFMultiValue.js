import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { REDUX_FORM_NAME } from 'react-admin';
import { destroy } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { isEmpty } from 'lodash';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';

import {
  extractMatrixDataByDefinition,
  extractFieldsMetas
} from '../../../utils/custom-fields';

import SimpleTable from '../Table/SimpleTable';
import Drawer from './Drawer';

const styles = () => ({
  wrapper: {}
});

class CFMultiValue extends PureComponent {
  static propTypes = {
    record: PropTypes.object.isRequired,
    field: PropTypes.object.isRequired,
    cfCode: PropTypes.string.isRequired,
    localSort: PropTypes.bool,
    verticalTable: PropTypes.bool,
    cfIndex: PropTypes.number.isRequired,
    fieldsDefinition: PropTypes.object.isRequired,
    resource: PropTypes.string.isRequired,
    config: PropTypes.object.isRequired,
    page: PropTypes.string.isRequired,
    history: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    handleSave: PropTypes.func,
    rowStyleHandler: PropTypes.func,
    classes: PropTypes.object.isRequired,
    form: PropTypes.string
  };

  extractCF = () => {
    const { field = {}, fieldsDefinition } = this.props;

    if (isEmpty(field)) {
      return {};
    }

    return extractMatrixDataByDefinition(field, fieldsDefinition);
  };

  handleClosingDrawer = () => {
    const { history } = this.props;

    return history.push(this.getBaseRoute());
  };

  handleRowClick = (cfId) => {
    const { history, dispatch, form } = this.props;

    dispatch(destroy(form || REDUX_FORM_NAME));

    return history.push(`${this.getCfRoute()}/${cfId}`);
  };

  handleCreate = () => {
    const { history, dispatch, form } = this.props;

    dispatch(destroy(form || REDUX_FORM_NAME));

    return history.push(`${this.getCfRoute()}/create`);
  };

  getBaseRoute = () => {
    const { resource, record = {}, page } = this.props;

    return `/${resource}/${record.id}${page === 'show' ? `/show` : ``}`;
  };

  getCfRoute = () => {
    const { cfCode, cfIndex } = this.props;

    return `${this.getBaseRoute()}/cf/${cfCode.toLowerCase()}/${cfIndex}`;
  };

  render = () => {
    const {
      resource,
      record,
      field,
      cfCode,
      localSort,
      cfIndex,
      fieldsDefinition,
      handleSave,
      config,
      rowStyleHandler,
      classes,
      verticalTable
    } = this.props;
    const { data = [], columns } = this.extractCF();
    const {
      withCreate,
      withExport,
      withImport,
      withDelete,
      createOrUpdate,
      errorMessage,
      withMappedInsert,
      isEditable,
      actions = [],
      customActions = [],
      bulkCustomActions = [],
      bulkActions,
      roles
    } = config || {};
    const fieldsWithMetas = extractFieldsMetas(columns, config);

    return (
      <div className={classes.wrapper}>
        <SimpleTable
          fields={fieldsWithMetas}
          data={data}
          roles={roles}
          verticalTable={verticalTable}
          total={data.length}
          record={record}
          relatedResource={resource}
          cfCode={cfCode}
          localSort={localSort}
          cfIndex={cfIndex}
          fieldsDefinition={fieldsDefinition}
          basePath={this.getCfRoute()}
          onRowClick={isEditable ? (id) => this.handleRowClick(id) : false}
          rowStyleHandler={rowStyleHandler}
          handleCreate={this.handleCreate}
          withCreate={withCreate}
          withImport={withImport}
          withDelete={withDelete}
          withMappedInsert={withMappedInsert}
          exporter={!!withExport}
          withPagination
          bulkActions={typeof bulkActions !== 'undefined' ? bulkActions : true}
          actions={actions}
          customActions={customActions}
          bulkCustomActions={bulkCustomActions}
          isEdit
        />

        <Drawer
          field={field}
          resource={resource}
          record={record}
          data={data}
          cfCode={cfCode}
          createOrUpdate={createOrUpdate}
          errorMessage={errorMessage}
          cfIndex={cfIndex}
          columns={fieldsWithMetas}
          fieldsDefinition={fieldsDefinition}
          baseRoute={this.getBaseRoute()}
          onClose={this.handleClosingDrawer}
          handleSave={handleSave}
        />
      </div>
    );
  };
}

export default withRouter(
  compose(
    connect(),
    withStyles(styles)
  )(CFMultiValue)
);
