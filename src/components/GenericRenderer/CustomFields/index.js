import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Row from '../../Globals/Row';

import RenderField from './RenderField';

const styles = ({ spacing }) => ({
  wrapper: {}
});

const CustomFields = ({
  record,
  resource,
  category,
  rows = [],
  fields = [],
  classes
}) => (
  <div className={classes.wrapper}>
    {rows.map((row) => (
      <Row>
        {row.map((row, key) => (
          <RenderField
            key={key}
            config={row}
            record={record}
            resource={resource}
            fields={fields}
          />
        ))}
      </Row>
    ))}
  </div>
);

CustomFields.propTypes = {
  resource: PropTypes.string.isRequired,
  record: PropTypes.object.isRequired,
  category: PropTypes.string.isRequired,
  rows: PropTypes.array,
  fields: PropTypes.array,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CustomFields);
