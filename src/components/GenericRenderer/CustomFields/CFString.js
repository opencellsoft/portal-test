import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { withStyles } from '@material-ui/core/styles';

const styles = ({ typography, spacing }) => ({
  wrapper: {
    display: 'flex',
    margin: spacing.unit,
    flexDirection: 'column',
    justifyContent: 'center'
  },
  field: {
    fontSize: typography.body1.fontSize
  },
  value: {
    fontWeight: typography.fontWeightMedium
  }
});

const CFString = ({ field, classes }) => {
  const { description, stringValue, defaultValue } = field || {};
  const value = stringValue || defaultValue;

  return (
    <div className={classes.wrapper}>
      {description && <div className={classes.field}>{description}</div>}

      {value && <div className={cx(classes.field, classes.value)}>{value}</div>}
    </div>
  );
};

CFString.propTypes = {
  field: PropTypes.object,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CFString);
