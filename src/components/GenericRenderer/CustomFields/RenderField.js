import React, { useContext, memo } from 'react';
import PropTypes from 'prop-types';

import {
  CUSTOM_FIELD_MULTI,
  CUSTOM_FIELD_STRING
} from '../../../constants/generic';

import { extractCF } from '../../../utils/custom-fields';

import PageContext from '../Layout/PageContext';

import CFMultiValue from './CFMultiValue';
import CFString from './CFString';

const RenderField = memo(
  ({
    record,
    resource,
    config,
    fields,
    mappedField,
    fieldsDefinition,
    cfIndex,
    handleSave,
    verticalTable,
    rowStyleHandler,
    ...props
  }) => {
    const { page } = useContext(PageContext);
    const { code, cfCode, localSort } = config || {};
    const fieldCfCode = code || cfCode;

    const Components = {
      [CUSTOM_FIELD_MULTI]: CFMultiValue,
      [CUSTOM_FIELD_STRING]: CFString
    };

    const data = fields
      ? !Array.isArray(fields)
        ? extractCF(record, fieldCfCode)
        : fields.find(({ code }) => code === fieldCfCode)
      : [];

    const { field: definition = [] } = fieldsDefinition || {};
    const { fieldType } =
      definition.find(({ code }) => code === fieldCfCode) || {};

    const Component = Components[fieldType];
    if (!Component) {
      return false;
    }

    return (
      <Component
        resource={resource}
        record={record}
        verticalTable={verticalTable}
        field={data}
        mappedField={mappedField}
        fieldsDefinition={fieldsDefinition}
        cfIndex={cfIndex}
        cfCode={cfCode}
        config={config}
        localSort={localSort}
        page={page}
        handleSave={handleSave}
        rowStyleHandler={rowStyleHandler}
      />
    );
  }
);

RenderField.propTypes = {
  record: PropTypes.object.isRequired,
  resource: PropTypes.string.isRequired,
  config: PropTypes.object.isRequired,
  fields: PropTypes.array,
  mappedField: PropTypes.array,
  fieldsDefinition: PropTypes.array,
  cfIndex: PropTypes.number,
  handleSave: PropTypes.func,
  rowStyleHandler: PropTypes.func
};

export default RenderField;
