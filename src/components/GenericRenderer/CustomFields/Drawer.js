import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route, withRouter, matchPath } from 'react-router-dom';
import {
  withDataProvider,
  crudUpdate as crudUpdateAction,
  refreshView as refreshViewAction,
  showNotification as showNotificationAction,
  REDUX_FORM_NAME
} from 'react-admin';
import { destroy } from 'redux-form';
import { Drawer } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';
import { get, isEmpty, isNil } from 'lodash';

import SimpleForm from '../Form/SimpleForm';
import withTranslate from '../../Hoc/withTranslate';

const styles = () => ({
  drawerRow: {
    gridTemplateColumns: 'repeat(auto-fit, minmax(220px, auto))'
  },
  ModalProps: {
    zIndex: 1
  }
});

class CustomDrawer extends PureComponent {
  static propTypes = {
    field: PropTypes.object.isRequired,
    fieldsDefinition: PropTypes.array.isRequired,
    data: PropTypes.object.isRequired,
    cfCode: PropTypes.string.isRequired,
    cfIndex: PropTypes.number.isRequired,
    resource: PropTypes.string.isRequired,
    config: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    baseRoute: PropTypes.string.isRequired,
    dataProvider: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
    handleSave: PropTypes.func
  };

  componentWillMount = () => this.resetForm();

  resetForm = () =>
    this.props.dispatch(destroy(this.props.form || REDUX_FORM_NAME));

  handleSave = (data) => {
    const {
      resource,
      record,
      dataProvider,
      history,
      refreshView,
      showNotification,
      translate,
      dispatch,
      baseRoute,
      handleSave,
      form
    } = this.props;

    const handler = handleSave
      ? new Promise((resolve, reject) => handleSave(data, resolve, reject))
      : dataProvider('UPDATE', resource, {
          data: {
            ...record,
            ...data
          }
        });

    return handler
      .then(refreshView)
      .then(() => showNotification(translate('input.action.success')))
      .then(() => dispatch(destroy(form || REDUX_FORM_NAME)))
      .then(() => history.replace(baseRoute))
      .catch((error) => showNotification(error.message));
  };

  render = () => {
    const {
      resource,
      data,
      cfCode,
      cfIndex,
      columns,
      fieldsDefinition,
      onClose,
      baseRoute,
      location,
      classes
    } = this.props;

    if (isEmpty(cfCode)) {
      return false;
    }

    return (
      <Route>
        {() => {
          const matchParams = matchPath(location.pathname, {
            path: `${baseRoute}/cf/${cfCode.toLowerCase()}/${cfIndex}/:cfSubIndex`
          });
          const cfSubIndex = get(matchParams, 'params.cfSubIndex');

          if (!matchParams || isNil(cfSubIndex)) {
            return false;
          }

          const record = {
            __cf: {
              [cfCode]: { [cfIndex]: { [cfSubIndex]: data[cfSubIndex] } }
            },
            __cfDefinition: fieldsDefinition
          };
          const columnsWithPrefix = {
            rows: [
              columns
                .filter(({ inForm = true }) => inForm)
                .map(({ source, ...rest }) => ({
                  ...rest,
                  source: `__cf.${cfCode}.${cfIndex}.${cfSubIndex}.${source}`
                }))
            ]
          };

          return (
            <Drawer
              open
              anchor="right"
              className={classes.ModalProps}
              onClose={onClose}
            >
              <SimpleForm
                {...this.props}
                record={record}
                basePath={`/${resource}`}
                redirect={false}
                isEdit={cfSubIndex !== 'create'}
                validate={() => {}}
                save={this.handleSave}
                config={columnsWithPrefix}
                original={data}
                rowClassname={classes.drawerRow}
                inDrawer
              />
            </Drawer>
          );
        }}
      </Route>
    );
  };
}

export default withRouter(
  compose(
    withTranslate,
    connect(
      ({ router }) => ({ location: router.location }),
      {
        crudUpdate: crudUpdateAction,
        refreshView: refreshViewAction,
        showNotification: showNotificationAction
      }
    ),
    withDataProvider,
    withStyles(styles)
  )(CustomDrawer)
);
