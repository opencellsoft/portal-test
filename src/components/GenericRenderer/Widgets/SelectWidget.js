import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  withDataProvider,
  refreshView as refreshViewAction,
  showNotification as showNotificationAction,
  REDUX_FORM_NAME,
  crudGetList as crudGetListAction,
  crudGetOne as crudGetOneAction
} from 'react-admin';
import { orange } from '@material-ui/core/colors';
import { get, isEmpty } from 'lodash';
import { compose } from 'recompose';
import { reduxForm, getFormValues, destroy, change } from 'redux-form';
import { processParams } from '../../../utils/generic-render';
import withTranslate from '../../Hoc/withTranslate';
import AutocompleteCard from '../../Globals/Widgets/AutocompleteCard';
import Row from '../../Globals/Row';
import NestedWidget from './index';

class SelectWidget extends PureComponent {
  static propTypes = {
    resource: PropTypes.string.isRequired,
    data: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
    selectedEntity: PropTypes.string,
    entityList: PropTypes.object,
    translate: PropTypes.func.isRequired
  };

  state = {
    selectedEntity: null
  };

  componentDidMount = () => {
    const {
      resource,
      filters,
      pagination,
      sort,
      crudGetList,
      record
    } = this.props;
    this.props.dispatch(destroy(this.props.form || REDUX_FORM_NAME));
    const filtersAction = processParams({
      params: filters,
      source: {
        record
      }
    });
    crudGetList(resource, pagination, sort, filtersAction);
  };

  handleOptionStyle = (base, option) => {
    const { record, highlightExistingData, mappedIdentifier } = this.props;
    const { selectedEntity } = this.state;

    if (!highlightExistingData || !selectedEntity) {
      return base;
    }

    const recordMappedData = get(record, highlightExistingData, []);
    if (isEmpty(recordMappedData)) {
      return base;
    }

    return recordMappedData.find(
      ({ [mappedIdentifier]: relevantColumn }) =>
        relevantColumn === option.value
    )
      ? { ...base, backgroundColor: orange.A100, color: 'rgba(0, 0, 0, 0.87)' }
      : base;
  };

  componentDidUpdate = () => {
    const {
      resourceData,
      formValues,
      record,
      crudGetOne,
      dependsRecord,
      dependsOn,
      formSource,
      dependsSource = ''
    } = this.props;

    const { data } = resourceData;
    if (isEmpty(formValues) && !!dependsSource && data !== null) {
      if (isEmpty(dependsRecord) && dependsOn) {
        const { resource, source } = dependsOn;
        const id = get(record, source);
        if (id) {
          crudGetOne(resource, id, '', false);
        }
      }
      const [firstSource, secondSource] = dependsSource.split('&');

      const defaultValue = !!dependsSource
        ? Object.values(data).filter(
            (el) =>
              get(el, firstSource) === record.key &&
              get(el, secondSource) === null
          )
        : {};

      return (
        !isEmpty(defaultValue) &&
        this.props.dispatch(
          change(REDUX_FORM_NAME, formSource, get(defaultValue[0], formSource))
        )
      );
    }

    return true;
  };

  getSelectedData = () => {
    const {
      resourceData,
      mappedIdentifier,
      identifierData,
      formValues,
      formSource
    } = this.props;

    const { data } = resourceData;
    const selectedData = !!mappedIdentifier ? identifierData : data;

    return (
      !isEmpty(formValues) &&
      !isEmpty(selectedData) &&
      Object.keys(selectedData).reduce((res, key) => {
        if (parseInt(key, 10) === formValues[formSource])
          return get(selectedData, key);

        return res;
      }, {})
    );
  };

  render = () => {
    const {
      resource,
      record,
      formValues,
      formSource,
      content,
      identifierData,
      fieldsDefinition,
      translate,
      fields,
      resourceData,
      customActions,
      withUpload,
      withTitle = true,
      uploadedFileFormat,
      mainData,
      ...rest
    } = this.props;
    const selectedData = this.getSelectedData();

    return (
      <>
        {(fields || []).map((field) => (
          <AutocompleteCard
            field={field}
            identifierData={identifierData}
            title={
              withTitle &&
              translate(`resources.${field.reference}.name`, {
                smart_count: 2
              })
            }
            record={isEmpty(record) ? mainData : record}
            customActions={customActions}
            withUpload={withUpload}
            uploadedFileFormat={uploadedFileFormat}
            resource={resource}
          />
        ))}

        {!isEmpty(content) &&
          !isEmpty(selectedData) &&
          content.map((row, index) => (
            <>
              {' '}
              <Row key={index}>
                <NestedWidget
                  {...rest}
                  resource={resource}
                  resourceData={resourceData}
                  record={this.props.mainData}
                  formSource={formSource}
                  customActions={customActions}
                  parentRecord={record}
                  selectedData={selectedData}
                  mappedFieldDefinition={fieldsDefinition}
                  component={row}
                />
              </Row>
            </>
          ))}
      </>
    );
  };
}
const mapStateToProps = (
  state,
  { form, saving, mappedIdentifier, resource, record, dependsOn }
) => {
  const formValues = getFormValues(form || REDUX_FORM_NAME)(state) || {};
  const { resource: resourceDepends, source } = dependsOn || {};
  const id = get(record, source);
  const profileState = state.admin.resources[resourceDepends];

  const mainData = get(state, `admin.resources.${resource}.data`);

  const identifierData = get(
    state,
    `admin.resources.${mappedIdentifier}.data`,
    mainData
  );

  return {
    form: form || REDUX_FORM_NAME,
    dependsRecord: profileState ? profileState.data[id] : null,
    formValues,
    mainData,
    resourceData: state.admin.resources[resource],
    identifierData,
    saving: saving || state.admin.saving
  };
};

export default compose(
  connect(
    mapStateToProps,
    {
      showNotification: showNotificationAction,
      refreshView: refreshViewAction,
      crudGetList: crudGetListAction,
      crudGetOne: crudGetOneAction
    }
  ),
  withTranslate, // Must be before reduxForm so that it can be used in validation
  withDataProvider,
  reduxForm({
    form: REDUX_FORM_NAME,
    destroyOnUnmount: false,
    enableReinitialize: true,
    keepDirtyOnReinitialize: true
  })
)(SelectWidget);
