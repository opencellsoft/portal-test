import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = ({ spacing, typography }) => ({
  root: {
    margin: `${spacing.unit * 3}px ${spacing.unit}px 0`,
    textTransform: 'uppercase',
    letterSpacing: '.15rem',
    flex: 1
  }
});

const WidgetTitle = ({ title, classes, className }) =>
  title ? (
    <Typography
      variant="subheading"
      noWrap
      classes={classes}
      className={cx(className)}
    >
      {title}
    </Typography>
  ) : null;

WidgetTitle.propTypes = {
  title: PropTypes.string,
  classes: PropTypes.object.isRequired,
  className: PropTypes.string
};

export default withStyles(styles)(WidgetTitle);
