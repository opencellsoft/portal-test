import React, { memo } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import compose from 'recompose/compose';
import { isEmpty } from 'lodash';
import { Paper } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import withTranslate from '../../Hoc/withTranslate';

import Row from '../../Globals/Row';
import DynamicIcon from '../../Globals/DynamicIcon';

import Field from '../Fields';
import WidgetTitle from './WidgetTitle';
import CustomActions from '../Table/CustomActions';

const styles = ({ spacing, palette, typography, breakpoints }) => ({
  wrapper: {
    flex: '1',
    display: 'flex',
    flexDirection: 'column'
  },
  paper: {
    flex: 1,
    minWidth: 320
  },
  subWrapper: {
    display: 'grid',
    gridAutoRows: 'minmax(52px, auto)',
    gridTemplateColumns: 'repeat(auto-fit, minmax(180px, auto))',
    gridGap: `0 ${spacing.unit}px`,
    padding: spacing.unit * 2
  },
  item: {
    display: 'flex',
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    overflow: 'hidden'
  },
  itemIcon: {
    display: 'flex',
    height: '100%',
    alignItems: 'flex-start',
    marginRight: spacing.unit * 2
  },
  icon: {
    fontSize: typography.display1.fontSize
  },
  itemContent: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flex: 1,
    height: '100%',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis'
  },
  standardField: {
    color: palette.grey[700],
    fontSize: typography.body1.fontSize,
    width: '100%'
  },
  emailField: {
    fontSize: typography.caption.fontSize
  },
  fieldLabel: {
    fontSize: typography.caption.fontSize,
    fontWeight: typography.fontWeightMedium,
    lineHeight: typography.caption.lineHeight,
    color: typography.title.color,
    display: 'inline-block',
    textTransform: 'uppercase'
  },
  customActionsContainer: {
    padding: '20px'
  }
});

const SimpleWidget = memo(
  ({
    resource,
    customActions,
    record,
    title,
    resourceData,
    formSource,
    classes,
    basePath,
    className,
    fields = [],
    translate,
    selectedData,
    id
  }) => (
    <div className={cx(classes.wrapper)} id={id}>
      <WidgetTitle title={title} />

      <Paper className={cx(classes.paper, className)}>
        <div className={classes.customActionsContainer}>
          <CustomActions
            record={record}
            formSource={formSource}
            resourceData={resourceData}
            selectedData={selectedData}
            actions={customActions}
          />
        </div>

        {!isEmpty(fields) && (
          <Row className={classes.subWrapper}>
            {fields.map(({ source, label, icon, color, ...config }) => (
              <div key={source} className={classes.item}>
                {icon && (
                  <div className={classes.itemIcon}>
                    <DynamicIcon
                      className={classes.icon}
                      icon={icon}
                      style={{ color }}
                      defaultColor
                    />
                  </div>
                )}

                <div className={classes.itemContent}>
                  {label !== false ? (
                    <span className={classes.fieldLabel}>
                      {label ||
                        translate(`resources.${resource}.fields.${source}`)}
                    </span>
                  ) : null}

                  <Field
                    key={source}
                    record={record}
                    source={source}
                    className={classes.standardField}
                    {...config}
                  />
                </div>
              </div>
            ))}
          </Row>
        )}
      </Paper>
    </div>
  )
);

SimpleWidget.propTypes = {
  resource: PropTypes.string.isRequired,
  record: PropTypes.object,
  classes: PropTypes.object.isRequired,
  basePath: PropTypes.string.isRequired,
  className: PropTypes.string,
  fields: PropTypes.array.isRequired,
  translate: PropTypes.func.isRequired
};

export default compose(
  withStyles(styles),
  withTranslate
)(SimpleWidget);
