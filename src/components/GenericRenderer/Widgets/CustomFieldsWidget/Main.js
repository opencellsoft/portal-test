import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Card, LinearProgress } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
// import { memoize } from 'lodash';
import { compose } from 'recompose';

// import { regroupTabs } from '../../../../utils/custom-fields';

import Tabs from '../../../Globals/Tabs';
import CustomFields from '../../CustomFields';

const styles = () => ({
  root: {
    display: 'flex'
  },
  card: {
    position: 'relative',
    flex: '1 1 auto'
  }
});

class Main extends PureComponent {
  static propTypes = {
    loading: PropTypes.bool,
    fieldsDefinition: PropTypes.object.isRequired,
    record: PropTypes.object.isRequired,
    resource: PropTypes.string.isRequired,
    classes: PropTypes.object.isRequired,
    config: PropTypes.object.isRequired,
    isEditable: PropTypes.bool
  };

  state = {
    selectedTab: 0
  };

  getFormattedTabs = (defaultTabs) => {
    const { config } = this.props;
    const { tabs } = config || {};

    return Array.isArray(tabs)
      ? tabs.map(({ title }, index) => ({
          id: index,
          label: title
        }))
      : Object.keys(defaultTabs).map((key) => ({
          id: key,
          label: key
        }));
  };

  handleTab = (e, selectedTab) => this.setState({ selectedTab });

  render = () => {
    const {
      loading,
      fieldsDefinition,
      record,
      resource,
      isEditable,
      config,
      classes
    } = this.props;
    const { selectedTab } = this.state;
    const { field } = fieldsDefinition || {};

    const { tabs } = config || {};
    const currentTab = tabs[selectedTab];
    const { rows = [] } = currentTab || {};
    const formattedTabs = this.getFormattedTabs();

    return (
      <div className={classes.root}>
        <Card className={classes.card}>
          {loading ? (
            <LinearProgress />
          ) : (
            <>
              <Tabs
                tabs={formattedTabs}
                selectedTab={selectedTab}
                onChange={this.handleTab}
              />

              <CustomFields
                resource={resource}
                record={record}
                category={selectedTab}
                rows={rows}
                fields={field}
                isEditable={isEditable}
              />
            </>
          )}
        </Card>
      </div>
    );
  };
}

export default compose(withStyles(styles))(Main);
