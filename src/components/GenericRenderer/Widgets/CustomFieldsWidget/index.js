import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import PageContext from '../../Layout/PageContext';

import Main from './Main';
import WithEntityCustomization from '../../../Hoc/WithEntityCustomization';

const CustomFieldsWidget = ({ resource, tabs, isEditable }) => {
  const { record = {} } = useContext(PageContext);

  return (
    <WithEntityCustomization resource={resource}>
      <Main
        record={record}
        resource={resource}
        config={{ tabs }}
        isEditable={isEditable}
      />
    </WithEntityCustomization>
  );
};

CustomFieldsWidget.propTypes = {
  resource: PropTypes.string.isRequired,
  tabs: PropTypes.array,
  isEditable: PropTypes.bool
};

export default connect(({ router }) => ({ location: router.location }))(
  CustomFieldsWidget
);
