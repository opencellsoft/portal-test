import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import compose from 'recompose/compose';
import {
  SaveButton,
  withDataProvider,
  refreshView as refreshViewAction,
  showNotification as showNotificationAction,
  REDUX_FORM_NAME
} from 'react-admin';
import { withRouter } from 'react-router-dom';
import {
  reduxForm,
  getFormValues,
  isValid,
  touch as touchAction
} from 'redux-form';
import { connect } from 'react-redux';
import { Paper, Toolbar } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { isEmpty } from 'lodash';
import CustomFieldsSection from '../../Form/CustomFieldsSection';
import WidgetTitle from '../WidgetTitle';
import DeleteButtonConfirm from '../../../Globals/Buttons/DeleteButtonConfirm';
import { flattenKeys } from '../../../../utils/general';
import { collectErrors } from '../../../../utils/form';

import withTranslate from '../../../Hoc/withTranslate';

const styles = ({ spacing }) => ({
  wrapper: {
    flex: '1',
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    margin: `0 ${spacing.unit}px 0 0`,
    overflow: 'hidden'
  },
  main: {
    flex: 1,
    margin: spacing.unit,
    width: 'inherit'
  },
  button: {
    margin: '0 4px'
  }
});

const Main = ({
  classes,
  translate,
  templateSource,
  fields,
  fieldsDefinition,
  extraConfiguration,
  refreshView,
  showNotification,
  dataProvider,
  formValues,
  parentFormValues,
  resource,
  record = {},
  title,
  isFormValid,
  isParentFormValid,
  includeProperties = [],
  basePath,
  id,
  redirectAfterSubmit,
  parentFormErrors = {},
  history,
  touch,
  formErrors = {},
  form,
  parentForm,
  parentTouch,
  withDelete,
  ...rest
}) => {
  const save = (data) => {
    const existingRecord = !isEmpty(record);
    const verb = existingRecord ? 'UPDATE' : 'CREATE';

    if (isFormValid && isParentFormValid) {
      dataProvider(verb, resource, {
        data: {
          ...parentFormValues,
          ...formValues,
          __cfDefinition: {
            ...fieldsDefinition,
            includeProperties
          }
        }
      })
        .then(() => {
          showNotification(
            translate(
              existingRecord
                ? 'ra.notification.updated'
                : 'ra.notification.created',
              { smart_count: 1 }
            )
          );
          refreshView();
        })
        .then(() => {
          if (redirectAfterSubmit) {
            return history.push(`/${resource}`);
          }

          return false;
        })
        .catch((error) => showNotification(error.message));
    } else {
      touch(...Object.keys(flattenKeys(formErrors)));
      parentTouch(...Object.keys(flattenKeys(parentFormErrors)));
      showNotification('ra.message.invalid_form');
    }
  };

  return (
    <div className={cx(classes.wrapper)} id={id}>
      <WidgetTitle title={title} />

      <Paper className={classes.main}>
        <CustomFieldsSection
          fields={fields}
          extraConfiguration={extraConfiguration}
          cfPath={templateSource}
          formID={id}
        />
        <Toolbar>
          <SaveButton
            id={`${id}--submit`}
            className={classes.button}
            onClick={save}
          />

          {withDelete && !isEmpty(record) && (
            <DeleteButtonConfirm
              id={`${id}--delete`}
              className={classes.button}
              record={record}
              undoable={false}
              redirect="list"
              resource={resource}
            />
          )}
        </Toolbar>
      </Paper>
    </div>
  );
};

Main.propTypes = {
  classes: PropTypes.object,
  resource: PropTypes.string,
  templateSource: PropTypes.string,
  fieldsDefinition: PropTypes.object,
  extraConfiguration: PropTypes.object,
  initialValues: PropTypes.object,
  translate: PropTypes.func.isRequired,
  fields: PropTypes.array.isRequired,
  refreshView: PropTypes.func,
  record: PropTypes.object,
  title: PropTypes.string,
  showNotification: PropTypes.func,
  dataProvider: PropTypes.func,
  formValues: PropTypes.object,
  parentFormValues: PropTypes.object,
  isFormValid: PropTypes.bool,
  isParentFormValid: PropTypes.bool,
  includeProperties: PropTypes.array,
  basePath: PropTypes.string,
  id: PropTypes.string,
  redirectAfterSubmit: PropTypes.string,
  history: PropTypes.object,
  touch: PropTypes.func,
  formErrors: PropTypes.object,
  parentFormErrors: PropTypes.object,
  form: PropTypes.string,
  parentForm: PropTypes.string,
  parentTouch: PropTypes.func,
  withDelete: PropTypes.bool
};

const mapStateToProps = (state, { source, initialValues, form }) => ({
  form: source,
  parentForm: form || REDUX_FORM_NAME,
  formValues: getFormValues(source)(state) || {},
  isFormValid: isValid(source)(state),
  isParentFormValid: isValid(form || REDUX_FORM_NAME)(state),
  formErrors: collectErrors(state, source),
  parentFormErrors: collectErrors(state, form || REDUX_FORM_NAME),
  parentFormValues: getFormValues(form || REDUX_FORM_NAME)(state) || {},
  initialValues
});

export default compose(
  withTranslate,
  connect(
    mapStateToProps,
    {
      refreshView: refreshViewAction,
      showNotification: showNotificationAction,
      touch: touchAction,
      parentTouch: touchAction.bind(null, REDUX_FORM_NAME)
    }
  ),
  reduxForm({
    destroyOnUnmount: true,
    enableReinitialize: true,
    keepDirtyOnReinitialize: false
  }),
  withRouter,
  withStyles(styles),
  withDataProvider
)(Main);
