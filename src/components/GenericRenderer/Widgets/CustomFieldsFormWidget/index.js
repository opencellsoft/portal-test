import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { get, isEmpty, includes } from 'lodash';
import {
  getCFsInitialValues,
  mergeCfsWithDefinitions
} from '../../../../utils/custom-fields';

import PageContext from '../../Layout/PageContext';
import Main from './Main';

const CustomFieldsFormWidget = ({
  fieldsDefinition = {},
  exclude = [],
  ...rest
}) => {
  const { record } = useContext(PageContext);
  const cfs = get(record, 'customFields.customField') || [];
  //if cfs is not empty merge it with cfs definitions (for edit)
  //otherwise use cfs definition (for create)
  if (isEmpty(fieldsDefinition)) return null;

  const fields = !isEmpty(cfs)
    ? mergeCfsWithDefinitions(cfs, fieldsDefinition)
    : get(fieldsDefinition, 'field') || [];

  if (isEmpty(fields)) return null;

  const isCreate = window.location.href.endsWith('/create');
  const filteredFields = fields.reduce((res, field) => {
    if ((isCreate && !!field.hideOnNew) || includes(exclude, field.code)) {
      return res;
    }

    res.push(field);

    return res;
  }, []);

  return (
    <Main
      fields={filteredFields}
      fieldsDefinition={fieldsDefinition}
      initialValues={getCFsInitialValues(filteredFields)}
      {...rest}
    />
  );
};

CustomFieldsFormWidget.propTypes = {
  fieldsDefinition: PropTypes.object,
  exclude: PropTypes.array
};

export default CustomFieldsFormWidget;
