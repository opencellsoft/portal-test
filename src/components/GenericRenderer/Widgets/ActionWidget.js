import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { isEmpty } from 'lodash';
import { EditButton } from 'react-admin';
import { Paper } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import {
  ACTION_EDIT,
  ACTION_DELETE,
  ACTION_API,
  ACTION_NEW
} from '../../../constants/generic';

import PageContext from '../Layout/PageContext';
import DeleteButtonConfirm from '../../Globals/Buttons/DeleteButtonConfirm';
import APIButton from '../Buttons/APIButton';
import CreateButton from '../Buttons/CreateButton';
import CustomActions from '../Table/CustomActions';

const styles = ({ spacing, palette }) => ({
  wrapper: {
    padding: spacing.unit * 2,
    textAlign: 'right',
    color: palette.text.secondary,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    alignSelf: 'flex-start',
    justifySelf: 'flex-end'
  }
});

const ActionsComponent = {
  [ACTION_EDIT]: EditButton,
  [ACTION_DELETE]: DeleteButtonConfirm,
  [ACTION_API]: APIButton,
  [ACTION_NEW]: CreateButton
};

const ActionWidget = ({
  resource,
  record: recordProps,
  classes,
  children,
  actions,
  className
}) => {
  const { record, basePath } = useContext(PageContext);

  const standardActions = actions.filter(
    (action) => typeof action === 'string'
  );
  const customActions = actions.filter((action) => typeof action !== 'string');

  return (
    <Paper className={cx(classes.wrapper, className)}>
      {standardActions.map((action) => {
        const isObject = typeof action === 'object';

        let rest = {};
        let key = action;
        if (isObject) {
          ({ action: key, ...rest } = action);
        }
        const data = action.forwardRecord && !isEmpty(record) ? record : null;

        const Component = ActionsComponent[key];
        if (!Component) {
          return false;
        }

        return (
          <Component
            key={action}
            data={data}
            basePath={basePath}
            resource={resource}
            record={record}
            {...rest}
          />
        );
      })}

      <CustomActions record={recordProps} actions={customActions} />
    </Paper>
  );
};

ActionWidget.propTypes = {
  resource: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node)
  ]),
  record: PropTypes.object.isRequired,
  actions: PropTypes.array.isRequired,
  className: PropTypes.string
};

export default withStyles(styles)(ActionWidget);
