import React, { PureComponent, createRef } from 'react';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themesAnimated from '@amcharts/amcharts4/themes/animated';
import am4themesThemes from '@amcharts/amcharts4/themes/material';
import { Paper, Button } from '@material-ui/core';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { isEmpty } from 'lodash';
import cx from 'classnames';
import moment from 'moment-timezone';
import compose from 'recompose/compose';
import withQueryProps from '../../../Hoc/withQueryProps';
import DynamicIcon from '../../../Globals/DynamicIcon';
import withTranslate from '../../../Hoc/withTranslate';

am4core.useTheme(am4themesAnimated);
am4core.useTheme(am4themesThemes);

const CURRENT_DATE = 'currentDate';
const MONTH_DATE = 'monthDate';
const WEEK_DATE = 'weekDate';

const styles = ({ palette, spacing, typography }) => ({
  paper: {
    width: 400,
    margin: 0,
    padding: spacing.unit
  },
  title: {
    fontSize: '1rem',
    textAlign: 'center',
    margin: 0,
    fontWeight: typography.fontWeightLight,
    textTransform: 'uppercase',
    letterSpacing: '.15rem',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  textAlign: {
    textAlign: 'center',
    margin: '8px 0'
  },
  noData: {
    fontSize: '.8rem',
    textAlign: 'center',
    fontWeight: typography.fontWeightLight,
    textTransform: 'uppercase',
    letterSpacing: '.15rem',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  button: {
    textTransform: 'none'
  },
  buttonActive: {
    color: palette.secondary.main
  },
  icon: {
    width: '100%',
    height: 250,
    fill: '#0f0f0f0f'
  },
  placeholder: {
    maxWidth: 400,
    height: 300
  },
  hiddenPlaceholder: {
    display: 'none'
  }
});

class AmChartPie extends PureComponent {
  static propTypes = {
    title: PropTypes.string,
    data: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
    categories: PropTypes.object.isRequired,
    translate: PropTypes.object.isRequired
  };

  state = { action: CURRENT_DATE };

  placeholder = createRef();

  currentDate = moment();

  weekDate = moment().subtract(7, 'days');

  monthDate = moment().subtract(30, 'days');

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }

  updateAction = (action) => this.setState({ action });

  drawPie = (data) => {
    setTimeout(() => {
      if (!this.placeholder.current) {
        return false;
      }

      const chart = am4core.create(
        this.placeholder.current,
        am4charts.PieChart
      );
      chart.data = data;
      chart.innerRadius = am4core.percent(30);

      const pieSeries = chart.series.push(new am4charts.PieSeries());

      pieSeries.fontSize = 11;
      pieSeries.dataFields.value = 'data';
      pieSeries.dataFields.category = 'status';

      pieSeries.slices.template.stroke = am4core.color('#fff');
      pieSeries.slices.template.strokeWidth = 2;
      pieSeries.slices.template.strokeOpacity = 1;
      pieSeries.slices.template.cursorOverStyle = [
        {
          property: 'cursor',
          value: 'pointer'
        }
      ];
      pieSeries.alignLabels = false;
      pieSeries.labels.template.bent = true;
      pieSeries.labels.template.radius = 3;
      pieSeries.labels.template.padding(0, 0, 0, 0);
      pieSeries.ticks.template.disabled = true;

      const shadow = pieSeries.slices.template.filters.push(
        new am4core.DropShadowFilter()
      );
      shadow.opacity = 0;
      const hoverState = pieSeries.slices.template.states.getKey('hover');

      const hoverShadow = hoverState.filters.push(
        new am4core.DropShadowFilter()
      );
      hoverShadow.opacity = 0.7;
      hoverShadow.blur = 5;

      // Add a legend
      chart.legend = new am4charts.Legend();
      chart.legend.fontSize = 9;

      return true;
    });
  };

  renderEmptyData = () => {
    const { classes } = this.props;

    return (
      <div>
        <DynamicIcon icon="PieChart" className={classes.icon} />
        <h3 className={cx(classes.noData, classes.textAlign)}>
          No results found
        </h3>
      </div>
    );
  };

  filterRelevantData = (data, action) => {
    const dataArr = data || [];

    switch (action) {
      case WEEK_DATE:
        return dataArr.filter((dat) =>
          moment(Object.values(dat)[0]).isBetween(
            this.weekDate,
            this.currentDate,
            null,
            '[]'
          )
        );
      case CURRENT_DATE:
        return dataArr.filter((dat) =>
          moment(Object.values(dat)[0]).isSame(this.currentDate, 'day')
        );
      case MONTH_DATE:
        return dataArr.filter((dat) =>
          moment(Object.values(dat)[0]).isBetween(
            this.monthDate,
            this.currentDate,
            null,
            '[]'
          )
        );
      default:
        return [];
    }
  };

  reducer = (data, action) => {
    const { categories } = this.props;

    const dataFiltered = this.filterRelevantData(data, action);
    const dataChart = [];

    categories.forEach((element) => {
      const sumData = dataFiltered.reduce(
        (res, el) =>
          Object.values(el)['1'] === element
            ? res + Object.values(el)['2']
            : res,
        0
      );

      if (sumData !== 0) {
        dataChart.push({ status: element, data: sumData });
      }
    });

    return dataChart;
  };

  render() {
    const { classes, data, translate, title } = this.props;
    const { action } = this.state;
    const filteredData = this.reducer(data, action);
    const hasData = !isEmpty(filteredData);

    return (
      <Paper className={cx(classes.paper)}>
        <h3 className={classes.title}>{title}</h3>

        <div className={classes.textAlign}>
          <Button
            className={cx(
              classes.button,
              action === CURRENT_DATE && classes.buttonActive
            )}
            onClick={() => this.updateAction(CURRENT_DATE)}
          >
            {translate(CURRENT_DATE)}
          </Button>

          <Button
            className={cx(
              classes.button,
              action === WEEK_DATE && classes.buttonActive
            )}
            onClick={() => this.updateAction(WEEK_DATE)}
          >
            {translate(WEEK_DATE)}
          </Button>

          <Button
            className={cx(
              classes.button,
              action === MONTH_DATE && classes.buttonActive
            )}
            onClick={() => this.updateAction(MONTH_DATE)}
          >
            {translate(MONTH_DATE)}
          </Button>
        </div>

        {!hasData && this.renderEmptyData()}

        <div
          ref={this.placeholder}
          className={hasData ? classes.placeholder : classes.hiddenPlaceholder}
        >
          {this.drawPie(filteredData)}
        </div>
      </Paper>
    );
  }
}

export default compose(
  withTranslate,
  withQueryProps(),
  withStyles(styles)
)(AmChartPie);
