import React, { PureComponent, createRef } from 'react';
import { connect } from 'react-redux';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themesAnimated from '@amcharts/amcharts4/themes/animated';
import am4themesThemes from '@amcharts/amcharts4/themes/material';
import PropTypes from 'prop-types';
import { isEmpty, get, debounce } from 'lodash';
import moment from 'moment-timezone';
import { compose } from 'recompose';
import { Paper } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import { red } from '@material-ui/core/colors';

import {
  STATUS_ACTIVE,
  STATUS_RENEWAL,
  STATUS_RESILIATED,
  STATUS_INSTANTIATED
} from '../../../../constants/status';

import withQueryProps from '../../../Hoc/withQueryProps';
import withTranslate from '../../../Hoc/withTranslate';

import WidgetTitle from '../WidgetTitle';

am4core.useTheme(am4themesAnimated);
am4core.useTheme(am4themesThemes);

const styles = ({ spacing, typography }) => ({
  paper: {
    width: '100%',
    margin: spacing.unit
  },
  placeholder: {
    width: '100%',
    height: '130px'
  }
});

class TimeLineWidget extends PureComponent {
  static propTypes = {
    data: PropTypes.array.isRequired,
    verb: PropTypes.string,
    source: PropTypes.string,
    resource: PropTypes.string,
    record: PropTypes.object,
    labelKey: PropTypes.string,
    valueKey: PropTypes.string,
    aggregateProps: PropTypes.array,
    sidebarOpen: PropTypes.bool,
    translate: PropTypes.func.isRequired,
    classes: PropTypes.string.isRequired
  };

  placeholder = createRef();

  drawTimeline = debounce(() => {
    if (!this.placeholder.current) {
      return false;
    }

    if (this.chart) {
      this.chart.dispose();
    }

    const chartData = this.handleData() || [];

    const chart = am4core.create(this.placeholder.current, am4charts.XYChart);

    // Create axes
    const xAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    xAxis.dataFields.category = 'x';
    xAxis.renderer.grid.template.disabled = true;
    xAxis.renderer.labels.template.disabled = true;
    xAxis.tooltip.disabled = true;
    xAxis.title.fontWeight = 'bold';

    const yAxis = chart.yAxes.push(new am4charts.ValueAxis());
    yAxis.min = 0;
    yAxis.max = 1.99;
    yAxis.renderer.grid.template.disabled = true;
    yAxis.renderer.labels.template.disabled = true;
    yAxis.renderer.baseGrid.disabled = true;
    yAxis.tooltip.disabled = true;

    // Create series
    const series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.categoryX = 'x';
    series.dataFields.valueY = 'y';
    series.strokeWidth = 4;
    series.sequencedInterpolation = true;
    series.propertyFields.stroke = 'color';
    series.propertyFields.strokeDasharray = 'lineDash';
    series.propertyFields.fill = 'color';

    const bullet = series.bullets.push(new am4charts.CircleBullet());
    bullet.setStateOnChildren = true;
    bullet.states.create('hover');
    bullet.circle.radius = 10;
    bullet.circle.states.create('hover').properties.radius = 15;
    bullet.propertyFields.fill = 'color';
    bullet.propertyFields.stroke = 'color';

    const labelBullet = series.bullets.push(new am4charts.LabelBullet());
    labelBullet.setStateOnChildren = true;
    labelBullet.states.create('hover').properties.scale = 1.15;
    labelBullet.label.text = '{text}';
    labelBullet.label.maxWidth = 150;
    labelBullet.label.wrap = true;
    labelBullet.label.truncate = false;
    labelBullet.label.textAlign = 'middle';
    labelBullet.label.paddingTop = 10;
    labelBullet.label.paddingBottom = 10;
    labelBullet.label.fontSize = '0.875rem';
    labelBullet.label.states.create('hover').properties.fill = am4core.color(
      '#7878787 '
    );
    labelBullet.label.propertyFields.verticalCenter = 'center';

    chart.cursor = new am4charts.XYCursor();
    chart.cursor.lineX.disabled = true;
    chart.cursor.lineY.disabled = true;
    chart.data = chartData;

    return true;
  });

  componentDidMount = () => {
    window.addEventListener('resize', this.drawTimeline);
  };

  componentWillUnmount = () => {
    window.removeEventListener('resize', this.drawTimeline);

    if (this.chart) {
      this.chart.dispose();
    }
  };

  componentDidUpdate = ({
    record: prevRecord,
    sidebarOpen: prevSidebarOpen
  }) => {
    const { record, sidebarOpen } = this.props;

    if (
      (!isEmpty(record) && record !== prevRecord) ||
      sidebarOpen !== prevSidebarOpen
    ) {
      this.drawTimeline();
    }
  };

  handleColor = (status) => {
    switch (status) {
      case STATUS_ACTIVE:
      case STATUS_RENEWAL:
        return '#00c853';

      case STATUS_INSTANTIATED:
        return '#ccc';

      case STATUS_RESILIATED:
        return red.A100;

      default:
        return '#ccc';
    }
  };

  getData = () => {
    const { data, verb, record } = this.props;

    return (!verb ? record : data) || {};
  };

  sortData = (chartData) => {
    chartData.sort((a, b) => {
      const firstValue = a.created;
      const secondValue = b.created;
      if (!moment(firstValue).isSame(secondValue)) {
        if (moment(firstValue).isAfter(secondValue)) {
          return 1;
        }
        if (moment(firstValue).isBefore(secondValue)) {
          return -1;
        }
      }

      return firstValue - secondValue;
    });

    return chartData;
  };

  handleData = () => {
    const {
      labelKey = 'label',
      valueKey = 'value',
      resource,
      source,
      aggregateProps = [],
      translate
    } = this.props;

    const data = this.getData();
    const values = [...get(data, source, '')] || [];

    aggregateProps.forEach((element) => {
      const { source, label } = element;
      const displayLabel =
        label || translate(`resources.${resource}.fields.${source}`);

      return (
        data[source] &&
        values.push({
          [valueKey]: data[source],
          [labelKey]: displayLabel.toUpperCase()
        })
      );
    });

    let chartData = (values || []).map((element, index) => {
      const { [valueKey]: elementValue } = element;
      let { [labelKey]: elementLabel } = element;
      const color = this.handleColor(elementLabel);
      const dateFormated =
        (elementValue && moment(elementValue).format('DD/MM/YYYY')) || '';
      const lineDash = moment(elementValue).isAfter() ? '4' : '0';

      //@todo
      //improve the aliasing of labels
      //here to display Terminated in interface for EIR
      //although record is actually processed as RESILIATED in core
      if (elementLabel === 'RESILIATED') {
        elementLabel = 'TERMINATED';
      }

      return {
        x: index,
        y: 1,
        text: `[bold]${dateFormated}[/]\n [${color}]${elementLabel} [/]`,
        center: 'bottom',
        color: `${color}`,
        lineDash: `${lineDash}`,
        created: moment(elementValue)
      };
    });

    chartData = this.sortData(chartData);
    const hasChartData = !isEmpty(chartData);
    const size = (hasChartData && chartData.length) || 0;
    const lastColor = hasChartData && chartData[size - 1].color;
    const lastlabel = hasChartData && chartData[size - 1].text;
    chartData.push({
      x: size,
      y: 1,
      text: `[bold]Today[/]\n[bold]${lastlabel.split('\n').pop()}[/]\n`,
      center: 'bottom',
      color: `${lastColor}`,
      lineDash: `4`,
      created: moment()
    });

    return chartData;
  };

  render = () => {
    const { classes } = this.props;

    return (
      <>
        <WidgetTitle title="Timeline" />

        <Paper className={classes.paper}>
          <div ref={this.placeholder} className={classes.placeholder} />
        </Paper>
      </>
    );
  };
}

export default compose(
  connect(({ admin: { ui: { sidebarOpen } } }) => ({ sidebarOpen })),

  withQueryProps(),
  withTranslate,
  withStyles(styles)
)(TimeLineWidget);
