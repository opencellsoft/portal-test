import React, { PureComponent, createRef } from 'react';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themesAnimated from '@amcharts/amcharts4/themes/animated';
import am4themesThemes from '@amcharts/amcharts4/themes/material';
import { Paper } from '@material-ui/core';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { isEmpty } from 'lodash';
import cx from 'classnames';
import compose from 'recompose/compose';
import withQueryProps from '../../../Hoc/withQueryProps';
import DynamicIcon from '../../../Globals/DynamicIcon';

am4core.useTheme(am4themesAnimated);
am4core.useTheme(am4themesThemes);

const styles = ({ palette, spacing, typography }) => ({
  paper: {
    width: 800,
    height: 359,
    margin: 0,
    padding: spacing.unit
  },
  title: {
    fontSize: '1rem',
    textAlign: 'center',
    margin: 10,
    fontWeight: typography.fontWeightLight,
    textTransform: 'uppercase',
    letterSpacing: '.15rem',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  textAlign: {
    textAlign: 'center',
    margin: '8px 0'
  },
  noData: {
    fontSize: '.8rem',
    textAlign: 'center',
    fontWeight: typography.fontWeightLight,
    textTransform: 'uppercase',
    letterSpacing: '.15rem',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  button: {
    textTransform: 'none'
  },
  buttonActive: {
    color: palette.secondary.main
  },
  icon: {
    width: '100%',
    height: 250,
    fill: '#0f0f0f0f'
  },
  placeholder: {
    maxWidth: 800,
    height: 400
  },
  hiddenPlaceholder: {
    display: 'none'
  }
});

class AmChartDuration extends PureComponent {
  static propTypes = {
    title: PropTypes.string,
    leftCol: PropTypes.string.isRequired,
    rightCol: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    data: PropTypes.array.isRequired,
    source: PropTypes.string.isRequired,
    classes: PropTypes.object.isRequired
  };

  placeholder = createRef();

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }

  getProperties = (data) => {
    const { source, date, leftCol, rightCol } = this.props;

    if (!data) return false;

    return data.map((el) => {
      const {
        [source]: elementSource,
        [date]: elementDate,
        [leftCol]: elementValueLeft,
        [rightCol]: elementValueRight
      } = el;

      return {
        source: elementSource,
        date: elementDate,
        leftCol: elementValueLeft,
        rightCol: elementValueRight
      };
    });
  };

  drawPie = (data) => {
    setTimeout(() => {
      if (!this.placeholder.current) {
        return false;
      }

      const chart = am4core.create(this.placeholder.current, am4charts.XYChart);
      chart.data = this.getProperties(data);
      chart.colors.step = 2;
      chart.maskBullets = false;
      chart.height = 340;

      // Create axes
      const dateAxis = chart.xAxes.push(new am4charts.DateAxis());
      dateAxis.renderer.grid.template.location = 0;
      dateAxis.renderer.minGridDistance = 50;
      dateAxis.renderer.grid.template.disabled = false;
      dateAxis.renderer.fullWidthTooltip = true;

      const distanceAxis = chart.yAxes.push(new am4charts.ValueAxis());
      distanceAxis.title.text = chart.data.leftCol;
      distanceAxis.renderer.grid.template.disabled = true;

      const durationAxis = chart.yAxes.push(new am4charts.DurationAxis());
      durationAxis.title.text = chart.data.rightCol;
      durationAxis.baseUnit = 'minute';
      durationAxis.renderer.grid.template.disabled = true;
      durationAxis.renderer.opposite = true;

      durationAxis.durationFormatter.durationFormat = "hh'h' mm'min'";

      // Create series
      const distanceSeries = chart.series.push(new am4charts.ColumnSeries());
      distanceSeries.dataFields.valueY = 'leftCol';
      distanceSeries.dataFields.dateX = 'date';
      distanceSeries.yAxis = distanceAxis;
      distanceSeries.name = 'Distance';
      distanceSeries.tooltipText = '{valueY} {name}';
      distanceSeries.columns.template.fillOpacity = 0.7;
      distanceSeries.columns.template.propertyFields.strokeDasharray =
        'dashLength';
      distanceSeries.columns.template.propertyFields.fillOpacity = 'alpha';

      const distanceSeries2 = chart.series.push(new am4charts.ColumnSeries());
      distanceSeries2.dataFields.valueY = 'rightCol';
      distanceSeries2.dataFields.dateX = 'date';
      distanceSeries2.yAxis = durationAxis;
      distanceSeries2.name = 'Duration';
      distanceSeries2.tooltipText = '{valueY} {name}';
      distanceSeries2.columns.template.fillOpacity = 0.7;
      distanceSeries2.columns.template.propertyFields.strokeDasharray =
        'dashLength';
      distanceSeries2.columns.template.propertyFields.fillOpacity = 'alpha';

      const disatnceState = distanceSeries.columns.template.states.create(
        'hover'
      );
      disatnceState.properties.fillOpacity = 0.9;

      // Add legend
      chart.legend = new am4charts.Legend();

      // Add cursor
      chart.cursor = new am4charts.XYCursor();
      chart.cursor.fullWidthLineX = true;
      chart.cursor.xAxis = dateAxis;
      chart.cursor.lineX.strokeOpacity = 0;
      chart.cursor.lineX.fill = am4core.color('#000');
      chart.cursor.lineX.fillOpacity = 0.1;

      return true;
    });
  };

  renderEmptyData = () => {
    const { classes } = this.props;

    return (
      <div>
        <DynamicIcon icon="PieChart" className={classes.icon} />
        <h3 className={cx(classes.noData, classes.textAlign)}>
          No results found
        </h3>
      </div>
    );
  };

  render() {
    const { classes, title, data } = this.props;
    const hasData = !isEmpty(data);

    return (
      <Paper className={cx(classes.paper)}>
        <h3 className={classes.title}>{title}</h3>

        {!hasData && this.renderEmptyData()}

        <div
          ref={this.placeholder}
          className={hasData ? classes.placeholder : classes.hiddenPlaceholder}
        >
          {this.drawPie(data)}
        </div>
      </Paper>
    );
  }
}

export default compose(
  withQueryProps(),
  withStyles(styles)
)(AmChartDuration);
