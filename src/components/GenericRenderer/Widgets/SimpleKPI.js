import React, { useState, useContext } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { crudGetList as crudGetListAction } from 'react-admin';
import { Paper, Typography, colors } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';
import { get } from 'lodash';

import { useList } from '../../../utils/hooks/crud';
import { extractCF } from '../../../utils/custom-fields';
import { hydrateUrlFromParams } from '../../../utils/generic-render';

import PageContext from '../Layout/PageContext';
import DynamicIcon from '../../Globals/DynamicIcon';

import withTranslate from '../../Hoc/withTranslate';

const styles = ({ palette, shadows, typography }) => ({
  main: {
    display: 'block',
    textDecoration: 'none',
    flex: 1,
    minWidth: 180
  },
  mainLink: {
    cursor: 'pointer',
    '&:hover': {
      boxShadow: shadows[1]
    }
  },
  card: {
    position: 'relative',
    borderRadius: '.2rem',
    textDecoration: 'none',
    boxShadow: 'none'
  },
  box: {
    padding: '32px 15px',
    fontSize: 40,
    lineHeight: '40px',
    textAlign: 'center',
    fontWeight: 300
  },
  bottomBox: {
    borderTop: '1px solid rgba(0,0,0,.15)'
  },
  counter: {
    fontSize: '2rem'
  },
  titleWrapper: {
    position: 'absolute',
    width: '100%',
    top: '50%',
    marginTop: -22,
    textAlign: 'center'
  },
  title: {
    height: 24,
    background: 'white',
    display: 'inline-block',
    padding: '4px 10px',
    textTransform: 'uppercase',
    lineHeight: '24px',
    border: '1px solid rgba(0,0,0,.15)',
    fontSize: typography.caption.fontSize,
    color: palette.grey[700],
    borderRadius: '1em'
  },
  titleHover: {
    textDecoration: 'underline'
  },
  icon: {
    width: 40,
    height: 40
  }
});

const SimpleKPI = ({
  title,
  resource,
  record,
  cfCode,
  crudGetList,
  filters = {},
  dependsOn = {},
  modules,
  resources,
  icon,
  color,
  onClick,
  classes,
  className,
  translate
}) => {
  const [hover, setHover] = useState(false);
  const toggleHover = () => setHover(!hover);

  const { record: contextRecord = {} } = useContext(PageContext);
  const themeColor = get(colors, color);
  const relatedResource = resources[resource];
  const relatedModule = modules[resource];
  const { icon: moduleIcon, label: moduleLabel, themeColor: moduleColor } =
    relatedModule || {};
  const actualRecord = contextRecord || record;

  if (!cfCode && relatedResource) {
    useList({
      resource,
      actualRecord,
      filters,
      crudGetList,
      watchProp: [actualRecord]
    });
  }

  let shouldRender = true;
  Object.keys(dependsOn).forEach((key) => {
    const recordValue = get(actualRecord, key, false);

    if (dependsOn[key] !== recordValue) {
      shouldRender = false;
    }
  });

  if (!shouldRender) {
    return false;
  }

  let list;
  let total = 0;
  if (!cfCode) {
    ({ list } = relatedResource || {});
    ({ total = 0 } = list || {});
  } else {
    const cfValue = actualRecord[cfCode] || extractCF(actualRecord, cfCode);
    const [cfData] = cfValue || [];
    const { mapValue } = cfData || {};
    const { key, ...values } = mapValue || {};

    total = Object.keys(values).length;
  }

  const linkTo = onClick
    ? hydrateUrlFromParams({ params: { record: actualRecord }, url: onClick })
    : `/${resource}/${actualRecord.code}/show`;

  return (
    <Link
      to={linkTo}
      className={cx(classes.main, className)}
      onMouseEnter={toggleHover}
      onMouseLeave={toggleHover}
    >
      <Paper className={classes.card} square>
        <div className={classes.box}>
          <Typography
            variant="title"
            component="span"
            className={classes.counter}
          >
            {total}
          </Typography>
        </div>

        <div className={classes.titleWrapper}>
          <span className={cx(classes.title, hover && classes.titleHover)}>
            {title || moduleLabel || translate(`resources.${resource}.name`)}
          </span>
        </div>

        <div className={cx(classes.box, classes.bottomBox)}>
          <DynamicIcon
            icon={icon || moduleIcon}
            className={classes.icon}
            style={{ color: themeColor || moduleColor }}
          />
        </div>
      </Paper>
    </Link>
  );
};

SimpleKPI.propTypes = {
  title: PropTypes.string,
  resource: PropTypes.string,
  record: PropTypes.object,
  cfCode: PropTypes.string,
  crudGetList: PropTypes.func.isRequired,
  filters: PropTypes.object,
  dependsOn: PropTypes.object,
  resources: PropTypes.object.isRequired,
  modules: PropTypes.object.isRequired,
  icon: PropTypes.string,
  color: PropTypes.string,
  onClick: PropTypes.string,
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  translate: PropTypes.func.isRequired
};

export default compose(
  connect(
    ({
      admin: { resources },
      config: {
        config: { modules }
      },
      loading
    }) => ({
      modules,
      resources,
      isLoading: loading > 0
    }),
    { crudGetList: crudGetListAction }
  ),
  withStyles(styles),
  withTranslate
)(SimpleKPI);
