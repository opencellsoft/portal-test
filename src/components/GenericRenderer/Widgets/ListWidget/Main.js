import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import { Divider } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import Pagination from '../../../Globals/Tables/Pagination';

import Header from './Header';
import List from './List';

const styles = ({ shadows, palette, typography, spacing }) => ({
  main: {
    flex: '1',
    marginTop: 20,
    minWidth: 320
  },
  card: {
    overflow: 'inherit',
    textAlign: 'right',
    padding: spacing.unit * 2,
    minHeight: 52
  }
});

class Main extends PureComponent {
  static propTypes = {
    title: PropTypes.string,
    icon: PropTypes.string,
    color: PropTypes.string,
    resource: PropTypes.string.isRequired,
    source: PropTypes.string,
    noHeader: PropTypes.bool,
    onRowClick: PropTypes.string,
    keyColumn: PropTypes.string,
    list: PropTypes.object,
    total: PropTypes.number,
    rowTextPrimary: PropTypes.string.isRequired,
    rowTextSecondary: PropTypes.string.isRequired,
    page: PropTypes.number,
    rowsPerPage: PropTypes.number,
    handleChangePage: PropTypes.func,
    handleChangeRowsPerPage: PropTypes.func
  };

  render = () => {
    const {
      title,
      icon,
      color,
      resource,
      source,
      noHeader,
      onRowClick,
      keyColumn,
      list,
      total,
      rowTextPrimary,
      rowTextSecondary,
      page,
      rowsPerPage,
      handleChangePage,
      handleChangeRowsPerPage
    } = this.props;

    return (
      <>
        {!noHeader && (
          <Header
            title={title}
            icon={icon}
            color={color}
            resource={resource}
            total={total}
          />
        )}

        <List
          list={list}
          resource={resource}
          source={source}
          keyColumn={keyColumn}
          onRowClick={onRowClick}
          rowTextPrimary={rowTextPrimary}
          rowTextSecondary={rowTextSecondary}
        />

        <Divider />
        <Pagination
          count={total}
          page={page - 1}
          rowsPerPage={rowsPerPage}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </>
    );
  };
}

export default compose(withStyles(styles))(Main);
