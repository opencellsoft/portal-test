import React, { useContext, memo } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Card } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import PageContext from '../../Layout/PageContext';

import MainController from './MainController';
import Tabs from './Tabs';

const styles = ({ spacing }) => ({
  main: {
    flex: '1',
    minWidth: 320,
    margin: spacing.unit
  },
  card: {
    overflow: 'inherit',
    textAlign: 'right',
    minHeight: 52,
    margin: 0,
    boxShadow: '0 7px 14px rgba(50, 50, 93, 0.1), 0 3px 6px rgba(0, 0, 0, 0.08)'
  },
  cardPadding: {
    padding: 0
  }
});

const ListWidget = memo(({ tabs, classes, ...rest }) => {
  const { record, basePath } = useContext(PageContext);
  const hasTabs = Array.isArray(tabs);

  return (
    <div className={cx(classes.main)}>
      <Card className={cx(classes.card, !hasTabs && classes.cardPadding)}>
        {hasTabs ? (
          <Tabs tabs={tabs} record={record} basePath={basePath} {...rest} />
        ) : (
          <MainController record={record} basePath={basePath} {...rest} />
        )}
      </Card>
    </div>
  );
});

ListWidget.propTypes = {
  tabs: PropTypes.array,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ListWidget);
