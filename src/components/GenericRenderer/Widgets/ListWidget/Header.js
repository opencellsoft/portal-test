import React from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import get from 'lodash/get';
import { Typography, colors } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import DynamicIcon from '../../../Globals/DynamicIcon';
import withTranslate from '../../../Hoc/withTranslate';

const styles = ({ palette, spacing }) => ({
  topWrapper: {
    display: 'flex',
    padding: `${spacing.unit * 2}px ${spacing.unit * 2}px ${spacing.unit *
      0}px ${spacing.unit * 2}px`
  },
  iconWrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: `0 ${spacing.unit}px`
  },
  icon: {
    width: 54,
    height: 54
  },
  titleWrapper: {
    flex: 1
  },
  title: {
    color: palette.grey[700],
    display: 'inline-block',
    lineHeight: '24px',
    textTransform: 'uppercase'
  }
});

const Header = ({
  title,
  icon,
  color,
  total,
  resource,
  classes,
  translate
}) => {
  const themeColor = get(colors, color, color);

  return (
    <div className={classes.topWrapper}>
      <div className={classes.iconWrapper}>
        <DynamicIcon
          icon={icon}
          className={classes.icon}
          style={{ color: themeColor }}
        />
      </div>

      <div className={classes.titleWrapper}>
        <Typography variant="caption" classes={{ caption: classes.title }}>
          {title || translate(`resources.${resource}.name`, { smart_count: 2 })}
        </Typography>

        <Typography variant="headline" component="h2">
          {total}
        </Typography>
      </div>
    </div>
  );
};

Header.propTypes = {
  title: PropTypes.string,
  icon: PropTypes.string,
  color: PropTypes.string,
  resource: PropTypes.string.isRequired,
  total: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  classes: PropTypes.object.isRequired,
  translate: PropTypes.func.isRequired
};

export default compose(
  withStyles(styles),
  withTranslate
)(Header);
