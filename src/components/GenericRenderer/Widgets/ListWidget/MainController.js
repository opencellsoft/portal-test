import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { crudGetList as crudGetListAction } from 'react-admin';
import { connect } from 'react-redux';

import Main from './Main';

import {
  extractListFromRecord,
  extractListFromFetchedResource
} from '../../../../utils/record';

const MainController = ({
  resourceData,
  crudGetList,
  resource,
  record,
  source,
  noHeader,
  rowsPerPage,
  filters = [],
  ...rest
}) => {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(rowsPerPage || 5);

  const { list = [], total = 0 } = !!source
    ? extractListFromRecord({ record, source })
    : extractListFromFetchedResource({
        resourceData,
        resource,
        actualRecord: record,
        filters,
        pagination: { page, perPage },
        crudGetList
      });

  return (
    <Main
      {...rest}
      resource={resource}
      handleChangePage={(e, page) => setPage(page + 1)}
      handleChangeRowsPerPage={({ target }) => {
        setPage(1);
        setPerPage(target.value);
      }}
      page={page}
      rowsPerPage={perPage}
      list={list}
      total={total}
    />
  );
};

MainController.propTypes = {
  resourceData: PropTypes.object.isRequired,
  crudGetList: PropTypes.func.isRequired,
  rowsPerPage: PropTypes.number
};

export default connect(
  ({ admin: { resources }, loading }, { resource }) => ({
    resourceData: resources[resource],
    isLoading: loading > 0
  }),
  { crudGetList: crudGetListAction }
)(MainController);
