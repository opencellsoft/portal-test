import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {
  withDataProvider,
  refreshView as refreshViewAction,
  showNotification as showNotificationAction
} from 'react-admin';
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import compose from 'recompose/compose';
import { get } from 'lodash';

import { ACTION_API } from '../../../../constants/generic';
import { processParams } from '../../../../utils/generic-render';

import DynamicIcon from '../../../Globals/DynamicIcon';

const styles = ({ palette, typography, spacing }) => ({
  listItem: {
    padding: 0
  },
  list: {
    padding: `${spacing.unit}px ${spacing.unit * 2}px`
  },
  clickable: {
    cursor: 'pointer'
  },
  textWrapper: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    padding: spacing.unit
  },
  noResult: {
    color: palette.text.hint
  },
  linkWrapper: {
    textDecoration: 'none',
    '&:hover': {
      background: palette.action.hover
    }
  },
  listItemText: {
    padding: 0
  },
  listItemTitle: {
    fontWeight: typography.title.fontWeight
  }
});

// @TODO
// Generalize the handleRow
const handleRowClick = (
  onRowClick = {},
  { record, dataProvider, refreshView, showNotification }
) => {
  const { action, verb, resource, params } = onRowClick;

  if (action === ACTION_API) {
    return dataProvider(verb, resource, {
      data: {
        ...processParams({ params, source: { record } })
      }
    })
      .then(({ data: raw }) => {
        const link = document.createElement('a');
        const nameArr = get(record, 'pdfFilename', '').split('/');

        link.href = `data:application/pdf;base64,${raw}`;
        link.download = nameArr[nameArr.length - 1];
        link.click();
      })
      .then(refreshView)
      .catch((error) => showNotification(error.message));
  }

  return false;
};

const ListWidget = ({
  resource,
  source,
  list,
  keyColumn = 'code',
  onRowClick = false,
  rowTextPrimary,
  rowTextSecondary,
  dataProvider,
  refreshView,
  showNotification,
  classes
}) => {
  const listValues = Object.values(list || {});

  if (listValues.length < 1) {
    return (
      <List classes={{ root: classes.list }}>
        <ListItem disableGutters classes={{ root: classes.listItem }} dense>
          <div className={classes.textWrapper}>
            <Typography
              className={classes.noResult}
              component="span"
              variant="caption"
              noWrap
            >
              No results found
            </Typography>
          </div>
        </ListItem>
      </List>
    );
  }

  const listDisplay = Object.values(listValues).map((fetchedRecord) => ({
    ...fetchedRecord,
    primary: get(fetchedRecord, rowTextPrimary),
    secondary: get(fetchedRecord, rowTextSecondary) || `No ${rowTextSecondary}`,
    url:
      typeof onRowClick === 'object'
        ? undefined
        : `/${source || resource}/${get(fetchedRecord, keyColumn)}/${
            typeof onRowClick === 'string' ? onRowClick : ''
          }`,
    onRowClick
  }));

  return (
    <List classes={{ root: classes.list }}>
      {listDisplay.map(
        ({ primary, secondary, url, onRowClick, ...rest }, index) => {
          const WrappedComponent = url ? Link : 'div';
          const icon = get(onRowClick, 'icon');

          return (
            <ListItem
              key={index}
              disableGutters
              classes={{ root: classes.listItem }}
              dense
            >
              <WrappedComponent
                to={url}
                onClick={() =>
                  handleRowClick(onRowClick, {
                    record: rest,
                    dataProvider,
                    refreshView,
                    showNotification
                  })
                }
                className={cx(
                  classes.textWrapper,
                  classes.linkWrapper,
                  onRowClick && classes.clickable
                )}
              >
                {icon && (
                  <ListItemIcon>
                    <DynamicIcon icon={icon} />
                  </ListItemIcon>
                )}

                <ListItemText
                  primary={primary}
                  secondary={secondary}
                  classes={{
                    dense: classes.listItemText,
                    primary: classes.listItemTitle
                  }}
                />
              </WrappedComponent>
            </ListItem>
          );
        }
      )}
    </List>
  );
};

ListWidget.propTypes = {
  resource: PropTypes.string.isRequired,
  source: PropTypes.string,
  list: PropTypes.object.isRequired,
  keyColumn: PropTypes.string,
  onRowClick: PropTypes.string,
  rowTextPrimary: PropTypes.string.isRequired,
  rowTextSecondary: PropTypes.string.isRequired,
  dataProvider: PropTypes.func.isRequired,
  refreshView: PropTypes.func.isRequired,
  showNotification: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired
};

export default compose(
  connect(
    null,
    { refreshView: refreshViewAction, showNotification: showNotificationAction }
  ),
  withDataProvider,
  withStyles(styles)
)(ListWidget);
