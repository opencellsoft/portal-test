import React from 'react';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';

import WidgetTitle from '../WidgetTitle';

describe('WidgetTitle', () => {
  it('should render without crashing', () => {
    const title = 'Test!';
    const wrapper = shallow(<WidgetTitle title={title} />);

    expect(toJSON(wrapper.dive().children())).toEqual(title);

    expect(toJSON(wrapper)).toMatchSnapshot();
    expect(toJSON(wrapper.dive())).toMatchSnapshot();
  });

  it("shouldn't render if no title was provided", () => {
    const title = '';
    const title2 = undefined;

    const wrapper = shallow(<WidgetTitle title={title} />);
    const wrapper2 = shallow(<WidgetTitle title={title2} />);

    const serializedWrapper = toJSON(wrapper.dive());
    const serializedWrapper2 = toJSON(wrapper2.dive());

    expect(serializedWrapper).toEqual(serializedWrapper2);

    expect(serializedWrapper).toMatchSnapshot();
    expect(serializedWrapper2).toMatchSnapshot();
  });

  it('should accept class override', () => {
    const title = 'Test!';
    const wrapper = shallow(<WidgetTitle title={title} className="test" />);

    expect(wrapper.hasClass('test')).toBeTruthy();

    expect(toJSON(wrapper)).toMatchSnapshot();
    expect(toJSON(wrapper.dive())).toMatchSnapshot();
  });
});
