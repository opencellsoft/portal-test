import React, { useContext } from 'react';

import PageContext from '../../Layout/PageContext';

import Main from './Main';

const ContextWrapper = (props) => {
  const { resource, basePath } = useContext(PageContext);

  return <Main {...props} resource={resource} basePath={basePath} />;
};

export default ContextWrapper;
