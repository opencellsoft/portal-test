import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { createPortal } from 'react-dom';
import { connect } from 'react-redux';
import {
  crudGetManyReference as crudGetManyReferenceAction,
  setListSelectedIds as setListSelectedIdsAction,
  refreshView as refreshViewAction,
  nameRelatedTo,
  getReferences,
  getIds
} from 'react-admin';
// eslint-disable-next-line import/no-extraneous-dependencies
import { getTotal } from 'ra-core/esm/reducer/admin/references/oneToMany';

import { Card } from '@material-ui/core';
import { orange } from '@material-ui/core/colors';
import { get, isEmpty, sortBy, uniq } from 'lodash';
import { compose } from 'recompose';

import {
  extractCF,
  extractMatrixDefinition,
  getCFByVersion,
  getLatestCFVersion,
  getAvailableCFVersions,
  EDITABLE_VERSIONS_FIRST,
  EDITABLE_VERSIONS_LAST
} from '../../../../utils/custom-fields';
import { transformDataByHashTable } from '../../../../utils/data-transformer';

import SimpleTable from '../../Table/SimpleTable';
import RenderField from '../../CustomFields/RenderField';
import SwitchButton from '../../Buttons/SwitchButton';

import VersionList from './VersionList';
import { processParams } from '../../../../utils/generic-render';
import WithEntityCustomization from '../../../Hoc/WithEntityCustomization';

const MAPPING_TYPE_REDUCE = 'reduce';
const MAPPING_TYPE_INJECT = 'inject';

class Main extends PureComponent {
  static propTypes = {
    content: PropTypes.object,
    mappedEntityId: PropTypes.string,
    mappedIdentifier: PropTypes.string,
    extraContentRef: PropTypes.oneOfType([PropTypes.object, PropTypes.node])
      .isRequired,
    classes: PropTypes.object.isRequired,
    fieldsDefinition: PropTypes.object,
    setSelectedIds: PropTypes.func.isRequired
  };

  state = {
    version: -1,
    indexRow: -1
  };

  updatingRecord = false;

  loadingReference = false;

  filteredValues = {};

  customRowsIndexes = [];

  componentDidMount = () => {
    this.setLatestVersion();
  };

  componentDidUpdate = ({
    record: prevRecord,
    content: prevContent,
    viewVersion: prevViewVersion
  }) => {
    const { record, content, viewVersion } = this.props;
    const { version } = this.state;

    const { cfCode, reference } = content;
    const { cfCode: prevCfCode } = prevContent || {};

    if (
      !isEmpty(record) &&
      (cfCode && (cfCode !== prevCfCode || version < 0))
    ) {
      this.setLatestVersion();
    }

    if (viewVersion !== prevViewVersion) {
      this.loadingReference = false;
    }

    if (!!reference && !isEmpty(record) && isEmpty(prevRecord)) {
      this.fetchReferenceData();
    }
  };

  setVersion = (version, indexRow) => this.setState({ version, indexRow });

  // @todo
  // find latest (but first in index)
  setLatestVersion = () => {
    const { record, content } = this.props;
    const { version } = this.state;
    const { cfCode } = content;

    if (!isEmpty(record)) {
      const { valuePeriodPriority: latestVersion } =
        getLatestCFVersion(record, cfCode) || {};

      if (
        Number(latestVersion) === latestVersion &&
        version !== latestVersion
      ) {
        this.setVersion(latestVersion, 0);
      }
    }
  };

  isEditableVersion = () => {
    const { version } = this.state;
    const { record, content } = this.props;
    const { isEditable, cfCode, editableVersions = [] } = content;

    if (!isEditable) {
      return false;
    }

    if (isEmpty(editableVersions)) {
      return true;
    }

    const availableVersions = getAvailableCFVersions(record, cfCode);

    return editableVersions.reduce((res, rule) => {
      if (res) {
        return res;
      }

      switch (rule) {
        case EDITABLE_VERSIONS_FIRST:
          return version === availableVersions[0];
        case EDITABLE_VERSIONS_LAST:
          return version === availableVersions[availableVersions.length - 1];
        default:
          return false;
      }
    }, false);
  };

  fetchReferenceData = () => {
    const {
      crudGetManyReference,
      resource,
      record,
      content,
      referenceData
    } = this.props;
    const { source, reference, filter = null } = content;

    if (!this.loadingReference && !!reference && !!source && !isEmpty(record)) {
      const [referenceSource, paramSource] = source.split(':');
      const finalSource =
        typeof paramSource === 'string' && paramSource.includes('.')
          ? paramSource.split('.')[1]
          : paramSource;

      const relatedTo = nameRelatedTo(
        reference,
        get(record, finalSource),
        resource,
        referenceSource
      );

      const nextFilters = processParams({
        params: filter,
        source: { record }
      });

      crudGetManyReference(
        reference,
        referenceSource,
        get(record, finalSource),
        relatedTo,
        { page: 1, perPage: 50 },
        null,
        nextFilters,
        finalSource
      );

      this.loadingReference = true;
    }

    return referenceData ? Object.values(referenceData) : [];
  };

  extractData = () => {
    const { record, content } = this.props;
    const { source, reference, cfCode, cfMapping, containerProperty } = content;

    let data = [];
    switch (true) {
      case !!cfCode && cfMapping:
        data = this.handleMappedData();
        break;
      case !!cfCode:
        data = extractCF(record, cfCode);
        break;
      case !!reference:
        data = this.fetchReferenceData();
        break;
      default:
        data = get(record, source) || [];
    }

    if (containerProperty) {
      data.forEach(({ [containerProperty]: nestedData }, index) => {
        if (nestedData) {
          data[index] = nestedData;
        }
      });
    }

    return Array.isArray(data) ? [...data] : [];
  };

  handleMappedData = () => {
    const { record, content, fieldsDefinition } = this.props;
    const { cfCode, cfMappingType } = content;

    const originalData = extractCF(record, cfCode) || [];
    const data = Array.isArray(originalData) ? [...originalData] : [];

    if (isEmpty(fieldsDefinition)) {
      return data;
    }

    switch (cfMappingType) {
      case MAPPING_TYPE_REDUCE:
        return this.reduceMappedData(data);
      case MAPPING_TYPE_INJECT:
        return this.injectMappedData(data);
      default:
        return data;
    }
  };

  reduceMappedData = (data) => {
    const { record, fieldsDefinition, content } = this.props;
    const { version } = this.state;
    const { cfCode, cfMapping } = content;

    const { keyColumns, valueColumns } = extractMatrixDefinition(
      fieldsDefinition,
      cfCode
    );

    // CF will only be modified if it is latest version
    const { valuePeriodPriority: latestVersion } =
      getLatestCFVersion(record, cfCode) || {};

    const mappedData = get(record, cfMapping) || [];

    // rebuilding mapped data with current CF schema with available columns
    const formattedMappedData = mappedData.reduce((res, el) => {
      const elKey = keyColumns.map((column) => el[column] || '').join('|');
      const elValues = valueColumns.map((column) => el[column] || '').join('|');

      res[elKey] = { value: elValues };

      return res;
    }, {});
    const formattedMappedDataKeys = Object.keys(formattedMappedData);

    // retrieving record CF based on selected version
    const currentVersion = getCFByVersion(record, cfCode, version);
    const currentVersionIndex = data.findIndex((el) => el === currentVersion);

    const { mapValue } = currentVersion || {};
    const { key, ...values } = mapValue || {};

    // filtering record CF current version with selected mapped entity
    this.filteredValues = Object.keys(values).reduce((res, valueKey) => {
      if (formattedMappedDataKeys.includes(valueKey)) {
        res[valueKey] = values[valueKey];
      }

      return res;
    }, {});

    // keeping only unique matrix keys
    const filteredValuesKeys = Object.keys(this.filteredValues);
    const uniqueKeys = uniq([
      ...filteredValuesKeys,
      ...formattedMappedDataKeys
    ]);

    if (version !== latestVersion && version > 0) {
      data[currentVersionIndex] = {
        ...currentVersion,
        mapValue: { ...this.filteredValues, key }
      };

      return data;
    }

    const displayedMapValue = {
      ...uniqueKeys.reduce((res, uniqueKey) => {
        res[uniqueKey] =
          this.filteredValues[uniqueKey] || formattedMappedData[uniqueKey];

        return res;
      }, {}),
      key
    };

    const enhancedVersion = {
      ...currentVersion,
      mapValue: displayedMapValue
    };

    data[currentVersionIndex] = enhancedVersion;

    return data;
  };

  injectMappedData = (data) => {
    const {
      record,
      content,
      mappedEntityId,
      mappedIdentifier,
      injectedResource = {},
      fieldsDefinition
    } = this.props;
    const { version } = this.state;
    const { cfCode, cfMapping } = content;

    if (!mappedEntityId || !injectedResource[mappedEntityId]) {
      return data;
    }

    const { keyColumns, valueColumns, versionable } = extractMatrixDefinition(
      fieldsDefinition,
      cfCode
    );

    // retrieving record CF based on selected version
    const currentVersion = versionable
      ? getCFByVersion(record, cfCode, version)
      : extractCF(record, cfCode, true);
    const currentVersionIndex = data.findIndex((el) => el === currentVersion);

    // CF will only be modified if it is latest version
    const { valuePeriodPriority: latestVersion } =
      getLatestCFVersion(record, cfCode) || {};

    const { mapValue } = currentVersion || {};
    const { key = {}, ...values } = mapValue || {};

    if (
      isEmpty(key) &&
      Array.isArray(keyColumns) &&
      Array.isArray(valueColumns)
    ) {
      key.value = [...keyColumns, ...valueColumns].join('/');
    }

    // filtering record CF current version with selected mapped entity
    this.customRowsIndexes = [];
    this.filteredValues = Object.keys(values).reduce((res, valueKey) => {
      const rawValues = get(values[valueKey], 'value', '');
      const rawValuesArray = rawValues.split('|');
      const formattedValues = valueColumns.reduce((res, key, index) => {
        res[key] = rawValuesArray[index];

        return res;
      }, {});

      if (formattedValues[mappedIdentifier] === mappedEntityId) {
        this.customRowsIndexes.push(Object.keys(res).length);

        res[valueKey] = values[valueKey];
      }

      return res;
    }, {});

    if (versionable && (version !== latestVersion && version > 0)) {
      data[currentVersionIndex] = {
        ...currentVersion,
        mapValue: { ...this.filteredValues, key }
      };

      return data;
    }

    const selectedResource = injectedResource[mappedEntityId];
    const mappedData = get(selectedResource, cfMapping) || [];

    // rebuilding mapped data with current CF schema with available columns
    const formattedMappedData = mappedData.reduce((res, el) => {
      el[mappedIdentifier] = mappedEntityId;

      const elKey = keyColumns.map((column) => el[column] || '').join('|');
      const elValues = valueColumns.map((column) => el[column] || '').join('|');

      res[elKey] = { value: elValues };

      return res;
    }, {});

    // keeping only unique matrix keys
    const filteredValuesKeys = Object.keys(this.filteredValues);
    const formattedMappedDataKeys = Object.keys(formattedMappedData);
    const uniqueKeys = uniq([
      ...filteredValuesKeys,
      ...formattedMappedDataKeys
    ]);

    if (isEmpty(uniqueKeys)) {
      return [];
    }

    // adding mapped data to record CF, but favoring record CF over mapped CF
    const displayedMapValue = {
      ...uniqueKeys.reduce((res, uniqueKey) => {
        res[uniqueKey] =
          this.filteredValues[uniqueKey] || formattedMappedData[uniqueKey];

        return res;
      }, {}),
      key
    };

    const enhancedVersion = {
      ...currentVersion,
      code: cfCode,
      mapValue: displayedMapValue
    };

    if (currentVersionIndex < 0) {
      data[0] = enhancedVersion;
    } else {
      data[currentVersionIndex] = enhancedVersion;
    }

    return data;
  };

  rowStyleHandler = (record, index) =>
    this.customRowsIndexes.includes(index)
      ? {
          backgroundColor: orange.A100
        }
      : {};

  saveInitialData = (data, index) => {
    const {
      resource,
      record,
      dataProvider,
      fieldsDefinition,
      refreshView
    } = this.props;

    if (this.updatingRecord) {
      return false;
    }

    this.updatingRecord = true;

    const customFields = [...record.customFields.customField];
    customFields[index > -1 ? index : customFields.length] = data;

    return dataProvider('UPDATE', resource, {
      data: {
        ...record,
        customFields: {
          customField: customFields
        },
        __cf: {},
        __cfDefinition: fieldsDefinition
      }
    })
      .then(() => {
        this.updatingRecord = false;
      })
      .then(refreshView);
  };

  getEnhancedRecord = (data) => {
    const { record, content } = this.props;
    const { version } = this.state;

    const { cfCode } = content;

    if (isEmpty(data)) {
      return record;
    }

    const customFields = [...get(record, 'customFields.customField', [])];
    const relevantCFIndex = customFields.findIndex(
      ({ code, valuePeriodPriority }) =>
        code === cfCode && valuePeriodPriority === version
    );

    const updatedCF = data.find(
      ({ code, valuePeriodPriority }) =>
        code === cfCode && valuePeriodPriority === version
    );

    if (!updatedCF) {
      return record;
    }

    customFields[relevantCFIndex] = updatedCF;

    return {
      ...record,
      customFields: {
        customField: customFields
      }
    };
  };

  handleToggleMappedInsert = () => {
    const hasCustomData = Object.keys(this.filteredValues).length > 0;

    const { record, content, mappedEntityId } = this.props;
    const { cfCode } = content;

    const data = this.handleMappedData();
    const [enhancedData] = data;

    const { mapValue } = enhancedData || {};
    const { key } = mapValue || {};

    const customFields = get(record, 'customFields.customField', []);
    const currentStoredCFIndex = customFields.findIndex(
      ({ code }) => code === cfCode
    );
    const currentStoredData =
      currentStoredCFIndex > -1 ? customFields[currentStoredCFIndex] : null;
    const { mapValue: existingMapValue } = currentStoredData || {};
    const { key: existingKey, ...existingValues } = existingMapValue || {};

    const unrelatedValues = Object.keys(existingValues).reduce(
      (res, valueKey) => {
        const rawValues = get(existingValues[valueKey], 'value', '');

        if (rawValues.indexOf(mappedEntityId) < 0) {
          res[valueKey] = existingValues[valueKey];
        }

        return res;
      },
      {}
    );

    const nextData = hasCustomData
      ? {
          ...enhancedData,
          mapValue: {
            ...unrelatedValues,
            key
          }
        }
      : {
          ...enhancedData,
          mapValue: {
            ...unrelatedValues,
            ...enhancedData.mapValue
          }
        };

    return this.saveInitialData(
      nextData,
      customFields.findIndex(({ code }) => code === cfCode)
    );
  };

  renderCustomActions = (data) => {
    const { content } = this.props;
    const { withMappedInsert } = content || {};

    return !isEmpty(data) && withMappedInsert
      ? [
          <SwitchButton
            label={withMappedInsert}
            onClick={this.handleToggleMappedInsert}
            value={Object.keys(this.filteredValues).length > 0}
          />
        ]
      : [];
  };

  updateDataIndex = (data, filterValuesHandler) => {
    const { record, content } = this.props;
    const { cfCode } = content;
    const { __cf = {}, ...rest } = data || {};
    const { [cfCode]: cfData } = __cf;
    const [cfDataByIndex] = Object.keys(cfData);
    const [cfDataLineByIndex] = Object.keys(cfData[cfDataByIndex]);

    const customFields = get(record, 'customFields.customField') || [];
    const filteredCFs = customFields.filter(({ code }) => code === cfCode);
    const modifiedCF = filteredCFs[cfDataByIndex];

    const { mapValue = {} } = modifiedCF || {};
    const { key, ...values } = mapValue;

    const filteredValuesKeys = Object.keys(this.filteredValues);
    const modifiedKey = filteredValuesKeys[cfDataLineByIndex];
    const modifiedLine = this.filteredValues[modifiedKey];

    const actualIndex = Object.keys(values).findIndex(
      (valueKey) => values[valueKey] === modifiedLine
    );

    return {
      __cf: {
        [cfCode]: {
          [cfDataByIndex]: {
            [actualIndex]: cfData[cfDataByIndex][cfDataLineByIndex]
          }
        }
      },
      ...rest
    };
  };

  handleSave = (data, resolve, reject) => {
    const { dataProvider, resource, record, content } = this.props;
    const { cfMapping } = content || {};

    let finalData = data;
    if (cfMapping) {
      finalData = this.updateDataIndex(data);
    }

    return dataProvider('UPDATE', resource, {
      data: {
        ...record,
        ...finalData
      }
    }).then(() => resolve(true));
  };

  getPriority = (newIndex, index, data) => {
    if (
      get(data, `[${newIndex}].valuePeriodPriority`, 1) ===
      get(data, `[${index}].valuePeriodPriority`, 1)
    )
      return true;

    return false;
  };

  render = () => {
    const {
      content,
      extraContentRef,
      classes,
      resource,
      record,
      basePath,
      setSelectedIds,
      fieldsDefinition,
      verticalTable
    } = this.props;

    const { version, indexRow } = this.state;

    const {
      cfCode,
      cfMapping,
      withImport,
      withExport,
      withCreate,
      fields,
      nestedDataField,
      withVersion,
      reference,
      actions = []
    } = content;
    const { current: placeholder } = extraContentRef || {};

    const isCF = !!cfCode;
    let data =
      (nestedDataField ? get(record, nestedDataField) : this.extractData()) ||
      [];

    const { field: definitions = [] } = fieldsDefinition || {};
    const cfDefinition =
      (isCF && definitions.find(({ code }) => code === cfCode)) || {};

    const isVersionable = !!cfDefinition.versionable || withVersion;
    const isVersionEditionAllowed = this.isEditableVersion();

    const cfDataIndex = isCF
      ? isVersionable
        ? data.findIndex(({ valuePeriodPriority }, index) => {
            if (
              this.getPriority(index + 1, index, data) ||
              this.getPriority(index - 1, index, data)
            ) {
              return index === indexRow;
            }

            return valuePeriodPriority === version;
          })
        : 0
      : null;

    const cfData = data
      ? data[cfDataIndex] &&
        sortBy([data[cfDataIndex]], ['valuePeriodPriority'])
      : {};

    const mappedData = cfMapping ? getLatestCFVersion(record, cfMapping) : [];
    const enhancedRecord = this.getEnhancedRecord(data);

    data = transformDataByHashTable(data, fields);

    return isCF ? (
      <>
        {isVersionable ? (
          <WithEntityCustomization resource={reference || resource}>
            <VersionList
              data={data}
              activeRow={cfDataIndex}
              onRowClick={(index) => {
                this.setVersion(
                  get(data, `[${index}].valuePeriodPriority`, 1),
                  Number(index)
                );
                setSelectedIds(resource, []);
              }}
              basePath={basePath}
              record={record}
              resource={resource}
              cfCode={cfCode}
              withImport={withImport}
              withExport={withExport}
              withCreate={withCreate}
              isEditable={isVersionEditionAllowed}
              version={version}
              fields={fields}
            />
          </WithEntityCustomization>
        ) : (
          <RenderField
            resource={resource}
            record={record}
            config={{
              ...content,
              actions: [...actions, ...this.renderCustomActions(cfData)]
            }}
            fields={cfData}
            cfIndex={cfDataIndex}
            fieldsDefinition={fieldsDefinition}
            rowStyleHandler={this.rowStyleHandler}
            handleSave={this.handleSave}
          />
        )}

        {placeholder &&
          isVersionable &&
          !isEmpty(cfData) &&
          createPortal(
            <div className={cx(classes.main)}>
              <Card className={cx(classes.card)}>
                <WithEntityCustomization resource={reference || resource}>
                  <RenderField
                    resource={resource}
                    record={enhancedRecord}
                    config={{
                      ...content,
                      withCreate: isVersionEditionAllowed && content.withCreate,
                      withImport: false,
                      bulkActions: isVersionEditionAllowed,
                      isEditable: isVersionEditionAllowed,
                      withExport: !isVersionable && content.withExport
                    }}
                    verticalTable={verticalTable}
                    fields={cfData}
                    mappedField={mappedData}
                    cfIndex={cfDataIndex}
                    handleSave={this.handleSave}
                  />
                </WithEntityCustomization>
              </Card>
            </div>,
            placeholder
          )}
      </>
    ) : nestedDataField ? (
      <SimpleTable
        {...this.props}
        {...content}
        resource=""
        data={data}
        total={data.length}
        withPagination
        relatedResource={resource}
        record={record}
      />
    ) : (
      <SimpleTable
        {...this.props}
        {...content}
        resource=""
        data={data}
        total={data.length}
        withPagination
      />
    );
  };
}

const mapStateToProps = (state, props) => {
  const { source = '', reference } = props.content || {};
  if (isEmpty(props.record) || !reference) {
    return {};
  }

  const [referenceSource, paramSource] = source.split(':');
  const finalSource =
    typeof paramSource === 'string' && paramSource.includes('.')
      ? paramSource.split('.')[1]
      : paramSource;

  const relatedTo = nameRelatedTo(
    reference,
    get(props.record, finalSource),
    props.resource,
    referenceSource
  );

  return {
    referenceData: getReferences(state, reference, relatedTo),
    ids: getIds(state, relatedTo),
    total: getTotal(state, relatedTo)
  };
};

export default compose(
  connect(
    mapStateToProps,
    {
      setSelectedIds: setListSelectedIdsAction,
      refreshView: refreshViewAction,

      crudGetManyReference: crudGetManyReferenceAction
    }
  )
)(Main);
