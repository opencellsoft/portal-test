import React, { useContext, useRef, useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import cx from 'classnames';
import {
  setListSelectedIds as setListSelectedIdsAction,
  ListToolbar
} from 'react-admin';
import { Card } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';
import { isEmpty, get, isNull } from 'lodash';

import PageContext from '../../Layout/PageContext';

import Tabs from '../../../Globals/Tabs';

import ContextWrapper from './ContextWrapper';
import WidgetTitle from '../WidgetTitle';
import ListActions from '../../Table/ListActions';
import { SIMPLE_TABLE, SELECT_WIDGET } from '../../../../constants/generic';
import SimpleTable from '../../Table/SimpleTable';
import SelectWidget from '../SelectWidget';

const styles = ({ spacing }) => ({
  columnWrapper: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    maxWidth: '100%'
  },
  wrapper: {
    flex: '1',
    display: 'flex',
    flexDirection: 'column'
  },
  main: {
    flex: 1,
    margin: spacing.unit
  },
  nestedMain: {
    flex: 1,
    margin: 0
  },
  nestedTitle: {
    margin: `${spacing.unit * 2}px 0 ${spacing.unit}px 0`
  },
  versionMain: {
    display: 'flex'
  },
  card: {
    flex: 1,
    overflow: 'inherit',
    textAlign: 'right',
    minHeight: 52,
    boxShadow:
      '0 7px 14px rgba(50, 50, 93, 0.1), 0 3px 6px rgba(0, 0, 0, 0.08)',
    maxWidth: '100%'
  },
  versionable: {
    marginTop: 20,
    overflow: 'hidden'
  },
  extraContent: {
    flex: 1
  }
});

const Components = {
  [SIMPLE_TABLE]: SimpleTable,
  [SELECT_WIDGET]: SelectWidget
};

const DetailedListWidget = ({
  classes,
  title,
  content,
  record = {},
  setListSelectedIds,
  reference,
  parentRecord,
  resource: propResource,
  dependsOn = {},
  withUpload,
  customActions = {},
  isNested = false,
  ...props
}) => {
  const [tab, setCurrentTab] = useState(0);
  const extraContent = useRef(null);
  const wrapper = useRef(null);

  const { resource: pageResource } = useContext(PageContext);
  const resource = reference || pageResource;

  let shouldRender = true;
  Object.keys(dependsOn).forEach((key) => {
    const recordValue = get(record, key, false);

    if (dependsOn[key] !== recordValue) {
      shouldRender = false;
    }
  });

  if (!shouldRender || isEmpty(content)) {
    return false;
  }

  const hasTabs = Array.isArray(content);

  let mainContent = content || {};
  if (hasTabs) {
    mainContent = content[tab];
  }
  const { type, resource: mainResource, icon, ...res } = mainContent;
  mainContent = mainContent.hasOwnProperty('content')
    ? mainContent.content
    : mainContent;

  const { cfCode } = mainContent;

  const changeHandler = useCallback(
    (e, tab) => {
      e.preventDefault();
      setCurrentTab(tab);
      setListSelectedIds(resource, []);
    },
    [resource]
  );

  const getItemsCount = (content = {}, record = {}) => {
    const dataField = get(content, 'nestedDataField');
    const status = get(content, 'nestedDataFilter.status');
    const relatedData = get(record, dataField);
    if (!relatedData) return '';

    return ` (${relatedData.filter((item) => item.status === status).length})`;
  };

  const Component = Components[type];

  return (
    <div className={classes.columnWrapper}>
      <div className={cx(classes.wrapper)} ref={wrapper}>
        <WidgetTitle
          title={title}
          className={isNested ? classes.nestedTitle : ''}
        />

        <div
          className={cx(
            isNested ? classes.nestedMain : classes.main,
            cfCode && classes.versionMain
          )}
        >
          <Card className={cx(classes.card)}>
            {!isEmpty(customActions) ||
              (withUpload && (
                <ListToolbar
                  actions={
                    <ListActions
                      record={record}
                      withUpload={withUpload}
                      resource={resource}
                      customActions={customActions}
                    />
                  }
                  exporter={false}
                >
                  {[]}
                </ListToolbar>
              ))}
            {hasTabs && (
              <Tabs
                tabs={content.map(
                  ({ title: label, icon, color, content }, id) => ({
                    id,
                    label: `${label}${getItemsCount(content, record)}`,
                    icon,
                    color
                  })
                )}
                selectedTab={tab}
                onChange={changeHandler}
              />
            )}
            {!type && (
              <ContextWrapper
                key={tab}
                record={isNull(record) ? parentRecord : record}
                content={mainContent}
                extraContentRef={extraContent}
                classes={classes}
                reference={reference}
                isNested={isNested}
                {...props}
              />
            )}
          </Card>

          {type && (
            <Component
              key={tab}
              record={isNull(record) ? parentRecord : record}
              content={mainContent}
              resource={mainResource}
              type={type}
              extraContentRef={extraContent}
              classes={classes}
              isNested={isNested}
              {...res}
              {...props}
            />
          )}
        </div>
      </div>

      <div
        ref={extraContent}
        className={classes.extraContent}
        style={{ width: get(wrapper, 'current.offsetWidth', 'auto') }}
      />
    </div>
  );
};

DetailedListWidget.propTypes = {
  title: PropTypes.string,
  content: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  classes: PropTypes.object.isRequired,
  record: PropTypes.object,
  dependsOn: PropTypes.object
};

export default compose(
  connect(
    null,
    { setListSelectedIds: setListSelectedIdsAction }
  ),
  withStyles(styles)
)(DetailedListWidget);
