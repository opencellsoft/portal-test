import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { ExportButton, downloadCSV } from 'react-admin';
import { unparse as convertToCSV } from 'papaparse/papaparse.min';
import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Typography,
  Divider
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { withStyles } from '@material-ui/core/styles';
import { isEmpty } from 'lodash';

import {
  extractCF,
  extractMatrixDataByDefinition
} from '../../../../utils/custom-fields';

import SimpleTable from '../../Table/SimpleTable';

import CreateVersionButton from '../../Buttons/CreateVersionButton';
import ImportVersionButton from '../../Buttons/ImportVersionButton';
import UpdateVersionButton from '../../Buttons/UpdateVersionButton';

const styles = () => ({
  expanded: {
    margin: 0
  },
  detailsRoot: {
    padding: 0
  }
});

class VersionList extends PureComponent {
  static propTypes = {
    data: PropTypes.array.isRequired,
    activeRow: PropTypes.number,
    onRowClick: PropTypes.func.isRequired,
    fields: PropTypes.array.isRequired,
    fieldsDefinition: PropTypes.array.isRequired,
    withImport: PropTypes.bool,
    version: PropTypes.number
  };

  state = {
    expanded: false
  };

  componentDidUpdate = ({ data: prevData }) => {
    const { data, onRowClick } = this.props;

    if (!isEmpty(data) && isEmpty(prevData)) {
      onRowClick(0);
    }
  };

  toggleExpanded = () => this.setState({ expanded: !this.state.expanded });

  hasVersion = () => {
    const { version, data } = this.props;

    return version && version > -1 && !isEmpty(data);
  };

  exporter = () => {
    const {
      resource,
      record,
      cfCode,
      version,
      fields,
      fieldsDefinition
    } = this.props;

    const { code } = record;
    const relevantCFs = extractCF(record, cfCode) || [];

    const cfData =
      relevantCFs.find(
        ({ code, valuePeriodPriority = 0 }) =>
          code === cfCode && version === valuePeriodPriority
      ) || {};

    const { data: formattedData = [] } =
      extractMatrixDataByDefinition(cfData, fieldsDefinition) || {};
    const dataWithFilteredColumns = formattedData.map((el) =>
      fields.reduce((res, { source }) => {
        res[source] = el[source];

        return res;
      }, {})
    );

    return downloadCSV(
      convertToCSV(dataWithFilteredColumns),
      `${resource}_${code}_${cfCode}_v${version}`.toUpperCase()
    );
  };

  getData = () => {
    const { data } = this.props;

    return data.map(
      (
        {
          valuePeriodPriority = 0,
          valuePeriodStartDate,
          valuePeriodEndDate,
          mapValue
        },
        index
      ) => {
        const { key, ...values } = mapValue || {};

        return {
          valuePeriodPriority: `#${valuePeriodPriority}`,
          valuePeriodStartDate,
          valuePeriodEndDate,
          nbRows: Object.keys(values).length || 0
        };
      }
    );
  };

  renderActions = () => {
    const {
      data,
      activeRow,
      onRowClick,
      withImport,
      withExport,
      withCreate,
      isEditable,
      record,
      cfCode,
      ...props
    } = this.props;

    const actions = [];

    if (withCreate) {
      actions.push(
        <CreateVersionButton
          {...props}
          cfCode={cfCode}
          recordId={record.id}
          label="input.file.create_version"
        />,
        <ImportVersionButton
          {...props}
          cfCode={cfCode}
          fileType={withImport}
          recordId={record.id}
          label="input.file.import_version"
        />
      );
    }

    if (data.length > 0) {
      if (isEditable) {
        actions.push(
          <UpdateVersionButton
            {...props}
            cfCode={cfCode}
            fileType={withImport}
            recordId={record.id}
            label="input.file.update_version"
            isUpdate
          />
        );
      }

      if (withExport) {
        actions.push(
          <ExportButton
            {...props}
            cfCode={cfCode}
            record={data[activeRow]}
            label="input.file.export_version"
            exporter={this.exporter}
          />
        );
      }
    }

    return actions;
  };

  renderTable = () => {
    const { data, activeRow, onRowClick, withCreate } = this.props;
    const { expanded } = this.state;

    return (
      <SimpleTable
        fields={[
          {
            label: 'Version',
            source: 'valuePeriodPriority'
          },
          {
            label: 'Start date',
            source: 'valuePeriodStartDate',
            type: 'date'
          },
          {
            label: 'End date',
            source: 'valuePeriodEndDate',
            type: 'date'
          },
          {
            label: 'Nb elements',
            source: 'nbRows',
            type: 'number'
          }
        ]}
        data={this.getData()}
        total={data.length}
        activeRow={activeRow}
        onRowClick={onRowClick}
        withImport={false}
        noDataLabel={withCreate}
        toolbarBottom
        actions={(expanded || !data.length) && this.renderActions()}
        withPagination={data.length > 5}
      />
    );
  };

  render = () => {
    const { version, classes } = this.props;
    const { expanded } = this.state;
    const hasVersion = this.hasVersion();

    if (!hasVersion) {
      return this.renderTable();
    }

    return (
      <ExpansionPanel
        expanded={expanded}
        defaultExpanded={!hasVersion}
        onChange={this.toggleExpanded}
        classes={{ expanded: classes.expanded }}
      >
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography
            variant="body1"
            component="span"
            className={classes.expandSummary}
          >
            {hasVersion ? `Version #${version}` : `No version`}
          </Typography>
        </ExpansionPanelSummary>
        <Divider />
        <ExpansionPanelDetails
          classes={{
            root: classes.detailsRoot
          }}
        >
          {this.renderTable()}
        </ExpansionPanelDetails>
        )}
      </ExpansionPanel>
    );
  };
}

export default withStyles(styles)(VersionList);
