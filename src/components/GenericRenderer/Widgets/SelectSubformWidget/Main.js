import React, { useCallback, useState, useMemo } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Paper } from '@material-ui/core';
import { compose } from 'recompose';
import { get, startsWith, isEmpty } from 'lodash';
import { getFormValues, isValid } from 'redux-form';
import {
  SimpleForm,
  withDataProvider,
  refreshView as refreshViewAction,
  showNotification as showNotificationAction,
  REDUX_FORM_NAME
} from 'react-admin';
import { push as pushAction } from 'react-router-redux';
import {
  getCFsInitialValues,
  mergeCfsWithDefinitions
} from '../../../../utils/custom-fields';
import { transformSubformCFsValues } from '../../../../providers/genericProvider/transformer';
import Select from './Select';

import SubForm from '../../Form/SubForm';

const styles = ({ spacing }) => ({
  main: {
    flex: 1,
    margin: spacing.unit
  }
});

const Main = ({
  classes,
  source,
  resource,
  reference,
  templates,
  templateName,
  templatePath,
  fieldsDefinition,
  record,
  dataProvider,
  parentFormValues,
  subformsValues,
  push,
  refreshView,
  showNotification,
  cfOutputContainer = 'parameter',
  isSubformsValid
}) => {
  if (isEmpty(templates)) return null;

  const [subformID, selectSubform] = useState('');
  const [subforms, setSubforms] = useState([]);

  const changeHandler = useCallback((event) => {
    selectSubform(event.target.value);
  }, []);

  const instantiateHandler = useCallback(() => {
    if (!subformID) return false;

    return setSubforms([...subforms, templates[subformID]]);
  });

  const resetHandler = useCallback(() => {
    setSubforms([]);
  });

  const choices = useMemo(
    () =>
      Object.values(templates).map((service) => ({
        id: service.code,
        name: service.description
      })),
    [templates]
  );

  const save = (data, redirect) => {
    const verb = !isEmpty(record) ? 'UPDATE' : 'CREATE';

    return isSubformsValid
      ? dataProvider(verb, resource, {
          data: {
            ...parentFormValues,
            [source]: transformSubformCFsValues(
              subformsValues,
              fieldsDefinition,
              { templatePath, cfOutputContainer }
            )
          }
        })
          .then(() => push(`/${resource}`))
          .then(refreshView)
          .catch((error) => showNotification(error.message))
      : showNotification('ra.message.invalid_form');
  };

  return (
    <Paper className={classes.main}>
      <Select
        onChange={changeHandler}
        onInstantiate={instantiateHandler}
        onReset={resetHandler}
        choices={choices}
        source={source}
        selected={subformID}
      />
      <div>
        {subforms
          .filter((item) => !!item)
          .map((subform, key) => {
            const template = get(subform, templateName) || {};

            const customFields =
              get(template, 'customFields.customField') || [];

            const fields = mergeCfsWithDefinitions(
              customFields,
              fieldsDefinition
            );

            return (
              <SubForm
                key={`subform_${source}${key}`}
                index={key}
                record={{ ...subform }}
                resource={reference}
                templateSource={templatePath}
                name={`${source}${key}`}
                description={subform.description}
                fields={fields}
                initialValues={{
                  code: subform.code,
                  [templatePath]: {
                    code: template.code,
                    ...getCFsInitialValues(fields)
                  }
                }}
              />
            );
          })}
      </div>
      <SimpleForm save={save} />
    </Paper>
  );
};

Main.propTypes = {
  classes: PropTypes.object.isRequired,
  resource: PropTypes.string.isRequired,
  source: PropTypes.string.isRequired,
  reference: PropTypes.string,
  templateName: PropTypes.string,
  templatePath: PropTypes.string,
  templates: PropTypes.array,
  fieldsDefinition: PropTypes.object,
  dataProvider: PropTypes.func,
  record: PropTypes.object,
  parentFormValues: PropTypes.object,
  subformsValues: PropTypes.object,
  push: PropTypes.func,
  refreshView: PropTypes.func,
  showNotification: PropTypes.func,
  isSubformsValid: PropTypes.bool,
  cfOutputContainer: PropTypes.string
};

const mapStateToProps = (state, { source, form: parentForm }) => {
  const subforms = Object.keys(state.form).filter((form) =>
    startsWith(form, source)
  );

  return {
    subformsValues: subforms.reduce((acc, form) => {
      const formData = getFormValues(form)(state);

      return {
        ...acc,
        [form]: formData
      };
    }, {}),
    isSubformsValid: subforms.every((subform) => isValid(subform)(state)),
    parentFormValues: getFormValues(parentForm || REDUX_FORM_NAME)(state) || {}
  };
};

export default compose(
  connect(
    mapStateToProps,
    {
      refreshView: refreshViewAction,
      showNotification: showNotificationAction,
      push: pushAction
    }
  ),
  withStyles(styles),
  withDataProvider
)(Main);
