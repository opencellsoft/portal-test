import React from 'react';
import { startCase } from 'lodash';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { TextField, Button, MenuItem } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import withTranslate from '../../../Hoc/withTranslate';

const styles = ({ spacing, palette }) => ({
  formControl: {
    margin: spacing.unit * 2,
    minWidth: 200
  },
  selectContainer: {
    width: '50%',
    padding: spacing.unit * 2
  },
  margin: {
    margin: spacing.unit * 2
  }
});

const Select = ({
  classes,
  onChange,
  onReset,
  onInstantiate,
  choices,
  source,
  selected,
  translate
}) => (
  <div className={classes.selectContainer}>
    <div>
      <TextField
        className={classes.formControl}
        select
        label={startCase(source)}
        value={selected}
        onChange={onChange}
      >
        {choices.map(({ id, name }) => (
          <MenuItem value={id} key={`choices_${id}`}>
            {name}
          </MenuItem>
        ))}
      </TextField>
    </div>
    <div>
      <Button
        variant="contained"
        color="secondary"
        onClick={onReset}
        className={classes.margin}
      >
        {translate(`action.reset`)}
      </Button>
      <Button
        variant="contained"
        color="primary"
        onClick={onInstantiate}
        className={classes.margin}
      >
        {translate(`action.instantiate`)}
      </Button>
    </div>
  </div>
);

Select.propTypes = {
  classes: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  onReset: PropTypes.func.isRequired,
  onInstantiate: PropTypes.func.isRequired,
  choices: PropTypes.array.isRequired,
  translate: PropTypes.func.isRequired,
  source: PropTypes.string,
  selected: PropTypes.string
};

export default compose(
  withTranslate,
  withStyles(styles)
)(Select);
