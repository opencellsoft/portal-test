import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { crudGetManyAccumulate as crudGetManyAccumulateAction } from 'react-admin';

import WithEntityCustomization from '../../../Hoc/WithEntityCustomization';
import Main from './Main';

const SelectSubformWidget = ({ reference, crudGetManyAccumulate, ...rest }) => {
  useEffect(() => {
    crudGetManyAccumulate(reference, []);
  }, []);

  return (
    <WithEntityCustomization resource={reference}>
      <Main reference={reference} {...rest} />
    </WithEntityCustomization>
  );
};

SelectSubformWidget.propTypes = {
  reference: PropTypes.string.isRequired,
  crudGetManyAccumulate: PropTypes.func
};

export default compose(
  connect(
    ({ admin }, { reference }) => ({
      templates: admin.resources[reference].data
    }),
    {
      crudGetManyAccumulate: crudGetManyAccumulateAction
    }
  )
)(SelectSubformWidget);
