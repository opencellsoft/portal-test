import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withStyles } from '@material-ui/core/styles';

import {
  SIMPLE_KPI,
  SIMPLE_WIDGET,
  ACTION_WIDGET,
  SELECT_WIDGET,
  LIST_WIDGET,
  DETAILED_LIST_WIDGET,
  DURATION_WIDGET,
  TABLE_WIDGET,
  CUSTOM_FIELD_WIDGET,
  CHART_PIE,
  TIME_LINE_WIDGET,
  SELECT_SUBFORM_WIDGET,
  CUSTOM_FIELDS_FORM_WIDGET
} from '../../../constants/generic';

import WithEntityCustomization from '../../Hoc/WithEntityCustomization';

import SimpleKPI from './SimpleKPI';
import SimpleWidget from './SimpleWidget';

import ActionWidget from './ActionWidget';
import ListWidget from './ListWidget';
import SelectWidget from './SelectWidget';
import DetailedListWidget from './DetailedListWidget';
import TableWidget from './TableWidget';
import CustomFieldsWidget from './CustomFieldsWidget';
import PieWidget from './ChartWidgets/PieWidget';
import TimeLineWidget from './ChartWidgets/TimeLineWidget';
import DurationWidget from './ChartWidgets/DurationWidget';
import SelectSubformWidget from './SelectSubformWidget';
import CustomFieldsFormWidget from './CustomFieldsFormWidget';

const styles = ({ spacing }) => ({
  wrapper: {
    margin: spacing.unit,
    boxShadow:
      '0 7px 14px rgba(50, 50, 93, 0.1), 0 3px 6px rgba(0, 0, 0, 0.08)',
    borderRadius: 6
  }
});

const Components = {
  [SIMPLE_KPI]: SimpleKPI,
  [SIMPLE_WIDGET]: SimpleWidget,
  [CHART_PIE]: PieWidget,
  [ACTION_WIDGET]: ActionWidget,
  [SELECT_WIDGET]: SelectWidget,
  [LIST_WIDGET]: ListWidget,
  [DETAILED_LIST_WIDGET]: DetailedListWidget,
  [TABLE_WIDGET]: TableWidget,
  [CUSTOM_FIELD_WIDGET]: CustomFieldsWidget,
  [TIME_LINE_WIDGET]: TimeLineWidget,
  [DURATION_WIDGET]: DurationWidget,
  [SELECT_SUBFORM_WIDGET]: SelectSubformWidget,
  [CUSTOM_FIELDS_FORM_WIDGET]: CustomFieldsFormWidget
};

const Widgets = ({ classes, resource, component, ...props }) => {
  const { type, testID, ...componentProps } = component || {};
  const Widget = Components[type];

  return Widget ? (
    <WithEntityCustomization resource={resource}>
      <Widget
        className={classes.wrapper}
        resource={resource}
        {...props}
        {...componentProps}
        id={testID}
      />
    </WithEntityCustomization>
  ) : null;
};

Widgets.propTypes = {
  classes: PropTypes.object.isRequired,
  component: PropTypes.object.isRequired
};

export default compose(
  connect(
    (state) => ({ viewVersion: state.admin.ui.viewVersion }),
    null
  ),
  withStyles(styles)
)(Widgets);
