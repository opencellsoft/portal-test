import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Tabs from '../../../Globals/Tabs';
import Main from './Main';

const formatTabs = (tabs) =>
  tabs.map(({ id, title, source, resource, ...rest }) => ({
    id,
    label: title,
    resource,
    source,
    ...rest
  }));

const ListWidget = ({ tabs, ...rest }) => {
  const [defaultTab] = tabs;
  const [currentTab, setCurrentTab] = useState(defaultTab.id);

  const formattedTabs = formatTabs(tabs);
  const currentFormattedTab = formattedTabs.find(({ id }) => id === currentTab);
  const { id } = currentFormattedTab || {};

  return (
    <>
      <Tabs
        tabs={formattedTabs}
        selectedTab={id}
        onChange={(e, tab) => setCurrentTab(tab)}
      />
      <Main {...rest} {...currentFormattedTab} noHeader />
    </>
  );
};

ListWidget.propTypes = {
  tabs: PropTypes.array,
  classes: PropTypes.object.isRequired
};

export default ListWidget;
