import React, { useState } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { crudGetList as crudGetListAction } from 'react-admin';
import { connect } from 'react-redux';
import { Card } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import compose from 'recompose/compose';
import get from 'lodash/get';
import withQueryProps from '../../../Hoc/withQueryProps';
import { transformDataByHashTable } from '../../../../utils/data-transformer';
import {
  extractListFromRecord,
  extractListFromFetchedResource,
  isVersionable
} from '../../../../utils/record';

import Tabs from '../../../Globals/Tabs';
import SimpleTable from '../../Table/SimpleTable';

import Main from './Main';

const styles = ({ spacing }) => ({
  main: {
    flex: '1',
    marginTop: 20,
    minWidth: 320
  },
  card: {
    overflow: 'inherit',
    textAlign: 'right',
    minHeight: 52,
    margin: `0 ${spacing.unit}px`,
    boxShadow: '0 7px 14px rgba(50, 50, 93, 0.1), 0 3px 6px rgba(0, 0, 0, 0.08)'
  },
  cardPadding: {
    padding: spacing.unit * 2
  },
  versionable: {
    marginTop: 20,
    overflow: 'hidden'
  }
});

const TableWidget = ({
  tabs,
  classes,
  record,
  resource,
  selectedData,
  customActions,
  dependsRecord,
  localFilter,
  filters,
  resourceData,
  crudGetList,
  withPagination,
  withHeader,
  fields,
  ...rest
}) => {
  const hasTabs = Array.isArray(tabs);

  const [defaultTab = {}] = hasTabs ? tabs : [];
  const [currentTab, setCurrentTab] = useState(defaultTab.id);
  const [currentVersion, setCurrentVersion] = useState(1);

  const formattedTabs = hasTabs ? formatTabs(tabs) : [];
  const currentFormattedTab =
    formattedTabs.find(({ id }) => id === currentTab) || {};
  const { id, source, reference, containerProperty } = currentFormattedTab;

  const {
    list,
    versionable,
    versionList,
    total,
    versionTotal
  } = getListDetails({
    resourceData,
    resource: reference || resource,
    record,
    source,
    selectedData,
    filters,
    crudGetList,
    containerProperty,
    currentVersion
  });

  const listTransformed = transformDataByHashTable(list, fields);

  return (
    <div className={cx(classes.main)}>
      <Card className={cx(classes.card, !hasTabs && classes.cardPadding)}>
        {hasTabs && (
          <>
            <Tabs
              tabs={formattedTabs}
              selectedTab={id}
              onChange={(e, tab) => setCurrentTab(tab)}
            />
          </>
        )}

        {versionable ? (
          <>
            <SimpleTable
              fields={[
                {
                  label: 'Version',
                  source: 'valuePeriodPriority'
                },
                {
                  label: 'Start date',
                  source: 'valuePeriodStartDate',
                  type: 'date'
                },
                {
                  label: 'End date',
                  source: 'valuePeriodEndDate',
                  type: 'date'
                }
              ]}
              data={list.map(
                ({
                  valuePeriodPriority,
                  valuePeriodStartDate,
                  valuePeriodEndDate
                }) => ({
                  valuePeriodPriority,
                  valuePeriodStartDate,
                  valuePeriodEndDate
                })
              )}
              total={total}
              onRowClick={(index) =>
                setCurrentVersion(
                  get(list, `[${index}].valuePeriodPriority`, 1)
                )
              }
            />
          </>
        ) : (
          <Main
            {...rest}
            resource={resource}
            fields={fields}
            selectedData={selectedData}
            dependsRecord={dependsRecord}
            record={record}
            localFilter={localFilter}
            customActions={customActions}
            list={listTransformed}
            total={total}
            noHeader={!withHeader || hasTabs}
            withPagination={withPagination}
          />
        )}
      </Card>

      {versionable ? (
        <Card className={cx(classes.card, classes.versionable)}>
          <Main
            {...currentFormattedTab}
            resource={resource}
            record={record}
            list={versionList}
            total={versionTotal}
            noHeader
          />
        </Card>
      ) : (
        undefined
      )}
    </div>
  );
};

TableWidget.propTypes = {
  tabs: PropTypes.array,
  classes: PropTypes.object.isRequired,
  fields: PropTypes.array
};

const formatTabs = (tabs) =>
  tabs.map(({ title, source, resource, ...rest }) => ({
    id: source || resource,
    label: title,
    resource,
    source,
    ...rest
  }));

const getListDetails = ({
  resourceData,
  resource,
  record,
  source,
  selectedData,
  filters,
  crudGetList,
  containerProperty,
  currentVersion
}) => {
  const { list = [], total = 0 } = !!source
    ? extractListFromRecord({ record, source })
    : extractListFromFetchedResource({
        resourceData,
        resource,
        actualRecord: !!selectedData ? selectedData : record,
        filters,
        crudGetList
      });
  const versionable = isVersionable(list);
  const versionList = versionable
    ? get(
        list.find(
          ({ valuePeriodPriority }) => valuePeriodPriority === currentVersion
        ),
        'data'
      ) || []
    : [];

  if (containerProperty) {
    list.forEach(({ [containerProperty]: data }, index) => {
      if (data) {
        list[index] = data;
      }
    });
  }

  return {
    list,
    versionable,
    versionList,
    total,
    versionTotal: versionable && versionList.length
  };
};

export default compose(
  connect(
    ({ admin: { resources }, loading }, { resource }) => ({
      resourceData: resources[resource],
      isLoading: loading > 0
    }),
    { crudGetList: crudGetListAction }
  ),
  withQueryProps(),
  withStyles(styles)
)(TableWidget);
