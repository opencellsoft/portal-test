import React from 'react';
import PropTypes from 'prop-types';
import Header from './Header';
import SimpleTable from '../../Table/SimpleTable';

const Main = ({
  title,
  icon,
  color,
  total,
  list,
  resource,
  record,
  source,
  noHeader,
  roles,
  dependsFields,
  dependsRecord,
  selectedData,
  customActions,
  localFilter,
  fields,
  expand,
  keyColumn,
  basePath,
  onRowClick,
  rowsPerPage = 5,
  rowsPerPageOptions,
  withPagination
}) => (
  <>
    {!noHeader && (
      <Header
        title={title}
        icon={icon}
        color={color}
        resource={resource}
        total={total}
      />
    )}

    <SimpleTable
      record={record}
      dependsRecord={dependsRecord}
      dependsFields={dependsFields}
      source={source}
      selectedData={selectedData}
      customActions={customActions}
      localFilter={localFilter}
      basePath={basePath}
      fields={fields}
      roles={roles}
      data={list}
      total={total}
      onRowClick={onRowClick}
      expand={expand}
      keyColumn={keyColumn}
      rowsPerPage={rowsPerPage}
      rowsPerPageOptions={rowsPerPageOptions}
      withPagination={withPagination}
    />
  </>
);

Main.propTypes = {
  title: PropTypes.string,
  icon: PropTypes.string,
  color: PropTypes.string,
  list: PropTypes.array.isRequired,
  total: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  resource: PropTypes.string.isRequired,
  record: PropTypes.object.isRequired,
  source: PropTypes.string,
  noHeader: PropTypes.bool,
  onRowClick: PropTypes.string,
  expand: PropTypes.object,
  rowsPerPage: PropTypes.number
};

export default Main;
