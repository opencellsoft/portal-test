import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import withStylesProps from '../../Hoc/WithStylesProps';
import Row from '../../Globals/Row';
import InputRenderer from './InputRenderer';

const styles = (theme, { itemsPerRow }) => ({
  row: {
    gridTemplateColumns: `repeat(${itemsPerRow ||
      'auto-fill'}, minmax(220px, auto))`
  }
});

const RowRenderer = ({
  items,
  itemsPerRow,
  isEdit,
  inDrawer,
  classes,
  rowClassname,
  record,
  formID
}) => {
  if (!Array.isArray(items)) {
    return false;
  }

  return (
    <Row className={cx(!inDrawer && classes.row, rowClassname)} grid>
      {items.map(({ autoCompleteByCreate, source, ...input }) => (
        <InputRenderer
          key={source}
          autoCompleteByCreate={autoCompleteByCreate}
          source={source}
          isEdit={isEdit}
          record={record}
          id={`${formID}--${source}`}
          {...input}
        />
      ))}
    </Row>
  );
};

RowRenderer.propTypes = {
  items: PropTypes.array.isRequired,
  itemsPerRow: PropTypes.number,
  isEdit: PropTypes.bool,
  inDrawer: PropTypes.bool,
  rowClassname: PropTypes.string,
  classes: PropTypes.object.isRequired,
  record: PropTypes.object,
  formID: PropTypes.string
};

export default withStylesProps(styles)(RowRenderer);
