import React from 'react';
import { connect } from 'react-redux';
import { pure, compose } from 'recompose';
import PropTypes from 'prop-types';
import { Tooltip } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import withTranslate, {
  getFieldLabelTranslationArgs
} from '../../Hoc/withTranslate';

const styles = ({ spacing }) => ({
  tooltip: {
    fontSize: `14px`
  }
});

export const FieldTitle = ({
  resource,
  modules,
  source,
  label,
  isRequired,
  translate,
  classes,
  withTooltip = true
}) => {
  const translatedLabel = translate(
    ...getFieldLabelTranslationArgs({ label, resource, source }, modules)
  );

  const finalLabel = `${translatedLabel}${isRequired ? ' *' : ''}`;

  return (
    <>
      {withTooltip ? (
        <Tooltip
          title={finalLabel}
          placement="top-start"
          classes={{ tooltip: classes.tooltip }}
        >
          <span>{finalLabel}</span>
        </Tooltip>
      ) : (
        <span>{finalLabel}</span>
      )}
    </>
  );
};

FieldTitle.propTypes = {
  resource: PropTypes.string,
  modules: PropTypes.object.isRequired,
  source: PropTypes.string,
  label: PropTypes.string,
  isRequired: PropTypes.bool,
  translate: PropTypes.func,
  classes: PropTypes.object,
  withTooltip: PropTypes.bool
};

const mapStateToProps = ({
  config: {
    config: { modules }
  }
}) => ({
  modules
});

const enhance = compose(
  connect(mapStateToProps),
  withTranslate,
  withStyles(styles),
  pure
);

export default enhance(FieldTitle);
