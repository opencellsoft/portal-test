import React from 'react';
import PropTypes from 'prop-types';
import { FormTab, REDUX_FORM_NAME } from 'react-admin';
import { isEmpty, get, flatten } from 'lodash';

import { connect } from 'react-redux';
import { collectErrors } from '../../../utils/form';

import WithEntityCustomization from '../../Hoc/WithEntityCustomization';

import TabbedForm from './RATabbedForm';
import FormRows from './FormRows';
import Toolbar from './Toolbar';

const GenericTabbedForm = ({
  config,
  classes,
  history,
  isEdit,
  inDrawer,
  save,
  permissions,
  fieldsDefinition,
  rowClassname,
  redirectAfterSubmit,
  redirectAfterDelete,
  tabsWithErrors,
  ...rest
}) => {
  const { tabs, withSave = true, withDelete, withBack = false } = config || {};
  const { resource, id } = rest;

  const hasToolbar = withSave || withDelete || withBack;

  if (!Array.isArray(tabs)) {
    return false;
  }

  const form = (
    <TabbedForm
      {...rest}
      save={save}
      redirect={redirectAfterSubmit}
      toolbar={
        hasToolbar ? (
          <Toolbar
            withDelete={withDelete}
            withSave={withSave}
            withBack={withBack}
            formID={id}
            redirectAfterDelete={redirectAfterDelete}
          />
        ) : null
      }
      tabsWithErrors={tabsWithErrors}
    >
      {tabs.map(({ title, rows }, index) => (
        <FormTab key={title} label={title} id={`${id}-tab--${index}`}>
          <FormRows
            rows={rows}
            isEdit={isEdit}
            inDrawer={inDrawer}
            rowClassname={rowClassname}
            formID={id}
          />
        </FormTab>
      ))}
    </TabbedForm>
  );

  return isEmpty(fieldsDefinition) ? (
    <WithEntityCustomization resource={resource}>
      {form}
    </WithEntityCustomization>
  ) : (
    form
  );
};

GenericTabbedForm.propTypes = {
  config: PropTypes.object.isRequired,
  isEdit: PropTypes.bool,
  inDrawer: PropTypes.bool,
  fieldsDefinition: PropTypes.object,
  rowClassname: PropTypes.string
};

export const findTabsWithErrors = (
  state,
  props,
  collectErrorsImpl = collectErrors
) => {
  const errors = collectErrorsImpl(state, props.form);
  const tabs = get(props, 'config.tabs', []);

  return tabs.reduce((acc, child) => {
    const inputs = flatten(get(child, 'rows', []));

    if (inputs.some((input) => get(errors, input.source))) {
      return [...acc, child.title];
    }

    return acc;
  }, []);
};

export default React.memo(
  connect(
    (state, props) => ({
      tabsWithErrors: findTabsWithErrors(state, {
        form: props.form || REDUX_FORM_NAME,
        ...props
      })
    }),
    null
  )(GenericTabbedForm)
);
