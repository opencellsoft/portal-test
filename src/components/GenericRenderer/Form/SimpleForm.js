import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { destroy } from 'redux-form';
import {
  SimpleForm,
  refreshView as refreshViewAction,
  showNotification as showNotificationAction,
  REDUX_FORM_NAME
} from 'react-admin';
import { isEmpty } from 'lodash';

import WithEntityCustomization from '../../Hoc/WithEntityCustomization';
import { getKeyFieldsFromMatrix } from '../../../utils/custom-fields';

import FormRows from './FormRows';
import Toolbar from './Toolbar';

class GenericSimpleForm extends PureComponent {
  static propTypes = {
    config: PropTypes.object.isRequired,
    history: PropTypes.func.isRequired,
    isEdit: PropTypes.bool,
    save: PropTypes.func.isRequired,
    refreshView: PropTypes.func.isRequired,
    fieldsDefinition: PropTypes.object,
    original: PropTypes.object
  };

  state = {
    openConfirm: false
  };

  componentWillMount = () => {
    const { isEdit } = this.props;
    if (!isEdit) this.resetForm();
  };

  resetForm = () =>
    this.props.dispatch(destroy(this.props.form || REDUX_FORM_NAME));

  /**
   * @todo: Move this to a utility module
   * This function checks if the key fields exist already in the parent entity
   * If we are creating a new multi-value CF with the same key fields and different value
   * the creation will become an update.
   */
  isUpdate = (data) => {
    // Check the original data and the CF fields definition
    const {
      original,
      fieldsDefinition,
      createOrUpdate,
      errorMessage
    } = this.props;
    // Early exit if we aren't updating a CF
    if (!data.__cf) {
      return false;
    }
    // Get the CF code
    const cfCode = Object.keys(data.__cf)[0];

    // Exit if no CF code
    if (!cfCode || !fieldsDefinition.field) {
      return false;
    }

    const key = Object.keys(data.__cf[cfCode]);

    // Check if its a create operation
    if (
      !data.__cf[cfCode] ||
      !data.__cf[cfCode][key] ||
      !data.__cf[cfCode][key].create
    ) {
      return false;
    }

    const newCF = data.__cf[cfCode][key].create;

    // Find the definition if this CF
    const definition = fieldsDefinition.field.find(
      (item) => item.code === cfCode
    );

    // Exit if its not a multi-value CF
    if (!definition || !definition.matrixColumn) {
      return false;
    }

    // Read the key indexes
    const keys = getKeyFieldsFromMatrix(definition.matrixColumn);

    // Exist if no keys found
    if (!keys) {
      return false;
    }

    // Make sure the keys are sorted
    keys.sort((key1, key2) => key1.position - key2.position);

    // Get all the CF codes
    const codes = keys.map((key) => key.code);

    // Map the existing codes with the new ones
    const originalKeys = original.map((element) =>
      codes.map((key) => element[key]).join('|')
    );
    const newKey = codes.map((key) => newCF[key]).join('|');
    // Check if the new key exist createOrUpdate === false - if yes its an error
    if (
      originalKeys.includes(newKey) &&
      typeof createOrUpdate !== 'undefined' &&
      !createOrUpdate
    ) {
      throw new Error(errorMessage.replace('key', newKey));
    }

    return originalKeys.includes(newKey);
  };

  /**
   * This checks if its an update
   * if yes: show the dialog message
   * if no: carry on with the save
   */
  enhancedSave = (data, redirect) => {
    const { fieldsDefinition, showNotification, save } = this.props;
    try {
      if (this.isUpdate(data, fieldsDefinition)) {
        this.setState({ openConfirm: true });
      } else {
        return save(data, redirect);
      }
    } catch (error) {
      return showNotification(error.message);
    }

    return true;
  };

  handleClose = () => {
    this.setState({ openConfirm: false });
  };

  render = () => {
    const {
      config,
      history,
      isEdit,
      inDrawer,
      save,
      permissions,
      fieldsDefinition,
      refreshView,
      rowClassname,
      redirectAfterDelete,
      form: formName,
      ...rest
    } = this.props;
    const { rows, withSave = true, withDelete, withBack = false } =
      config || {};
    const { resource, id } = rest;
    if (!Array.isArray(rows)) {
      return false;
    }

    const hasToolbar = withSave || withDelete || withBack;

    /**
     * @todo: onSubmit is here only because its needed by the dialog - need a better solution
     */
    const form = (
      <>
        <SimpleForm
          {...rest}
          onSubmit={this.handleSave}
          save={this.enhancedSave}
          form={formName}
          toolbar={
            hasToolbar ? (
              <Toolbar
                openConfirm={this.state.openConfirm}
                handleClose={this.handleClose}
                withDelete={withDelete}
                withSave={withSave}
                withBack={withBack}
                redirectAfterDelete={redirectAfterDelete}
                formID={id}
              />
            ) : null
          }
        >
          <FormRows
            rows={rows}
            isEdit={isEdit}
            inDrawer={inDrawer}
            rowClassname={rowClassname}
            formID={id}
          />
        </SimpleForm>
      </>
    );

    return isEmpty(fieldsDefinition) ? (
      <WithEntityCustomization resource={resource}>
        {form}
      </WithEntityCustomization>
    ) : (
      form
    );
  };
}

export default connect(
  null,
  { showNotification: showNotificationAction, refreshView: refreshViewAction }
)(GenericSimpleForm);
