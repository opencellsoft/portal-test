import React, { useState, useMemo } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import compose from 'recompose/compose';
import { get, isUndefined } from 'lodash';
import { withStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

import withTranslate from '../../Hoc/withTranslate';

import {
  extractCustomFieldChoices,
  extractValidators,
  getFieldTypeByValueType,
  filterEmptyItems
} from '../../../utils/custom-fields';
import InputRenderer from './InputRenderer';
import {
  CUSTOM_FIELD_LIST,
  CUSTOM_FIELD_MULTI,
  CUSTOM_STORAGE_MATRIX,
  CUSTOM_STORAGE_MAP,
  CUSTOM_FIELD_ENTITY
} from '../../../constants/generic';
import { sortFieldsByGui } from '../../../utils/gui-transformer';

import Tabs from '../../Globals/Tabs';
import Row from '../../Globals/Row';

const styles = ({ spacing }) => ({
  section: {
    width: 'auto'
  },
  row: {
    padding: `0 ${spacing.unit * 3}px`,
    gridTemplateColumns: 'repeat(auto-fill, minmax(220px, auto))',
    width: 'inherit'
  },
  formGroupContainer: {
    display: 'inherit'
  },
  multiValueContainer: {
    gridColumn: 'span 3',
    width: 'inherit'
  },
  formGroupRow: {
    display: 'flex',
    flexDirection: 'column',
    margin: `${spacing.unit}px 0`,
    width: 'inherit'
  },
  formGroupTitle: {
    padding: `${spacing.unit * 4}px ${spacing.unit * 2}px 0`,
    textTransform: ' uppercase',
    letterSpacing: '.0875rem',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    fontSize: '0.875rem'
  },
  fieldTitle: {
    padding: `${spacing.unit * 4}px 0 0`,
    fontSize: '0.875rem'
  },
  expandInput: {
    flex: 1
  }
});

const CustomFieldsSection = ({
  classes,
  className,
  translate,
  cfPath,
  extraConfiguration = {},
  isMandatory = false,
  fields,
  formID
}) => {
  const [selectedTab, selectTab] = useState(0);

  const tabs = useMemo(
    () => {
      const sortedTabs = sortFieldsByGui(fields);
      const filteredTabs = filterEmptyItems(sortedTabs);

      return filteredTabs;
    },
    [fields]
  );

  if (tabs.length === 0) {
    return null;
  }

  const handleSelectTab = (event, value) => {
    selectTab(value);
  };

  const renderCustomFields = (items = []) =>
    items.map((group, groupKey) => (
      <div className={classes.section} key={groupKey}>
        {(group.fieldGroup || group.description) && (
          <Typography
            variant="subheading"
            noWrap
            component="h2"
            classes={{
              root: classes.formGroupTitle
            }}
          >
            {group.fieldGroup || group.description}
          </Typography>
        )}
        <Row className={classes.row} grid>
          {group.items.map((field = {}, fieldKey) => {
            const { code: cfCode, description } = field;
            const fieldExtraConfiguration = extraConfiguration[cfCode];
            const { referenceField: referenceFieldOverride } =
              fieldExtraConfiguration || {};

            const fieldType = get(
              field,
              'fieldType',
              getFieldTypeByValueType(field)
            );

            const storageType = get(field, 'storageType');

            const isMatrixType =
              fieldType === CUSTOM_FIELD_MULTI ||
              storageType === CUSTOM_STORAGE_MATRIX ||
              storageType === CUSTOM_STORAGE_MAP;

            const source = isUndefined(cfPath)
              ? `customFields.${cfCode}`
              : `${cfPath}.customFields.${cfCode}`;

            const disabled =
              typeof field.allowEdit === 'boolean' && !field.allowEdit;

            return (
              <div
                className={cx(
                  classes.formGroupContainer,
                  isMatrixType
                    ? classes.multiValueContainer
                    : classes.formGroupRow
                )}
                key={`${groupKey}_${fieldKey}`}
              >
                {isMatrixType && description && (
                  <Typography
                    variant="subheading"
                    noWrap
                    component="h5"
                    classes={{
                      root: classes.fieldTitle
                    }}
                  >
                    {description}
                  </Typography>
                )}

                <InputRenderer
                  className={classes.expandInput}
                  cfField={field}
                  source={source}
                  label={field.description || cfCode}
                  type={isMatrixType ? CUSTOM_FIELD_MULTI : fieldType}
                  storageType={storageType}
                  choices={
                    field.fieldType === CUSTOM_FIELD_LIST
                      ? extractCustomFieldChoices(field)
                      : []
                  }
                  validate={extractValidators(field, isMandatory)}
                  disabled={disabled}
                  reference={
                    fieldType === CUSTOM_FIELD_ENTITY
                      ? get(field, 'code', undefined)
                      : null
                  }
                  referenceField={
                    fieldType === CUSTOM_FIELD_ENTITY
                      ? referenceFieldOverride || 'code'
                      : null
                  }
                  id={`${formID}--${source}`}
                />
              </div>
            );
          })}
        </Row>
      </div>
    ));

  const currentFields = get(tabs[selectedTab], 'items', []);

  return (
    <div className={classes.cfWrapper}>
      {tabs.length > 1 && (
        <Tabs
          tabs={tabs.map(({ tab: label, icon, color }, id) => ({
            id,
            label,
            icon,
            color
          }))}
          selectedTab={selectedTab}
          onChange={handleSelectTab}
          wrapperID={formID}
        />
      )}
      {renderCustomFields(currentFields)}
    </div>
  );
};

CustomFieldsSection.propTypes = {
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  translate: PropTypes.func.isRequired,
  cfPath: PropTypes.string,
  extraConfiguration: PropTypes.object,
  fields: PropTypes.array.isRequired,
  isMandatory: PropTypes.bool,
  formID: PropTypes.string
};

export default compose(
  withTranslate,
  withStyles(styles)
)(CustomFieldsSection);
