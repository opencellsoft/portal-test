import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { addField } from 'react-admin';
import {
  FormGroup,
  FormControlLabel,
  Switch,
  withStyles
} from '@material-ui/core';
import { isEmpty } from 'lodash';
import { compose } from 'recompose';

import sanitizeRestProps from '../sanitizeRestProps';
import FieldTitle from '../FieldTitle';

const styles = () => ({
  wrapper: {
    justifyContent: 'center'
  }
});

export class BooleanInput extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    id: PropTypes.string,
    input: PropTypes.object,
    isRequired: PropTypes.bool,
    label: PropTypes.string,
    resource: PropTypes.string,
    source: PropTypes.string,
    options: PropTypes.object,
    choices: PropTypes.object,
    defaultValue: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
  };

  static defaultProps = {
    options: {},
    choices: []
  };

  componentDidMount = () => {
    const { input, defaultValue, choices } = this.props;
    const { value } = input;

    const initialValue =
      typeof defaultValue !== 'undefined'
        ? defaultValue
        : !isEmpty(choices) && Array.isArray(choices)
        ? choices[0]
        : false;

    if (!value) {
      input.onChange(initialValue);
    }
  };

  handleChange = (event, value) => {
    const { choices, input } = this.props;
    const nextValue =
      Array.isArray(choices) && !isEmpty(choices)
        ? choices[Number(value)]
        : value;

    return input.onChange(nextValue);
  };

  isChecked = (value) => {
    const { choices } = this.props;

    switch (true) {
      case Array.isArray(choices) && !isEmpty(choices):
        if (choices.findIndex((choice) => choice === value) === 1) {
          return true;
        }

        if (value === 'true') {
          return true;
        }

        return false;
      case typeof value === 'boolean' && value:
      case typeof value === 'string' && !!value && value !== 'false':
        return true;
      default:
        return false;
    }
  };

  render = () => {
    const {
      className,
      id,
      input,
      isRequired,
      label,
      source,
      resource,
      classes,
      options,
      choices,
      fullWidth,
      ...rest
    } = this.props;

    const { value, onBlur, ...inputProps } = input;

    return (
      <FormGroup
        className={cx(classes.wrapper, className)}
        {...sanitizeRestProps(rest)}
      >
        <FormControlLabel
          htmlFor={id}
          control={
            <Switch
              id={id}
              color="primary"
              checked={this.isChecked(value)}
              {...inputProps}
              {...options}
              onChange={this.handleChange}
              value={value}
            />
          }
          label={
            <FieldTitle
              label={label}
              source={source}
              resource={resource}
              isRequired={isRequired}
            />
          }
        />
      </FormGroup>
    );
  };
}

export default compose(
  addField,
  withStyles(styles)
)(BooleanInput);
