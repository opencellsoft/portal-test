import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { addField, ResettableTextField } from 'react-admin';
import { useCfInitialValue } from '../../../../utils/hooks/useCFInitialValue';
import FieldTitle from '../FieldTitle';

import sanitizeRestProps from '../sanitizeRestProps';

/**
 * An Input component for a string
 *
 * @example
 * <TextInput source="first_name" />
 *
 * You can customize the `type` props (which defaults to "text").
 * Note that, due to a React bug, you should use `<NumberField>` instead of using type="number".
 * @example
 * <TextInput source="email" type="email" />
 * <NumberInput source="nb_views" />
 *
 * The object passed as `options` props is passed to the <ResettableTextField> component
 */
const TextInput = ({
  input,
  filter,
  className,
  classes,
  isRequired,
  label,
  meta,
  options,
  resource,
  source,
  type,
  onBlur,
  onFocus,
  onChange,
  field = {},
  ...rest
}) => {
  useCfInitialValue({ input, field });

  const [displayValue, setDisplayValue] = useState(input.value);

  useEffect(
    () => {
      const { value } = input;

      if (typeof filter === 'object' && value) {
        const newDisplayValue = value.startsWith('@filterKey')
          ? value
              .split(':')
              .filter((e, i) => i !== 0)
              .join(':')
              .slice(1, -1)
          : value.replace(/\*/g, '');
        setDisplayValue(newDisplayValue);
      }
    },
    [input]
  );

  const handleValue = (eventOrValue) => {
    const { value } = eventOrValue.target;

    setDisplayValue(value);
    if (typeof filter !== 'object') {
      return eventOrValue;
    }

    const { wildcard, filterKey = '' } = filter;

    return wildcard
      ? value
        ? `${filterKey && `@filterKey[${filterKey}]:`}*${value}*`
        : value
      : eventOrValue;
  };

  const handleBlur = useCallback(
    (eventOrValue) => {
      onBlur(handleValue(eventOrValue));
      input.onBlur(handleValue(eventOrValue));
    },
    [input]
  );

  const handleFocus = useCallback(
    (event) => {
      onFocus(handleValue(event));
      input.onFocus(handleValue(event));
    },
    [input]
  );

  const handleChange = useCallback(
    (eventOrValue) => {
      onChange(handleValue(eventOrValue));
      input.onChange(handleValue(eventOrValue));
    },
    [input]
  );

  if (typeof meta === 'undefined') {
    throw new Error(
      "The TextInput component wasn't called within a redux-form <Field>. Did you decorate it and forget to add the addField prop to your component? See https://marmelab.com/react-admin/Inputs.html#writing-your-own-input-component for details."
    );
  }
  const { touched, error } = meta;

  return (
    <ResettableTextField
      margin="normal"
      type={type}
      label={
        label === false ? (
          label
        ) : (
          <FieldTitle
            label={label}
            source={source}
            resource={resource}
            isRequired={isRequired}
          />
        )
      }
      error={!!(touched && error)}
      helperText={touched && error}
      className={className}
      {...options}
      {...sanitizeRestProps(rest)}
      {...input}
      onBlur={handleBlur}
      onFocus={handleFocus}
      onChange={handleChange}
      value={displayValue}
    />
  );
};

TextInput.propTypes = {
  className: PropTypes.string,
  input: PropTypes.object,
  isRequired: PropTypes.bool,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
    PropTypes.bool
  ]),
  meta: PropTypes.object,
  name: PropTypes.string,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  onFocus: PropTypes.func,
  wildcard: PropTypes.bool,
  options: PropTypes.object,
  resource: PropTypes.string,
  source: PropTypes.string,
  type: PropTypes.string,
  field: PropTypes.object.isRequired
};

TextInput.defaultProps = {
  onBlur: () => {},
  onChange: () => {},
  onFocus: () => {},
  options: {},
  type: 'text'
};

export default addField(TextInput);
