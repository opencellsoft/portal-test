import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { addField } from 'react-admin';
import { DatePicker } from 'material-ui-pickers';
import moment from 'moment-timezone';
import { useCfInitialValue } from '../../../../utils/hooks/useCFInitialValue';
import FieldTitle from '../FieldTitle';

const DateInput = ({
  meta,
  input = {},
  dateFormat,
  isRequired,
  label,
  source,
  resource,
  style,
  field,
  disabled
}) => {
  useCfInitialValue({ field, input });

  const handleDateChange = useCallback(
    (selectedDate) => {
      const nextValue = selectedDate
        ? dateFormat
          ? moment(selectedDate).format(dateFormat)
          : moment(selectedDate).valueOf()
        : null;

      return input.onChange(nextValue);
    },
    [input]
  );

  if (typeof meta === 'undefined') {
    throw new Error(
      "The DateInput component wasn't called within a redux-form <Field>. Did you decorate it and forget to add the addField prop to your component?"
    );
  }

  const dateToParse =
    typeof input.value === 'number'
      ? input.value
      : /^\d+$/.test(input.value)
      ? Number(input.value)
      : input.value;
  const safeDateFormat =
    typeof formattedDate === 'number' ? undefined : dateFormat;

  const { touched, error } = meta;
  const value =
    input.value && dateToParse !== 'null'
      ? moment(dateToParse, safeDateFormat)
      : null;

  return (
    <DatePicker
      margin="normal"
      format="DD/MM/YYYY"
      error={!!(touched && error)}
      helperText={touched && error}
      label={
        <FieldTitle
          label={label}
          source={source}
          resource={resource}
          isRequired={isRequired}
        />
      }
      value={value}
      onChange={handleDateChange}
      clearable
      style={style}
      disabled={disabled}
    />
  );
};

DateInput.propTypes = {
  resource: PropTypes.string,
  input: PropTypes.object,
  dateFormat: PropTypes.string,
  source: PropTypes.string.isRequired,
  isRequired: PropTypes.bool,
  meta: PropTypes.object,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  style: PropTypes.object,
  field: PropTypes.object.isRequired,
  disabled: PropTypes.bool
};

export default addField(DateInput);
