import { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { createSelector } from 'reselect';
import { debounce, isEqual, get, isEmpty, isString } from 'lodash';
import {
  crudGetManyAccumulate as crudGetManyAccumulateAction,
  crudGetMatchingAccumulate as crudGetMatchingAccumulateAction,
  getPossibleReferences,
  getPossibleReferenceValues,
  getReferenceResource
} from 'react-admin';

import { getStatusForInput as getDataStatus } from './referenceDataStatus';

import { processParams } from '../../../../../utils/generic-render';
import withTranslate from '../../../../Hoc/withTranslate';

const defaultReferenceSource = (resource: string, source: string) =>
  `${resource}@${source}`;

/**
 * An Input component for choosing a reference record. Useful for foreign keys.
 *
 * This component fetches the possible values in the reference resource
 * (using the `CRUD_GET_MATCHING` REST method), then delegates rendering
 * to a subcomponent, to which it passes the possible choices
 * as the `choices` attribute.
 *
 * Use it with a selector component as child, like `<AutocompleteInput>`,
 * `<SelectInput>`, or `<RadioButtonGroupInput>`.
 *
 * @example
 * export const CommentEdit = (props) => (
 *     <Edit {...props}>
 *         <SimpleForm>
 *             <ReferenceInput label="Post" source="post_id" reference="posts">
 *                 <AutocompleteInput optionText="title" />
 *             </ReferenceInput>
 *         </SimpleForm>
 *     </Edit>
 * );
 *
 * @example
 * export const CommentEdit = (props) => (
 *     <Edit {...props}>
 *         <SimpleForm>
 *             <ReferenceInput label="Post" source="post_id" reference="posts">
 *                 <SelectInput optionText="title" />
 *             </ReferenceInput>
 *         </SimpleForm>
 *     </Edit>
 * );
 *
 * By default, restricts the possible values to 25. You can extend this limit
 * by setting the `perPage` prop.
 *
 * @example
 * <ReferenceInput
 *      source="post_id"
 *      reference="posts"
 *      perPage={100}>
 *     <SelectInput optionText="title" />
 * </ReferenceInput>
 *
 * By default, orders the possible values by id desc. You can change this order
 * by setting the `sort` prop (an object with `field` and `order` properties).
 *
 * @example
 * <ReferenceInput
 *      source="post_id"
 *      reference="posts"
 *      sort={{ field: 'title', order: 'ASC' }}>
 *     <SelectInput optionText="title" />
 * </ReferenceInput>
 *
 * Also, you can filter the query used to populate the possible values. Use the
 * `filter` prop for that.
 *
 * @example
 * <ReferenceInput
 *      source="post_id"
 *      reference="posts"
 *      filter={{ is_published: true }}>
 *     <SelectInput optionText="title" />
 * </ReferenceInput>
 *
 * The enclosed component may filter results. ReferenceInput passes a `setFilter`
 * function as prop to its child component. It uses the value to create a filter
 * for the query - by default { q: [searchText] }. You can customize the mapping
 * searchText => searchQuery by setting a custom `filterToQuery` function prop:
 *
 * @example
 * <ReferenceInput
 *      source="post_id"
 *      reference="posts"
 *      filterToQuery={searchText => ({ title: searchText })}>
 *     <SelectInput optionText="title" />
 * </ReferenceInput>
 */
export class UnconnectedReferenceInputController extends PureComponent {
  static defaultProps = {
    filter: {},
    filterToQuery: (searchText, referenceField, filterType) => {
      const key = filterType
        ? `${filterType} ${referenceField}`
        : referenceField;

      return {
        [key]: searchText
      };
    },
    matchingReferences: null,
    perPage: 25,
    sort: { field: 'id', order: 'DESC' },
    referenceRecord: null
  };

  static propTypes = {
    translate: PropTypes.func.isRequired,
    perPage: PropTypes.number,
    sort: PropTypes.string,
    filter: PropTypes.object,
    filterToQuery: PropTypes.func,
    matchingReferences: PropTypes.array,
    referenceRecord: PropTypes.object,
    record: PropTypes.object,
    input: PropTypes.object,
    onChange: PropTypes.func.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    formValues: PropTypes.object,
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.array]),
    allowWildcard: PropTypes.bool,
    referenceField: PropTypes.string.isRequired,
    uniqueSource: PropTypes.bool,
    filterType: PropTypes.string.isRequired
  };

  lastProcessedFilters = null;

  shouldRefreshOptions = false;

  constructor(props) {
    super(props);
    const { perPage, sort, filter } = props;
    this.state = { pagination: { page: 1, perPage }, sort, filter };
    this.debouncedSetFilter = debounce(this.setFilter.bind(this), 500);
  }

  componentDidMount = () => this.fetchReferenceAndOptions(this.props);

  componentWillReceiveProps = (nextProps) => {
    const {
      filter: nextFilterFromProps,
      record: nextRecord,
      formValues: nextFormValues
    } = nextProps;
    const { filter } = this.state;

    if (!isEmpty(nextFilterFromProps)) {
      try {
        const processedFilters = processParams({
          params: {
            ...nextFilterFromProps,
            ...filter
          },
          source: { record: nextRecord, form: nextFormValues }
        });

        if (!isEqual(this.lastProcessedFilters, processedFilters)) {
          this.lastProcessedFilters = processedFilters;
          this.shouldRefreshOptions = true;

          this.props.input.onChange('');
        } else {
          this.shouldRefreshOptions = false;
        }
      } catch (error) {
        this.shouldRefreshOptions = false;
      }
    }

    if (
      (this.props.record || { id: undefined }).id !==
        (nextProps.record || { id: undefined }).id ||
      this.shouldRefreshOptions
    ) {
      this.fetchReferenceAndOptions(nextProps);
    } else if (this.props.input.value !== nextProps.input.value) {
      this.fetchReference(nextProps);
    } else if (
      !isEqual(nextProps.filter, this.props.filter) ||
      !isEqual(nextProps.sort, this.props.sort) ||
      nextProps.perPage !== this.props.perPage
    ) {
      this.setState(
        (state) => ({
          filter: nextProps.filter,
          pagination: {
            ...state.pagination,
            perPage: nextProps.perPage
          },
          sort: nextProps.sort
        }),
        this.fetchOptions
      );
    }
  };

  setFilter = (filter) => {
    const { referenceField, filterType } = this.props;

    if (filter !== this.state.filter) {
      this.setState(
        {
          filter: this.props.filterToQuery(filter, referenceField, filterType)
        },
        this.fetchOptions
      );
    }
  };

  setPagination = (pagination) => {
    if (pagination !== this.state.pagination) {
      this.setState({ pagination }, this.fetchOptions);
    }
  };

  setSort = (sort) => {
    if (sort !== this.state.sort) {
      this.setState({ sort }, this.fetchOptions);
    }
  };

  fetchReference = (props = this.props) => {
    const {
      crudGetManyAccumulate,
      input,
      reference,
      referenceRecord,
      withReferenceRecord
    } = props;
    const id = input.value;

    if (id && isEmpty(referenceRecord) && withReferenceRecord) {
      crudGetManyAccumulate(reference, [id]);
    }
  };

  fetchOptions = (props = this.props) => {
    const {
      crudGetMatchingAccumulate,
      filter: filterFromProps,
      reference,
      referenceSource,
      resource,
      source,
      record = {},
      formValues = {}
    } = props;
    const { pagination, sort, filter } = this.state;

    try {
      const nextFilters = processParams({
        params: {
          ...filterFromProps,
          ...filter
        },
        source: {
          record,
          form: formValues,
          selectedItems: Object.values(record)
        }
      });

      crudGetMatchingAccumulate(
        reference,
        referenceSource(resource, source),
        pagination,
        sort,
        nextFilters
      );
    } catch (error) {
      console.error(error);
    }
  };

  fetchReferenceAndOptions = (props) => {
    this.fetchReference(props);
    this.fetchOptions(props);
  };

  removeDuplicates = (choices, source) => {
    const unique = {};
    choices.forEach((choice) => {
      const element = get(choice, source);
      if (!unique[element]) {
        unique[element] = choice;
      }
    });

    return Object.values(unique);
  };

  render = () => {
    const {
      input,
      referenceField,
      uniqueSource,
      referenceRecord,
      matchingReferences,
      onChange,
      children,
      allowWildcard,
      translate
    } = this.props;
    const { pagination, sort, filter } = this.state;

    const dataStatus = getDataStatus({
      input,
      matchingReferences: this.shouldRefreshOptions ? null : matchingReferences,
      referenceRecord,
      allowWildcard,
      translate
    });
    const choices = uniqueSource
      ? this.removeDuplicates(dataStatus.choices, referenceField)
      : dataStatus.choices;

    return children({
      choices,
      error: dataStatus.error,
      isLoading: dataStatus.waiting,
      onChange,
      filter,
      setFilter: this.debouncedSetFilter,
      pagination,
      setPagination: this.setPagination,
      sort,
      setSort: this.setSort,
      warning: dataStatus.warning
    });
  };
}

const sanitizeInputId = (inputId) => {
  // inputId is number in some cases
  if (isString(inputId) && inputId.includes('@filterKey')) {
    const value = inputId.split(']:')[1] || '';

    return value.replace(/\*/g, '');
  }

  return inputId;
};

const makeMapStateToProps = () =>
  createSelector(
    [
      getReferenceResource,
      getPossibleReferenceValues,
      (_, props) => props.input.value
    ],
    (referenceState, possibleValues, inputId) => {
      const sanitizedInputId = sanitizeInputId(inputId);

      return {
        matchingReferences: getPossibleReferences(
          referenceState,
          possibleValues,
          [sanitizedInputId]
        ),
        referenceRecord: referenceState && referenceState.data[sanitizedInputId]
      };
    }
  );

const ReferenceInputController = compose(
  withTranslate,
  connect(
    makeMapStateToProps(),
    {
      crudGetManyAccumulate: crudGetManyAccumulateAction,
      crudGetMatchingAccumulate: crudGetMatchingAccumulateAction
    }
  )
)(UnconnectedReferenceInputController);

ReferenceInputController.defaultProps = {
  referenceSource: defaultReferenceSource // used in makeMapStateToProps
};

export default ReferenceInputController;
