import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { REDUX_FORM_NAME, DisabledInput, withDataProvider } from 'react-admin';
import { change, getFormValues } from 'redux-form';
import { get } from 'lodash';
import { compose } from 'recompose';

import { processParams } from '../../../../utils/generic-render';

import ReferenceInput from './ReferenceInput';

class ConditionalInput extends PureComponent {
  static propTypes = {
    dependentField: PropTypes.any,
    source: PropTypes.string.isRequired,
    dispatch: PropTypes.func.isRequired,
    isEdit: PropTypes.bool,
    disabledOnEdit: PropTypes.any,
    record: PropTypes.object,
    dataProvider: PropTypes.func,
    reference: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.array]).isRequired
  };

  state = {
    disabled: false
  };

  componentDidUpdate = ({ dependentField: prevDependantField }) => {
    const { dispatch, source, dependentField, form } = this.props;

    if (!dependentField && prevDependantField) {
      dispatch(change(form || REDUX_FORM_NAME, source, ''));
    }
  };

  componentDidMount = () => {
    const { disabledOnEdit, isEdit, record } = this.props;

    if (typeof disabledOnEdit === 'object' && isEdit && record) {
      this.fetchApi();
    }
  };

  fetchApi = () => {
    const { dataProvider, disabledOnEdit, record } = this.props;

    const { verb, params, resource } = disabledOnEdit || {};

    dataProvider(verb, resource, {
      data: {
        ...processParams({ params, source: { record } })
      }
    })
      .then(() => {
        this.setState({ disabled: false });
      })
      .catch(() => {
        this.setState({ disabled: true });
      });
  };

  render = () => {
    const { dependentField, children, source, reference } = this.props;
    const { disabled } = this.state;

    if (disabled) {
      return <DisabledInput source={source} />;
    }

    return dependentField ? (
      reference ? (
        <ReferenceInput resource={reference} {...this.props} />
      ) : (
        children
      )
    ) : null;
  };
}

export const mapStateToProps = (
  state,
  { dependsOn = {}, reference, record = {}, form }
) => {
  const dependentValues = {};
  const formValues = getFormValues(form || REDUX_FORM_NAME)(state) || record;

  const dependentField = Object.keys(dependsOn).reduce((res, key) => {
    const [formField, referenceField] = key.split(':');
    const expectedValue = dependsOn[key];
    let referenceValue;

    if (formField.indexOf('||') < 0) {
      referenceValue = get(formValues, formField);
    } else {
      const [firstPart, secondPart] = formField.split('||');
      referenceValue =
        get(formValues, firstPart) || get(formValues, secondPart);
    }

    const dependentValue =
      reference && referenceValue && referenceField
        ? get(
            state,
            `admin.resources.${reference}.data.${referenceValue}.${referenceField}`
          )
        : referenceValue;

    dependentValues[key] =
      typeof dependentValue === 'undefined' ? null : dependentValue;

    const iterationResult = computeIterationResult(
      expectedValue,
      dependentValue,
      referenceField
    );

    return res && iterationResult;
  }, true);

  return { dependentField, dependentValues, formValues };
};

const computeIterationResult = (
  expectedValue,
  dependentField,
  referenceField
) => {
  switch (true) {
    case expectedValue === '$__NOT_NULL':
      return !!dependentField;
    case typeof expectedValue === 'string' && expectedValue.startsWith('!'):
      //remove '!' from string
      const expectedValueToCompare = expectedValue.slice(1);

      return dependentField !== expectedValueToCompare;
    default:
      return dependentField === expectedValue;
  }
};

export default compose(
  connect(mapStateToProps),
  withDataProvider
)(ConditionalInput);
