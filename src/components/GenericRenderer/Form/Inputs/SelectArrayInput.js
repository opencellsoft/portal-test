import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { get, isArray } from 'lodash';
import Select from '@material-ui/core/Select';
import {
  MenuItem,
  InputLabel,
  Input,
  FormHelperText,
  FormControl,
  Chip
} from '@material-ui/core';

import { withStyles, createStyles } from '@material-ui/core/styles';
import compose from 'recompose/compose';
import classnames from 'classnames';
import { addField } from 'react-admin';
import { useCfInitialValue } from '../../../../utils/hooks/useCFInitialValue';
import FieldTitle from '../FieldTitle';

import withTranslate from '../../../Hoc/withTranslate';

const sanitizeRestProps = ({
  addLabel,
  allowEmpty,
  allowWildcard,
  basePath,
  choices,
  className,
  component,
  crudGetMatching,
  crudGetOne,
  defaultValue,
  filter,
  filterToQuery,
  formClassName,
  initializeForm,
  input,
  isRequired,
  label,
  limitChoicesToValue,
  locale,
  meta,
  onChange,
  options,
  optionValue,
  optionText,
  perPage,
  record,
  reference,
  resource,
  setFilter,
  setPagination,
  setSort,
  sort,
  source,
  textAlign,
  translate,
  translateChoice,
  validation,
  InputLabelProps,
  ...rest
}) => rest;

const styles = (theme) =>
  createStyles({
    root: {},
    chips: {
      display: 'flex',
      flexWrap: 'wrap'
    },
    chip: {
      margin: theme.spacing.unit / 4
    },
    select: {
      height: 'auto',
      overflow: 'auto'
    }
  });

const SelectArrayInput = ({
  choices,
  classes,
  className,
  isRequired,
  label,
  meta,
  options,
  resource,
  source,
  optionText,
  optionValue,
  input,
  translate,
  translateChoice,
  field,
  ...rest
}) => {
  useCfInitialValue({ field, input });

  const handleChange = useCallback(
    (event) => {
      input.onChange(event.target.value);
      // HACK: For some reason, redux-form does not consider this input touched without calling onBlur manually
      input.onBlur(event.target.value);
    },
    [input]
  );

  const renderMenuItemOption = (choice) => {
    if (React.isValidElement(optionText))
      return React.cloneElement(optionText, {
        record: choice
      });

    const choiceName =
      typeof optionText === 'function'
        ? optionText(choice)
        : get(choice, optionText);

    return translateChoice
      ? translate(choiceName, { _: choiceName })
      : choiceName;
  };

  const renderMenuItem = (choice) => (
    <MenuItem key={get(choice, optionValue)} value={get(choice, optionValue)}>
      {renderMenuItemOption(choice)}
    </MenuItem>
  );

  if (typeof meta === 'undefined') {
    throw new Error(
      "The SelectInput component wasn't called within a redux-form <Field>. Did you decorate it and forget to add the addField prop to your component? See https://marmelab.com/react-admin/Inputs.html#writing-your-own-input-component for details."
    );
  }
  const { touched, error, helperText = false } = meta;

  const value = isArray(input.value) ? input.value : [];

  return (
    <FormControl
      margin="normal"
      className={classnames(classes.root, className)}
      error={!!(touched && error)}
      {...sanitizeRestProps(rest)}
    >
      <InputLabel htmlFor={source}>
        <FieldTitle
          label={label}
          source={source}
          resource={resource}
          isRequired={isRequired}
        />
      </InputLabel>
      <Select
        autoWidth
        multiple
        input={<Input id={source} />}
        value={value}
        error={!!(touched && error)}
        renderValue={(selected) => (
          <div className={classes.chips}>
            {isArray(selected) &&
              selected
                .map((item) =>
                  choices.find((choice) => get(choice, optionValue) === item)
                )
                .map((item) => (
                  <Chip
                    key={get(item, optionValue)}
                    label={renderMenuItemOption(item)}
                    className={classes.chip}
                  />
                ))}
          </div>
        )}
        data-testid="selectArray"
        {...options}
        onChange={handleChange}
      >
        {choices.map(renderMenuItem)}
      </Select>
      {touched && error && <FormHelperText error>{error}</FormHelperText>}
      {helperText && <FormHelperText>{helperText}</FormHelperText>}
    </FormControl>
  );
};

SelectArrayInput.propTypes = {
  choices: PropTypes.arrayOf(PropTypes.object),
  classes: PropTypes.object,
  className: PropTypes.string,
  children: PropTypes.node,
  input: PropTypes.object,
  isRequired: PropTypes.bool,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  meta: PropTypes.object,
  options: PropTypes.object,
  optionText: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.func,
    PropTypes.element
  ]),
  optionValue: PropTypes.string,
  resource: PropTypes.string,
  source: PropTypes.string,
  translate: PropTypes.func.isRequired,
  translateChoice: PropTypes.bool,
  field: PropTypes.object
};

SelectArrayInput.defaultProps = {
  classes: {},
  choices: [],
  options: {},
  optionText: 'name',
  optionValue: 'id',
  translateChoice: true,
  field: {}
};

const EnhancedSelectArrayInput = compose(
  addField,
  withTranslate,
  withStyles(styles)
)(SelectArrayInput);

export default EnhancedSelectArrayInput;
