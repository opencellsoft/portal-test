import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { addField } from 'react-admin';
import { TimePicker, DatePicker } from 'material-ui-pickers';
import moment from 'moment-timezone';

class DateTimeInput extends PureComponent {
  static propTypes = {
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    input: PropTypes.object.isRequired,
    dateFormat: PropTypes.string
  };

  handleDateChange = (selectedDate) => {
    const { input, dateFormat } = this.props;
    const nextValue = dateFormat
      ? moment(selectedDate).format(dateFormat)
      : moment(selectedDate).format('DD/MM/YYYY HH:mm:ss');

    return input.onChange(nextValue);
  };

  render = () => {
    const { label, input, dateFormat } = this.props;

    const dateToParse =
      typeof input.value === 'number'
        ? input.value
        : /^\d+$/.test(input.value)
        ? Number(input.value)
        : input.value;
    const safeDateFormat =
      typeof formattedDate === 'number' ? undefined : dateFormat;

    const value = dateFormat
      ? moment(dateToParse, safeDateFormat).toISOString()
      : moment(dateToParse, 'DD/MM/YYYY HH:mm:ss').toISOString();

    return (
      <>
        <DatePicker
          margin="normal"
          label={`${label} date`}
          value={value}
          onChange={this.handleDateChange}
        />
        <TimePicker
          margin="normal"
          label={`${label} time`}
          value={value}
          onChange={this.handleDateChange}
        />
      </>
    );
  };
}

export default addField(DateTimeInput);
