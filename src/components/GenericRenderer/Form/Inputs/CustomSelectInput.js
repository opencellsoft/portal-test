import React, { useState, useMemo, useCallback, useEffect, memo } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles, createStyles } from '@material-ui/core/styles';
import {
  withDataProvider,
  refreshView as refreshViewAction,
  showNotification as showNotificationAction,
  addField,
  LinearProgress,
  Labeled
} from 'react-admin';

import { compose } from 'recompose';
import { MenuItem, TextField } from '@material-ui/core';
import { get, capitalize } from 'lodash';
import FieldTitle from '../FieldTitle';

const styles = (theme) =>
  createStyles({
    input: {
      minWidth: theme.spacing.unit * 20
    }
  });

const CustomSelectInput = memo(
  ({
    classes,
    resource,
    source,
    record,
    dataProvider,
    verb,
    refreshView,
    showNotification,
    label,
    container,
    target,
    input,
    isRequired,
    className,
    meta,
    isLoading,
    ...rest
  }) => {
    const [services, setServices] = useState([]);
    const [loading, setLoading] = useState(false);
    const offerTemplate = get(record, 'offerTemplate');

    useEffect(
      () => {
        setLoading(true);
        dataProvider(verb, resource, { id: offerTemplate })
          .then(({ data }) => {
            const parent = get(data, container, []);
            const extractedServices = parent.map((item) =>
              get(item, target, {})
            );
            setServices(extractedServices);
          })
          .then(refreshView)
          .catch((error) => {
            showNotification(capitalize(error.message));
          })
          .finally(() => {
            setLoading(false);
            refreshView();
          });
      },
      [offerTemplate]
    );

    const choices = useMemo(
      () =>
        Object.values(services).map((service) => ({
          id: service.code,
          name: service.description
        })),
      [services]
    );

    const handleChange = useCallback(
      (event) => {
        const nextValue = event.target.value;
        const finalValue = services.find((item) => item.code === nextValue);
        input.onChange(finalValue);
      },
      [services, input]
    );

    const displayValue = get(input.value, 'code', input.value);

    const { touched, error, helperText = false } = meta;

    return (
      <>
        {loading ? (
          <Labeled
            label={label}
            source={source}
            resource={resource}
            className={className}
            isRequired={isRequired}
          >
            <LinearProgress />
          </Labeled>
        ) : (
          <TextField
            select
            label={
              <FieldTitle
                label={label}
                source={source}
                resource={resource}
                isRequired={isRequired}
              />
            }
            className={`${classes.input} ${className}`}
            margin="normal"
            name={input.name}
            error={!!(touched && error)}
            helperText={(touched && error) || helperText}
            clearAlwaysVisible
            value={displayValue}
            onChange={handleChange}
          >
            {choices.map(({ id, name }) => (
              <MenuItem value={id} key={`choices_${id}`}>
                {name}
              </MenuItem>
            ))}
          </TextField>
        )}
      </>
    );
  }
);

CustomSelectInput.propTypes = {
  classes: PropTypes.object,
  source: PropTypes.string,
  resource: PropTypes.string.isRequired,
  record: PropTypes.object,
  dataProvider: PropTypes.func,
  refreshView: PropTypes.func,
  showNotification: PropTypes.func,
  label: PropTypes.string,
  verb: PropTypes.string,
  container: PropTypes.string,
  target: PropTypes.string,
  input: PropTypes.object,
  isRequired: PropTypes.bool,
  className: PropTypes.string,
  meta: PropTypes.object
};

export default compose(
  connect(
    null,
    {
      refreshView: refreshViewAction,
      showNotification: showNotificationAction
    }
  ),
  addField,
  withDataProvider,
  withStyles(styles)
)(CustomSelectInput);
