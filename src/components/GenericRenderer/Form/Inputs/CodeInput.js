import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { addField } from 'react-admin';
import MonacoEditor from 'react-monaco-editor';
import { FormControl, FormLabel } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';
import FieldTitle from '../FieldTitle';

import sanitizeRestProps from '../sanitizeRestProps';

const styles = ({ palette, spacing, typography }) => ({
  control: {
    minHeight: 250
  },
  label: {
    ...typography.caption,
    marginBottom: spacing.unit
  },
  editor: {
    display: 'flex',
    flex: 1,
    border: `1px solid ${palette.divider}`
  }
});

export class CodeInput extends PureComponent {
  editor = null;

  componentWillUnmount = () =>
    window.removeEventListener('resize', this.handleResize);

  editorDidMount = (editor, monaco) => {
    this.editor = editor;

    window.addEventListener('resize', this.handleResize);
    setTimeout(this.handleResize, 500);
  };

  handleResize = () => {
    if (!this.editor) {
      return;
    }

    this.editor.layout();
  };

  handleChange = (nextValue) => this.props.input.onChange(nextValue);

  render = () => {
    const {
      classes,
      className,
      input,
      isRequired,
      label,
      source,
      resource,
      defaultValue,
      options,
      ...rest
    } = this.props;

    const { value } = input;

    return (
      <FormControl
        className={cx(classes.control, className)}
        {...sanitizeRestProps(rest)}
        margin="normal"
      >
        <FormLabel className={classes.label}>
          <FieldTitle
            label={label}
            source={source}
            resource={resource}
            isRequired={isRequired}
          />
        </FormLabel>

        <div className={classes.editor}>
          <MonacoEditor
            height="inherit"
            width="100%"
            language="xml"
            theme="vs-light"
            value={value}
            defaultValue={defaultValue}
            options={options}
            onChange={this.handleChange}
            editorDidMount={this.editorDidMount}
          />
        </div>
      </FormControl>
    );
  };
}

CodeInput.propTypes = {
  className: PropTypes.string,
  input: PropTypes.object,
  defaultValue: PropTypes.string,
  isRequired: PropTypes.bool,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  resource: PropTypes.string,
  source: PropTypes.string,
  options: PropTypes.object
};

CodeInput.defaultProps = {
  options: {
    minimap: {
      enabled: false
    },
    wordWrap: 'bounded'
  }
};

export default compose(
  addField,
  withStyles(styles)
)(CodeInput);
