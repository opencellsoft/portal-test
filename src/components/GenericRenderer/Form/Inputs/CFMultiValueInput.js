import React, { useState, useEffect, useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import { isEmpty, flatten, uniq, findIndex, get } from 'lodash';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';
import { Button as RAButton, addField } from 'react-admin';
import { Button, TextField } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import SimpleTable from '../../Table/SimpleTable';
import Row from '../../../Globals/Row';
import Dialog from '../../../Globals/Dialogs/Dialog';

import {
  extractMatrixData,
  extractFieldsMetas,
  extractValidators,
  extractCFMultiValueConfig,
  getKeyFields,
  isRowExist,
  isValueMatchesPattern
} from '../../../../utils/custom-fields';

import withTranslate from '../../../Hoc/withTranslate';

const styles = ({ spacing }) => ({
  wrapper: {
    padding: `${spacing.unit}px 0`,
    gridColumn: 'span 3'
  },
  textField: {
    margin: spacing.unit
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  row: {
    gridTemplateColumns: 'repeat(3, minmax(220px, auto))'
  }
});

const CFMultiValueInput = ({
  field = {},
  resource,
  classes,
  source,
  input,
  translate
}) => {
  const extractCF = () => {
    if (isEmpty(field)) {
      return {};
    }

    const { mapValue = {}, matrixColumn } = field;

    return extractMatrixData(mapValue, matrixColumn);
  };

  const { data = [], columns } = useMemo(() => extractCF(), [field]);

  const [error, setError] = useState([]);
  const [selectedIds, setSelectedIds] = useState([]);
  const [row, setRow] = useState({});
  const [editRowIndex, setEditRowIndex] = useState(null);
  const [open, setOpen] = useState(false);

  const fieldsWithMetas = useMemo(
    () => extractFieldsMetas(columns, extractCFMultiValueConfig(field) || []),
    [columns, field]
  );

  const validators = useMemo(() => extractValidators(field), [field]);

  const keyFields = useMemo(() => getKeyFields(fieldsWithMetas, field), [
    fieldsWithMetas,
    field
  ]);

  useEffect(
    () => {
      handleChange(data);
    },
    [data]
  );

  const handleModalOpen = useCallback(() => {
    setOpen(true);
  }, []);

  const handleModalClose = useCallback(() => {
    setRow({});
    setEditRowIndex(null);
    setOpen(false);
    setError([]);
  }, []);

  const handleTextInputChange = useCallback((source, e) => {
    const currentRow = { ...row };
    currentRow[source] = e.target.value;
    const { value } = input;
    validate(currentRow, value);
    setRow(currentRow);
  });

  /**
   * Transform toggled element position id to keyField value
   *
   * @param {array} data
   * @param {string} id
   *
   * @return {array} array of keyFields
   */
  const transformPositionIdToKeyField = useCallback(
    (index) =>
      selectedIds.includes(index)
        ? selectedIds.filter((item) => item !== index)
        : [...selectedIds, index],
    [selectedIds]
  );

  const handleToggleItem = useCallback((data, id) => {
    setSelectedIds(transformPositionIdToKeyField(id));
  });

  const handleToggleItems = useCallback((data, ids) => {
    const newIds = ids.map((id) => transformPositionIdToKeyField(id));
    setSelectedIds(uniq(flatten(newIds)));
  });

  const getSelectedIdsCustom = useCallback(
    (data = []) =>
      data.reduce(
        (acc, current, index) =>
          selectedIds.includes(current[keyFields[0]])
            ? [...acc, String(index)]
            : [...acc],
        []
      ),
    [selectedIds]
  );

  const handleAdd = useCallback(
    () => {
      const newRow = { ...row };
      const { value } = input;
      if (validate(newRow, value)) {
        handleChange([...value, newRow]);
        handleModalClose();
      }
    },
    [row]
  );

  const handleDelete = useCallback((keys, data, cbs) => {
    const newData = data.filter((item, key) => !keys.includes(String(key)));

    handleChange([...newData]);
    setSelectedIds([]);
    cbs.forEach((cb) => cb());
  }, []);

  const validate = useCallback(
    (row, value = []) => {
      const errors = [];

      if (isRowExist(row, value, keyFields, editRowIndex) && value.length >= 1)
        errors.push({
          field: keyFields[0],
          message: translate(`validation.unique`)
        });

      validators.forEach((validationData) => {
        const [key] =
          typeof validationData === 'string'
            ? validationData.split(':')
            : [get(validationData, 'type', '')];

        switch (true) {
          case key === 'regExp': {
            const reg = validationData.split(':')[1];
            const isMatched = isValueMatchesPattern(
              get(row, 'value'),
              new RegExp(`^${reg}$`) //eslint-disable-line security/detect-non-literal-regexp
            );

            if (!isMatched) {
              errors.push({
                field: 'value',
                message: translate(`validation.pattern`, {
                  pattern: reg
                })
              });
            }
            break;
          }
          case key === 'maxLength': {
            const maxLength = validationData.split(':')[1];

            Object.entries(row).forEach(([key, value]) => {
              if (value.length > maxLength) {
                errors.push({
                  field: key,
                  message: translate(`validation.maxLength`, { maxLength })
                });
              }
            });

            break;
          }
          default:
            return undefined;
        }

        return undefined;
      });

      fieldsWithMetas.forEach((field) => {
        if (!(row.hasOwnProperty(field.source) && !!row[field.source])) {
          errors.push({
            field: field.source,
            message: translate(`ra.validation.required`)
          });
        }
      });

      setError(errors);

      if (typeof editRowIndex === 'number') return errors.length === 0;

      return (
        !isRowExist(row, value, keyFields, editRowIndex) && errors.length === 0
      );
    },
    [keyFields, validators, editRowIndex]
  );

  const handleChange = useCallback(
    (nextValue) => {
      input.onChange(nextValue);
    },
    [input]
  );

  const handleEditOpen = useCallback(
    (data = [], id) => {
      setRow(data[id]);

      const { value } = input;
      const index = findIndex(value, (row) =>
        keyFields.every((keyField) => row[keyField] === data[id][keyField])
      );

      setEditRowIndex(index);

      handleModalOpen();
    },
    [input, keyFields]
  );

  const handleEdit = useCallback(
    () => {
      const editedRow = { ...row };
      const { value } = input;

      if (validate(editedRow, value)) {
        const editedValue = [...value];
        editedValue[editRowIndex] = editedRow;
        handleChange([...editedValue]);
        handleModalClose();
      }
    },
    [row]
  );

  return (
    <div className={classes.wrapper}>
      <div className={classes.toolbar}>
        <RAButton
          variant="flat"
          color="primary"
          component="button"
          label="ra.action.create"
          onClick={handleModalOpen}
        >
          <AddIcon />
        </RAButton>
      </div>
      <SimpleTable
        fields={fieldsWithMetas}
        data={input.value || []}
        total={get(input.value, 'length', 0)}
        keyField={keyFields[0]}
        title={field.description}
        withPagination
        localSort
        bulkActions
        isCFInputTable
        handleToggleItemCustom={handleToggleItem}
        getSelectedIdsCustom={getSelectedIdsCustom}
        setSelectedIdsCustom={handleToggleItems}
        selectedIdsCustom={selectedIds}
        handleDeleteCustom={handleDelete}
        onRowClick={handleEditOpen}
      />
      <Dialog
        open={open}
        onClose={handleModalClose}
        title={
          typeof editRowIndex === 'number'
            ? translate(`ra.action.edit`)
            : translate(`ra.action.create`)
        }
        maxWidth="md"
        actions={[
          <Button onClick={handleModalClose} color="primary">
            {translate('ra.action.cancel')}
          </Button>,
          <Button
            onClick={typeof editRowIndex === 'number' ? handleEdit : handleAdd}
            color="primary"
          >
            {translate('ra.action.confirm')}
          </Button>
        ]}
      >
        <Row className={classes.row} grid key="dialogRow">
          {fieldsWithMetas.map((field, key) => {
            const fieldError = error.find((el) => el.field === field.source);

            return (
              <TextField
                key={`dialogField-${field.source}-${key}`}
                className={classes.textField}
                label={field.label}
                onChange={(e) => handleTextInputChange(field.source, e)}
                onBlur={(e) => handleTextInputChange(field.source, e)}
                value={row[field.source] || ''}
                required
                error={!!fieldError}
                helperText={fieldError ? fieldError.message : ''}
              />
            );
          })}
        </Row>
      </Dialog>
    </div>
  );
};

CFMultiValueInput.propTypes = {
  input: PropTypes.object,
  field: PropTypes.object.isRequired,
  resource: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
  source: PropTypes.string.isRequired,
  translate: PropTypes.func
};

export default compose(
  withStyles(styles),
  withTranslate,
  addField
)(CFMultiValueInput);
