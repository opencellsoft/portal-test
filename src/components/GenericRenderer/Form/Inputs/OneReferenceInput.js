import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { capitalize, get } from 'lodash';
import TextField from '@material-ui/core/TextField';

import {
  addField,
  FieldTitle,
  withDataProvider,
  refreshView as refreshViewAction,
  showNotification as showNotificationAction
} from 'react-admin';

export const OneReferenceInput = ({
  input,
  label,
  source,
  isRequired,
  type,
  verb,
  referenceField,
  reference,
  dataProvider,
  refreshView,
  showNotification,
  ...rest
}) => {
  useEffect(() => {
    const { value } = input;
    //do only one request here
    if (!value) {
      dataProvider(verb, reference, {})
        .then(({ data }) => {
          const referenceData = get(data, referenceField);
          input.onChange(referenceData);
          refreshView();
        })
        .catch((error) => showNotification(capitalize(error.message)));
    }
  }, []);

  const { value } = input;

  return (
    <TextField
      margin="normal"
      type={type}
      value={value}
      disabled
      label={
        <FieldTitle label={label} source={source} isRequired={isRequired} />
      }
    />
  );
};

OneReferenceInput.propTypes = {
  input: PropTypes.object.isRequired,
  type: PropTypes.string,
  label: PropTypes.string,
  source: PropTypes.string,
  isRequired: PropTypes.bool,
  verb: PropTypes.string,
  referenceField: PropTypes.string,
  reference: PropTypes.string,
  dataProvider: PropTypes.func.isRequired,
  refreshView: PropTypes.func.isRequired,
  showNotification: PropTypes.func.isRequired
};

export default compose(
  connect(
    undefined,
    {
      refreshView: refreshViewAction,
      showNotification: showNotificationAction
    }
  ),
  addField,
  withDataProvider
)(OneReferenceInput);
