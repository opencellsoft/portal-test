import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { addField, DisabledInput, REDUX_FORM_NAME } from 'react-admin';
import { getFormValues } from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { get, isEmpty } from 'lodash';
import FieldTitle from '../FieldTitle';

export class ComputedInput extends PureComponent {
  displayValue = this.props.input.value;

  componentDidUpdate = (prevProps) => {
    const { resultField, referenceData } = this.props;
    if (resultField && prevProps.input.value !== resultField) {
      this.displayValue = resultField;

      return this.setValue(resultField);
    }

    if (referenceData && prevProps.input.value !== referenceData) {
      return this.setValue(referenceData);
    }

    return null;
  };

  setValue = (value) => this.props.input.onChange(value);

  render() {
    const {
      isRequired,
      label,
      resource,
      source,
      type,
      input,
      style
    } = this.props;

    return (
      <DisabledInput
        margin="normal"
        type={type}
        label={
          label === false ? (
            label
          ) : (
            <FieldTitle
              label={label}
              source={source}
              resource={resource}
              isRequired={isRequired}
            />
          )
        }
        {...input}
        defaultValue={this.displayValue}
        style={style}
      />
    );
  }
}

ComputedInput.propTypes = {
  isRequired: PropTypes.bool,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
    PropTypes.bool
  ]),
  resource: PropTypes.string,
  source: PropTypes.string,
  input: PropTypes.object,
  resultField: PropTypes.string,
  referenceData: PropTypes.string,
  type: PropTypes.string,
  style: PropTypes.object
};

ComputedInput.defaultProps = {
  type: 'text'
};

const getValueByReferenceField = (data, state, formData) => {
  const [referenceField, dataField] = data.split(':');
  const referenceValue = get(formData, dataField);

  return get(
    state,
    `admin.resources.${referenceField}.data.${referenceValue}.${dataField}`
  );
};

const getOperand = (operand, state, formData) => {
  const value =
    operand.split(':').length > 1
      ? getValueByReferenceField(operand, state, formData)
      : get(formData, operand);

  return value;
};

const operation = (operandA, operator, operandB) => {
  switch (true) {
    case operator === '%':
      return operandA * `0.${operandB}`;
    case operator === '*':
      return operandA * operandB;
    case operator === '+':
      return operandA + operandB;
    case operator === '-':
      return operandA - operandB;
    case operator === '/':
      return operandA / operandB;
    default:
      return 0;
  }
};

export default compose(
  connect(
    (state, { reference, referenceField, resultOf, dependsOnSource, form }) => {
      const formData = getFormValues(form || REDUX_FORM_NAME)(state);
      const resultField =
        !isEmpty(resultOf) &&
        resultOf.reduce((res, { operandA, operandB, operator }) => {
          const key1 =
            (!!operandA && getOperand(operandA, state, formData)) || res;
          const key2 =
            (!!operandB && getOperand(operandB, state, formData)) || res;

          return operation(key1, operator, key2);
        }, 0);

      if (!isEmpty(dependsOnSource)) {
        const rawFormData = get(formData, dependsOnSource.source);
        const values = get(
          state,
          `admin.resources.${reference}.data[${rawFormData}]`
        );

        const referenceData = get(values, referenceField);

        return { referenceData };
      }

      return { resultField };
    }
  ),
  addField
)(ComputedInput);
