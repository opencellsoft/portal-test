import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { get } from 'lodash';
import { MenuItem } from '@material-ui/core';
import { withStyles, createStyles } from '@material-ui/core/styles';
import compose from 'recompose/compose';
import { addField, ResettableTextField } from 'react-admin';
import { useCfInitialValue } from '../../../../utils/hooks/useCFInitialValue';
import FieldTitle from '../FieldTitle';

import withTranslate from '../../../Hoc/withTranslate';

const sanitizeRestProps = ({
  addLabel,
  allowEmpty,
  emptyValue,
  basePath,
  choices,
  className,
  component,
  crudGetMatching,
  crudGetOne,
  defaultValue,
  filter,
  filterToQuery,
  formClassName,
  initializeForm,
  input,
  isRequired,
  label,
  locale,
  meta,
  onChange,
  options,
  optionValue,
  optionText,
  disableValue,
  perPage,
  record,
  reference,
  resource,
  setFilter,
  setPagination,
  setSort,
  sort,
  source,
  textAlign,
  translate,
  translateChoice,
  validation,
  ...rest
}) => rest;

const styles = (theme) =>
  createStyles({
    input: {
      minWidth: theme.spacing.unit * 20
    }
  });

/**
 * An Input component for a select box, using an array of objects for the options
 *
 * Pass possible options as an array of objects in the 'choices' attribute.
 *
 * By default, the options are built from:
 *  - the 'id' property as the option value,
 *  - the 'name' property an the option text
 **/

const SelectInput = ({
  optionValue,
  disableValue,
  allowEmpty,
  choices,
  classes,
  className,
  input,
  isRequired,
  label,
  meta,
  options,
  resource,
  source,
  optionText,
  translate,
  translateChoice,
  field = {},
  emptyValue,
  disabled,
  ...rest
}) => {
  useCfInitialValue({ field, input });

  const handleChange = useCallback(
    (eventOrValue) => {
      const value = eventOrValue.target
        ? eventOrValue.target.value
        : eventOrValue;
      input.onChange(value);

      // HACK: For some reason, redux-form does not consider this input touched without calling onBlur manually
      input.onBlur(value);
    },
    [input]
  );

  const addAllowEmpty = (choices) => {
    if (allowEmpty) {
      return [<MenuItem value={emptyValue} key="null" />, ...choices];
    }

    return choices;
  };

  const renderMenuItemOption = (choice) => {
    if (React.isValidElement(optionText))
      return React.cloneElement(optionText, {
        record: choice
      });
    const choiceName =
      typeof optionText === 'function'
        ? optionText(choice)
        : get(choice, optionText);

    return translateChoice
      ? translate(choiceName, { _: choiceName })
      : choiceName;
  };

  const renderMenuItem = (choice) => (
    <MenuItem
      key={get(choice, optionValue)}
      value={get(choice, optionValue)}
      disabled={get(choice, disableValue)}
    >
      {renderMenuItemOption(choice)}
    </MenuItem>
  );

  if (typeof meta === 'undefined') {
    throw new Error(
      "The SelectInput component wasn't called within a redux-form <Field>. Did you decorate it and forget to add the addField prop to your component? See https://marmelab.com/react-admin/Inputs.html#writing-your-own-input-component for details."
    );
  }
  const { touched, error, helperText = false } = meta;

  return (
    <ResettableTextField
      select
      label={
        <FieldTitle
          label={label}
          source={source}
          resource={resource}
          isRequired={isRequired}
        />
      }
      margin="normal"
      onChange={handleChange}
      name={input.name}
      className={`${classes.input} ${className}`}
      clearAlwaysVisible
      error={!!(touched && error)}
      helperText={(touched && error) || helperText}
      {...options}
      {...sanitizeRestProps(rest)}
      {...input}
      disabled={disabled}
    >
      {addAllowEmpty(choices.map(renderMenuItem))}
    </ResettableTextField>
  );
};

SelectInput.propTypes = {
  allowEmpty: PropTypes.bool,
  emptyValue: PropTypes.any,
  choices: PropTypes.arrayOf(PropTypes.object),
  classes: PropTypes.object,
  className: PropTypes.string,
  input: PropTypes.object,
  isRequired: PropTypes.bool,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  meta: PropTypes.object,
  options: PropTypes.object,
  optionText: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.func,
    PropTypes.element
  ]),
  optionValue: PropTypes.string,
  disableValue: PropTypes.string,
  resource: PropTypes.string,
  source: PropTypes.string,
  translate: PropTypes.func.isRequired,
  translateChoice: PropTypes.bool,
  disabled: PropTypes.bool
};

SelectInput.defaultProps = {
  allowEmpty: false,
  emptyValue: '',
  classes: {},
  choices: [],
  options: {},
  optionText: 'name',
  optionValue: 'id',
  translateChoice: true,
  disableValue: 'disabled'
};

export default compose(
  addField,
  withStyles(styles),
  withTranslate
)(SelectInput);
