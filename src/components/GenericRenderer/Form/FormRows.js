import React from 'react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import RowRenderer from './RowRenderer';

const FormRows = ({ rows, isEdit, inDrawer, rowClassname, record, formID }) => {
  if (!Array.isArray(rows)) {
    return false;
  }

  return rows.map((row, index) => {
    const items = Array.isArray(row) ? row : get(row, 'items') || [];
    const itemsPerRow = get(row, 'itemsPerRow');

    return (
      <RowRenderer
        key={index}
        items={items}
        itemsPerRow={itemsPerRow}
        isEdit={isEdit}
        inDrawer={inDrawer}
        record={record}
        rowClassname={rowClassname}
        formID={formID}
      />
    );
  });
};

FormRows.propTypes = {
  rows: PropTypes.array.isRequired,
  isEdit: PropTypes.bool,
  rowClassname: PropTypes.string,
  record: PropTypes.object
};

export default FormRows;
