import React, { cloneElement } from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';

import WithEntityCustomization from '../../Hoc/WithEntityCustomization';
import { hydrateUrlFromParams } from '../../../utils/generic-render';

const hydrateRedirect = (redirect, record) => {
  let finalRedirect = redirect;

  if (typeof redirect === 'string' && redirect.includes('@record')) {
    finalRedirect = hydrateUrlFromParams({
      url: redirect,
      params: { record }
    });
  }

  return finalRedirect;
};

const RenderForm = ({
  children,
  source,
  record,
  config,
  fields,
  index,
  redirectAfterSubmit,
  redirectAfterDelete,
  ...props
}) => {
  const { rows, testID } = config || {};
  const { resource } = props;
  let fieldsWithSource;
  if (!isEmpty(fields)) {
    fieldsWithSource = fields.map((field) => ({
      ...field,
      source: `${source}[${index}].${field.source}`
    }));
  }

  const finalRedirect = hydrateRedirect(redirectAfterSubmit, record);
  const finalRedirectAfterDelete = hydrateRedirect(redirectAfterDelete, record);

  return (
    <WithEntityCustomization resource={resource}>
      {cloneElement(children, {
        ...props,
        record,
        redirectAfterSubmit: finalRedirect,
        redirectAfterDelete: finalRedirectAfterDelete,
        config: {
          ...config,
          rows: fieldsWithSource ? [fieldsWithSource] : rows
        },
        id: testID,
        index,
        source
      })}
    </WithEntityCustomization>
  );
};

RenderForm.propTypes = {
  save: PropTypes.func,
  resource: PropTypes.string,
  source: PropTypes.string,
  record: PropTypes.object,
  config: PropTypes.object,
  fields: PropTypes.array,
  index: PropTypes.number,
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.array]),
  redirectAfterSubmit: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  redirectAfterDelete: PropTypes.oneOfType([PropTypes.string, PropTypes.func])
};

export default RenderForm;
