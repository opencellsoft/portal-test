import React, { PureComponent, cloneElement } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  DisabledInput,
  LongTextInput,
  ReferenceArrayInput,
  FileInput,
  FileField,
  REDUX_FORM_NAME
} from 'react-admin';
import { getFormValues, change } from 'redux-form';
import { compose } from 'recompose';
import { get, isEmpty, isArray } from 'lodash';

import { withStyles } from '@material-ui/core/styles';
import PageContext from '../Layout/PageContext';

import {
  FIELD_STATIC_LIST,
  FIELD_REFERENCE_LIST,
  FIELD_LIST,
  FIELD_PASSWORD,
  FIELD_DATE,
  FIELD_DATE_TIME,
  FIELD_NUMBER,
  FIELD_LONG_TEXT,
  FIELD_CODE,
  FIELD_FILE,
  FIELD_BOOLEAN,
  FIELD_AUTOCOMPLETE,
  FIELD_CUSTOM_CHOICES_SELECT,
  FIELD_SELECT_SUBFORM,
  FIELD_ONE_REFERENCE,
  CUSTOM_FIELD_DOUBLE,
  CUSTOM_FIELD_LONG,
  CUSTOM_FIELD_LIST,
  CUSTOM_FIELD_MULTI,
  CUSTOM_FIELD_DATE,
  CUSTOM_FIELD_ENTITY,
  CUSTOM_FIELD_STRING
} from '../../../constants/generic';

import withTranslate from '../../Hoc/withTranslate';

import ConditionalInput from './Inputs/ConditionalInput';
import TextInput from './Inputs/TextInput';
import BooleanInput from './Inputs/BooleanInput';
import CodeInput from './Inputs/CodeInput';
import DateInput from './Inputs/DateInput';
import ReferenceInput from './Inputs/ReferenceInput';
import DateTimeInput from './Inputs/DateTimeInput';
import NumberInput from './Inputs/NumberInput';
import ComputedInput from './Inputs/ComputedInput';
import SelectInput from './Inputs/SelectInput';
import SelectArrayInput from './Inputs/SelectArrayInput';
import CFMultiValueInput from './Inputs/CFMultiValueInput';
import CustomSelectInput from './Inputs/CustomSelectInput';
import WithRoles from '../../Hoc/WithRoles';
import AutocompleteInput from './Inputs/AutocompleteInput';
import OneReferenceInput from './Inputs/OneReferenceInput';
import { getValidatorByKey } from '../../../validators';
import {
  findValidatorType,
  isValidatorActive,
  getDependsOn,
  getDependsOnValues
} from '../../../validators/helpers';
import { extractFormNestedData } from '../../../utils/record';

const styles = ({ spacing }) => ({
  formControl: {
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    width: '260px'
  }
});
class InputRenderer extends PureComponent {
  static contextType = PageContext;

  static propTypes = {
    source: PropTypes.string,
    label: PropTypes.string,
    dependentFields: PropTypes.object,
    translate: PropTypes.func.isRequired
  };

  state = {
    validators: []
  };

  activeValidators = [];

  extractForwardedData = () => {
    const { forwardedSource } = this.props;
    const { data } = this.context;

    if (!isEmpty(data) && typeof forwardedSource !== 'undefined') {
      return forwardedSource.includes('|')
        ? forwardedSource
            .split('|')
            .reduce(
              (res, possibleSource) => res || get(data, possibleSource),
              null
            )
        : get(data, forwardedSource);
    }

    return null;
  };

  componentDidMount = () => {
    const { dispatch, forwardedSource, source, form } = this.props;
    const { data } = this.context;

    if (!isEmpty(data) && typeof forwardedSource !== 'undefined') {
      const inputData = this.extractForwardedData();

      dispatch(change(form || REDUX_FORM_NAME, source, inputData));
    }
  };

  determineInputComponent = ({
    reference,
    referenceField = 'description',
    isEdit,
    disabledOnCreate,
    disabledOnEdit,
    type,
    dateFormat,
    dependsOn,
    allowWildcard,
    choices = [],
    validate,
    cfField = {},
    record,
    disabled,
    disabledReference = false,
    disabledDate = false,
    defaultValue,
    ...rest
  }) => {
    const {
      translate,
      source,
      label,
      resultOf,
      forwardedSource,
      dependsOnSource,
      inForm,
      form,
      defaultValue: propsDefaultValue
    } = this.props;
    const { resource } = this.context;
    const hasForwardedData = !isEmpty(this.extractForwardedData());
    const finalLabel =
      label || translate(`resources.${resource}.fields.${source}`);

    switch (true) {
      case type === FIELD_ONE_REFERENCE:
        return (
          <OneReferenceInput
            {...rest}
            reference={reference}
            referenceField={referenceField}
          />
        );

      case !isEmpty(resultOf) || !isEmpty(dependsOnSource):
        return (
          <ComputedInput
            reference={reference}
            referenceField={referenceField}
            resultOf={resultOf}
            dependsOnSource={dependsOnSource}
            form={form}
          />
        );
      case !hasForwardedData && inForm === false:
      case !isEdit && disabledOnCreate === true:
        return null;
      case disabled:
      case isEdit && disabledOnEdit === true:
      case hasForwardedData && inForm === false:
      case hasForwardedData && forwardedSource !== undefined:
        return <DisabledInput label={finalLabel} />;
      case type === FIELD_REFERENCE_LIST:
      case type === CUSTOM_FIELD_ENTITY &&
        get(cfField, 'storageType') === CUSTOM_FIELD_LIST:
        return (
          <ReferenceArrayInput reference={reference} source="id">
            <SelectArrayInput
              optionText={({ [referenceField]: defaultField, id } = {}) =>
                defaultField || id
              }
              source={referenceField}
              field={cfField}
            />
          </ReferenceArrayInput>
        );
      case !!reference &&
        (!type ||
          type === FIELD_LIST ||
          type === FIELD_AUTOCOMPLETE ||
          type === CUSTOM_FIELD_ENTITY):
        return type === FIELD_AUTOCOMPLETE ? (
          <AutocompleteInput
            optionText={({ [referenceField]: defaultField, id }) =>
              defaultField || id
            }
            source={referenceField}
            options={{
              fullWidth: true,
              autoComplete: 'no',
              label: finalLabel
            }}
          />
        ) : (
          <SelectInput
            optionText={({ [referenceField]: defaultField, id }) =>
              defaultField || id
            }
            required={
              this.activeValidators.includes('required') ||
              (isArray(validate) &&
                validate
                  .map((item) => {
                    const [key] =
                      typeof item === 'string' ? item.split(':') : [];

                    return key;
                  })
                  .filter((item) => item === 'required').length >= 1) ||
              (typeof validate === 'string' && validate === 'required')
            }
            source={referenceField}
            options={{
              label: finalLabel
            }}
            disabled={disabledReference}
          />
        );
      case type === FIELD_STATIC_LIST:
      case type === CUSTOM_FIELD_LIST:
        return (
          <SelectInput
            choices={choices.map(({ id, label }) => ({
              id,
              name:
                label || translate(`resources.${resource}.fields.choices.${id}`)
            }))}
            field={cfField}
          />
        );
      case type === FIELD_PASSWORD:
        return <TextInput autoComplete="new-password" type={type} />;
      case type === CUSTOM_FIELD_DATE:
      case type === FIELD_DATE:
        return (
          <DateInput
            dateFormat={dateFormat}
            field={cfField}
            disabled={disabledDate}
          />
        );
      case type === FIELD_DATE_TIME:
        return <DateTimeInput dateFormat={dateFormat} />;
      case type === CUSTOM_FIELD_DOUBLE:
      case type === CUSTOM_FIELD_LONG:
      case type === FIELD_NUMBER:
        return (
          <NumberInput
            defaultValue={propsDefaultValue || defaultValue}
            field={cfField}
          />
        );
      case type === FIELD_BOOLEAN:
        return <BooleanInput choices={choices} />;
      case type === FIELD_LONG_TEXT:
        return <LongTextInput />;
      case type === FIELD_CODE:
        return <CodeInput />;
      case type === FIELD_CUSTOM_CHOICES_SELECT:
        return <CustomSelectInput {...this.props} />;
      case type === FIELD_FILE:
        return (
          <FileInput>
            <FileField source="src" title="title" />
          </FileInput>
        );

      case type === CUSTOM_FIELD_MULTI:
        return <CFMultiValueInput field={cfField} />;
      case type === CUSTOM_FIELD_STRING:
      default:
        return <TextInput field={cfField} type={type} />;
    }
  };

  isSameValidators = (nextValidators) => {
    const { validators } = this.state;

    if (validators.length !== nextValidators.length) {
      return false;
    }

    return validators.reduce(
      (res, el, index) => res && el === nextValidators[index],
      true
    );
  };

  getValidator = (validationData) => {
    const { formValues, dependentFields } = this.props;

    const key = findValidatorType(validationData);

    const dependenciesRules = get(validationData, 'dependsOn', {});

    if (!isValidatorActive(dependenciesRules, dependentFields)) {
      this.activeValidators = this.activeValidators.filter((el) => el !== key);

      return undefined;
    }

    if (!this.activeValidators.includes(key)) {
      this.activeValidators.push(key);
    }

    return getValidatorByKey(key, validationData, formValues);
  };

  extractValidators = ({ validate }) => {
    const { dispatch, form } = this.props;

    if (!validate) {
      return undefined;
    }

    const validators = Array.isArray(validate) ? validate : [validate];
    const extractedValidators = validators
      .map((el) => this.getValidator(el))
      .filter((el) => el);

    if (extractedValidators.length === 0) {
      return undefined;
    }

    if (!this.isSameValidators(extractedValidators)) {
      dispatch(
        change(form || REDUX_FORM_NAME, '__triggerValidation', Date.now())
      );
      this.setState({ validators: extractedValidators });
    }

    return extractedValidators;
  };

  wrapInput = (inputDefinition, input) => {
    const {
      source,
      uniqueSource,
      label,
      type: sourceType,
      isEdit,
      record,
      form,
      translate
    } = this.props;
    const { resource } = this.context;

    const {
      dependsOn,
      reference,
      allowWildcard,
      disabledOnEdit,
      filter,
      visible,
      referenceField,
      filterType
    } = inputDefinition;

    const wrappers = [];

    const finalLabel =
      label || translate(`resources.${resource}.fields.${source}`);

    if (
      !!reference &&
      !dependsOn &&
      sourceType !== FIELD_SELECT_SUBFORM &&
      sourceType !== FIELD_ONE_REFERENCE
    ) {
      wrappers.push({
        Component: ReferenceInput,
        props: {
          source,
          uniqueSource,
          referenceField,
          type: sourceType,
          reference,
          resource,
          allowWildcard,
          validate: this.extractValidators(inputDefinition),
          label: finalLabel,
          allowEmpty: !this.activeValidators.includes('required'),
          filter,
          record,
          withReferenceRecord: sourceType !== CUSTOM_FIELD_ENTITY,
          form,
          visible,
          filterType
        }
      });
    }

    if (!!dependsOn || (typeof disabledOnEdit === 'object' && isEdit)) {
      wrappers.push({
        Component: ConditionalInput,
        props: {
          dependsOn,
          disabledOnEdit,
          isEdit,
          reference,
          source,
          record,
          filter,
          form,
          visible,
          allowWildcard
        }
      });
    }

    return wrappers.reduce(
      (res, { Component, props }) => <Component {...props}>{res}</Component>,
      input
    );
  };

  render = () => {
    const {
      translate,
      source,
      label,
      defaultValue,
      formValues,
      permissions,
      form,
      identifierData,
      id,
      classes,
      ...inputDefinition
    } = this.props;
    const { visible } = inputDefinition;
    const { resource } = this.context;

    const inputComponent = this.determineInputComponent(inputDefinition);

    const finalLabel = label
      ? translate(label)
      : translate(`resources.${resource}.fields.${source}`);
    const finalComponent = inputComponent
      ? cloneElement(inputComponent, {
          source,
          validate: this.extractValidators(inputDefinition),
          style: visible === false ? { display: 'none' } : null,
          form,
          identifierData,
          classes,
          InputLabelProps: {
            classes: {
              formControl: classes.formControl
            }
          },
          id,
          label: finalLabel
        })
      : null;

    return finalComponent ? (
      <WithRoles permissions={permissions}>
        {this.wrapInput(inputDefinition, finalComponent)}
      </WithRoles>
    ) : null;
  };
}

export default compose(
  connect(
    (
      state,
      { validate = [], reference, record = {}, source, resultOf, form }
    ) => {
      const validateArray = !Array.isArray(validate) ? [validate] : validate;

      const dependsOn = getDependsOn(validateArray);
      const dependsOnValues = getDependsOnValues(validateArray);

      if (
        isEmpty(validate || (isEmpty(dependsOnValues) && isEmpty(dependsOn)))
      ) {
        return {};
      }
      const formData = getFormValues(form || REDUX_FORM_NAME)(state) || record;

      const rawFormData = extractFormNestedData(formData);

      const dependentFields = Object.keys(dependsOn).reduce((res, key) => {
        const [formField, referenceField] = key.split('.');

        const referenceValue = rawFormData[formField];

        res[key] = referenceField
          ? get(
              state,
              `admin.resources.${reference}.data.${referenceValue}.${referenceField}`
            )
          : referenceValue;

        return res;
      }, {});

      return {
        formValues: formData,
        dependentFields,
        currentValue: formData[source]
      };
    }
  ),
  withTranslate,
  withStyles(styles)
)(InputRenderer);
