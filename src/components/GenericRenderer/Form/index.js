import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';
import { destroy } from 'redux-form';
import {
  Create,
  Edit,
  REDUX_FORM_NAME,
  refreshView as refreshViewAction
} from 'react-admin';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';
import { isEmpty } from 'lodash';

import Title from '../../Layout/Title';
import WithExtendedSave from '../../Hoc/WithExtendedSave';
import withTranslate from '../../Hoc/withTranslate';

import SimpleForm from './SimpleForm';
import TabbedForm from './TabbedForm';
import RenderForm from './RenderForm';

const styles = ({ spacing }) => ({
  wrapper: {
    margin: `${spacing.unit * 2}px ${spacing.unit}px`
  },
  notLoaded: {
    display: 'none'
  }
});

class GenericForm extends PureComponent {
  static propTypes = {
    config: PropTypes.object.isRequired,
    resource: PropTypes.string.isRequired,
    record: PropTypes.object,
    isEdit: PropTypes.bool,
    inDrawer: PropTypes.bool
  };

  componentWillMount = () => this.resetForm();

  componentWillUnmount = () => {
    const { refreshView } = this.props;
    this.props.dispatch({ type: 'RA/RESET_FORM' });
    refreshView();
  };

  componentDidUpdate = ({
    resource: prevResource,
    record: prevRecord,
    page: prevPage
  }) => {
    const { resource, page } = this.props;

    if (resource !== prevResource || page !== prevPage) {
      this.resetForm();
    }
  };

  resetForm = () =>
    this.props.dispatch(destroy(this.props.form || REDUX_FORM_NAME));

  render = () => {
    const {
      hasList,
      hasEdit,
      hasCreate,
      hasShow,
      classes,
      className,
      save,
      translate,
      ...props
    } = this.props;
    const { record } = props;

    const {
      config,
      isEdit,
      inDrawer,
      redirectAfterSubmit = 'show',
      redirectAfterDelete,
      form,
      ...rest
    } = props;
    const { tabs } = config || {};

    const Wrapper = isEdit ? Edit : Create;
    const Form = Array.isArray(tabs) ? TabbedForm : SimpleForm;

    const defaultTitle = translate('ra.page.show', {
      name: '',
      id: record ? record.id : undefined
    });

    return (
      <>
        <Title defaultTitle={defaultTitle} />

        <Wrapper
          {...rest}
          className={cx(
            classes.wrapper,
            isEdit && isEmpty(record) && classes.notLoaded,
            className
          )}
          undoable={false}
        >
          <RenderForm {...props} save={save}>
            <WithExtendedSave>
              <Form
                {...props}
                redirect={redirectAfterSubmit}
                redirectAfterDelete={redirectAfterDelete}
                isEdit={isEdit}
                inDrawer={inDrawer}
                undoable={false}
              />
            </WithExtendedSave>
          </RenderForm>
        </Wrapper>
      </>
    );
  };
}

export default compose(
  connect(
    null,
    { refreshView: refreshViewAction }
  ),
  withTranslate,
  withStyles(styles)
)(GenericForm);
