import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import compose from 'recompose/compose';

import { Checkbox, FormControlLabel } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import InputRenderer from './InputRenderer';
import CustomFieldsSection from './CustomFieldsSection';
import withTranslate from '../../Hoc/withTranslate';

const styles = () => ({
  grid: {
    display: 'grid',
    gridTemplateColumns: '0.6fr 1.4fr',
    gridGap: '2vw',
    padding: '20px'
  },
  '@media (max-width: 1280px)': {
    grid: {
      gridTemplateColumns: '1fr'
    }
  }
});

const SubForm = ({
  resource,
  record,
  classes,
  className,
  translate,
  templateSource,
  fields,
  description,
  name
}) => {
  const [isSelected, setSelected] = useState(true);
  const [isMandatory, setMandatory] = useState(false);

  const selectHandler = useCallback(
    () => {
      setSelected(!isSelected);
    },
    [isSelected]
  );

  const changeMandatoryHandler = useCallback(
    () => {
      setMandatory(!isMandatory);
    },
    [isMandatory]
  );

  return (
    <form className={cx(classes.grid, className)}>
      <div>
        <FormControlLabel
          control={
            <Checkbox
              checked={isSelected}
              onChange={selectHandler}
              color="primary"
            />
          }
          label={description}
        />
        <InputRenderer
          source={`${templateSource}.description`}
          label="Long Description"
        />
      </div>

      {isSelected && (
        <div>
          <FormControlLabel
            control={
              <Checkbox
                checked={isMandatory}
                onChange={changeMandatoryHandler}
                color="primary"
              />
            }
            label="mandatory"
          />
          <CustomFieldsSection
            fields={fields}
            cfPath={templateSource}
            isMandatory={isMandatory}
          />
        </div>
      )}
    </form>
  );
};

SubForm.propTypes = {
  resource: PropTypes.string.isRequired,
  record: PropTypes.object,
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  translate: PropTypes.func.isRequired,
  templateSource: PropTypes.string,
  fields: PropTypes.array.isRequired,
  description: PropTypes.string,
  name: PropTypes.string
};

const mapStateToProps = (state, ownProps) => ({
  form: ownProps.name,
  initialValues: ownProps.initialValues
});

export default compose(
  connect(mapStateToProps),
  withTranslate,
  reduxForm({
    destroyOnUnmount: true,
    enableReinitialize: true,
    keepDirtyOnReinitialize: true
  }),
  withStyles(styles)
)(React.memo(SubForm));
