import React from 'react';
import { get, isEmpty } from 'lodash';
import PropTypes from 'prop-types';

import {
  FIELD_DATE,
  FIELD_AUTOCOMPLETE,
  FIELD_BOOLEAN
} from '../../../constants/generic';
import ReferenceInput from './Inputs/ReferenceInput';
import FiltersSelectInput from './Inputs/FiltersSelectInput';
import DateInput from './Inputs/DateInput';
import AutocompleteInput from './Inputs/AutocompleteInput';
import BooleanInput from './Inputs/BooleanInput';
import TextInput from './Inputs/TextInput';

const getSource = (source, filter) => {
  const prefix = get(filter, 'prefix') || null;
  const filterSource = get(filter, 'source') || null;
  switch (true) {
    case !!prefix:
      return `${prefix} ${source}`;
    case !!filterSource:
      return filterSource;
    default:
      return source;
  }
};

const InputFilterRenderer = ({
  source,
  reference,
  choices = [],
  type,
  filter,
  referenceField,
  lazyReference,
  label,
  ...rest
}) => {
  if (!isEmpty(choices)) {
    return (
      <FiltersSelectInput
        key={source}
        label={label}
        source={getSource(source, filter)}
        optionText={({ label, id }) => label || id}
        choices={choices}
        {...rest}
      />
    );
  }
  if (FIELD_BOOLEAN === type) {
    return (
      <BooleanInput
        key={source}
        label={label}
        source={getSource(source, filter)}
        {...rest}
      />
    );
  }
  if (FIELD_DATE === type) {
    return (
      <DateInput
        key={source}
        label={label}
        source={getSource(source, filter)}
        {...rest}
      />
    );
  }
  if (!reference || lazyReference) {
    return (
      <TextInput
        key={source}
        label={label}
        source={getSource(source, filter)}
        type={type}
        filter={filter}
        {...rest}
      />
    );
  }

  return (
    <ReferenceInput
      key={source}
      label={label}
      source={getSource(source, filter)}
      referenceField={referenceField}
      reference={reference}
      allowEmpty
      alwaysOn={rest.alwaysOn}
    >
      {type === FIELD_AUTOCOMPLETE ? (
        <AutocompleteInput
          optionText={({ [referenceField]: defaultField, id }) =>
            defaultField || id
          }
          source={referenceField}
          options={{
            fullWidth: true,
            autoComplete: 'no'
          }}
        />
      ) : (
        <FiltersSelectInput
          optionText={(props) => {
            const { description, code } = props;

            return get(props, referenceField, description || code);
          }}
          source={source}
          choices={choices}
          filter={filter}
        />
      )}
    </ReferenceInput>
  );
};

InputFilterRenderer.propTypes = {
  source: PropTypes.string.isRequired,
  reference: PropTypes.string,
  choices: PropTypes.array,
  type: PropTypes.string,
  filter: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  referenceField: PropTypes.string,
  lazyReference: PropTypes.string,
  label: PropTypes.string
};

export default InputFilterRenderer;
