import React, { Children, isValidElement, memo } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { SaveButton } from 'react-admin';
import { Toolbar as MuiToolbar, withWidth } from '@material-ui/core';
import { withStyles, createStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';

import ConfirmDialog from '../../Globals/Dialogs/ConfirmDialog';
import DeleteButtonConfirm from '../../Globals/Buttons/DeleteButtonConfirm';
import BackButton from '../Buttons/BackButton';

const styles = (theme) =>
  createStyles({
    toolbar: {
      backgroundColor:
        theme.palette.type === 'light'
          ? theme.palette.grey[100]
          : theme.palette.grey[900]
    },
    desktopToolbar: {
      marginTop: theme.spacing.unit * 2
    },
    mobileToolbar: {
      position: 'fixed',
      bottom: 0,
      left: 0,
      right: 0,
      padding: '16px',
      width: '100%',
      boxSizing: 'border-box',
      flexShrink: 0,
      zIndex: 2
    },
    button: {
      margin: '0 4px'
    },
    defaultToolbar: {
      flex: 1,
      display: 'flex',
      justifyContent: 'flex-start'
    },
    spacer: {
      [theme.breakpoints.down('xs')]: {
        height: '5em'
      }
    }
  });

const valueOrDefault = (value, defaultValue) =>
  typeof value === 'undefined' ? defaultValue : value;

const Toolbar = memo(
  ({
    basePath,
    children,
    classes,
    className,
    handleSubmit,
    handleSubmitWithRedirect,
    redirectAfterDelete,
    invalid,
    pristine,
    record,
    redirect,
    resource,
    saving,
    submitOnEnter,
    undoable,
    width,
    withDelete = true,
    withSave = true,
    withBack = false,
    openConfirm = false,
    handleClose,
    formID,
    ...rest
  }) => (
    <>
      <MuiToolbar
        className={cx(
          classes.toolbar,
          {
            [classes.mobileToolbar]: width === 'xs',
            [classes.desktopToolbar]: width !== 'xs'
          },
          className
        )}
        role="toolbar"
        {...rest}
      >
        {Children.count(children) === 0 ? (
          <div className={classes.defaultToolbar}>
            <ConfirmDialog
              open={openConfirm}
              title="confirm"
              content="confirm_content"
              translateOptions={{
                label: 'update existing resource'
              }}
              onConfirm={handleSubmit}
              onClose={handleClose}
            />

            {withSave && (
              <SaveButton
                handleSubmitWithRedirect={handleSubmitWithRedirect}
                invalid={invalid}
                redirect={redirect}
                saving={saving}
                submitOnEnter={submitOnEnter}
                className={classes.button}
                id={`${formID}--submit`}
              />
            )}

            {withDelete && record && typeof record.id !== 'undefined' && (
              <DeleteButtonConfirm
                basePath={basePath}
                record={record}
                resource={resource}
                undoable={undoable}
                className={classes.button}
                id={`${formID}--delete`}
                redirectAfterDelete={redirectAfterDelete}
              />
            )}

            {withBack && <BackButton />}
          </div>
        ) : (
          Children.map(children, (button, index) =>
            button && isValidElement(button)
              ? React.cloneElement(button, {
                  basePath,
                  handleSubmit: valueOrDefault(
                    button.props.handleSubmit,
                    handleSubmit
                  ),
                  handleSubmitWithRedirect: valueOrDefault(
                    button.props.handleSubmitWithRedirect,
                    handleSubmitWithRedirect
                  ),
                  invalid,
                  pristine,
                  record,
                  resource,
                  saving,
                  submitOnEnter: valueOrDefault(
                    button.props.submitOnEnter,
                    submitOnEnter
                  ),
                  undoable: valueOrDefault(button.props.undoable, undoable),
                  id: `${formID}--${index}`
                })
              : null
          )
        )}
      </MuiToolbar>

      <div className={classes.spacer} />
    </>
  )
);

Toolbar.propTypes = {
  basePath: PropTypes.string,
  children: PropTypes.node,
  classes: PropTypes.object,
  className: PropTypes.string,
  handleSubmit: PropTypes.func,
  handleSubmitWithRedirect: PropTypes.func,
  invalid: PropTypes.bool,
  pristine: PropTypes.bool,
  record: PropTypes.object,
  redirect: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
    PropTypes.func
  ]),
  resource: PropTypes.string,
  saving: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  submitOnEnter: PropTypes.bool,
  undoable: PropTypes.bool,
  width: PropTypes.string
};

Toolbar.defaultProps = {
  submitOnEnter: true
};

const enhance = compose(
  withWidth(),
  withStyles(styles)
);

export default enhance(Toolbar);
