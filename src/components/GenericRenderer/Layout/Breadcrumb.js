import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import BreadcrumbItem from './BreadcrumbItem';

const styles = () => ({
  menuBreadcrumb: {
    display: 'flex',
    justifyContent: 'center',
    borderTop: '1px solid #f0f0f0',
    borderBottom: '1px solid #f0f0f0'
  }
});

const Breadcrumb = ({ items, classes }) => (
  <div className={classes.menuBreadcrumb}>
    {items.map((element, index) => (
      <BreadcrumbItem element={element} key={index} />
    ))}
  </div>
);

Breadcrumb.propTypes = {
  items: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Breadcrumb);
