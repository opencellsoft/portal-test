import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';

import PageContext from './PageContext';

import Row from '../../Globals/Row';
import Widgets from '../Widgets';

class SectionRenderer extends PureComponent {
  static contextType = PageContext;

  static propTypes = {
    section: PropTypes.string.isRequired
  };

  render = () => {
    const { section, classes, rowClassname, ...props } = this.props;
    const { config } = this.context;

    const { [section]: pageSection } = config;
    const { rows, content } = pageSection || {};

    let pageContent = rows || content;

    if (isEmpty(pageContent)) {
      return false;
    }

    if (!Array.isArray(pageContent)) {
      pageContent = [pageContent];
    }

    return pageContent.map((row, index) => (
      <Row key={index} className={rowClassname}>
        {Array.isArray(row) ? (
          row.map((widget, index) => (
            <Widgets key={index} component={widget} {...props} />
          ))
        ) : (
          <Widgets key={index} component={row} {...props} />
        )}
      </Row>
    ));
  };
}

export default SectionRenderer;
