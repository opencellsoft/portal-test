import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = ({ palette, spacing, typography }) => ({
  breadcrumbItem: {
    display: 'flex',
    alignItems: 'center',
    textTransform: 'capitalize',
    ...typography.subheading,
    color: palette.text.secondary,
    margin: `0 ${spacing.unit}px`,
    padding: 0
  }
});

const BreadcrumbItem = ({ element, classes }) => (
  <div className={classes.breadcrumbItem}>{element}</div>
);

BreadcrumbItem.propTypes = {
  element: PropTypes.node.isRequired,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(BreadcrumbItem);
