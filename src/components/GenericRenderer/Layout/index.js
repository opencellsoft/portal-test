import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';
import { isEmpty } from 'lodash';
import {
  HEADER,
  LEFT_COL,
  RIGHT_COL,
  FOOTER,
  CRUD_LIST,
  CRUD_SHOW,
  CRUD_CREATE,
  CRUD_EDIT
} from '../../../constants/generic';

import PageContext from './PageContext';
import SectionRenderer from './SectionRenderer';

import List from '../List';
import Show from '../Show';
import Form from '../Form';
import CustomPage from '../CustomPage';
import WithEntityCustomization from '../../Hoc/WithEntityCustomization';
import withTranslate from '../../Hoc/withTranslate';

const Components = {
  [CRUD_LIST]: List,
  [CRUD_SHOW]: Show,
  [CRUD_CREATE]: Form,
  [CRUD_EDIT]: Form
};

const TitleTranslations = {
  [CRUD_LIST]: 'ra.page.list',
  [CRUD_SHOW]: 'ra.page.show',
  [CRUD_CREATE]: 'ra.page.create',
  [CRUD_EDIT]: 'ra.page.edit'
};

const styles = ({ spacing }) => ({
  wrapper: {},
  flex: {
    display: 'flex',
    flexDirection: 'column'
  },
  main: {
    flex: 1,
    padding: 0
  }
});

class Layout extends PureComponent {
  static propTypes = {
    page: PropTypes.string.isRequired,
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.arrayOf(PropTypes.node)
    ]),
    classes: PropTypes.object.isRequired,
    resource: PropTypes.string,
    record: PropTypes.object,
    basePath: PropTypes.string,
    isAllowedCreate: PropTypes.bool,
    isAllowedEdit: PropTypes.bool
  };

  state = {
    context: {
      config: this.props.config || {},
      page: this.props.page,
      resource: this.props.resource,
      record: this.props.record,
      basePath: this.props.basePath,
      drawer: this.props.drawer,
      data: ''
    }
  };

  componentWillMount = () => {
    const { location } = this.props;
    const { context } = this.state;

    if (!isEmpty(location.data)) {
      this.setState({ context: { ...context, data: location.data } });
    }
  };

  componentDidUpdate = ({ record: prevRecord, location: prevLocation }) => {
    const { record, location } = this.props;
    const { context } = this.state;
    const hasRecord = !isEmpty(record);

    if (
      (isEmpty(prevRecord) && hasRecord) ||
      (hasRecord && record !== prevRecord) ||
      location !== prevLocation
    ) {
      this.setState({ context: { ...context, record } });
    }
  };

  getDefaultTitle = () => {
    const { record = {}, page, translate } = this.props;

    return translate(TitleTranslations[page], {
      name: '',
      id: record.id
    });
  };

  render = () => {
    const {
      config,
      page,
      options,
      record,
      children,
      classes,
      dispatch,
      drawer,
      resource,
      translate,
      isAllowedEdit,
      isAllowedCreate,
      ...props
    } = this.props;
    const { context } = this.state;
    const {
      main: mainConfig = {},
      redirectAfterSubmit,
      redirectAfterDelete = redirectAfterSubmit
    } = config;
    const MainComponent = Components[page] || CustomPage;
    const defaultTitle = this.getDefaultTitle();

    return (
      <WithEntityCustomization resource={resource}>
        <PageContext.Provider value={context}>
          <div className={classes.wrapper}>
            <div className={classes.flex}>
              <SectionRenderer
                section={HEADER}
                resource={resource}
                record={record}
              />
            </div>

            <div className={classes.flex}>
              <div className={classes.flex}>
                <SectionRenderer
                  section={LEFT_COL}
                  resource={resource}
                  record={record}
                />
              </div>

              {!isEmpty(mainConfig) && (
                <div className={classes.main}>
                  <MainComponent
                    {...props}
                    dispatch={dispatch}
                    record={record}
                    defaultTitle={defaultTitle}
                    resource={resource}
                    config={mainConfig}
                    page={page}
                    drawer={drawer}
                    redirectAfterSubmit={redirectAfterSubmit}
                    redirectAfterDelete={redirectAfterDelete}
                    isAllowedEdit={isAllowedEdit}
                    isAllowedCreate={isAllowedCreate}
                  />
                </div>
              )}

              <div className={classes.flex}>
                <SectionRenderer
                  section={RIGHT_COL}
                  resource={resource}
                  record={record}
                />
              </div>
            </div>

            <div className={classes.flex}>
              <SectionRenderer
                section={FOOTER}
                resource={resource}
                record={record}
              />
            </div>
          </div>
        </PageContext.Provider>
      </WithEntityCustomization>
    );
  };
}

export default compose(
  connect(({ admin, router }, { id, resource }) => ({
    record: admin.resources[resource]
      ? admin.resources[resource].data[id]
      : null,
    location: router.location
  })),
  withTranslate,
  withStyles(styles)
)(Layout);
