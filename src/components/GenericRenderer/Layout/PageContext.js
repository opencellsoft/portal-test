import { createContext } from 'react';

export default createContext({
  config: {},
  page: '',
  resource: '',
  record: {},
  basePath: '',
  drawer: {}
});
