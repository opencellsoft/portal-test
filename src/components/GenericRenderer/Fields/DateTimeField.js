import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { TimePicker, DatePicker } from 'material-ui-pickers';
import moment from 'moment-timezone';
import get from 'lodash/get';

class DateTimeField extends PureComponent {
  static propTypes = {
    record: PropTypes.object,
    source: PropTypes.string.isRequired,
    label: PropTypes.string
  };

  state = {
    selectedDate: null
  };

  componentDidMount = () => {
    const date = get(this.props.record, this.props.source);

    if (date) {
      this.setState({
        selectedDate: moment(date, 'DD/MM/YYYY HH:mm:ss').format()
      });
    }
  };

  handleDateChange = (selectedDate) => {
    if (this.onChange) {
      return this.onChange(selectedDate);
    }

    return this.setState({ selectedDate });
  };

  render = () => {
    const { label } = this.props;
    const { selectedDate } = this.state;

    return (
      <>
        <DatePicker
          margin="normal"
          label={`${label} date`}
          value={this.value || selectedDate}
          onChange={this.handleDateChange}
        />
        <TimePicker
          margin="normal"
          label={`${label} time`}
          value={this.value || selectedDate}
          onChange={this.handleDateChange}
        />
      </>
    );
  };
}

export default DateTimeField;
