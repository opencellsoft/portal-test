import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Tooltip, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { get } from 'lodash';
import { compose, pure } from 'recompose';
import moment from 'moment-timezone';

import withTranslate from '../../Hoc/withTranslate';

const styles = ({ palette }) => ({
  notFound: {
    color: palette.error.main
  }
});

const TextField = ({
  className,
  source = '',
  record = {},
  addLabel,
  noEmptyLabel,
  initialValue,
  isWildcard,
  translate,
  error,
  classes,
  dateFormat,
  ...rest
}) => {
  let value = isWildcard
    ? '*'
    : source
        .split('+')
        .map((field) => get(record, field))
        .filter((el) => el)
        .join(' ') || initialValue;

  if (!value && typeof value !== 'number') {
    value = !noEmptyLabel ? initialValue : '';
  }

  if (!!dateFormat && !!value) {
    value = moment(
      typeof value === 'string' && /^\d{10}$/.test(value)
        ? parseInt(value, 10)
        : value
    ).format(dateFormat);
  }

  const label = !!error ? (value ? `${value} (${error})` : error) : value;

  return (
    <Tooltip title={label} placement="bottom-start">
      <Typography
        component="span"
        body1="body1"
        className={cx(!!error && classes.notFound, className)}
        noWrap
        {...rest}
      >
        {value}
      </Typography>
    </Tooltip>
  );
};

TextField.propTypes = {
  basePath: PropTypes.string,
  className: PropTypes.string,
  record: PropTypes.object,
  source: PropTypes.string.isRequired,
  noEmptyLabel: PropTypes.string,
  error: PropTypes.string,
  classes: PropTypes.object,
  dateFormat: PropTypes.string
};

const PureTextField = pure(TextField);

export default compose(
  withStyles(styles),
  withTranslate
)(PureTextField);
