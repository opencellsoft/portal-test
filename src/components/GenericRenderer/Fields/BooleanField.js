import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Tooltip, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { grey, red } from '@material-ui/core/colors';
import OnIcon from '@material-ui/icons/Check';
import OffIcon from '@material-ui/icons/Close';
import { compose } from 'recompose';
import { isEmpty, get } from 'lodash';

import withTranslate from '../../Hoc/withTranslate';

const styles = () => ({
  value: {
    fontWeight: 'inherit'
  },
  'ra.message.yes': {
    color: '#00c853'
  },
  'ra.message.no': {
    color: grey[200]
  },
  disabled: {
    color: red.A100
  }
});

const isActive = (choices, value) => {
  switch (true) {
    case Array.isArray(choices) && !isEmpty(choices):
      return (
        choices.findIndex((choice) =>
          typeof choice === 'string' && typeof value === 'string'
            ? choice.toLowerCase() === value.toLowerCase()
            : choice === value
        ) === 1
      );
    case typeof value === 'boolean' && value:
    case typeof value === 'string' && !!value && value !== 'false':
      return true;
    default:
      return false;
  }
};

const BooleanField = ({
  className,
  source,
  record = {},
  addLabel,
  translate,
  classes,
  choices,
  ...rest
}) => {
  const value = get(record, source);
  const boolValue = isActive(choices, value);
  const valueDisplay = boolValue ? 'ra.message.yes' : 'ra.message.no';

  return (
    <Tooltip title={translate(valueDisplay)} placement="bottom-start">
      <Typography
        component="span"
        body1="body1"
        className={className}
        noWrap
        {...rest}
      >
        <span
          className={cx(
            classes.value,
            classes[source === 'disabled' && boolValue ? source : valueDisplay]
          )}
        >
          {boolValue ? <OnIcon /> : <OffIcon />}
        </span>
      </Typography>
    </Tooltip>
  );
};

BooleanField.propTypes = {
  basePath: PropTypes.string,
  className: PropTypes.string,
  record: PropTypes.object,
  source: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired
};

export default compose(
  withTranslate,
  withStyles(styles)
)(BooleanField);
