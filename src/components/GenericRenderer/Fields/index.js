import React, { memo, useContext } from 'react';
import PropTypes from 'prop-types';
import { EmailField } from 'react-admin';
import { isEmpty, get } from 'lodash';

import {
  FIELD_TEXT,
  FIELD_DATE,
  FIELD_DATE_TIME,
  FIELD_REFERENCE_LIST,
  FIELD_EMAIL,
  FIELD_BUTTON,
  FIELD_NUMBER,
  FIELD_STATUS,
  FIELD_STATUS_ICON,
  FIELD_ACTIONS,
  FIELD_BOOLEAN,
  FIELD_REFERENCE
} from '../../../constants/generic';

import PageContext from '../Layout/PageContext';

import ResourceListField from './ResourceListField';
import OneReferenceField from './OneReferenceField';
import DateField from './DateField';
import DateTimeField from './DateTimeField';
import TextField from './TextField';
import ButtonField from './ButtonField';
import NumberField from './NumberField';
import StatusField from './StatusField';
import ActionsField from './ActionsField';
import BooleanField from './BooleanField';

const Components = {
  [FIELD_TEXT]: TextField,
  [FIELD_REFERENCE]: OneReferenceField,
  [FIELD_REFERENCE_LIST]: ResourceListField,
  [FIELD_DATE]: DateField,
  [FIELD_DATE_TIME]: DateTimeField,
  [FIELD_EMAIL]: EmailField,
  [FIELD_NUMBER]: NumberField,
  [FIELD_BUTTON]: ButtonField,
  [FIELD_STATUS]: StatusField,
  [FIELD_STATUS_ICON]: StatusField,
  [FIELD_ACTIONS]: ActionsField,
  [FIELD_BOOLEAN]: BooleanField
};

const Fields = memo(
  ({ source, reference, type = 'text', label, record, ...rest }) => {
    const key =
      !isEmpty(reference) && type !== FIELD_REFERENCE_LIST
        ? FIELD_REFERENCE
        : type;
    const FieldComponent = Components[key] || TextField;

    const { record: pageRecord } = useContext(PageContext);

    return FieldComponent ? (
      <FieldComponent
        type={type}
        reference={reference}
        record={record}
        source={source}
        {...rest}
        parentRecord={pageRecord || get(rest, 'parentRecord')}
      />
    ) : null;
  }
);

Fields.propTypes = {
  source: PropTypes.string.isRequired,
  type: PropTypes.string
};

export default Fields;
