import React from 'react';
import PropTypes from 'prop-types';
import { Tooltip, Typography } from '@material-ui/core';
import moment from 'moment-timezone';
import { get, isEmpty } from 'lodash';
import { pure } from 'recompose';

import withTranslate from '../../Hoc/withTranslate';

const DateField = ({
  className,
  source,
  record = {},
  addLabel,
  dateFormat = 'DD/MM/YYYY',
  showTime,
  translate,
  ...rest
}) => {
  const dates = source.split('+').map((field) => get(record, field));
  const format = showTime ? 'DD/MM/YYYY - HH:mm:ss' : 'DD/MM/YYYY';

  const value = !isEmpty(dates)
    ? dates
        .filter((el) => el)
        .map((date) => {
          if (date === 'null') return '';
          const formattedDate =
            typeof date === 'string' && /^\d+$/.test(date)
              ? Number(date)
              : date;
          const safeDateFormat =
            typeof formattedDate === 'number' ? undefined : dateFormat;

          return date
            ? moment(formattedDate, safeDateFormat).format(format)
            : '';
        })
        .join(' / ')
    : '';

  return (
    <Tooltip title={value} placement="bottom-start">
      <Typography
        component="span"
        body1="body1"
        className={className}
        noWrap
        {...rest}
      >
        {value}
      </Typography>
    </Tooltip>
  );
};

DateField.propTypes = {
  basePath: PropTypes.string,
  className: PropTypes.string,
  record: PropTypes.object,
  source: PropTypes.string.isRequired,
  dateFormat: PropTypes.string
};

const PureDateField = pure(DateField);

export default withTranslate(PureDateField);
