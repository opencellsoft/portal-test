import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Tooltip, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/icons/Lens';
import { get } from 'lodash';
import { compose } from 'recompose';

import { amber, red } from '@material-ui/core/colors';

import withTranslate from '../../Hoc/withTranslate';

import { FIELD_STATUS_ICON } from '../../../constants/generic';

import {
  STATUS_INSTANTIATED,
  STATUS_VALID,
  STATUS_WELL_FORMED,
  STATUS_ACTIVE,
  STATUS_COMPLETED,
  STATUS_IN_STUDY,
  STATUS_IN_DESIGN,
  STATUS_IN_TEST,
  STATUS_PENDING,
  STATUS_DRAFT,
  STATUS_OPEN,
  STATUS_NEW,
  STATUS_VALIDATED,
  STATUS_BILLED,
  STATUS_RERATED,
  STATUS_MATCHING_O,
  STATUS_MATCHING_L,
  STATUS_MATCHING_P,
  STATUS_MATCHING_I
} from '../../../constants/status';

const styles = ({ typography }) => ({
  status: {
    fontWeight: 'inherit',
    padding: '0 10px',
    borderRadius: 10,
    color: 'white',
    maxWidth: 'fit-content',
    textTransform: 'uppercase'
  },
  statusActive: {
    background: '#00c853'
  },
  statusWarning: {
    background: amber[700]
  },
  statusError: {
    background: red[500]
  },
  icon: {
    display: 'block',
    padding: 1,
    fontSize: 20
  },
  iconActive: {
    color: '#00c853'
  },
  iconWarning: {
    color: amber[700]
  },
  iconError: {
    color: red[500]
  }
});

const getAdditionnalClass = (status, classes, withIcon) => {
  switch (status) {
    case STATUS_VALID:
    case STATUS_ACTIVE:
    case STATUS_BILLED:
    case STATUS_VALIDATED:
    case STATUS_WELL_FORMED:
    case STATUS_COMPLETED:
    case STATUS_MATCHING_O:
    case STATUS_MATCHING_L:
      return withIcon ? classes.iconActive : classes.statusActive;
    case STATUS_OPEN:
    case STATUS_NEW:
    case STATUS_RERATED:
    case STATUS_INSTANTIATED:
    case STATUS_DRAFT:
    case STATUS_IN_DESIGN:
    case STATUS_IN_STUDY:
    case STATUS_IN_TEST:
    case STATUS_PENDING:
    case STATUS_MATCHING_P:
    case STATUS_MATCHING_I:
      return withIcon ? classes.iconWarning : classes.statusWarning;
    default:
      return withIcon ? classes.iconError : classes.statusError;
  }
};

const StatusTextField = ({
  type,
  record,
  source,
  defaultValue,
  classes,
  translate
}) => {
  const value = (get(record, source) || defaultValue).toUpperCase();

  if (!value) {
    return null;
  }

  const label = translate(`status.${value}`, { _: value });
  const withIcon = type === FIELD_STATUS_ICON;

  return (
    <Tooltip title={label} placement="bottom-start">
      <Typography
        component="span"
        body1="body1"
        className={cx(
          !withIcon && classes.status,
          getAdditionnalClass(value, classes, withIcon)
        )}
        noWrap
      >
        {withIcon ? (
          <Icon
            className={cx(
              classes.icon,
              getAdditionnalClass(value, classes, withIcon)
            )}
          />
        ) : (
          label
        )}
      </Typography>
    </Tooltip>
  );
};

StatusTextField.propTypes = {
  type: PropTypes.string.isRequired,
  record: PropTypes.object,
  source: PropTypes.string,
  defaultValue: PropTypes.string,
  classes: PropTypes.object.isRequired,
  translate: PropTypes.func.isRequired
};

StatusTextField.defaultProps = {
  defaultValue: ''
};

export default compose(
  withTranslate,
  withStyles(styles)
)(StatusTextField);
