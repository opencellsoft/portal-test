import React from 'react';
import PropTypes from 'prop-types';
import { compose, pure } from 'recompose';

import OneReferenceFieldController from './OneReferenceFieldController';
import OneReferenceFieldView from './OneReferenceFieldView';
import withTranslate from '../../../Hoc/withTranslate';

const OneReferenceField = ({
  source,
  reference,
  referenceField,
  linkType = false,
  ...props
}) => (
  <OneReferenceFieldController
    {...props}
    source={source}
    reference={reference}
    basePath={`/${reference}`}
    linkType={linkType}
  >
    {(controllerProps) => (
      <OneReferenceFieldView
        {...props}
        {...controllerProps}
        referenceField={referenceField || 'description'}
      />
    )}
  </OneReferenceFieldController>
);

OneReferenceField.propTypes = {
  record: PropTypes.object,
  source: PropTypes.string.isRequired
};

OneReferenceField.displayName = 'OneReferenceField';

export default compose(
  pure,
  withTranslate
)(OneReferenceField);
