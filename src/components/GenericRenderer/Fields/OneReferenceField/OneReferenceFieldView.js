import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { LinearProgress, Link } from 'react-admin';
import { withStyles } from '@material-ui/core/styles';
import { get, isEmpty } from 'lodash';

import TextField from '../TextField';

const styles = () => ({
  progress: {
    width: 42
  },
  link: {
    '&:hover': {
      pointer: 'cursor',
      textDecoration: 'underline'
    }
  }
});

// useful to prevent click bubbling in a datagrid with rowClick
const stopPropagation = (e) => e.stopPropagation();

export const ReferenceFieldView = ({
  allowEmpty,
  basePath,
  children,
  className,
  classes = {},
  record,
  reference,
  referenceRecord,
  referenceField,
  resource,
  resourceLinkPath,
  source,
  translateChoice = false,
  fetchedOnce,
  isLoading,
  ...rest
}) => {
  const value =
    referenceField
      .split('+')
      .map((field) => get(referenceRecord, field, ''))
      .filter((el) => el)
      .join(' ') || get(record, source);
  const isWildcard = value === '*';

  if (!isWildcard && !fetchedOnce && isLoading) {
    return <LinearProgress className={classes.progress} />;
  }

  if (resourceLinkPath && value !== 'null') {
    return typeof resourceLinkPath === 'function' ? (
      // eslint-disable-next-line jsx-a11y/click-events-have-key-events
      <div className={cx(classes.link, className)} onClick={resourceLinkPath}>
        {value}
      </div>
    ) : (
      <Link
        to={resourceLinkPath}
        className={cx(classes.link, className)}
        onClick={stopPropagation}
      >
        {value}
      </Link>
    );
  }

  return (
    <TextField
      source={referenceField}
      record={referenceRecord}
      isWildcard={isWildcard}
      {...rest}
      initialValue={value}
      error={
        isEmpty(referenceRecord) && !isEmpty(value) && !isWildcard
          ? 'Reference not found'
          : false
      }
      noEmptyLabel
    />
  );
};

ReferenceFieldView.propTypes = {
  allowEmpty: PropTypes.bool,
  basePath: PropTypes.string,
  children: PropTypes.element,
  className: PropTypes.string,
  classes: PropTypes.object,
  record: PropTypes.object,
  reference: PropTypes.string,
  referenceRecord: PropTypes.object,
  resource: PropTypes.string,
  resourceLinkPath: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  source: PropTypes.string,
  fetchedOnce: PropTypes.bool,
  isLoading: PropTypes.bool,
  translateChoice: PropTypes.bool
};

export default withStyles(styles)(ReferenceFieldView);
