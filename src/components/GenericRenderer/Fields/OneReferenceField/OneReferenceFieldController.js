import { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { get, has, isEqual, capitalize, isEmpty } from 'lodash';
import { compose } from 'recompose';

import {
  crudGetManyAccumulate as crudGetManyAccumulateAction,
  crudGetMany as crudGetManyAction,
  linkToRecord,
  withDataProvider,
  showNotification as showNotificationAction
} from 'react-admin';

import {
  processParams,
  hydrateUrlFromParams
} from '../../../../utils/generic-render';

/**
 * Fetch reference record, and delegate rendering to child component.
 *
 * The reference prop sould be the name of one of the <Resource> components
 * added as <Admin> child.
 *
 * @example
 * <OneReferenceField label="User" source="userId" reference="users">
 *     <TextField source="name" />
 * </OneReferenceField>
 *
 * By default, includes a link to the <Edit> page of the related record
 * (`/users/:userId` in the previous example).
 *
 * Set the linkType prop to "show" to link to the <Show> page instead.
 *
 * @example
 * <OneReferenceField label="User" source="userId" reference="users" linkType="show">
 *     <TextField source="name" />
 * </OneReferenceField>
 *
 * You can also prevent `<OneReferenceField>` from adding link to children by setting
 * `linkType` to false.
 *
 * @example
 * <OneReferenceField label="User" source="userId" reference="users" linkType={false}>
 *     <TextField source="name" />
 * </OneReferenceField>
 */
export class UnconnectedOneReferenceFieldController extends Component {
  static defaultProps = {
    linkType: 'edit',
    referenceRecord: null,
    record: { id: '' }
  };

  static propTypes = {
    record: PropTypes.object,
    linkType: PropTypes.string,
    referenceRecord: PropTypes.any,
    crudGetManyAccumulate: PropTypes.func.isRequired,
    crudGetMany: PropTypes.func.isRequired,
    basePath: PropTypes.string,
    children: PropTypes.any,
    reference: PropTypes.string,
    referenceField: PropTypes.string,
    lazyReference: PropTypes.bool,
    resource: PropTypes.string,
    source: PropTypes.string,
    isLoading: PropTypes.bool,
    dataProvider: PropTypes.func.isRequired,
    history: PropTypes.func.isRequired,
    showNotification: PropTypes.func.isRequired
  };

  state = {
    fetchedOnce: false,
    lazyLink: false,
    localReferenceRecord: {}
  };

  // eslint-disable-next-line react/sort-comp
  componentDidMount = () => {
    if (!this.props.lazyReference) {
      this.fetchReference(this.props);
    }
  };

  componentWillReceiveProps = (nextProps) => {
    if (this.props.lazyReference) {
      return;
    }

    if (!isEqual(this.props.record, nextProps.record) && nextProps.record.id) {
      this.fetchReference(nextProps);
    }
  };

  componentDidUpdate = (
    prevProps,
    { localReferenceRecord: prevLocalReferenceRecord }
  ) => {
    const { history } = this.props;
    const { localReferenceRecord, lazyLink } = this.state;

    if (lazyLink && !isEmpty(localReferenceRecord)) {
      history.push(this.buildResourceLinkPath(lazyLink, localReferenceRecord));
    }
  };

  hasSourceProperty = ({ record = {}, source }) => has(record, source);

  fetchReference = (props) => {
    const {
      crudGetMany,
      crudGetManyAccumulate,
      showNotification,
      dataProvider
    } = this.props;
    const { record = {}, params = {}, verb, accumulateRequests = true } = props;
    const source = get(record, props.source, null);
    const hasSourceProperty = this.hasSourceProperty(props);

    if (!!verb && !!record.id) {
      return dataProvider(verb, props.reference, {
        data: { ...processParams({ params, source: { record } }) }
      })
        .then((response) => {
          const dataContainer = get(response, 'data', []);
          const item = Array.isArray(dataContainer)
            ? dataContainer[0]
            : dataContainer;

          setTimeout(() => this.setFetched(item));
        })
        .catch((error) => showNotification(capitalize(error.message)));
    }

    if (
      hasSourceProperty &&
      source !== null &&
      source !== '*' &&
      typeof source !== 'undefined'
    ) {
      if (accumulateRequests) {
        crudGetManyAccumulate(props.reference, [source]);
      } else {
        crudGetMany(props.reference, [source]);
      }
      setTimeout(() => this.setFetched());
    }

    return false;
  };

  setFetched = (localReferenceRecord = {}) =>
    this.setState({ fetchedOnce: true, localReferenceRecord });

  buildResourceLinkPath = (linkType, referenceRecord) => {
    const { record, basePath, resource, reference, source } = this.props;

    const rootPath = basePath.replace(resource, reference);
    const linkHasPlaceholder =
      typeof linkType === 'string' && linkType.includes('@');
    const resourceLinkPath = !linkType
      ? false
      : linkHasPlaceholder
      ? hydrateUrlFromParams({
          params: { record, referenceRecord },
          url: linkType
        })
      : linkToRecord(rootPath, get(record, source), linkType);

    return resourceLinkPath;
  };

  handleLazyClick = (resourceLink) => (e) => {
    e.preventDefault();
    e.stopPropagation();

    this.setState({ lazyLink: resourceLink }, () =>
      this.fetchReference(this.props)
    );

    return '';
  };

  render = () => {
    const {
      children,
      linkType,
      record,
      referenceRecord = {},
      referenceField,
      lazyReference,
      source,
      isLoading
    } = this.props;
    const { fetchedOnce, lazyLink, localReferenceRecord } = this.state;

    const finalReferenceRecord = isEmpty(localReferenceRecord)
      ? referenceRecord
      : localReferenceRecord;
    let resourceLinkPath = this.buildResourceLinkPath(
      linkType,
      finalReferenceRecord
    );

    if (lazyReference) {
      resourceLinkPath = lazyLink
        ? 'loading'
        : this.handleLazyClick(resourceLinkPath);
    }

    return children({
      source,
      record,
      fetchedOnce,
      referenceRecord: finalReferenceRecord,
      referenceField,
      resourceLinkPath,
      isLoading
    });
  };
}

const mapStateToProps = (
  { admin, loading },
  { reference, record, source, compareBy }
) => ({
  referenceRecord:
    admin.resources[reference] &&
    admin.resources[reference].data[get(record, source)],
  isLoading: loading > 0
});

const OneReferenceFieldController = connect(
  mapStateToProps,
  {
    crudGetManyAccumulate: crudGetManyAccumulateAction,
    crudGetMany: crudGetManyAction,
    showNotification: showNotificationAction
  }
)(UnconnectedOneReferenceFieldController);

export default compose(
  withDataProvider,
  withRouter
)(OneReferenceFieldController);
