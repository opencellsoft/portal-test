import React from 'react';
import PropTypes from 'prop-types';
import { Tooltip, Typography } from '@material-ui/core';
import { pure } from 'recompose';
import { get, isNaN } from 'lodash';

import sanitizeRestProps from './sanitizeRestProps';

const hasNumberFormat = !!(
  typeof Intl === 'object' &&
  Intl &&
  typeof Intl.NumberFormat === 'function'
);

/**
 * Display a numeric value as a locale string.
 *
 * Uses Intl.NumberFormat() if available, passing the locales and options props as arguments.
 * If Intl is not available, it outputs number as is (and ignores the locales and options props).
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/toLocaleString
 * @example
 * <NumberField source="score" />
 * // renders the record { id: 1234, score: 567 } as
 * <span>567</span>
 *
 * <NumberField source="score" className="red" />
 * // renders the record { id: 1234, score: 567 } as
 * <span class="red">567</span>
 *
 * <NumberField source="share" options={{ style: 'percent' }} />
 * // renders the record { id: 1234, share: 0.2545 } as
 * <span>25%</span>
 *
 * <NumberField source="price" options={{ style: 'currency', currency: 'USD' }} />
 * // renders the record { id: 1234, price: 25.99 } as
 * <span>$25.99</span>
 *
 * <NumberField source="price" locales="fr-FR" options={{ style: 'currency', currency: 'USD' }} />
 * // renders the record { id: 1234, price: 25.99 } as
 * <span>25,99 $US</span>
 */
export const NumberField = ({
  className,
  record,
  source,
  locales,
  options,
  textAlign,
  ...rest
}) => {
  if (!record) {
    return null;
  }

  const value = get(record, source, '');
  if (value === null) {
    return null;
  }

  if (!hasNumberFormat) {
    return (
      <Tooltip title={value} placement="bottom-start">
        <Typography
          component="span"
          variant="body1"
          className={className}
          {...sanitizeRestProps(rest)}
        >
          {value}
        </Typography>
      </Tooltip>
    );
  }

  const label = !isNaN(value)
    ? Intl.NumberFormat(locales, options).format(value)
    : '';

  return (
    <Tooltip title={label} placement="bottom-start">
      <Typography
        component="span"
        variant="body1"
        className={className}
        {...sanitizeRestProps(rest)}
      >
        {label}
      </Typography>
    </Tooltip>
  );
};

const EnhancedNumberField = pure(NumberField);

EnhancedNumberField.defaultProps = {
  addLabel: true,
  textAlign: 'right'
};

EnhancedNumberField.propTypes = {
  ...Typography.propTypes,
  locales: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string)
  ]),
  options: PropTypes.object
};

EnhancedNumberField.displayName = 'EnhancedNumberField';

export default EnhancedNumberField;
