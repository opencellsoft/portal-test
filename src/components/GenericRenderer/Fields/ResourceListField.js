import React from 'react';
import PropTypes from 'prop-types';
import { pure } from 'recompose';
import { ReferenceArrayField, SingleFieldList, ChipField } from 'react-admin';

import withTranslate from '../../Hoc/withTranslate';

const ResourceListField = (props) => {
  const { referenceField } = props;

  return (
    <ReferenceArrayField {...props}>
      <SingleFieldList>
        <ChipField source={referenceField} />
      </SingleFieldList>
    </ReferenceArrayField>
  );
};

ResourceListField.propTypes = {
  record: PropTypes.object,
  source: PropTypes.string.isRequired,
  referenceField: PropTypes.string
};

ResourceListField.displayName = 'ResourceListField';
const PureResourceListField = pure(ResourceListField);

export default withTranslate(PureResourceListField);
