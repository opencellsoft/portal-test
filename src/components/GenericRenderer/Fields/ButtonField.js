import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Button as RAButton } from 'react-admin';
import { Button, Tooltip } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import get from 'lodash/get';
import compose from 'recompose/compose';

import { ACTION_MODAL, FORMAT_XML } from '../../../constants/generic';

import DynamicIcon from '../../Globals/DynamicIcon';
import Dialog from '../../Globals/Dialogs/Dialog';
import XMLTree from '../../Globals/XMLFormat';
import withTranslate from '../../Hoc/withTranslate';

const styles = ({ spacing, palette }) => ({
  wrapper: {
    display: 'flex'
  },
  button: {
    paddingLeft: 0,
    paddingRight: 0
  },
  buttonLabel: {
    whiteSpace: 'nowrap'
  }
});

class ButtonField extends PureComponent {
  static propTypes = {
    record: PropTypes.object.isRequired,
    source: PropTypes.string.isRequired,
    translate: PropTypes.func,
    label: PropTypes.string,
    icon: PropTypes.string,
    action: PropTypes.string,
    format: PropTypes.string,
    classes: PropTypes.object
  };

  state = {
    dialog: false
  };

  toggleDialog = (event) => {
    event.stopPropagation();
    event.preventDefault();

    return this.setState({ dialog: !this.state.dialog });
  };

  renderValue = () => {
    const { source, record = {}, format } = this.props;
    const value = get(record, source, '');

    switch (true) {
      case format === FORMAT_XML:
        return <XMLTree xml={value} />;
      default:
        return value;
    }
  };

  render = () => {
    const { classes, icon, action, label, translate } = this.props;
    const { dialog } = this.state;

    return (
      <div className={classes.wrapper}>
        <Tooltip title={label} placement="bottom-start">
          <div>
            <RAButton className={classes.button} onClick={this.toggleDialog}>
              <DynamicIcon icon={icon} alignIcon="left" />
            </RAButton>
          </div>
        </Tooltip>

        {action === ACTION_MODAL && (
          <Dialog
            open={dialog}
            title={label}
            onClose={this.toggleDialog}
            actions={
              <Button onClick={this.toggleDialog} color="primary">
                {translate('ra.action.back')}
              </Button>
            }
          >
            {this.renderValue()}
          </Dialog>
        )}
      </div>
    );
  };
}

export default compose(
  withStyles(styles),
  withTranslate
)(ButtonField);
