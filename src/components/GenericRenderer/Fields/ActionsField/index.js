import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { IconButton, Menu } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';
import { isEmpty, get, includes } from 'lodash';

import MenuItem from './MenuItem';
import MenuItemButton from './MenuItemButton';
import withTranslate from '../../../Hoc/withTranslate';

const ITEM_HEIGHT = 48;

const styles = () => ({
  wrapper: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  menu: {
    top: ITEM_HEIGHT,
    left: -62
  },
  paperProps: {
    maxHeight: ITEM_HEIGHT * 4.5,
    width: 200
  }
});

class ActionsField extends PureComponent {
  static propTypes = {
    record: PropTypes.object.isRequired,
    source: PropTypes.string.isRequired,
    translate: PropTypes.func,
    classes: PropTypes.object,
    items: PropTypes.array.isRequired
  };

  state = {
    menuAnchor: false
  };

  openMenu = (event) => {
    event.stopPropagation();
    event.preventDefault();

    return this.setState({ menuAnchor: event.currentTarget });
  };

  closeMenu = (event) => {
    if (event) {
      event.stopPropagation();
      event.preventDefault();
    }

    return this.setState({ menuAnchor: null });
  };

  render = () => {
    const {
      resource,
      pageResource,
      record = {},
      parentRecord = {},
      classes,
      items,
      rowId: fieldIndex
    } = this.props;
    const { menuAnchor } = this.state;

    const identifier = `menu__${record.id || parentRecord.id}`;
    const menuOpen = !!menuAnchor;

    const standardItems = items.filter(({ inMenu }) => !inMenu);
    const itemsInMenu = items.filter(({ inMenu }) => inMenu);

    return !isEmpty(standardItems) || !isEmpty(itemsInMenu) ? (
      <div className={classes.wrapper}>
        {!isEmpty(standardItems) &&
          standardItems.map((item, index) => {
            const { dependsOn = {} } = item;
            let shouldRender = true;

            Object.keys(dependsOn).forEach((key) => {
              const recordValue = key.includes('@')
                ? get(parentRecord, key.split('.')[1], false)
                : get(record, key, false);

              if (!includes(dependsOn[key], recordValue)) {
                shouldRender = false;
              }
            });

            return (
              shouldRender && (
                <MenuItemButton
                  key={index}
                  onClose={this.closeMenu}
                  resource={resource}
                  record={record}
                  pageResource={pageResource}
                  parentRecord={parentRecord}
                  index={fieldIndex}
                  {...item}
                  noLabel
                />
              )
            );
          })}

        {!isEmpty(itemsInMenu) && (
          <>
            <IconButton
              aria-label="More"
              aria-owns={menuAnchor ? identifier : undefined}
              aria-haspopup="true"
              onClick={this.openMenu}
            >
              <MoreVertIcon />
            </IconButton>

            <Menu
              id={identifier}
              anchorEl={menuAnchor}
              open={menuOpen}
              onClose={this.closeMenu}
              className={classes.menu}
              PaperProps={classes.paperProps}
            >
              {itemsInMenu.map((item, index) => (
                <MenuItem
                  key={index}
                  onClose={this.closeMenu}
                  resource={resource || pageResource}
                  record={record}
                  pageResource={pageResource}
                  parentRecord={parentRecord}
                  index={fieldIndex}
                  {...item}
                />
              ))}
            </Menu>
          </>
        )}
      </div>
    ) : null;
  };
}

export default compose(
  withStyles(styles),
  withTranslate
)(ActionsField);
