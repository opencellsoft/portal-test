import React from 'react';
import PropTypes from 'prop-types';
import { MenuItem } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { isEmpty } from 'lodash';
import { compose } from 'recompose';
import WithRoles from '../../../Hoc/WithRoles';

import MenuItemButton from './MenuItemButton';

const styles = ({ palette }) => ({
  menuItem: {
    paddingLeft: 0,
    paddingRight: 0
  }
});

const CustomMenuItem = ({ classes, roles = [], ...props }) => {
  const menuItem = (
    <MenuItem className={classes.menuItem}>
      <MenuItemButton {...props} />
    </MenuItem>
  );

  return !isEmpty(roles) ? (
    <WithRoles roles={roles}>{menuItem}</WithRoles>
  ) : (
    menuItem
  );
};

CustomMenuItem.propTypes = {
  classes: PropTypes.object.isRequired
};

export default compose(withStyles(styles))(CustomMenuItem);
