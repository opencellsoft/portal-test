import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { REDUX_FORM_NAME } from 'react-admin';
import { destroy } from 'redux-form';
import { Route } from 'react-router-dom';
import { Drawer } from '@material-ui/core';

class EditAction extends PureComponent {
  static propTypes = {
    resource: PropTypes.string.isRequired,
    editUrl: PropTypes.string,
    dispatch: PropTypes.func.isRequired,
    history: PropTypes.func.isRequired,
    form: PropTypes.string
  };

  handleClose = () => {
    const { history, resource } = this.props;

    this.resetForm();

    return history.push(`/${resource}`);
  };

  resetForm = () =>
    this.props.dispatch(destroy(this.props.form || REDUX_FORM_NAME));

  render = () => {
    const { editUrl } = this.props;

    return (
      <Route path={editUrl}>
        {({ match }) => {
          const isMatch = match && match.params && match.params.id !== 'create';

          return (
            <Drawer open={isMatch} anchor="right" onClose={this.handleClose}>
              {isMatch ? <div>working</div> : null}
            </Drawer>
          );
        }}
      </Route>
    );
  };
}

export default EditAction;
