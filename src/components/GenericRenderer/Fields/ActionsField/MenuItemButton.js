import React, { PureComponent, cloneElement } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import {
  withDataProvider,
  showNotification as showNotificationAction,
  refreshView as refreshViewAction,
  getDefaultValues
} from 'react-admin';
import inflection from 'inflection';
import { connect } from 'react-redux';
import {
  getFormValues,
  isValid,
  reset,
  touch as touchAction,
  reduxForm
} from 'redux-form';
import { Link } from 'react-router-dom';
import { Button, Drawer, Tooltip } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';
import { get, capitalize } from 'lodash';

import {
  ACTION_API,
  ACTION_MODAL,
  FORMAT_XML,
  ACTION_DOWNLOAD,
  MODAL_REDUX_FORM_NAME
} from '../../../../constants/generic';
import {
  processParams,
  hydrateUrlFromParams,
  hydrateStringFromParams
} from '../../../../utils/generic-render';

import DynamicIcon from '../../../Globals/DynamicIcon';
import Dialog from '../../../Globals/Dialogs/Dialog';
import ConfirmDialog from '../../../Globals/Dialogs/ConfirmDialog';
import XMLTree from '../../../Globals/XMLFormat';
import Row from '../../../Globals/Row';
import Form from '../../Form';
import InputRenderer from '../../Form/InputRenderer';

import { downloadFile } from '../../../../utils/files';
import { collectErrors } from '../../../../utils/form';
import { flattenKeys } from '../../../../utils/general';

import withTranslate from '../../../Hoc/withTranslate';

const styles = ({ palette }) => ({
  menuItem: {
    paddingLeft: 0,
    paddingRight: 0
  },
  button: {
    display: 'flex',
    flex: 1,
    justifyContent: 'flex-start',
    fontWeight: 'bold',
    minWidth: '15px',
    padding: '10px 25px',
    '&:hover': {
      background: 'none'
    }
  },
  noLabelButton: {
    padding: 0,
    minWidth: 48
  },
  menuItemIcon: {
    marginRight: 10,
    fontSize: 20,
    color: palette.primary.main
  },
  noLabelIcon: {
    marginRight: 0
  },
  drawerRow: {
    gridTemplateColumns: 'repeat(auto-fit, minmax(220px, auto))'
  }
});

const sanitizeRestProps = ({
  anyTouched,
  array,
  asyncBlurFields,
  asyncValidate,
  asyncValidating,
  autofill,
  blur,
  change,
  clearAsyncError,
  clearFields,
  clearSubmit,
  clearSubmitErrors,
  destroy,
  dirty,
  dispatch,
  form,
  handleSubmit,
  initialize,
  initialized,
  initialValues,
  pristine,
  pure,
  redirect,
  reset,
  resetSection,
  save,
  staticContext,
  submit,
  submitFailed,
  submitSucceeded,
  submitting,
  touch,
  translate,
  triggerSubmit,
  undoable,
  untouch,
  valid,
  validate,
  defaultTitle,
  ...props
}) => props;

class MenuItemButton extends PureComponent {
  static propTypes = {
    resource: PropTypes.string.isRequired,
    record: PropTypes.object.isRequired,
    source: PropTypes.string,
    classes: PropTypes.object.isRequired,
    href: PropTypes.string,
    action: PropTypes.string,
    verb: PropTypes.string,
    params: PropTypes.string,
    icon: PropTypes.string,
    index: PropTypes.number,
    pageResource: PropTypes.string,
    parentRecord: PropTypes.object,
    label: PropTypes.string.isRequired,
    status: PropTypes.string,
    format: PropTypes.string,
    upload: PropTypes.bool,
    onClose: PropTypes.func.isRequired,
    noLabel: PropTypes.bool,
    dispatch: PropTypes.func.isRequired,
    dataProvider: PropTypes.func.isRequired,
    showNotification: PropTypes.func.isRequired,
    refreshView: PropTypes.func.isRequired,
    translate: PropTypes.func.isRequired,
    isFormValid: PropTypes.bool.isRequired,
    formErrors: PropTypes.object.isRequired,
    touch: PropTypes.func.isRequired
  };

  state = {
    dialog: false,
    drawer: false,
    shouldConfirm: null
  };

  componentWillUnmount = () => this.resetForm();

  toggleDialog = () => {
    const { onClose } = this.props;
    const { dialog } = this.state;

    return this.setState({ dialog: !dialog }, () => {
      if (dialog && onClose) {
        onClose();
      }
    });
  };

  toggleDrawer = () =>
    this.setState({ drawer: !this.state.drawer }, this.resetForm);

  resetForm = () =>
    this.props.dispatch(reset(this.props.form || MODAL_REDUX_FORM_NAME));

  b64EncodeUnicode = (str) =>
    btoa(
      encodeURIComponent(str).replace(
        /%([0-9A-F]{2})/g,
        // eslint-disable-next-line prefer-template
        (match, p1) => String.fromCharCode('0x' + p1)
      )
    );

  handleDownLoad = () => {
    const { record, fileType, source, label } = this.props;

    const regex =
      '^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$';
    const raw = get(record, source);

    if (raw.match(regex)) {
      return this.handleFile(raw);
    }

    return downloadFile(get(record, source), label, fileType);
  };

  handleFile = (raw) => {
    const {
      action,
      record,
      pageRecord,
      isFile,
      upload,
      source,
      fileName
    } = this.props;

    if (action !== ACTION_DOWNLOAD && (!isFile || upload)) {
      return raw;
    }

    const link = document.createElement('a');
    const nameArr = get(record, source, '').split('/');
    const regex =
      '^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$';
    link.href = `data:application/pdf;base64,${
      raw.match(regex) ? raw : this.b64EncodeUnicode(raw)
    }`;

    link.download = fileName
      ? hydrateStringFromParams({
          params: {
            record,
            pageRecord
          },
          string: fileName
        })
      : nameArr[nameArr.length - 1];

    return link.click();
  };

  // eslint-disable-next-line consistent-return
  handleAPICall = () => {
    const {
      resource,
      record,
      parentRecord: pageRecord,
      verb,
      params,
      confirm,
      onClose,
      formValues,
      dataProvider,
      showNotification,
      refreshView,
      translate,
      upload,
      isFormValid,
      formErrors,
      touch,
      form
    } = this.props;
    const { shouldConfirm } = this.state;

    if (!confirm && onClose) {
      onClose();
    }

    const confirmValues = (get(confirm, 'fields') || []).reduce(
      (res, { source }) => ({ ...res, [source]: formValues[source] }),
      {}
    );

    const paramsToProcess = {
      ...params,
      ...(shouldConfirm && !upload ? confirmValues : {})
    };

    let requestData;
    try {
      requestData = processParams({
        params: paramsToProcess,
        source: { record, pageRecord }
      });
    } catch (error) {
      return showNotification(error.message, 'error');
    }

    if (isFormValid) {
      dataProvider(verb, resource, {
        data: requestData
      })
        .then(({ data: raw }) => this.handleFile(raw))
        .then(() => {
          showNotification(translate('input.action.success'));
          refreshView();
          this.setShouldConfirm(null);
        })
        .catch((error) => showNotification(error.message, 'error'))
        .finally(() => onClose && onClose());
    } else {
      touch(form, ...Object.keys(flattenKeys(formErrors)));
      showNotification('ra.message.invalid_form');
    }
  };

  renderDialog = () => {
    const { source, record = {}, format } = this.props;
    const value = get(record, source, '');

    switch (true) {
      case format === FORMAT_XML:
        return <XMLTree xml={value} />;
      default:
        return value;
    }
  };

  setShouldConfirm = (shouldConfirm) =>
    this.setState({ shouldConfirm }, () => {
      const { onClose } = this.props;

      return !shouldConfirm && onClose && onClose();
    });

  handleConfirm = (event, cb) => {
    const { confirm } = this.props;

    event.preventDefault();
    event.stopPropagation();

    return confirm ? this.setShouldConfirm(cb) : cb();
  };

  determineItemComponent = () => {
    const { href, action, record } = this.props;

    switch (true) {
      case !!href:
        return (
          <Link to={hydrateUrlFromParams({ params: { record }, url: href })} />
        );
      case action === 'edit':
        return (
          <Button onClick={(e) => this.handleConfirm(e, this.toggleDrawer)} />
        );
      case action === ACTION_API:
        return (
          <Button onClick={(e) => this.handleConfirm(e, this.handleAPICall)} />
        );
      case action === ACTION_MODAL:
        return (
          <Button onClick={(e) => this.handleConfirm(e, this.toggleDialog)} />
        );
      case action === ACTION_DOWNLOAD:
        return (
          <Button onClick={(e) => this.handleConfirm(e, this.handleDownLoad)} />
        );
      default:
        return (
          <Button onClick={(e) => this.handleConfirm(e, this.handleAPICall)} />
        );
    }
  };

  getEditUrl = () => {
    const {
      resource,
      pageResource,
      record,
      parentRecord,
      source,
      index
    } = this.props;
    const relatedResource = pageResource || resource;
    const relatedRecord = parentRecord || record || {};

    return `/${relatedResource}/${relatedRecord.id}/${source}/${index}`;
  };

  handleSave = (data) => {
    const {
      resource,
      record,
      dataProvider,
      refreshView,
      showNotification
    } = this.props;

    return dataProvider('UPDATE', resource, {
      data: {
        ...record,
        ...data
      }
    })
      .then(refreshView)
      .then(this.resetForm)
      .catch((error) => showNotification(capitalize(error.message)));
  };

  renderContextualElements = () => {
    const {
      label,
      action,
      fields,
      pageResource,
      form,
      index,
      resource,
      parentRecord,
      record,
      confirm,
      confirmMessage = true,
      formValues = {},
      translate,
      initialValues,
      classes
    } = this.props;
    const { dialog, drawer, shouldConfirm } = this.state;

    const relatedResource = pageResource || resource;
    const relatedRecord = parentRecord || record || {};
    const confirmFields = get(confirm, 'fields') || [];
    const finalConfirmMessage =
      typeof confirmMessage === 'string'
        ? confirmMessage
        : confirmMessage !== false
        ? 'confirm_content'
        : false;

    switch (true) {
      case !!shouldConfirm:
        return (
          <ConfirmDialog
            open
            title="confirm"
            content={finalConfirmMessage}
            translateOptions={{
              label: inflection.humanize(inflection.singularize(label), true)
            }}
            onConfirm={shouldConfirm}
            resource={resource}
            onClose={() => this.setShouldConfirm(null)}
            formValues={formValues}
            initialValues={initialValues}
          >
            {confirmFields.map((field, key) => (
              <Row key={key} grid>
                <InputRenderer
                  {...field}
                  record={record}
                  form={form || MODAL_REDUX_FORM_NAME}
                />
              </Row>
            ))}
          </ConfirmDialog>
        );
      case action === ACTION_MODAL:
        return (
          <Dialog
            open={dialog}
            title={label}
            onClose={this.toggleDialog}
            actions={
              <Button onClick={this.toggleDialog} color="primary">
                {translate('ra.action.back')}
              </Button>
            }
          >
            {this.renderDialog()}
          </Dialog>
        );
      case action === 'edit':
        return (
          <Drawer open={drawer} anchor="right" onClose={this.toggleDrawer}>
            {drawer ? (
              <Form
                {...sanitizeRestProps(this.props)}
                id={relatedRecord.id}
                index={index}
                resource={relatedResource}
                basePath={`/${relatedResource}`}
                config={{ rows: [fields] }}
                save={this.handleSave}
                redirectAfterSubmit={this.toggleDrawer}
                isEdit
                inDrawer
                rowClassname={classes.drawerRow}
              />
            ) : null}
          </Drawer>
        );
      default:
        return null;
    }
  };

  render = () => {
    const { label, icon, noLabel, classes } = this.props;

    return (
      <>
        <Tooltip title={label} placement="bottom-start">
          {cloneElement(this.determineItemComponent(), {
            className: cx(classes.button, noLabel && classes.noLabelButton),
            children: (
              <>
                <DynamicIcon
                  icon={icon}
                  className={cx(
                    classes.menuItemIcon,
                    noLabel && classes.noLabelIcon
                  )}
                />

                {!noLabel && <div>{label}</div>}
              </>
            )
          })}
        </Tooltip>

        {this.renderContextualElements()}
      </>
    );
  };
}

export default compose(
  connect(
    (state, props) => {
      const form = props.form || MODAL_REDUX_FORM_NAME;

      return {
        form,
        initialValues: getDefaultValues(state, props),
        saving: props.saving || state.admin.saving,
        formValues: getFormValues(form)(state),
        isFormValid: isValid(form)(state),
        formErrors: collectErrors(state, form)
      };
    },
    {
      showNotification: showNotificationAction,
      refreshView: refreshViewAction,
      touch: touchAction
    }
  ),
  withTranslate,
  withDataProvider,
  reduxForm({
    form: MODAL_REDUX_FORM_NAME,
    destroyOnUnmount: false,
    enableReinitialize: true,
    keepDirtyOnReinitialize: true
  }),
  withStyles(styles)
)(MenuItemButton);
