import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import cx from 'classnames';
import { Card, CardContent, Typography } from '@material-ui/core';
import { changeLocale } from 'react-admin';
import { withStyles } from '@material-ui/core/styles';
import compose from 'recompose/compose';
import SettingsSystemDaydreamIcon from '@material-ui/icons/SettingsSystemDaydream';
import AddIcon from '@material-ui/icons/Add';
import MoreVertIcon from '@material-ui/icons/Settings';

import withTranslate from '../Hoc/withTranslate';

import { changeTheme } from '../../actions/theme';
import { STATUS_ACTIVE } from '../../assets/data/status';

import StatusTextField from '../GenericRenderer/Fields/StatusField';
import Title from '../Layout/Title';

const styles = ({ palette, spacing }) => ({
  label: { width: '10em', display: 'inline-block' },
  button: { margin: '1em' },
  topWrapper: {
    display: 'flex',
    flexWrap: 'wrap',
    margin: '0 8px 1rem'
  },
  card: {
    display: 'flex',
    position: 'relative',
    flexBasis: 220,
    flexGrow: 0,
    flexShrink: 0,
    margin: '15px 25px 15px 0'
  },
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    '&:last-child': {
      paddingBottom: spacing.unit * 2
    }
  },
  icon: {
    marginBottom: 10,
    fontSize: 52
  },
  title: {
    display: 'flex',
    alignItems: 'flex-end',
    flex: 1,
    fontSize: '.8rem',
    fontWeight: 500,
    maxWidth: 100
  },
  status: {
    position: 'absolute',
    top: 10,
    right: 14,
    height: 25,
    fontSize: '0.6rem',
    fontWeight: 'bold'
  },
  admin: {
    color: palette.secondary.main
  },
  marketingManager: {
    color: 'rgb(240, 137, 90)'
  },
  customerCare: {
    color: 'rgb(28, 157, 202)'
  },
  addInstance: {
    color: 'rgb(153, 153, 153)',
    alignSelf: 'center'
  },
  detailWrapper: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    minWidth: 200
  },
  cardLabel: {
    fontWeight: 300,
    textTransform: 'uppercase'
  },
  bottomWrapper: {
    position: 'relative',
    display: 'flex',
    flex: 1,
    justifyContent: 'space-between'
  },
  more: {
    fontSize: 18,
    alignSelf: 'flex-end',
    marginBottom: '0.35em',
    position: 'absolute',
    right: -10,
    bottom: -2,
    color: '#666'
  }
});

const Configuration = ({
  classes,
  theme,
  locale,
  changeTheme,
  changeLocale,
  translate
}) => (
  <>
    <Title defaultTitle={translate('configuration')} />

    <div className={classes.topWrapper}>
      <Card className={classes.card}>
        <CardContent className={classes.wrapper}>
          <StatusTextField
            record={{ status: STATUS_ACTIVE }}
            source="status"
            classes={{ value: classes.status }}
          />

          <SettingsSystemDaydreamIcon
            className={cx(classes.icon, classes.admin)}
          />

          <div className={classes.bottomWrapper}>
            <Typography gutterBottom component="div" className={classes.title}>
              Admin
            </Typography>

            <MoreVertIcon className={classes.more} />
          </div>
        </CardContent>
      </Card>

      {/* <Card className={classes.card}>
        <CardContent className={classes.wrapper}>
          <StatusTextField
            record={{ status: STATUS_INACTIVE }}
            source="status"
            classes={{ value: classes.status }}
          />
          <TrendingUpIcon
            className={cx(classes.icon, classes.marketingManager)}
          />

          <div className={classes.bottomWrapper}>
            <Typography gutterBottom component="div" className={classes.title}>
              Marketing Manager
            </Typography>

            <MoreVertIcon className={classes.more} />
          </div>
        </CardContent>
      </Card> */}

      {/* <Card className={classes.card}>
        <CardContent className={classes.wrapper}>
          <StatusTextField
            record={{ status: STATUS_INACTIVE }}
            source="status"
            classes={{ value: classes.status }}
          />
          <PeopleIcon className={cx(classes.icon, classes.customerCare)} />

          <div className={classes.bottomWrapper}>
            <Typography gutterBottom component="div" className={classes.title}>
              Customer Care
            </Typography>

            <MoreVertIcon className={classes.more} />
          </div>
        </CardContent>
      </Card> */}

      <Card className={classes.card}>
        <CardContent className={classes.wrapper}>
          <AddIcon className={cx(classes.icon, classes.addInstance)} />
          <Typography gutterBottom component="div" className={classes.title}>
            Add
          </Typography>
        </CardContent>
      </Card>
    </div>

    {/* <Card> */}
    {/* <CardContent>
        <div className={classes.label}>{translate('theme.name')}</div>
        <Button
          variant="raised"
          className={classes.button}
          color={theme === 'light' ? 'primary' : 'default'}
          onClick={() => changeTheme('light')}
        >
          {translate('theme.light')}
        </Button>
        <Button
          variant="raised"
          className={classes.button}
          color={theme === 'dark' ? 'primary' : 'default'}
          onClick={() => changeTheme('dark')}
        >
          {translate('theme.dark')}
        </Button>
      </CardContent>

      <CardContent>
        <div className={classes.label}>{translate('language')}</div>
        <Button
          variant="raised"
          className={classes.button}
          color={locale === 'en' ? 'primary' : 'default'}
          onClick={() => changeLocale('en')}
        >
          en
        </Button>
        <Button
          variant="raised"
          className={classes.button}
          color={locale === 'fr' ? 'primary' : 'default'}
          onClick={() => changeLocale('fr')}
        >
          fr
        </Button>
      </CardContent> */}
    {/* </Card> */}
  </>
);

Configuration.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.string.isRequired,
  locale: PropTypes.string.isRequired,
  changeTheme: PropTypes.func.isRequired,
  changeLocale: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired
};

export default compose(
  connect(
    ({ theme: { key: theme }, i18n }) => ({
      theme,
      locale: i18n.locale
    }),
    {
      changeLocale,
      changeTheme
    }
  ),
  withTranslate,
  withStyles(styles)
)(Configuration);
