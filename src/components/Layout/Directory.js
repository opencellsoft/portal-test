import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { withStyles } from '@material-ui/core';
import Title from './Title';

const styles = () => ({
  list: {
    listStyleType: 'none'
  },
  link: {
    display: 'inline-block',
    padding: '0.25em',
    color: '#477cdd',
    textDecoration: 'none',
    fontSize: '1.2em',
    fontWeight: 'bold',

    '&:hover': {
      pointer: 'cursor',
      textDecoration: 'underline'
    }
  }
});

const Directory = ({ subItems, classes }) => (
  <>
    <Title defaultTitle="" />
    <ul className={classes.list}>
      {Object.entries(subItems).map(([resource, { label }]) => (
        <li>
          <NavLink to={`/${resource}`} className={classes.link}>
            {label}
          </NavLink>
        </li>
      ))}
    </ul>
  </>
);

Directory.propTypes = {
  subItems: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Directory);
