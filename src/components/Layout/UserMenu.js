import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { UserMenu } from 'react-admin';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';

import CustomActions from '../GenericRenderer/Table/CustomActions';

import withTranslate from '../Hoc/withTranslate';

const styles = ({ palette, spacing }) => ({
  wrapper: {
    marginTop: 20
  },
  cfWrapper: {
    marginTop: '20px'
  },
  row: {
    gridTemplateColumns: `repeat(3, minmax(220px, auto))`
  },
  dynamicInputRow: {
    marginLeft: '16px'
  },
  body: { display: 'flex', alignItems: 'flex-end' },

  // this class is used to override root Button class in CustomActionButton
  root: {
    color: palette.text.secondary,
    height: spacing.unit * 6,
    minWidth: '100%',
    display: 'flex',
    fontSize: '1rem',
    fontWeight: 400,
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderRadius: 0,
    lineHeight: '1.5em',
    whiteSpace: 'nowrap',
    paddingLeft: spacing.unit * 2,
    textOverflow: 'ellipsis',
    paddingRight: spacing.unit * 2
  },
  label: {
    textTransform: 'initial'
  },
  active: {
    color: palette.text.primary
  },
  icon: { paddingRight: spacing.unit * 2 }
});

const CustomUserMenu = ({
  translate,
  user,
  config,
  currentTheme = {},
  onClick = () => {},
  classes,
  ...rest
}) => {
  const { 'user-menu': userMenu } = config;
  const { label = 'userMenu', items: menuItems = [] } = userMenu || {};

  return (
    <UserMenu {...rest} className={classes.wrapper} label={label}>
      <CustomActions
        record={user}
        actions={menuItems}
        classesOverride={classes}
      />
    </UserMenu>
  );
};

CustomUserMenu.propTypes = {
  translate: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  currentTheme: PropTypes.object,
  onClick: PropTypes.func,
  classes: PropTypes.object.isRequired
};

export default compose(
  connect(({ user, config: { config }, theme: { currentTheme } }) => ({
    user,
    config,
    currentTheme
  })),
  withStyles(styles),
  withTranslate
)(CustomUserMenu);
