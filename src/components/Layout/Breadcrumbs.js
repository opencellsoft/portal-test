import React, { useCallback, useState } from 'react';
import { IconButton, Menu } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import { Responsive } from 'react-admin';

import withTranslate from '../Hoc/withTranslate';

import {
  buildPathFromResource,
  getHomePageFromConfig
} from '../../utils/routing';
import BreadcrumbsItem from './BreadcrumbsItem';

const styles = () => ({
  wrapper: {
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    display: 'flex',
    alignItems: 'center'
  },
  link: {
    padding: '.75em',
    textDecoration: 'none',
    color: 'inherit'
  }
});

const Breadcrumbs = ({ config, pathname, classes, appTitle, translate }) => {
  const resourceIndex = getHomePageFromConfig(config);

  const [menuAnchorEl, setMenuAnchorEl] = useState(null);
  const handleClose = useCallback(
    () => {
      setMenuAnchorEl(null);
    },
    [setMenuAnchorEl]
  );
  const handleOpen = useCallback(
    (event) => {
      setMenuAnchorEl(event.target);
    },
    [setMenuAnchorEl]
  );

  const menuIsOpen = !!menuAnchorEl;

  const resource = pathname.split('/')[1];

  const path = [
    ...(resourceIndex !== resource
      ? [
          {
            resource: resourceIndex,
            label: translate('index')
          }
        ]
      : []),
    ...(buildPathFromResource(pathname, config.menu) || [
      {
        resource,
        ...config.modules[resource]
      }
    ])
  ];

  return (
    <div className={classes.wrapper}>
      <Responsive
        medium={
          <>
            {path.length > 1 && (
              <>
                <IconButton onClick={handleOpen} color="inherit">
                  <MoreHorizIcon />
                </IconButton>
                <Menu
                  anchorEl={menuAnchorEl}
                  open={menuIsOpen}
                  onClose={handleClose}
                >
                  {path.slice(0, -1).map(({ resource, label }) => (
                    <BreadcrumbsItem
                      key={resource}
                      resource={resource}
                      label={label}
                      inMenu
                    />
                  ))}
                </Menu>
              </>
            )}
            {path.slice(-1).map(({ resource, label }) => (
              <BreadcrumbsItem
                key={resource}
                resource={!!config.modules[resource] && resource}
                label={label}
                isLast={!appTitle}
              />
            ))}
          </>
        }
        large={
          <>
            {path.map(({ resource, label }, idx, pathElements) => (
              <BreadcrumbsItem
                key={resource}
                resource={resource}
                label={label}
                isLast={!appTitle && idx === pathElements.length - 1}
              />
            ))}
          </>
        }
      />
      {appTitle && <BreadcrumbsItem label={appTitle} isLast />}
    </div>
  );
};

Breadcrumbs.propTypes = {
  classes: PropTypes.object.isRequired,
  config: PropTypes.object.isRequired,
  pathname: PropTypes.string.isRequired,
  appTitle: PropTypes.string.isRequired,
  translate: PropTypes.func.isRequired
};

export default compose(
  connect(
    ({
      config: { config },
      router: {
        location: { pathname }
      },
      layout: { appTitle }
    }) => ({
      config,
      pathname,
      appTitle
    })
  ),
  withStyles(styles),
  withTranslate
)(Breadcrumbs);
