import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { compose } from 'recompose';
import { withStyles } from '@material-ui/core/styles';
import { get, isEmpty, sortBy } from 'lodash';

import { getSubItems, getSubMenu, pathPop } from '../../utils/routing';

import withTranslate from '../Hoc/withTranslate';

import DynamicIcon from '../Globals/DynamicIcon';
import Responsive from '../Globals/Responsive';

import MenuHeader from './MenuHeader';
import MenuFooter from './MenuFooter';
import MenuItem from './MenuItem';

const styles = ({ typography, palette, breakpoints, spacing }) => ({
  linkActive: {
    fontWeight: 'bold',
    backgroundColor: `#999999 !important`,
    boxShadow: `0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px #9999998c`,
    color: 'white',
    '&:hover': {
      opacity: 0.7
    }
  },
  menuItemIcon: {
    display: 'flex',
    fontSize: '1rem',
    width: '1rem',
    marginLeft: 0,
    paddingLeft: 0,
    color: 'inherit'
  },
  icon: {
    height: 22,
    fontSize: 18
  },
  menuItemText: {
    alignItems: 'center',
    margin: '10px 15px 0',
    borderRadius: 6,
    textTransform: 'capitalize',
    fontSize: 13,
    fontFamily: typography.fontFamily,
    padding: '5px 15px',
    transition: 'all 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
    '&:hover': {
      fontWeight: 'bold',
      color: 'white',
      backgroundColor: `#999999`,
      boxShadow: `0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px #9999998c`
    }
  },
  title: {
    display: 'flex',
    alignItems: 'center',
    height: '100%',
    marginLeft: '.5em',
    fontSize: '1rem',
    marginTop: 2,
    fontWeight: 300,
    textTransform: 'uppercase',
    letterSpacing: '.15rem',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    [breakpoints.down('xs')]: {
      marginLeft: 0
    }
  },
  menuBreadcrumb: {
    display: 'flex',
    justifyContent: 'center',
    borderTop: '1px solid #f0f0f0',
    borderBottom: '1px solid #f0f0f0'
  },
  breadcrumbItem: {
    display: 'flex',
    alignItems: 'center',
    textTransform: 'capitalize',
    ...typography.subheading,
    color: palette.text.secondary,
    margin: `0 ${spacing.unit}px`,
    padding: 0
  },
  portalTitle: {
    marginLeft: 20
  },
  portalTitleOpen: {
    marginLeft: 75,
    marginTop: 15,
    fontWeight: 'bold'
  },
  menuWrapper: {
    position: 'fixed',
    top: 0,
    bottom: 0,
    left: 0,
    width: 75,
    zIndex: 1,
    boxShadow: '0 4px 25px 0 rgba(0,0,0,.12), 0 8px 10px -5px rgba(0,0,0,.2)',
    transition: 'all .4s',
    paddingBottom: '10vh',
    overflowY: 'auto',
    scrollbarWidth: 'none',
    '-ms-overflow-style': 'none',
    background: 'white',
    '&::-webkit-scrollbar': {
      width: '0 !important'
    }
  },
  relativeWrapper: {
    position: 'relative'
  },
  scrollWrapperPadding: {
    paddingTop: 120
  },
  menuOpen: {
    width: 240
  },
  wrapperBottom: {
    bottom: '0',
    position: 'fixed',
    width: 240,
    height: 32,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    background: 'white',
    borderTop: `1px solid ${palette.background.default}`
  }
});

class CustomMenu extends PureComponent {
  static propTypes = {
    config: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
    translate: PropTypes.func.isRequired
  };

  currentMenu = null;

  state = {
    currentPath: ''
  };

  componentDidMount = () => {
    this.setup(this.props);
  };

  componentDidUpdate = ({ pathname: prevPathname }) => {
    const { pathname } = this.props;

    if (pathname !== prevPathname) {
      this.setup(this.props);
    }
  };

  setup = (props) => {
    const { pathname } = props;

    // This should be called FIRST
    this.setCurrentMenu();

    const currentPath = this.checkPath(
      pathname
        .split('/')
        .slice(1)
        .join('/')
    );

    this.setCurrentPath(currentPath);
  };

  checkPath = (currentPath) => {
    const subMenu = getSubMenu(this.currentMenu, currentPath);

    if (currentPath.length && (!subMenu || !subMenu.subItems)) {
      return this.checkPath(pathPop(currentPath));
    }

    return currentPath;
  };

  setCurrentMenu = () => {
    const { config } = this.props;
    const { menu, modules } = config || {};

    const filteredModules = Object.keys(modules).reduce(
      (res, key) => ({
        ...res,
        ...(typeof modules[key].inMenu === 'boolean' && !!modules[key].inMenu
          ? { [key]: modules[key] }
          : {})
      }),
      {}
    );

    if (isEmpty(menu) && isEmpty(filteredModules)) {
      return false;
    }

    this.currentMenu = { ...filteredModules, ...menu };

    return this.currentMenu;
  };

  setCurrentPath = (currentPath) => {
    this.setState({ currentPath });
  };

  handleClick = (nextPath) => this.setCurrentPath(nextPath);

  handleBack = () => {
    const { currentPath } = this.state;
    this.setCurrentPath(pathPop(currentPath));
  };

  handleItemsPriority = (menu, menuKeys) =>
    sortBy(menuKeys, [
      (key) => (key === 'setup' ? 999 : get(menu[key], 'menuPriority', 10))
    ]);

  isActiveContainer = (srcPathname) => {
    const { pathname } = this.props;

    return pathname.includes(srcPathname);
  };

  renderItems = (menu, menuKeys) => {
    const { sidebarOpen, classes, translate } = this.props;

    return this.handleItemsPriority(menu, menuKeys).map((key) => {
      const item = menu[key];
      const {
        label,
        icon = 'KeyboardArrowRight',
        path,
        subItems,
        extraProps = {},
        requiredRoles = []
      } = item;
      const isContainer = typeof subItems === 'object' && !isEmpty(subItems);
      const isActiveContainer = this.isActiveContainer(key);

      return (
        <MenuItem
          key={key}
          to={path || `/${key}`}
          component={isContainer ? 'div' : NavLink}
          resource={key}
          withTooltip={!sidebarOpen}
          primaryText={translate(label)}
          leftIcon={
            icon && <DynamicIcon className={classes.menuItemIcon} icon={icon} />
          }
          classes={{
            root: cx(classes.menuItemText),
            active: classes.linkActive
          }}
          requiredRoles={requiredRoles}
          {...extraProps}
          onClick={isContainer ? () => this.handleClick(key) : null}
          isContainer={isContainer}
          isContainerActive={isActiveContainer}
        />
      );
    });
  };

  renderMenu = (menu = {}, wrapper, props = {}) => {
    const { logout, sidebarOpen, classes, config } = this.props;
    const { currentPath } = this.state;

    const menuKeys = Object.keys(menu);

    const previousMenuRoute = pathPop(currentPath);

    const previousMenuLevel =
      !previousMenuRoute.length && currentPath.length
        ? { label: 'Home' }
        : getSubMenu(config.menu, previousMenuRoute);
    const menuLevel = getSubMenu(config.menu, currentPath);

    return (
      <>
        <div
          className={cx(classes.menuWrapper, sidebarOpen && classes.menuOpen)}
        >
          <div className={classes.relativeWrapper}>
            <MenuHeader
              previousCategoryLabel={
                previousMenuLevel && previousMenuLevel.label
              }
              categoryLabel={menuLevel && menuLevel.label}
              currentPath={currentPath}
              handleBack={this.handleBack}
            />

            <div
              className={cx(
                classes.scrollWrapper,
                sidebarOpen && currentPath && classes.scrollWrapperPadding
              )}
            >
              {this.renderItems(menu, menuKeys)}
              <Responsive xsmall={logout} medium={null} />
            </div>

            <div className={classes.bg} />

            <MenuFooter />
          </div>
        </div>
      </>
    );
  };

  render = () => {
    const { config } = this.props;
    const { currentPath } = this.state;
    const { menu = {}, modules = {} } = config || {};

    const filteredModules = Object.keys(modules).reduce(
      (res, key) => ({
        ...res,
        ...(typeof modules[key].inMenu === 'boolean' && !!modules[key].inMenu
          ? { [key]: modules[key] }
          : {})
      }),
      {}
    );

    if (isEmpty(menu) && isEmpty(filteredModules)) {
      return false;
    }

    const currentMenu = { ...filteredModules, ...menu };

    return this.renderMenu(getSubItems(currentMenu, currentPath));
  };
}

const mapStateToProps = ({
  admin: {
    ui: { sidebarOpen }
  },
  config: { config },
  router: {
    location: { pathname }
  }
}) => ({
  sidebarOpen,
  config,
  pathname
});

const enhance = compose(
  connect(
    mapStateToProps,
    {}, // Avoid connect passing dispatch in props,
    null
  ),
  withTranslate,
  withStyles(styles)
);

export default enhance(CustomMenu);
