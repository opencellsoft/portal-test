import React, { cloneElement, PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { MenuItem, Tooltip } from '@material-ui/core';
import { isEmpty, get } from 'lodash';
import { compose } from 'recompose';

import { KeyboardArrowRight } from '@material-ui/icons';

import WithStylesProps from '../Hoc/WithStylesProps';
import WithRoles from '../Hoc/WithRoles';

const styles = (theme, { config: { modules, themeColor }, resource }) => {
  const bgColor = get(modules[resource], 'themeColor') || themeColor;

  return {
    root: {
      color: theme.palette.text.secondary,
      display: 'flex',
      alignItems: 'center',
      '&:hover': {
        backgroundColor: `${bgColor} !important`,
        boxShadow: `0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px ${bgColor}8c`
      }
    },
    active: {
      color: 'white',
      backgroundColor: `${bgColor} !important`,
      boxShadow: `0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px ${bgColor}8c`,
      fontWeight: 'bold',
      '&:hover': {
        opacity: 0.7
      }
    },
    icon: { paddingRight: '1.2em' },
    menuLabel: { flex: 1 },
    dropdownIcon: {
      color: 'inherit',
      fontSize: 18
    }
  };
};

export class MenuItemLink extends PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    className: PropTypes.string,
    leftIcon: PropTypes.node,
    onClick: PropTypes.func,
    primaryText: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    withTooltip: PropTypes.bool,
    staticContext: PropTypes.object,
    to: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired
  };

  handleMenuTap = (e) => this.props.onClick && this.props.onClick(e);

  render = () => {
    const {
      requiredRoles,
      isContainer,
      isContainerActive,
      classes,
      className,
      primaryText,
      leftIcon,
      withTooltip,
      staticContext,
      ...props
    } = this.props;

    const menuItem = (
      <MenuItem
        className={cx(
          classes.root,
          isContainerActive && classes.active,
          className
        )}
        activeClassName={classes.active}
        component={NavLink}
        {...props}
        onClick={this.handleMenuTap}
      >
        {leftIcon && (
          <span className={classes.icon}>
            {cloneElement(leftIcon, { titleAccess: primaryText })}
          </span>
        )}

        <span className={classes.menuLabel}>{primaryText}</span>

        {isContainer && <KeyboardArrowRight className={classes.dropdownIcon} />}
      </MenuItem>
    );

    const finalItem = withTooltip ? (
      <Tooltip title={primaryText} placement="bottom-start">
        {menuItem}
      </Tooltip>
    ) : (
      menuItem
    );

    return !isEmpty(requiredRoles) ? (
      <WithRoles roles={requiredRoles}>{finalItem}</WithRoles>
    ) : (
      finalItem
    );
  };
}

export default compose(
  connect(
    ({ config: { config } }) => ({
      config
    }),
    {}
  ),
  WithStylesProps(styles)
)(MenuItemLink);
