import React from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import cx from 'classnames';
import { MenuItemLink } from 'react-admin';
import { Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import DynamicIcon from '../Globals/DynamicIcon';

const styles = ({ spacing }) => ({
  link: {
    display: 'inline-block',
    padding: '.75em',
    textDecoration: 'none',
    color: 'inherit',
    height: spacing.unit * 3,
    overflow: 'hidden',
    lineHeight: '1.5em'
  },
  inMenuLink: {
    display: 'block'
  },
  arrowRight: {
    verticalAlign: 'middle'
  },
  breadcrubmItem: {
    display: 'flex',
    alignItems: 'center',
    fontSize: '1rem',
    fontWeight: 300,
    textTransform: 'uppercase',
    letterSpacing: '.15rem',
    whiteSpace: 'nowrap',
    position: 'relative'
  },
  inMenuItem: {
    display: 'block',
    '&:after': {
      content: '""'
    }
  },
  inMenuArrowRight: {
    display: 'none'
  }
});

const BreadcrumbsItem = ({ classes, resource, label, id, inMenu, isLast }) => (
  <Typography
    variant="title"
    color="inherit"
    component="span"
    className={cx(classes.breadcrubmItem, inMenu && classes.inMenuItem)}
  >
    {resource ? (
      <MenuItemLink
        to={`/${resource}`}
        className={cx(classes.link, inMenu && classes.inMenuLink)}
        primaryText={label}
      />
    ) : (
      <span className={cx(classes.link, inMenu && classes.inMenuLink)} id={id}>
        {label}
      </span>
    )}
    {!isLast && (
      <DynamicIcon
        icon="KeyboardArrowRight"
        className={cx(classes.arrowRight, inMenu && classes.inMenuArrowRight)}
      />
    )}
  </Typography>
);

BreadcrumbsItem.propTypes = {
  classes: PropTypes.object.isRequired,
  resource: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  label: PropTypes.string.isRequired,
  id: PropTypes.string,
  inMenu: PropTypes.bool,
  isLast: PropTypes.bool
};

BreadcrumbsItem.defaultProps = {
  resource: false,
  inMenu: false,
  isLast: false
};

export default compose(withStyles(styles))(BreadcrumbsItem);
