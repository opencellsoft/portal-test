import React, { cloneElement } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';
import {
  LoadingIndicator,
  toggleSidebar as toggleSidebarAction
} from 'react-admin';
import Headroom from 'react-headroom';
import { AppBar, Toolbar, withWidth } from '@material-ui/core';
import { withStyles, createStyles } from '@material-ui/core/styles';
import compose from 'recompose/compose';

import PageTitle from './PageTitle';
import UserMenu from './UserMenu';

import withTranslate from '../Hoc/withTranslate';

const styles = ({ breakpoints, palette, spacing, transitions }) =>
  createStyles({
    headroom: {
      position: 'fixed',
      zIndex: 3,
      left: 240,
      right: 0
    },
    headroomOpen: {
      left: 90
    },
    header: {
      textTransform: 'capitalize',
      justifyContent: 'flex-start',
      height: 65,
      boxShadow: 'none',
      background: 'transparent',
      color: palette.secondary.main,
      marginTop: spacing.unit,
      marginBottom: spacing.unit
    },
    toolbar: {
      paddingRight: spacing.unit * 5,
      height: 65
    },
    menuButton: {
      marginLeft: '0.5em',
      marginRight: '0.5em'
    },
    menuButtonIconClosed: {
      transition: transitions.create(['transform'], {
        easing: transitions.easing.sharp,
        duration: transitions.duration.leavingScreen
      }),
      transform: 'rotate(0deg)'
    },
    menuButtonIconOpen: {
      transition: transitions.create(['transform'], {
        easing: transitions.easing.sharp,
        duration: transitions.duration.leavingScreen
      }),
      transform: 'rotate(180deg)'
    },
    logo: {
      maxWidth: 128,
      marginLeft: 30,
      flex: 1,
      fill: palette.secondary.main,
      transition: 'all .4s',
      [breakpoints.down('xs')]: {
        display: 'none'
      }
    },
    logoOpen: {
      marginLeft: 0
    },
    title: {
      flex: 1,
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      overflow: 'hidden'
    }
  });

const CustomAppBar = ({
  children,
  classes,
  className,
  logo,
  logout,
  open,
  title,
  sidebarOpen,
  toggleSidebar,
  userMenu,
  width,
  ...rest
}) => (
  <Headroom
    className={cx(classes.headroom, !sidebarOpen && classes.headroomOpen)}
  >
    <AppBar className={classes.header} position="static" {...rest}>
      <Toolbar
        disableGutters
        variant={width === 'xs' ? 'regular' : 'dense'}
        className={classes.toolbar}
      >
        <PageTitle>
          <LoadingIndicator />

          {cloneElement(userMenu, { logout })}
        </PageTitle>
      </Toolbar>
    </AppBar>
  </Headroom>
);

AppBar.propTypes = {
  children: PropTypes.node,
  classes: PropTypes.object,
  className: PropTypes.string,
  logout: PropTypes.element,
  sidebarOpen: PropTypes.bool,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired,
  toggleSidebar: PropTypes.func,
  userMenu: PropTypes.node,
  width: PropTypes.string
};

CustomAppBar.defaultProps = {
  userMenu: <UserMenu />,
  toggleSidebar: () => {}
};

const enhance = compose(
  connect(
    ({
      admin: {
        ui: { sidebarOpen }
      },
      config: { config },
      i18n: { locale }
    }) => ({
      locale, // force redraw on locale change,
      sidebarOpen,
      config
    }),
    {
      toggleSidebar: toggleSidebarAction
    }
  ),
  withWidth(),
  withStyles(styles),
  withTranslate
);

export default enhance(CustomAppBar);
