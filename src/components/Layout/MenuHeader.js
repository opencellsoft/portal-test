import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { toggleSidebar as toggleSidebarAction } from 'react-admin';

// import { Button, IconButton } from '@material-ui/core';
import { IconButton } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import MenuIcon from '@material-ui/icons/Menu';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';

import { ReactComponent as Logo } from '../../assets/img/opencell.svg';
import EIRLogo from '../../assets/img/open_eir.png';
import EdenredLogo from '../../assets/img/edenred.png';
import LaPosteLogo from '../../assets/img/laposte.png';
import SmooveLogo from '../../assets/img/smoove.png';

const { REACT_APP_LOGO } = process.env;

const Logos = {
  open_eir: EIRLogo,
  edenred: EdenredLogo,
  laposte: LaPosteLogo,
  smoove: SmooveLogo
};

const styles = ({
  breakpoints,
  palette,
  spacing,
  transitions,
  typography,
  shadows
}) => ({
  menuHeader: {
    background: 'white',
    zIndex: 1
  },
  menuHeaderOpen: {
    width: 240
  },
  menuHeaderFixed: {
    position: 'fixed',
    left: 0,
    top: 0,
    boxShadow:
      '0 1px 22px rgba(50, 50, 93, 0.01), 0 1px 6px rgba(0, 0, 0, 0.08)'
  },
  wrapperTop: {
    display: 'flex',
    alignItems: 'center',
    height: 65,
    paddingTop: spacing.unit,
    paddingBottom: spacing.unit
  },
  wrapperTopSecondary: {
    height: 'auto',
    paddingBottom: 0,
    paddingTop: spacing.unit * 2
  },
  menuButton: {
    marginLeft: '0.5em',
    marginRight: '0.5em',
    color: palette.secondary.main
  },
  menuButtonIconClosed: {
    transition: transitions.create(['transform'], {
      easing: transitions.easing.sharp,
      duration: transitions.duration.leavingScreen
    }),
    transform: 'rotate(0deg)'
  },
  menuButtonIconOpen: {
    transition: transitions.create(['transform'], {
      easing: transitions.easing.sharp,
      duration: transitions.duration.leavingScreen
    }),
    transform: 'rotate(180deg)'
  },
  topLogoWrapper: {
    marginLeft: 5,
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flex: 1
  },
  topLogo: {
    maxWidth: 65
  },
  logo: {
    maxWidth: 96,
    marginLeft: 30,
    flex: 1,
    fill: '#eb2f2f',
    transition: 'all .4s',
    position: 'fixed',
    left: 75,
    [breakpoints.down('xs')]: {
      display: 'none'
    }
  },
  logoOpen: {
    marginLeft: 0,
    left: 0,
    position: 'relative'
  },
  backButton: {
    display: 'flex'
  },
  previousCategoryLabel: {
    color: palette.secondary.main,
    textTransform: 'uppercase',
    marginLeft: -16,
    fontSize: 13,
    marginRight: 30
  },
  categoryLabel: {
    textTransform: 'uppercase',
    fontSize: 18,
    color: 'rgba(0, 0, 0, 0.54)',
    margin: 0,
    padding: `5px 30px ${spacing.unit}px`,
    fontWeight: typography.fontWeightMedium
  },
  ellipsis: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  }
});

const MenuHeader = ({
  previousCategoryLabel,
  currentPath,
  categoryLabel,
  handleBack,
  sidebarOpen,
  toggleSidebar,
  classes
}) => {
  const ClientLogo = Logos[REACT_APP_LOGO];

  return (
    <div
      className={cx(
        classes.menuHeader,
        sidebarOpen && classes.menuHeaderOpen,
        previousCategoryLabel && sidebarOpen && classes.menuHeaderFixed
      )}
    >
      <div
        className={cx(
          classes.wrapperTop,
          previousCategoryLabel && sidebarOpen && classes.wrapperTopSecondary
        )}
      >
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={previousCategoryLabel ? handleBack : toggleSidebar}
          className={cx(classes.menuButton)}
        >
          {previousCategoryLabel ? (
            <div
              className={classes.backButton}
              classes={{
                root: sidebarOpen
                  ? classes.menuButtonIconOpen
                  : classes.menuButtonIconClosed
              }}
            >
              <KeyboardArrowLeftIcon className={classes.rightIcon} />
            </div>
          ) : (
            <MenuIcon
              classes={{
                root: sidebarOpen
                  ? classes.menuButtonIconOpen
                  : classes.menuButtonIconClosed
              }}
            />
          )}
        </IconButton>

        {sidebarOpen &&
          (!currentPath ? (
            <div className={classes.topLogoWrapper}>
              {ClientLogo ? (
                <img
                  src={ClientLogo}
                  alt="logo"
                  className={cx(classes.topLogo)}
                />
              ) : (
                <Logo
                  className={cx(classes.logo, sidebarOpen && classes.logoOpen)}
                />
              )}
            </div>
          ) : (
            <div
              className={cx(classes.previousCategoryLabel, classes.ellipsis)}
            >
              {previousCategoryLabel}
            </div>
          ))}
      </div>

      {sidebarOpen && categoryLabel && (
        <div className={cx(classes.categoryLabel, classes.ellipsis)}>
          {categoryLabel}
        </div>
      )}
    </div>
  );
};

MenuHeader.propTypes = {
  previousCategoryLabel: PropTypes.string,
  currentPath: PropTypes.string,
  categoryLabel: PropTypes.string,
  handleBack: PropTypes.func,
  classes: PropTypes.object,
  sidebarOpen: PropTypes.bool,
  toggleSidebar: PropTypes.func.isRequired
};

const enhance = compose(
  connect(
    ({
      admin: {
        ui: { sidebarOpen }
      }
    }) => ({
      sidebarOpen
    }),
    {
      toggleSidebar: toggleSidebarAction
    }
  ),
  withStyles(styles)
);

export default enhance(MenuHeader);
