import { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { warning } from 'react-admin';
import { compose } from 'recompose';
import { isEqual } from 'lodash';
import { withStyles } from '@material-ui/core/styles';

import { setAppTitle as setAppTitleAction } from '../../actions/layout';

import withTranslate from '../Hoc/withTranslate';

const styles = ({ spacing }) => ({
  separator: {
    margin: `0 12px`,
    fontSize: 14
  },
  wrapper: {},
  icon: {},
  token: {}
});

class Title extends PureComponent {
  static propTypes = {
    // pathname: PropTypes.string.isRequired,
    appTitle: PropTypes.string.isRequired,
    setAppTitle: PropTypes.func.isRequired,
    defaultTitle: PropTypes.string,
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.element])
  };

  formatTitle = () => {
    const { defaultTitle } = this.props;

    const result = defaultTitle
      .replace('-', ' ')
      .replace(/ *%\{[^)]*\} */g, '');

    return result === ' #' ? '' : result;
  };

  componentDidMount = () => this.setCurrentTitle();

  componentDidUpdate = () => this.setCurrentTitle();

  setCurrentTitle = () => {
    const { appTitle, defaultTitle, title, setAppTitle } = this.props;
    const nextTitle = this.formatTitle(title || defaultTitle);

    if (isEqual(nextTitle, appTitle)) {
      return false;
    }

    return setAppTitle(nextTitle);
  };

  render = () => {
    const { defaultTitle, title } = this.props;

    if (!defaultTitle && !title) {
      warning(!defaultTitle && !title, 'Missing title prop in <Title> element');
    }

    return null;
  };
}

export default compose(
  connect(
    ({
      layout: { appTitle },
      router: {
        location: { pathname }
      }
    }) => ({ appTitle, pathname }),
    { setAppTitle: setAppTitleAction }
  ),
  withTranslate,
  withStyles(styles)
)(Title);
