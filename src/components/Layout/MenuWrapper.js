import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';
import { compose } from 'recompose';

import { withStyles } from '@material-ui/core/styles';

import { ReactComponent as Logo } from '../../assets/img/opencell.svg';
import EIRLogo from '../../assets/img/open_eir.png';
import EdenredLogo from '../../assets/img/edenred.png';
import LaPosteLogo from '../../assets/img/laposte.png';
import SmooveLogo from '../../assets/img/smoove.png';

const { REACT_APP_LOGO } = process.env;

const Logos = {
  open_eir: EIRLogo,
  edenred: EdenredLogo,
  laposte: LaPosteLogo,
  smoove: SmooveLogo
};

const styles = ({ palette }) => ({
  menuWrapper: {
    position: 'fixed',
    top: 0,
    bottom: 0,
    left: 0,
    width: 75,
    zIndex: 1,
    boxShadow:
      '0 16px 38px -12px rgba(0,0,0,.56), 0 4px 25px 0 rgba(0,0,0,.12), 0 8px 10px -5px rgba(0,0,0,.2)',
    transition: 'all .4s',
    paddingBottom: '10vh',
    overflowY: 'auto',
    scrollbarWidth: 'none',
    '-ms-overflow-style': 'none',
    background: 'white',
    '&::-webkit-scrollbar': {
      width: '0 !important'
    }
  },
  relativeWrapper: {
    position: 'relative'
  },
  scrollWrapper: {},
  menuOpen: {
    width: 240
  },
  wrapperBottom: {
    bottom: '0',
    position: 'fixed',
    width: 240,
    height: 32,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    background: 'white',
    borderTop: `1px solid ${palette.background.default}`
  }
});

class MenuWrapper extends PureComponent {
  static propTypes = {
    classes: PropTypes.object,
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.array]),
    sidebarOpen: PropTypes.bool
  };

  render = () => {
    const { classes, children, sidebarOpen } = this.props;

    const ClientLogo = Logos[REACT_APP_LOGO];

    return (
      <div className={cx(classes.menuWrapper, sidebarOpen && classes.menuOpen)}>
        <div className={classes.relativeWrapper}>
          <div className={classes.scrollWrapper}>{children}</div>
          {ClientLogo && sidebarOpen && (
            <div className={classes.wrapperBottom}>
              <Logo
                className={cx(classes.logo, sidebarOpen && classes.logoOpen)}
              />
            </div>
          )}
        </div>
      </div>
    );
  };
}

const enhance = compose(
  connect(({ admin: { ui: { sidebarOpen } } }) => ({
    sidebarOpen
  })),
  withStyles(styles)
);

export default enhance(MenuWrapper);
