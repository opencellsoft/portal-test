import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { get } from 'lodash';
import { createStyles } from '@material-ui/core/styles';
import { findResourceFromPathname } from '../../utils/api';

import withStylesProps from '../Hoc/WithStylesProps';

import DynamicIcon from '../Globals/DynamicIcon';
import Breadcrumbs from './Breadcrumbs';
import { getSubMenu } from '../../utils/routing';

const styles = (
  { breakpoints, spacing, palette, modules },
  { config, pathname }
) => {
  const resource = findResourceFromPathname(pathname, config.modules);

  return createStyles({
    wrapper: {
      display: 'flex',
      alignItems: 'center',
      position: 'relative',
      flex: 1,
      overflow: 'hidden',
      boxSizing: 'border-box',
      marginLeft: 12,
      color: 'white',
      boxShadow: `0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px ${get(
        modules[resource],
        'themeColor'
      ) || palette.secondary.main}8c`,
      backgroundColor: `${get(modules[resource], 'themeColor') ||
        palette.secondary.main} !important`,
      padding: `0 0 0 50px`,
      height: spacing.unit * 6,
      borderRadius: 6
    },
    wrapperOpen: {
      marginLeft: spacing.unit * 5
    },
    icon: {
      position: 'absolute',
      left: 20,
      color: 'white'
    },
    rightSide: {
      display: 'flex',
      marginLeft: 'auto',
      color: 'white',
      marginRight: spacing.unit
    }
  });
};

class PageTitle extends PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    config: PropTypes.object.isRequired,
    sidebarOpen: PropTypes.bool,
    theme: PropTypes.object,
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.array]),
    pathname: PropTypes.string.isRequired
  };

  render = () => {
    const {
      classes,
      config,
      sidebarOpen,
      theme,
      pathname,
      children
    } = this.props;
    const { modules = {} } = theme || {};
    const resource = findResourceFromPathname(pathname, config.modules);

    return (
      <div className={cx(classes.wrapper, sidebarOpen && classes.wrapperOpen)}>
        <DynamicIcon
          className={classes.icon}
          icon={
            get(modules[resource], 'icon') ||
            get(
              getSubMenu(
                config.menu,
                pathname
                  .split('/')
                  .slice(1)
                  .join('/')
              ),
              'icon'
            ) ||
            'KeyboardArrowRight'
          }
        />

        <Breadcrumbs />

        <div className={classes.rightSide}>{children}</div>
      </div>
    );
  };
}

const enhance = compose(
  connect(
    ({
      admin: {
        ui: { sidebarOpen }
      },
      config: { config },
      router: {
        location: { pathname }
      },
      theme: {
        theme: { currentTheme: theme }
      }
    }) => ({
      config,
      sidebarOpen,
      pathname,
      theme
    }),
    {}
  ),
  withStylesProps(styles, null)
);

export default enhance(PageTitle);
