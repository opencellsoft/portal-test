import React, { useMemo } from 'react';
import { connect } from 'react-redux';
import { Layout } from 'react-admin';
import cx from 'classnames';
import { compose } from 'recompose';
import { withStyles } from '@material-ui/core/styles';

import AppBar from './AppBar';
import Menu from './Menu';

const styles = ({ spacing, breakpoints }) => ({
  content: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
    padding: `${spacing.unit * 10}px 20px 21vh ${spacing.unit * 5}px`,
    maxWidth: `calc(100vw - 75px - ${spacing.unit * 6}px)`,
    [breakpoints.down('sm')]: {
      padding: 0,
      paddingLeft: 0
    },
    overflow: 'hidden'
  },
  open: {
    padding: `${spacing.unit * 10}px 0 21vh ${spacing.unit * 4}px`,
    maxWidth: `calc(100vw - ${spacing.unit * 30}px - ${spacing.unit * 8}px)`
  }
});

const CustomLayout = ({ classes, sidebarOpen, ...props }) =>
  useMemo(() => (
    <Layout
      {...props}
      appBar={AppBar}
      menu={Menu}
      classes={{ content: cx(classes.content, sidebarOpen && classes.open) }}
    />
  ));

export default compose(
  connect(
    ({
      admin: {
        ui: { sidebarOpen }
      },
      config: { config }
    }) => ({ sidebarOpen }),
    {}
  ),
  withStyles(styles)
)(CustomLayout);
