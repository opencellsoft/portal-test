import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import cx from 'classnames';
import { Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = ({ breakpoints, palette, spacing, modules }) => ({
  title: {
    display: 'flex',
    alignItems: 'center',
    height: '100%',
    marginLeft: '.5em',
    fontSize: '1rem',
    fontWeight: 300,
    textTransform: 'uppercase',
    letterSpacing: '.15rem',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    [breakpoints.down('xs')]: {
      marginLeft: 0
    }
  },
  portalTitle: {
    marginLeft: 15
  },
  portalTitleOpen: {
    marginLeft: 75,
    marginTop: 15,
    fontWeight: 'bold'
  },

  hiddenXs: {
    [breakpoints.down('xs')]: {
      display: 'none'
    }
  }
});

const CompactTitle = ({ config, sidebarOpen, classes }) =>
  !sidebarOpen && (
    <>
      <Typography
        variant="title"
        color="inherit"
        className={cx(
          classes.title,
          classes.portalTitle,
          sidebarOpen && classes.portalTitleOpen,
          classes.hiddenXs
        )}
      >
        {config.title}
      </Typography>

      <Typography
        variant="title"
        color="inherit"
        className={cx(classes.title, classes.hiddenXs)}
      >
        /
      </Typography>
    </>
  );

CompactTitle.propTypes = {
  classes: PropTypes.object.isRequired,
  sidebarOpen: PropTypes.bool,
  config: PropTypes.object.isRequired
};

CompactTitle.defaultProps = {
  config: {}
};

export default compose(
  connect(({ admin: { ui: { sidebarOpen } }, config: { config } }) => ({
    config,
    sidebarOpen
  })),
  withStyles(styles)
)(CompactTitle);
