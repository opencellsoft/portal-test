import React, { Component, Children, cloneElement } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';
import { Collapse, MenuItem } from '@material-ui/core';
import { KeyboardArrowLeft, KeyboardArrowDown } from '@material-ui/icons';
import { createStyles, withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';

import DynamicIcon from '../Globals/DynamicIcon';

const styles = ({ palette }) =>
  createStyles({
    root: {
      color: palette.text.secondary,
      display: 'flex',
      '&:hover': {
        background: '#999' || palette.action.hover,
        boxShadow: `0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px #${'#999'}8c`
      }
    },
    rootOpen: {
      color: 'white',
      background: '#999' || palette.action.hover
    },
    itemActive: {
      color: palette.text.primary
    },
    icon: { paddingRight: '1.2em' },
    menuLabel: { flex: 1 },
    dropdownIcon: {
      color: 'inherit',
      fontSize: 18
    }
  });

class MenuItemCollapsible extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    className: PropTypes.string,
    leftIcon: PropTypes.node,
    rightIcon: PropTypes.node,
    primaryText: PropTypes.string.isRequired,
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]).isRequired
  };

  state = {
    menuOpen: false
  };

  handleMenuOpen = () =>
    this.setState(({ menuOpen }) => ({
      menuOpen: !menuOpen
    }));

  render = () => {
    const { menuOpen } = this.state;
    const {
      classes,
      className,
      rootClass,
      activeClass,
      subIconClass,
      primaryText,
      rightIcon,
      children,
      icon,
      ...props
    } = this.props;

    return (
      <>
        <MenuItem
          {...props}
          component="div"
          onClick={this.handleMenuOpen}
          classes={{
            root: menuOpen
              ? cx(classes.root, classes.rootOpen, rootClass)
              : cx(classes.root, classes.root, rootClass)
          }}
        >
          {icon && (
            <span className={classes.icon}>
              <DynamicIcon className={subIconClass} icon={icon} />
            </span>
          )}

          <span className={classes.menuLabel}>{primaryText}</span>

          {menuOpen ? (
            <KeyboardArrowLeft className={classes.dropdownIcon} />
          ) : (
            <KeyboardArrowDown className={classes.dropdownIcon} />
          )}
        </MenuItem>

        <Collapse in={menuOpen} className={classes.subItemClass} timeout="auto">
          {Children.map(children, (child) => cloneElement(child))}
        </Collapse>
      </>
    );
  };
}

export default compose(
  connect(
    ({
      config: { config },
      router: {
        location: { pathname }
      }
    }) => ({
      config,
      pathname
    }),
    {}
  ),
  withStyles(styles)
)(MenuItemCollapsible);
