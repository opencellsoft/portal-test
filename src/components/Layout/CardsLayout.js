import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  layout: {
    display: 'flex',
    flexDirection: 'column'
  },
  cardsWrapper: {
    display: 'grid',
    gridTemplateColumns: 'repeat(3, 1fr)',
    gridGap: '15px',
    marginBottom: 15
  },
  main: {}
};

const CardsLayout = ({ cards, id, children, classes }) => (
  <div className={classes.layout}>
    {cards && (
      <div className={classes.cardsWrapper}>
        {cards.map((Card, index) => (
          <Card id={id} key={index} />
        ))}
      </div>
    )}

    {children && <div className={classes.main}>{children}</div>}
  </div>
);

CardsLayout.propTypes = {
  cards: PropTypes.arrayOf(PropTypes.func),
  id: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CardsLayout);
