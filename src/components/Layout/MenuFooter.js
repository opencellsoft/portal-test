import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';
import { compose } from 'recompose';

import { withStyles } from '@material-ui/core/styles';

import { ReactComponent as Logo } from '../../assets/img/opencell.svg';
import EIRLogo from '../../assets/img/open_eir.png';
import EdenredLogo from '../../assets/img/edenred.png';
import LaPosteLogo from '../../assets/img/laposte.png';
import SmooveLogo from '../../assets/img/smoove.png';

const { REACT_APP_LOGO } = process.env;

const Logos = {
  open_eir: EIRLogo,
  edenred: EdenredLogo,
  laposte: LaPosteLogo,
  smoove: SmooveLogo
};

const styles = ({ breakpoints, palette }) => ({
  wrapperBottom: {
    bottom: '0',
    position: 'fixed',
    width: 240,
    height: 32,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    background: 'white',
    // borderTop: `1px solid ${palette.background.default}`
    boxShadow:
      '0 1px 22px rgba(50, 50, 93, 0.01), 0 -1px 6px rgba(0, 0, 0, 0.08)'
  },
  logo: {
    maxWidth: 96,
    marginLeft: 30,
    flex: 1,
    fill: '#eb2f2f',
    transition: 'all .4s',
    position: 'fixed',
    left: 75,
    [breakpoints.down('xs')]: {
      display: 'none'
    }
  },
  logoOpen: {
    marginLeft: 0,
    left: 0,
    position: 'relative'
  }
});

const MenuFooter = ({ sidebarOpen, classes }) => {
  const ClientLogo = Logos[REACT_APP_LOGO];

  return ClientLogo && sidebarOpen ? (
    <div className={classes.wrapperBottom}>
      <Logo className={cx(classes.logo, sidebarOpen && classes.logoOpen)} />
    </div>
  ) : null;
};

MenuFooter.propTypes = {
  sidebarOpen: PropTypes.bool,
  classes: PropTypes.object
};

const enhance = compose(
  connect(
    ({
      admin: {
        ui: { sidebarOpen }
      }
    }) => ({
      sidebarOpen
    }),
    null
  ),
  withStyles(styles)
);

export default enhance(MenuFooter);
