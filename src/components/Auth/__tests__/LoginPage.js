import React from 'react';
import { shallow } from 'enzyme';
import { Login } from 'react-admin';
import { checkProps } from '../../../utils/tests';
import LoginPage from '../LoginPage';

describe('LoginPage', () => {
  describe('when props are correct', () => {
    it('should not show a warning', () => {
      const expectedProps = {
        classes: {}
      };
      const propsErr = checkProps(LoginPage, expectedProps);
      expect(propsErr).toBeUndefined();
    });
    it('should render correctly', () => {
      const expectedProps = {
        classes: {}
      };
      const wrapper = shallow(<LoginPage {...expectedProps} />);
      expect(wrapper).toMatchSnapshot();
    });
  });
  describe('when props are incorrect', () => {
    it('should show a warning', () => {
      const invalidProps = {
        classes: '123'
      };
      const propsErr = checkProps(LoginPage, invalidProps);
      expect(propsErr).toEqual(
        'Failed props type: Invalid props `classes` of type `string` supplied to `WithStyles`, expected `object`.'
      );
    });
  });
});
