import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Login } from 'react-admin';
import { withStyles } from '@material-ui/core/styles';

import { ReactComponent as Logo } from '../../assets/img/opencell.svg';

import loginBg from '../../assets/img/splash.jpg';

const styles = ({ breakpoints }) => ({
  wrapper: {
    position: 'relative',
    width: '100%'
  },
  logoWrapper: {
    position: 'absolute',
    top: '3em',
    left: 0,
    right: 0,
    width: 150,
    margin: 'auto'
  },
  logo: {
    fill: 'white'
  },
  main: {
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    alignItems: 'center'
  },
  card: {
    boxShadow:
      '0 7px 14px rgba(50, 50, 93, 0.1), 0 3px 6px rgba(0, 0, 0, 0.08)',
    marginTop: '8em'
  }
});

const CustomLoginPage = ({ classes, ...props }) => (
  <div className={classes.wrapper}>
    <div className={classes.logoWrapper}>
      <Logo className={cx(classes.logo)} />
    </div>

    <Login backgroundImage={loginBg} {...props} />
  </div>
);

CustomLoginPage.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CustomLoginPage);
