import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardContent, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import compose from 'recompose/compose';

import withTranslate from '../../../Hoc/withTranslate';

import Title from '../Layout/Title';

const styles = ({ spacing, shadows }) => ({
  card: {
    margin: spacing.unit,
    boxShadow: shadows[1],
    borderRadius: 6
  }
});

const Dashboard = ({ classes, translate }) => (
  <Card className={classes.card}>
    <Title
      title={translate('dashboard')}
      defaultTitle={translate('dashboard')}
    />

    <CardContent>
      <Typography variant="subheading" gutterBottom>
        {translate('dashboardMessage')}
      </Typography>
    </CardContent>
  </Card>
);
Dashboard.propTypes = {
  classes: PropTypes.object.isRequired,
  translate: PropTypes.func.isRequired
};

export default compose(
  withTranslate,
  withStyles(styles)
)(Dashboard);
