import React from 'react';
// import React, { createElement, lazy, Suspense } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as MaterialIcons from '@material-ui/icons';

const DynamicIcon = ({
  icon,
  classes,
  className,
  style = {},
  themeColor,
  defaultColor
}) => {
  const Icon = MaterialIcons[icon];

  if (!Icon) {
    return false;
  }

  return (
    <Icon
      className={className}
      style={{
        ...style,
        color: style.color ? style.color : defaultColor ? themeColor : undefined
      }}
    />
  );

  //@todo
  // React Lazy disabled for now until buxfix since it triggers infinite loop on undefined resolution

  // return (
  //   <Suspense fallback={null}>
  //     {createElement(
  //       lazy(async () => {
  //         let icon;

  //         try {
  //           icon = await import(/* webpackMode: "lazy-once" */
  //           `@material-ui/icons/${icon}`);
  //         } catch (error) {
  //           return null;
  //         }

  //         return icon;
  //       }),
  //       {
  //         className,
  //         style: {
  //           ...style,
  //           color: style.color
  //             ? style.color
  //             : defaultColor
  //             ? themeColor
  //             : undefined
  //         }
  //       }
  //     )}
  //   </Suspense>
  // );
};

DynamicIcon.propTypes = {
  icon: PropTypes.string.isRequired,
  classes: PropTypes.object,
  className: PropTypes.string,
  style: PropTypes.object,
  themeColor: PropTypes.string,
  defaultColor: PropTypes.bool
};

export default connect(
  ({ config: { config: { themeColor } = {} } = {} }) => ({ themeColor }),
  {}
)(DynamicIcon);
