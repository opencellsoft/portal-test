import React from 'react';
import PropTypes from 'prop-types';
import { Tooltip } from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import get from 'lodash/get';

import gs from '../../../styles/global';

const styles = {
  link: {
    ...gs.ellipsis,
    display: 'block'
  }
};

const EmailField = ({
  record = {},
  source,
  className,
  noWrap,
  classes,
  noClick,
  ...rest
}) => {
  const email = get(record, source);
  const composedClasses = [className, noWrap && classes.link]
    .filter((el) => el)
    .join(' ');

  return email ? (
    <Tooltip title={email}>
      <a
        className={composedClasses}
        href={!noClick && `mailto:${email}`}
        {...rest}
      >
        {email}
      </a>
    </Tooltip>
  ) : (
    <div className={composedClasses}>Email not specified</div>
  );
};

EmailField.propTypes = {
  record: PropTypes.object,
  source: PropTypes.string.isRequired,
  className: PropTypes.string,
  noWrap: PropTypes.bool,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(EmailField);
