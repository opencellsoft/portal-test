import React from 'react';
import PropTypes from 'prop-types';
import { Typography, Tooltip } from '@material-ui/core';

const FullNameField = ({ record = {}, className, ...rest }) => {
  const { name } = record || {};
  const { title, firstName, lastName } = name || record;
  const label = [title, firstName, lastName].filter((el) => el).join(' ');

  return (
    <Tooltip title={label} placement="bottom-start">
      <Typography
        component="span"
        body1="body1"
        className={className}
        {...rest}
      >
        {label || 'No name'}
      </Typography>
    </Tooltip>
  );
};

FullNameField.propTypes = {
  record: PropTypes.object,
  className: PropTypes.string
};

export default FullNameField;
