import React from 'react';
import PropTypes from 'prop-types';
import GreenIcon from '@material-ui/icons/Wifi';
import RedIcon from '@material-ui/icons/Lens';
import withStyles from '@material-ui/core/styles/withStyles';
import get from 'lodash/get';

import green from '@material-ui/core/colors/green';
import red from '@material-ui/core/colors/red';

import { STATUS_ACTIVATED, STATUS_COMPLETED } from '../../../constants/status';

const styles = {
  greenIcon: {
    display: 'block',
    color: 'white',
    background: green.A200,
    borderRadius: '50%',
    padding: 4,
    fontSize: 13
  },
  RedIcon: {
    display: 'block',
    background: 'red',
    color: red.A200,
    borderRadius: '50%',
    padding: 1,
    fontSize: 16
  }
};
const StatusField = ({ record, source, defaultValue, classes, ...rest }) => {
  const value = get(record, source, defaultValue).toUpperCase();

  switch (value) {
    case STATUS_ACTIVATED:
    case STATUS_COMPLETED:
      return <GreenIcon className={classes.greenIcon} />;
    default:
      return <RedIcon className={classes.RedIcon} />;
  }
};

StatusField.propTypes = {
  classes: PropTypes.object,
  record: PropTypes.object
};

StatusField.defaultProps = {
  defaultValue: ''
};

export default withStyles(styles)(StatusField);
