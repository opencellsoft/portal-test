import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Chip from '@material-ui/core/Chip';
import get from 'lodash/get';
import withStyles from '@material-ui/core/styles/withStyles';
import green from '@material-ui/core/colors/green';

import { STATUS_ACTIVE } from '../../../assets/data/status';

const styles = ({ typography }) => ({
  status: {
    fontSize: typography.caption.fontSize,
    height: '1.5rem',
    marginTop: -20
  },
  statusActive: {
    background: green.A200,
    color: 'white',
    display: 'flex',
    width: 80
  }
});

const StatusTextField = ({ record, source, defaultValue, classes }) => {
  const value = get(record, source) || defaultValue;

  return (
    <Chip
      component="span"
      label={value}
      className={classnames(
        classes.value,
        value && value.toUpperCase() === STATUS_ACTIVE
          ? classes.statusActive
          : undefined
      )}
    />
  );
};

StatusTextField.propTypes = {
  record: PropTypes.object,
  source: PropTypes.string,
  defaultValue: PropTypes.string,
  classes: PropTypes.object.isRequired
};

StatusTextField.defaultProps = {
  defaultValue: ''
};

export default withStyles(styles)(StatusTextField);
