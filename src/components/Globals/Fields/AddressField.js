import React from 'react';
import PropTypes from 'prop-types';
import { Typography, Tooltip } from '@material-ui/core';

const AddressField = ({ record = {}, className, ...rest }) => {
  const { address } = record;
  const { address1, zipCode, city, country } = address || {};

  const addressLocation = [address1, zipCode].filter((el) => el).join(', ');
  const cityCountry = [city, country].filter((el) => el).join(', ');

  return (
    <Tooltip
      title={`${addressLocation} ${cityCountry}`}
      placement="bottom-start"
    >
      <Typography
        component="span"
        body1="body1"
        className={className}
        {...rest}
      >
        {addressLocation} {cityCountry}
      </Typography>
    </Tooltip>
  );
};

AddressField.propTypes = {
  record: PropTypes.object,
  className: PropTypes.string
};

export default AddressField;
