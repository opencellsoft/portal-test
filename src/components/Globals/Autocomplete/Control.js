import React, { PureComponent } from 'react';
import { TextField } from '@material-ui/core';

class Control extends PureComponent {
  inputComponent = ({ inputRef, ...props }) => (
    <div ref={inputRef} {...props} />
  );

  render = () => {
    const { selectProps, innerRef, children, innerProps } = this.props;

    return (
      <TextField
        fullWidth
        InputProps={{
          inputComponent: this.inputComponent,
          inputProps: {
            className: selectProps.classes.input,
            inputRef: innerRef,
            children,
            ...innerProps
          }
        }}
        {...selectProps.textFieldProps}
      />
    );
  };
}

export default Control;
