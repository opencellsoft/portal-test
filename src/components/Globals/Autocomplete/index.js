import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import { withStyles } from '@material-ui/core/styles';

import Control from './Control';
import Menu from './Menu';
import NoOptionsMessage from './NoOptionsMessage';
import Option from './Option';
import Placeholder from './Placeholder';
import SingleValue from './SingleValue';

const styles = ({ spacing }) => ({
  root: {
    flexGrow: 1,
    height: 250
  },
  input: {
    display: 'flex',
    padding: 0
  },
  valueContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    flex: 1,
    alignItems: 'center',
    overflow: 'hidden'
  },
  noOptionsMessage: {
    padding: `${spacing.unit}px ${spacing.unit * 2}px`
  },
  singleValue: {
    fontSize: 16
  },
  placeholder: {
    position: 'absolute',
    left: 2,
    fontSize: 16
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: spacing.unit,
    left: 0,
    right: 0
  },
  divider: {
    height: spacing.unit * 2
  }
});

class Autocomplete extends PureComponent {
  static propTypes = {
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    theme: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
    selectStyles: PropTypes.object,
    isClearable: PropTypes.bool,
    options: PropTypes.array
  };

  state = {
    value: null
  };

  components = [
    Control,
    Menu,
    NoOptionsMessage,
    Option,
    Placeholder,
    SingleValue
  ];

  componentDidMount = () => this.handleInjectedValue();

  componentDidUpdate = ({ value: prevValue }) => {
    const { value } = this.props;

    if (value !== prevValue) {
      this.handleInjectedValue();
    }
  };

  handleInjectedValue = () =>
    this.setState({
      value: this.props.options.find(({ value }) => value === this.props.value)
    });

  handleChange = (value) => {
    const { onChange } = this.props;
    const { value: actualValue } = value || {};

    this.setState(
      {
        value
      },
      () => onChange && onChange(actualValue)
    );
  };

  render = () => {
    const {
      classes,
      selectStyles: inheritedSelectStyles = {},
      theme,
      options = [],
      isClearable = true
    } = this.props;
    const { value } = this.state;

    const selectStyles = {
      input: (base) => ({
        ...base,
        color: theme.palette.text.primary,
        '& input': {
          font: 'inherit'
        }
      }),
      menu: (base) => ({ ...base, zIndex: 999 }),
      ...inheritedSelectStyles
    };

    return (
      <Select
        classes={classes}
        styles={selectStyles}
        options={options}
        components={this.components}
        value={value}
        onChange={this.handleChange}
        isClearable={isClearable}
      />
    );
  };
}

export default withStyles(styles, { withTheme: true })(Autocomplete);
