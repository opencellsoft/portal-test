import React from 'react';
import { shallow } from 'enzyme';
import NoOptionsMessage from '../NoOptionsMessage';

const setUp = (props = {}) => {
  const wrapper = shallow(<NoOptionsMessage {...props} />);

  return wrapper;
};

describe('NoOptionsMessage', () => {
  describe('when props are correct', () => {
    let wrapper;
    beforeEach(() => {
      const props = {
        selectProps: {
          classes: {
            input: 'className'
          }
        }
      };
      wrapper = setUp(props);
    });

    it('should be with textSecondary color', () => {
      const color = wrapper.find(`[color='textSecondary']`);
      expect(color.length).toBe(1);
    });
  });
});
