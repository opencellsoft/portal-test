import React from 'react';
import { shallow } from 'enzyme';
import { checkProps } from '../../../../utils/tests';
import Autocomplete from '../index';

const setUp = (props = {}) => {
  const wrapper = shallow(<Autocomplete {...props} />);

  return wrapper;
};

describe('Autocomplete', () => {
  describe('when props are correct', () => {
    let wrapper;
    beforeEach(() => {
      const props = {
        onChange: () => true,
        theme: {
          classes: {
            input: 'className'
          }
        }
      };
      wrapper = setUp(props);
    });

    it('should be render correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('when props are incorrect', () => {
    it('should show a warning', () => {
      const propsErr = checkProps(Autocomplete, {});
      expect(propsErr).toBeFalsy();
    });
  });
});
