import React from 'react';
import { shallow } from 'enzyme';
import Control from '../Control';

const setUp = (props = {}) => {
  const wrapper = shallow(<Control {...props} />);

  return wrapper;
};

describe('Control', () => {
  describe('when props are correct', () => {
    let wrapper;
    beforeEach(() => {
      const props = {
        selectProps: {
          classes: {
            input: 'className'
          }
        }
      };
      wrapper = setUp(props);
    });

    it('should be fullWidth', () => {
      const fullWidth = wrapper.find('[fullWidth=true]');
      expect(fullWidth.length).toBe(1);
    });

    it('should set required to false', () => {
      const required = wrapper.find('[required=false]');
      expect(required.length).toBe(1);
    });

    it('should set select to false', () => {
      const required = wrapper.find('[select=false]');
      expect(required.length).toBe(1);
    });
  });
});
