import React from 'react';
import { shallow } from 'enzyme';
import Option from '../Option';

const setUp = (props = {}) => {
  const wrapper = shallow(<Option {...props} />);

  return wrapper;
};

describe('Option', () => {
  describe('when props are correct', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = setUp();
    });

    it('should render a div component', () => {
      const div = wrapper.find(`[component='div']`);
      expect(div.length).toBe(1);
    });
  });
});
