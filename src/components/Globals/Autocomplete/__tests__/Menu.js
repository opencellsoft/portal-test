import React from 'react';
import { shallow } from 'enzyme';
import Menu from '../Menu';

const setUp = (props = {}) => {
  const wrapper = shallow(<Menu {...props} />);

  return wrapper;
};

describe('Menu', () => {
  describe('when props are correct', () => {
    let wrapper;
    beforeEach(() => {
      const props = {
        selectProps: {
          classes: {
            input: 'className'
          }
        }
      };
      wrapper = setUp(props);
    });

    it('should be square', () => {
      const square = wrapper.find('[square=true]');
      expect(square.length).toBe(1);
    });
  });
});
