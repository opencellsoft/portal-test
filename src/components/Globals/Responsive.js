import React from 'react';
import PropTypes from 'prop-types';
import withWidth from '@material-ui/core/withWidth';

export const Responsive = ({
  xsmall,
  small,
  medium,
  large,
  xlarge,
  width,
  ...rest
}) => {
  let element;

  switch (width) {
    case 'xs':
      element = typeof xsmall !== 'undefined' ? xsmall : null;
      break;
    case 'sm':
      element = typeof small !== 'undefined' ? small : xsmall;
      break;
    case 'md':
      element =
        typeof medium !== 'undefined'
          ? medium
          : typeof small !== 'undefined'
          ? small
          : xsmall;
      break;
    case 'lg':
      element =
        typeof large !== 'undefined'
          ? large
          : typeof medium !== 'undefined'
          ? medium
          : typeof small !== 'undefined'
          ? small
          : xsmall;
      break;
    case 'xl':
      element =
        typeof xlarge !== 'undefined'
          ? xlarge
          : typeof large !== 'undefined'
          ? large
          : typeof medium !== 'undefined'
          ? medium
          : typeof small !== 'undefined'
          ? small
          : xsmall;
      break;
    default:
      throw new Error(`Unknown width ${width}`);
  }

  return element ? React.cloneElement(element, rest) : null;
};

Responsive.propTypes = {
  xsmall: PropTypes.element,
  small: PropTypes.element,
  medium: PropTypes.element,
  large: PropTypes.element,
  xlarge: PropTypes.element,
  width: PropTypes.string
};

export default withWidth()(Responsive);
