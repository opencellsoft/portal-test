import * as React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import { pink } from '@material-ui/core/colors';

import XMLTreeContainer from './XMLTreeContainer';
import XMLTreeAttribute from './XMLTreeAttribute';
import {
  isText,
  isElement,
  isCDATASection,
  toArray,
  isIncluded
} from '../../../utils/xml-helpers';

const styles = ({ palette }) => ({
  element: {
    marginLeft: '3ch',
    marginRight: '3ch',
    '& + span': {
      display: 'none'
    }
  },
  start: {
    color: palette.primary.main
  },
  end: {
    color: palette.primary.main
  },
  text: {
    color: pink.A400
  },
  cdata: {
    color: 'gray',
    marginLeft: '3ch',
    marginRight: '3ch'
  }
});

const XMLTreeNode = ({ node, exclude, classes }) => {
  if (isText(node)) {
    return <span className={classes.text}>{node.data}</span>;
  }
  if (isElement(node)) {
    const { tagName } = node;

    return (
      <div className={classes.element}>
        <span className={classes.start}>
          {'<'}
          {tagName}
          {toArray(node.attributes)
            .filter((attr) => isIncluded(attr, exclude))
            .map(({ name, value }, i) => (
              <XMLTreeAttribute key={i} name={name} value={value} />
            ))}
          {'>'}
        </span>
        <XMLTreeContainer nodes={node.childNodes} exclude={exclude} />
        <span className={classes.end}>
          {'</'}
          {tagName}
          {'>'}
        </span>
      </div>
    );
  }
  if (isCDATASection(node)) {
    return (
      <div className={classes.cdata}>
        {'<![CDATA['}
        {node.data}
        {']]>'}
      </div>
    );
  }

  return <span>(Ignoring node type = {node.nodeType})</span>;
};

XMLTreeNode.propTypes = {
  node: PropTypes.object,
  exclude: PropTypes.array,
  classes: PropTypes.object
};

export default withStyles(styles)(XMLTreeNode);
