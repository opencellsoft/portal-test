import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import withStyles from '@material-ui/core/styles/withStyles';

import XMLTreeContainer from './XMLTreeContainer';

const styles = {
  wrapper: {
    width: '100%',
    display: 'block',
    maxHeight: '50vh',
    overflow: 'auto',
    whiteSpace: 'pre-line',
    margin: 0,
    padding: '0 5px',
    background: '#F3F2F2',
    backgroundImage: 'linear-gradient(#F3F2F2 50%, #EBEAEA 50%)',
    backgroundPosition: '0 0',
    backgroundRepeat: 'repeat',
    backgroundAttachment: 'local',
    backgroundSize: '4em 4em',
    lineHeight: '2em',

    '& > div': {
      marginLeft: 0
    }
  }
};

class XMLTree extends Component {
  static propTypes = {
    xml: PropTypes.string,
    exclude: PropTypes.array,
    className: PropTypes.string,
    classes: PropTypes.object.isRequired
  };

  static defaultProps = {
    xml: '',
    exclude: []
  };

  document = new DOMParser().parseFromString(
    this.props.xml || '<Empty>No XML found</Empty>',
    'application/xml'
  );

  render = () => {
    const { classes, className, exclude } = this.props;

    if (!this.document) {
      return false;
    }

    return (
      <XMLTreeContainer
        className={cx(classes.wrapper, className)}
        nodes={this.document.childNodes}
        exclude={exclude}
      />
    );
  };
}

export default withStyles(styles)(XMLTree);
