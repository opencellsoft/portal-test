import * as React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = {
  name: {
    color: 'deepskyblue'
  },
  value: {
    color: 'orange'
  }
};

const XMLTreeAttribute = ({ name, value, classes }) => (
  <span>
    {' '}
    <span className={classes.name}>{name}</span>
    {'='}
    <span className={classes.value}>
      {'"'}
      {value}
      {'"'}
    </span>
  </span>
);

XMLTreeAttribute.propTypes = {
  name: PropTypes.string,
  value: PropTypes.string,
  classes: PropTypes.object
};

export default withStyles(styles)(XMLTreeAttribute);
