import * as React from 'react';
import PropTypes from 'prop-types';

import XMLTreeNode from './XMLTreeNode';
import { toArray, isIncluded } from '../../../utils/xml-helpers';

const XMLTreeContainer = ({ nodes, exclude = [], className = '' }) => (
  <span className={className}>
    {toArray(nodes)
      .filter((node) => isIncluded(node, exclude))
      .map((node, i) => (
        <XMLTreeNode key={i} node={node} exclude={exclude} />
      ))}
  </span>
);

XMLTreeContainer.propTypes = {
  nodes: PropTypes.object,
  exclude: PropTypes.array,
  className: PropTypes.string
};

export default XMLTreeContainer;
