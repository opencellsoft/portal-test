import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';

import withTranslate from '../../Hoc/withTranslate';

const styles = ({ spacing }) => ({
  paper: {
    padding: spacing.unit,
    minWidth: 400
  },
  title: {
    '& h2::first-letter': {
      textTransform: 'capitalize'
    }
  },
  contentText: {
    minWidth: 400
  },
  overflow: {
    overflowY: 'visible'
  }
});

const CustomDialog = ({
  open,
  onClose,
  title,
  description,
  children,
  actions,
  classes,
  translate,
  translateOptions = {},
  ...rest
}) => (
  <Dialog
    open={open}
    onClose={onClose}
    aria-labelledby="alert-dialog-title"
    aria-describedby="alert-dialog-description"
    classes={{ paper: classes.paper }}
    {...rest}
  >
    {title && (
      <DialogTitle className={classes.title}>
        {translate(title, { _: title, ...translateOptions })}
      </DialogTitle>
    )}

    {description && (
      <DialogContent className={cx(classes.contentText, classes.overflow)}>
        <DialogContentText>{description}</DialogContentText>
      </DialogContent>
    )}

    {children && (
      <DialogContent className={classes.overflow}>{children}</DialogContent>
    )}
    {actions && <DialogActions>{actions}</DialogActions>}
  </Dialog>
);

CustomDialog.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  description: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.array]),
  actions: PropTypes.oneOfType([PropTypes.node, PropTypes.array]),
  classes: PropTypes.object.isRequired
};

export default compose(
  withTranslate,
  withStyles(styles)
)(CustomDialog);
