import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';
import {
  withDataProvider,
  showNotification as showNotificationAction,
  refreshView as refreshViewAction
} from 'react-admin';
import { Button } from '@material-ui/core';
import { withStyles, createStyles } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';
import ActionCheck from '@material-ui/icons/CheckCircle';
import AlertError from '@material-ui/icons/ErrorOutline';
import { compose } from 'recompose';

import Dialog from './Dialog';
import withTranslate from '../../Hoc/withTranslate';

const styles = (theme) =>
  createStyles({
    confirmPrimary: {
      color: theme.palette.primary.main
    },
    confirmWarning: {
      color: theme.palette.error.main,
      '&:hover': {
        backgroundColor: fade(theme.palette.error.main, 0.12),
        // Reset on mouse devices
        '@media (hover: none)': {
          backgroundColor: 'transparent'
        }
      }
    },
    iconPaddingStyle: {
      paddingRight: '0.5em'
    }
  });

class ConfirmDialog extends PureComponent {
  static defaultProps = {
    cancel: 'ra.action.cancel',
    confirm: 'ra.action.confirm',
    confirmColor: 'primary',
    open: false,
    onClose: () => {}
  };

  static propTypes = {
    onClose: PropTypes.func,
    onConfirm: PropTypes.func.isRequired,
    cancel: PropTypes.string,
    classes: PropTypes.object.isRequired,
    confirm: PropTypes.string,
    confirmColor: PropTypes.string,
    content: PropTypes.string.isRequired,
    open: PropTypes.bool,
    title: PropTypes.string.isRequired,
    translate: PropTypes.func.isRequired,
    translateOptions: PropTypes.object,
    loading: PropTypes.bool,
    maxWidth: PropTypes.string
  };

  handleConfirm = (e) => {
    const { onConfirm } = this.props;

    e.stopPropagation();

    return onConfirm && onConfirm();
  };

  render = () => {
    const {
      onClose,
      onConfirm,
      cancel,
      confirm,
      confirmColor,
      classes,
      translate,
      content,
      translateOptions = {},
      loading,
      disabled,
      maxWidth,
      ...props
    } = this.props;

    return (
      <Dialog
        {...props}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        onClose={onClose}
        description={translate(content, {
          _: content,
          ...translateOptions
        })}
        translateOptions={translateOptions}
        maxWidth={maxWidth}
        actions={
          <>
            <Button onClick={onClose}>
              <AlertError className={classes.iconPaddingStyle} />
              {translate(cancel, { _: cancel })}
            </Button>
            <Button
              disabled={disabled || loading}
              onClick={this.handleConfirm}
              className={cx('ra-confirm', {
                [classes.confirmWarning]: confirmColor === 'warning',
                [classes.confirmPrimary]: confirmColor === 'primary'
              })}
              autoFocus
            >
              <ActionCheck className={classes.iconPaddingStyle} />
              {translate(confirm, { _: confirm })}
            </Button>
          </>
        }
      />
    );
  };
}

export default compose(
  connect(
    null,
    {
      showNotification: showNotificationAction,
      refreshView: refreshViewAction
    }
  ),
  withStyles(styles),
  withTranslate, // Must be before reduxForm so that it can be used in validation
  withDataProvider
)(ConfirmDialog);
