import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, crudDelete, startUndoable } from 'react-admin';
import compose from 'recompose/compose';
import { withStyles } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';
import DeleteIcon from '@material-ui/icons/Delete';
import cx from 'classnames';

import ConfirmDialog from '../Dialogs/ConfirmDialog';

import withTranslate from '../../Hoc/withTranslate';

const styles = ({ palette }) => ({
  deleteButton: {
    color: palette.error.main,
    '&:hover': {
      backgroundColor: fade(palette.error.main, 0.12),
      // Reset on mouse devices
      '@media (hover: none)': {
        backgroundColor: 'transparent'
      }
    }
  }
});

const sanitizeRestProps = ({
  basePath,
  classes,
  dispatchCrudDelete,
  filterValues,
  label,
  resource,
  selectedIds,
  startUndoable,
  undoable,
  redirect,
  ...rest
}) => rest;

class DeleteButtonConfirm extends Component {
  static propTypes = {
    basePath: PropTypes.string,
    classes: PropTypes.object,
    className: PropTypes.string,
    dispatchCrudDelete: PropTypes.func.isRequired,
    label: PropTypes.string,
    record: PropTypes.object,
    redirect: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.bool,
      PropTypes.func
    ]),
    resource: PropTypes.string.isRequired,
    startUndoable: PropTypes.func,
    translate: PropTypes.func,
    undoable: PropTypes.bool,
    icon: PropTypes.element
  };

  state = {
    confirmModal: false
  };

  toggleModal = () => this.setState({ confirmModal: !this.state.confirmModal });

  handleDelete = (event) => {
    if (event) {
      event.stopPropagation();
    }

    const {
      dispatchCrudDelete,
      startUndoable,
      resource,
      record,
      basePath,
      redirect,
      redirectAfterDelete,
      undoable,
      onClick
    } = this.props;

    const finalRedirect = redirectAfterDelete || redirect;

    if (undoable) {
      startUndoable(
        crudDelete(resource, record.id, record, basePath, finalRedirect)
      );
    } else {
      dispatchCrudDelete(resource, record.id, record, basePath, finalRedirect);
    }

    if (typeof onClick === 'function') {
      onClick();
    }
  };

  render = () => {
    const {
      label = 'ra.action.delete',
      classes = {},
      className,
      icon,
      onClick,
      translate,
      ...rest
    } = this.props;
    const { confirmModal } = this.state;

    return (
      <Fragment>
        <Button
          onClick={this.toggleModal}
          label={label}
          className={cx('ra-delete-button', classes.deleteButton, className)}
          key="button"
          {...sanitizeRestProps(rest)}
        >
          {icon}
        </Button>

        <ConfirmDialog
          open={confirmModal}
          title={translate('ra.message.delete_content')}
          onClose={this.toggleModal}
          onConfirm={this.handleDelete}
        />
      </Fragment>
    );
  };
}

DeleteButtonConfirm.defaultProps = {
  redirect: 'list',
  undoable: true,
  icon: <DeleteIcon />
};

export default compose(
  connect(
    null,
    { startUndoable, dispatchCrudDelete: crudDelete }
  ),
  withTranslate,
  withStyles(styles)
)(DeleteButtonConfirm);
