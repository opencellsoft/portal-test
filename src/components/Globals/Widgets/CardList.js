import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Card, List, ListItem, ListItemText } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const styles = ({ shadows, palette, typography }) => ({
  main: {
    flex: '1',
    marginTop: 20
  },
  card: {
    overflow: 'inherit',
    textAlign: 'right',
    padding: 16,
    minHeight: 52
  },
  iconWrapper: {
    float: 'left',
    margin: '0 20px 0 15px',
    zIndex: 100,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  icon: {
    width: 54,
    height: 54,
    padding: 14
  },
  title: {
    color: palette.grey[700],
    display: 'inline-block',
    lineHeight: '24px',
    textTransform: 'uppercase'
  },
  titleHover: {
    textDecoration: 'underline'
  },
  listItem: {
    padding: 0
  },
  link: {
    display: 'block',
    textDecoration: 'none',
    width: '100%',
    padding: 12,
    '&:hover': {
      background: palette.action.hover
    }
  },
  listItemText: {
    padding: 0
  },
  listItemTitle: {
    fontWeight: typography.title.fontWeight
  }
});

const CardList = ({ title, icon: Icon, bgColor, classes, list, count }) => (
  <div className={classes.main}>
    <div className={classes.iconWrapper}>
      <Icon className={classes.icon} style={{ color: bgColor }} />
    </div>

    <Card className={classes.card}>
      <Typography variant="caption" classes={{ caption: classes.title }}>
        {title}
      </Typography>

      <Typography variant="headline" component="h2">
        {count || 0}
      </Typography>

      {list.length > 0 && (
        <List>
          {list.map(({ primary, secondary, url }, index) => (
            <ListItem
              key={index}
              disableGutters
              classes={{ root: classes.listItem }}
              dense
            >
              <Link to={url} className={classes.link}>
                <ListItemText
                  primary={primary}
                  secondary={secondary}
                  classes={{
                    dense: classes.listItemText,
                    primary: classes.listItemTitle
                  }}
                />
              </Link>
            </ListItem>
          ))}
        </List>
      )}
    </Card>
  </div>
);

CardList.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.func.isRequired,
  bgColor: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
  list: PropTypes.arrayOf(
    PropTypes.shape({
      primary: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
      secondary: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
      url: PropTypes.string
    })
  ),
  count: PropTypes.number
};

export default withStyles(styles)(CardList);
