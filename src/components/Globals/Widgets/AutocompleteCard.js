import React from 'react';
import PropTypes from 'prop-types';
import { CardActions } from 'react-admin';
import { withStyles } from '@material-ui/core/styles';
import { isEmpty } from 'lodash';
import { Paper, Typography } from '@material-ui/core';
import InputRenderer from '../../GenericRenderer/Form/InputRenderer';
import CustomActions from '../../GenericRenderer/Table/CustomActions';
import UploadButton from '../../GenericRenderer/Buttons/UploadButton';

const styles = ({ spacing }) => ({
  card: {
    position: 'relative',
    padding: spacing.unit * 2,
    margin: `20px auto 0 ${spacing.unit}px`,
    borderRadius: '.2rem',
    textDecoration: 'none',
    flex: 1,
    minWidth: 320,
    maxWidth: 360,
    display: 'inline-block'
  },
  title: {
    marginBottom: spacing.unit
  }
});

const AutocompleteCard = ({
  field,
  title,
  classes,
  record,
  customActions,
  identifierData,
  withUpload,
  uploadedFileFormat,
  resource,
  ...props
}) => (
  <>
    <Paper className={classes.card}>
      <CardActions>
        {withUpload && (
          <UploadButton
            verb="UPLOAD_FILE"
            resource={resource}
            uploadedFileFormat={uploadedFileFormat}
          />
        )}
      </CardActions>
      {title && (
        <Typography variant="title" className={classes.title}>
          {title}
        </Typography>
      )}
      <InputRenderer
        {...field}
        identifierData={identifierData}
        record={record}
      />
    </Paper>
    {!isEmpty(customActions) && (
      <Paper className={classes.card}>
        <CustomActions record={record} actions={customActions} />
      </Paper>
    )}
  </>
);

AutocompleteCard.propTypes = {
  title: PropTypes.string,
  classes: PropTypes.object.isRequired,
  withUpload: PropTypes.bool,
  uploadedFileFormat: PropTypes.string,
  resource: PropTypes.string
};

export default withStyles(styles)(AutocompleteCard);
