import React, { cloneElement } from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  card: {
    float: 'left',
    margin: '-20px 20px 0 15px',
    zIndex: 100
  },
  icon: {
    float: 'right',
    width: 54,
    height: 54,
    padding: 14,
    color: '#FFFFFF'
  }
};

const CardIcon = ({ icon: Icon, classes, bgColor }) => (
  <Card className={classes.card}>
    {cloneElement(<Icon />, {
      className: classes.icon,
      style: { color: bgColor }
    })}
  </Card>
);

CardIcon.propTypes = {
  icon: PropTypes.oneOfType([PropTypes.node, PropTypes.func]).isRequired,
  classes: PropTypes.object,
  bgColor: PropTypes.string.isRequired
};

export default withStyles(styles)(CardIcon);
