import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import CardIcon from './CardIcon';

const styles = ({ shadows }) => ({
  main: {
    flex: '1',
    marginTop: 20,
    textDecoration: 'none',
    cursor: 'pointer',
    '&:hover': {
      boxShadow: shadows[1]
    }
  },
  card: {
    overflow: 'inherit',
    textAlign: 'right',
    padding: 16,
    minHeight: 52
  },
  titleHover: {
    textDecoration: 'underline'
  }
});

class CustomCard extends PureComponent {
  state = {
    hover: false
  };

  toggleHover = () => this.setState({ hover: !this.state.hover });

  render = () => {
    const { title, icon, bgColor, href, classes, children } = this.props;
    const { hover } = this.state;
    const Component = (href && Link) || 'div';

    return (
      <Component
        className={classes.main}
        onMouseEnter={this.toggleHover}
        onMouseLeave={this.toggleHover}
        to={href}
      >
        <CardIcon icon={icon} bgColor={bgColor} />

        <Card className={classes.card} raised={hover}>
          <Typography
            className={(hover && classes.titleHover) || undefined}
            color="textSecondary"
          >
            {title}
          </Typography>

          <Typography variant="headline" component="h2">
            {children}
          </Typography>
        </Card>
      </Component>
    );
  };
}

CustomCard.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.func.isRequired,
  bgColor: PropTypes.string.isRequired,
  href: PropTypes.string,
  classes: PropTypes.object.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default withStyles(styles)(CustomCard);
