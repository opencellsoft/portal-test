import React, { Children, cloneElement } from 'react';
import PropTypes from 'prop-types';
import { Paper } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = ({ spacing, palette }) => ({
  wrapper: {
    padding: spacing.unit * 2,
    textAlign: 'right',
    color: palette.text.secondary,
    display: 'flex',
    flexFlow: 'row wrap'
  }
});

const ActionsCard = ({ classes, children, ...props }) => (
  <Paper className={classes.wrapper}>
    {Children.map(children, (child) => cloneElement(child, { ...props }))}
  </Paper>
);

ActionsCard.propTypes = {
  resource: PropTypes.string.isRequired,
  record: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  basePath: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.array])
};

export default withStyles(styles)(ActionsCard);
