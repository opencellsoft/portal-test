import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const styles = ({ palette, shadows, typography }) => ({
  main: {
    display: 'block',
    textDecoration: 'none'
  },
  mainLink: {
    cursor: 'pointer',
    '&:hover': {
      boxShadow: shadows[1]
    }
  },
  card: {
    position: 'relative',
    borderRadius: '.2rem',
    textDecoration: 'none'
  },
  box: {
    padding: '32px 15px',
    fontSize: 40,
    lineHeight: '40px',
    textAlign: 'center',
    fontWeight: 300
  },
  bottomBox: {
    borderTop: '1px solid rgba(0,0,0,.15)'
  },
  counter: {
    fontSize: '2rem'
  },
  titleWrapper: {
    position: 'absolute',
    width: '100%',
    top: '50%',
    marginTop: -22,
    textAlign: 'center'
  },
  title: {
    height: 24,
    background: 'white',
    display: 'inline-block',
    padding: '4px 10px',
    textTransform: 'uppercase',
    lineHeight: '24px',
    border: '1px solid rgba(0,0,0,.15)',
    fontSize: typography.caption.fontSize,
    color: palette.grey[700],
    borderRadius: '1em'
  },
  titleHover: {
    textDecoration: 'underline'
  },
  icon: {
    width: 40,
    height: 40
  }
});

class CustomCard extends PureComponent {
  state = {
    hover: false
  };

  toggleHover = () => this.setState({ hover: !this.state.hover });

  render = () => {
    const { title, icon: Icon, color, href, classes, counter } = this.props;
    const { hover } = this.state;
    const Component = (href && Link) || 'div';

    return (
      <Component
        to={href}
        className={classnames(classes.main, href && classes.mainLink)}
        onMouseEnter={this.toggleHover}
        onMouseLeave={this.toggleHover}
      >
        <Paper className={classes.card} square>
          <div className={classes.box}>
            <Typography
              variant="title"
              component="span"
              className={classes.counter}
            >
              {counter}
            </Typography>
          </div>

          <div className={classes.titleWrapper}>
            <span
              className={classnames(
                classes.title,
                href && hover && classes.titleHover
              )}
            >
              {title}
            </span>
          </div>

          <div className={classnames(classes.box, classes.bottomBox)}>
            <Icon className={classes.icon} style={{ fill: color }} />
          </div>
        </Paper>
      </Component>
    );
  };
}

CustomCard.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.func.isRequired,
  color: PropTypes.string.isRequired,
  counter: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  href: PropTypes.string,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CustomCard);
