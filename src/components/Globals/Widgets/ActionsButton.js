import React from 'react';
import { Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { SWITCH_BUTTON } from '../../../constants/generic';
import SwitchButton from '../../GenericRenderer/Buttons/SwitchButton';

const styles = ({ palette }) => ({
  icon: {
    marginRight: 5,
    fontSize: 16,
    color: palette.primary.main
  }
});

const renderButton = (props) => {
  const { item, onClick, finalClasses, getSwitchValue, children } = props;
  const { source, type, label, variant } = item;

  switch (true) {
    case type === SWITCH_BUTTON:
      return (
        <SwitchButton
          label={label}
          source={source}
          value={getSwitchValue}
          onClick={onClick}
        />
      );
    default:
      return (
        <Button
          onClick={onClick}
          size="small"
          component="div"
          source={source}
          classes={finalClasses}
          variant={variant}
          color="primary"
        >
          {children}
        </Button>
      );
  }
};
const ActionsButton = (props) => renderButton(props);

ActionsButton.prototype = {
  onClick: PropTypes.func.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default withStyles(styles)(ActionsButton);
