import React from 'react';
import PropTypes from 'prop-types';
import { Divider, Tab, Tabs } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { isEmpty } from 'lodash';

import { extractColor } from '../../utils/color';

import DynamicIcon from './DynamicIcon';

const styles = () => ({
  root: {
    fontSize: '16px'
  },
  label: {
    whiteSpace: 'nowrap'
  }
});

const CustomTabs = ({ tabs, selectedTab, onChange, classes, wrapperID }) => {
  if (isEmpty(tabs)) {
    return false;
  }

  return (
    <>
      <Tabs
        scrollable
        fullWidth
        value={selectedTab || tabs[0].id}
        indicatorColor="primary"
        onChange={onChange}
      >
        {tabs.map(({ id, label, icon, color }, index) => (
          <Tab
            classes={classes}
            key={id}
            value={id}
            label={label}
            icon={
              icon ? (
                <DynamicIcon
                  icon={icon}
                  style={{ color: color ? extractColor(color) : undefined }}
                />
              ) : (
                undefined
              )
            }
            component={(props) => (
              <button
                id={`${wrapperID}-tab--${index}`}
                type="button"
                {...props}
              />
            )}
          />
        ))}
      </Tabs>

      <Divider />
    </>
  );
};

CustomTabs.propTypes = {
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired
    })
  ).isRequired,
  onChange: PropTypes.func.isRequired,
  selectedTab: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CustomTabs);
