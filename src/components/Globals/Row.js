import React, { Children, cloneElement, memo } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = ({ spacing }) => ({
  row: {
    width: '100%',
    display: 'flex'
  },
  rowAsGrid: {
    display: 'grid',
    gridGap: '15px 20px',
    gridAutoRows: 'minmax(80px, auto)',
    gridTemplateColumns: 'repeat(auto-fit, minmax(220px, auto))'
  }
});

const Row = memo(({ classes, className, children, grid, ...rest }) => (
  <div className={cx(classes.row, grid && classes.rowAsGrid, className)}>
    {Children.map(children, (child, key) =>
      cloneElement(child, { ...rest, key })
    )}
  </div>
));

Row.propTypes = {
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  grid: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node)
  ])
};

export default withStyles(styles)(Row);
