import React from 'react';
import PropTypes from 'prop-types';
import { TablePagination } from '@material-ui/core';

import withTranslate from '../../Hoc/withTranslate';

const Pagination = ({
  count,
  page,
  rowsPerPage = 5,
  onChangePage,
  onChangeRowsPerPage,
  rowsPerPageOptions = [5, 15, 25],
  translate
}) => (
  <TablePagination
    rowsPerPageOptions={rowsPerPageOptions}
    component="div"
    count={count}
    rowsPerPage={rowsPerPage}
    page={page}
    labelRowsPerPage={translate('ra.navigation.page_rows_per_page')}
    backIconButtonProps={translate('ra.navigation.prev')}
    nextIconButtonProps={translate('ra.navigation.next')}
    onChangePage={onChangePage}
    onChangeRowsPerPage={onChangeRowsPerPage}
  />
);

Pagination.propTypes = {
  count: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired,
  onChangeRowsPerPage: PropTypes.func.isRequired,
  rowsPerPageOptions: PropTypes.array
};

export default withTranslate(Pagination);
