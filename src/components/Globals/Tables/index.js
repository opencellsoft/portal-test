import React from 'react';
import PropTypes from 'prop-types';

import PaginatedTable from './PaginatedTable';
import VirtualizedTable from './VirtualizedTable';

const Table = ({ data = [], columns, virtualized, ...props }) => {
  const formattedData = data.map(({ id, ...el }, index) => ({
    id: id || index,
    ...el
  }));

  return virtualized ? (
    <VirtualizedTable data={formattedData} columns={columns} {...props} />
  ) : (
    <PaginatedTable data={formattedData} columns={columns} {...props} />
  );
};

Table.propTypes = {
  virtualized: PropTypes.bool,
  columns: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired
};

export default Table;
