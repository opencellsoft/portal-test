import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { AutoSizer, Column, SortDirection, Table } from 'react-virtualized';
import compose from 'recompose/compose';
import TableCell from '@material-ui/core/TableCell';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import { withStyles } from '@material-ui/core/styles';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';

import withTranslate from '../../Hoc/withTranslate';

const styles = ({ typography, palette }) => ({
  table: {
    fontFamily: typography.fontFamily
  },
  wrapper: {
    display: 'flex',
    alignItems: 'center',
    boxSizing: 'border-box',
    outline: 'none'
  },
  grid: {
    outline: 'none'
  },
  tableRow: {
    cursor: 'pointer'
  },
  tableRowActive: {
    backgroundColor: palette.grey[200]
  },
  tableRowHover: {
    '&:hover': {
      backgroundColor: palette.grey[200]
    }
  },
  tableCell: {
    flex: 1
  },
  tableCellActions: {
    justifyContent: 'flex-end'
  },
  noRows: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '8px 0 ',
    fontSize: typography.body1.fontSize,
    color: palette.grey[400]
  },
  noRowsIcon: {
    marginRight: 5,
    width: typography.subheading.fontSize,
    height: typography.subheading.fontSize
  },
  noClick: {
    cursor: 'initial'
  }
});

class MuiVirtualizedTable extends PureComponent {
  static propTypes = {
    columns: PropTypes.arrayOf(
      PropTypes.shape({
        dataKey: PropTypes.string.isRequired,
        width: PropTypes.number.isRequired
      })
    ).isRequired,
    data: PropTypes.array.isRequired,
    headerHeight: PropTypes.number,
    onRowClick: PropTypes.func,
    rowClassName: PropTypes.string,
    rowHeight: PropTypes.oneOfType([PropTypes.number, PropTypes.func]),
    sort: PropTypes.func,
    classes: PropTypes.object.isRequired
  };

  static defaultProps = {
    headerHeight: 56,
    rowHeight: 56
  };

  getRowClassName = ({ index }) => {
    const { data, classes, rowClassName } = this.props;
    const currentRow = index >= 0 && data[index];

    return cx(classes.tableRow, classes.wrapper, rowClassName, {
      [classes.tableRowHover]: index !== -1,
      [classes.tableRowActive]: currentRow.isSelected
    });
  };

  cellRenderer = ({ cellData, columnIndex = null }) => {
    const { columns, classes, rowHeight, onRowClick } = this.props;

    return (
      <TableCell
        component="div"
        className={cx(classes.tableCell, classes.wrapper, {
          [classes.noClick]: onRowClick == null
        })}
        variant="body"
        style={{ height: rowHeight }}
        align={
          (columnIndex != null && columns[columnIndex].dataKey === 'actions') ||
          false
            ? 'right'
            : 'left'
        }
      >
        {cellData}
      </TableCell>
    );
  };

  headerRenderer = ({ label, columnIndex, dataKey, sortBy, sortDirection }) => {
    const { headerHeight, columns, classes, sort } = this.props;
    const direction = {
      [SortDirection.ASC]: 'asc',
      [SortDirection.DESC]: 'desc'
    };

    const inner =
      !columns[columnIndex].disableSort && sort != null ? (
        <TableSortLabel
          active={dataKey === sortBy}
          direction={direction[sortDirection]}
        >
          {label}
        </TableSortLabel>
      ) : (
        label
      );

    return (
      <TableCell
        component="div"
        className={cx(classes.tableCell, classes.wrapper, classes.noClick)}
        variant="head"
        style={{ height: headerHeight }}
        align={columns[columnIndex].numeric || false ? 'right' : 'left'}
      >
        {inner}
      </TableCell>
    );
  };

  noRowsRenderer = () => {
    const { classes, translate } = this.props;

    return (
      <div className={classes.noRows}>
        <ErrorOutlineIcon className={classes.noRowsIcon} />
        {translate('ra.navigation.no_results')}
      </div>
    );
  };

  handleRowClick = ({ rowData }) => {
    const { onRowClick } = this.props;

    return onRowClick && onRowClick(rowData.id);
  };

  render = () => {
    const { columns, data, classes, ...tableProps } = this.props;

    return (
      <AutoSizer>
        {({ height, width }) => (
          <Table
            className={classes.table}
            height={height}
            width={width}
            rowCount={data.length}
            rowGetter={({ index }) => data[index]}
            {...tableProps}
            onRowClick={this.handleRowClick}
            gridClassName={classes.grid}
            rowClassName={this.getRowClassName}
            noRowsRenderer={this.noRowsRenderer}
          >
            {columns.map(({ className, dataKey, ...other }, index) => (
              <Column
                key={dataKey}
                headerRenderer={(headerProps) =>
                  this.headerRenderer({
                    ...headerProps,
                    columnIndex: index
                  })
                }
                className={cx(classes.wrapper, className)}
                cellRenderer={this.cellRenderer}
                dataKey={dataKey}
                {...other}
              />
            ))}
          </Table>
        )}
      </AutoSizer>
    );
  };
}

export default compose(
  withTranslate,
  withStyles(styles)
)(MuiVirtualizedTable);
