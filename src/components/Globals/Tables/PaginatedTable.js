import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table, TableBody, Paper } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import isNil from 'lodash/isNil';
import isNaN from 'lodash/isNaN';

import Head from './Head';
import Toolbar from './Toolbar';
import Row from './Row';
import Pagination from './Pagination';

import { handleRecordClick } from '../../../utils/routing';

const styles = ({ spacing, shadows }) => ({
  root: {
    width: '100%',
    boxShadow: 'none'
  },
  table: {},
  tableWrapper: {
    overflowX: 'auto',
    boxShadow: shadows[1],
    borderRadius: 5
  }
});

class PaginatedTable extends Component {
  static propTypes = {
    title: PropTypes.string,
    columns: PropTypes.array.isRequired,
    data: PropTypes.array.isRequired,
    order: PropTypes.string,
    orderBy: PropTypes.string,
    rowsPerPage: PropTypes.number,
    onRowClick: PropTypes.func,
    expand: PropTypes.object,
    classes: PropTypes.object.isRequired,
    withPagination: PropTypes.bool,
    rowsPerPageOptions: PropTypes.array
  };

  state = {
    order: this.props.order || 'asc',
    orderBy: this.props.orderBy,
    selected: [],
    data: this.props.data,
    page: 0,
    rowsPerPage: this.props.rowsPerPage || 5
  };

  componentDidUpdate = ({ data: prevData }) => {
    const { data } = this.props;

    if (prevData !== data) {
      this.setState({ data });
    }
  };

  desc = (a, b, orderBy) => {
    const aVal = a[orderBy];
    const bVal = b[orderBy];

    switch (true) {
      case !isNil(aVal) &&
        !isNaN(Number(aVal)) &&
        !isNil(bVal) &&
        !isNaN(Number(bVal)):
        return aVal - bVal;
      case bVal < aVal:
        return -1;
      case bVal > aVal:
        return 1;
      default:
        return 0;
    }
  };

  stableSort = () => {
    const { data } = this.state;

    return data
      .map((el, index) => [el, index])
      .sort((a, b) => {
        const order = this.getSorting(a[0], b[0]);

        return order !== 0 ? order : a[1] - b[1];
      })
      .map((el) => el[0]);
  };

  getSorting = (a, b) => {
    const { order, orderBy } = this.state;

    return order === 'desc'
      ? this.desc(a, b, orderBy)
      : -this.desc(a, b, orderBy);
  };

  getPageRows = () => {
    const { rowsPerPage, page } = this.state;

    return this.stableSort().slice(
      page * rowsPerPage,
      page * rowsPerPage + rowsPerPage
    );
  };

  handleRequestSort = (e, orderBy) => {
    const { orderBy: currentOrderBy, order: currentOrder } = this.state;
    const order =
      currentOrderBy === orderBy && currentOrder === 'desc' ? 'asc' : 'desc';

    return this.setState({ order, orderBy });
  };

  handleSelectAllClick = ({ target }) => {
    const { data } = this.state;

    return this.setState({
      selected: target.checked ? data.map(({ id }) => id) : []
    });
  };

  computeSelection = (id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);

    switch (true) {
      case selectedIndex === -1:
        return [...selected, id];
      case selectedIndex === 0:
        return [...selected.slice(1)];
      case selectedIndex === selected.length - 1:
        return [...selected.slice(0, -1)];
      case selectedIndex > 0:
        return [
          ...selected.slice(0, selectedIndex),
          ...selected.slice(selectedIndex + 1)
        ];
      default:
        return [];
    }
  };

  handleClick = (id) => {
    const { onRowClick } = this.props;

    if (!onRowClick) {
      return false;
    }

    return handleRecordClick(onRowClick)(id);
  };

  handleChangePage = (e, page) => this.setState({ page });

  handleChangeRowsPerPage = ({ target: { value: rowsPerPage } }) =>
    this.setState({ rowsPerPage, page: 0 });

  isSelected = (id) => this.state.selected.indexOf(id) !== -1;

  render = () => {
    const {
      title,
      classes,
      columns,
      withPagination,
      rowsPerPageOptions,
      onRowClick,
      expand
    } = this.props;
    const { data, order, orderBy, selected, rowsPerPage, page } = this.state;

    return (
      <Paper className={classes.root}>
        <Toolbar title={title} numSelected={selected.length} />

        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <Head
              columns={columns}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={data.length}
            />

            <TableBody className={classes.tableBody}>
              {this.getPageRows().map((row) => (
                <Row
                  key={row.id}
                  columns={columns}
                  values={row}
                  expand={expand}
                  onClick={onRowClick ? this.handleClick : undefined}
                  isSelected={this.isSelected(row.id)}
                />
              ))}
            </TableBody>
          </Table>
        </div>

        {withPagination && (
          <Pagination
            className={classes.pagination}
            rowsPerPageOptions={rowsPerPageOptions}
            count={data.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
        )}
      </Paper>
    );
  };
}

export default withStyles(styles)(PaginatedTable);
