import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import { Toolbar, Typography, IconButton, Tooltip } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { lighten } from '@material-ui/core/styles/colorManipulator';
import DeleteIcon from '@material-ui/icons/Delete';

const styles = ({ spacing, palette }) => ({
  root: {
    minHeight: 0,
    paddingRight: spacing.unit
  },
  highlight:
    palette.type === 'light'
      ? {
          color: palette.secondary.main,
          backgroundColor: lighten(palette.secondary.light, 0.85)
        }
      : {
          color: palette.text.primary,
          backgroundColor: palette.secondary.dark
        },
  spacer: {
    flex: '1 1 100%'
  },
  actions: {
    color: palette.text.secondary
  },
  title: {
    flex: '0 0 auto'
  }
});

const TableToolbar = ({ title, numSelected, classes }) => (
  <Toolbar
    className={cx(classes.root, {
      [classes.highlight]: numSelected > 0
    })}
  >
    <div className={classes.title}>
      {numSelected > 0 ? (
        <Typography color="inherit" variant="subtitle1">
          {numSelected} selected
        </Typography>
      ) : title ? (
        <Typography variant="h6" id="tableTitle">
          {title}
        </Typography>
      ) : null}
    </div>

    <div className={classes.spacer} />

    <div className={classes.actions}>
      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton aria-label="Delete">
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      ) : null}
    </div>
  </Toolbar>
);

TableToolbar.propTypes = {
  title: PropTypes.string,
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired
};

export default withStyles(styles)(TableToolbar);
