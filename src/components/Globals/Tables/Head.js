import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
  Checkbox,
  Tooltip
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
  cell: {
    padding: '0 12px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap'
  },
  label: {
    display: 'block'
  }
});

class Head extends Component {
  static propTypes = {
    columns: PropTypes.array.isRequired,
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
    bulkActions: PropTypes.oneOfType([PropTypes.element, PropTypes.bool]),
    classes: PropTypes.array.isRequired
  };

  createSortHandler = (property) => (e) =>
    this.props.onRequestSort(e, property);

  render = () => {
    const {
      columns,
      onSelectAllClick,
      order,
      orderBy,
      numSelected,
      rowCount,
      bulkActions,
      classes
    } = this.props;

    return (
      <TableHead>
        <TableRow>
          {bulkActions && (
            <TableCell padding="checkbox">
              <Checkbox
                indeterminate={numSelected > 0 && numSelected < rowCount}
                checked={numSelected === rowCount}
                onChange={onSelectAllClick}
              />
            </TableCell>
          )}

          {columns.map(({ id, label }) => (
            <TableCell
              key={id}
              align="right"
              sortDirection={orderBy === id ? order : false}
              classes={{ root: classes.cell }}
            >
              <Tooltip title={label} placement="bottom-start" enterDelay={300}>
                <TableSortLabel
                  active={orderBy === id}
                  direction={order}
                  onClick={this.createSortHandler(id)}
                  className={classes.label}
                >
                  {label}
                </TableSortLabel>
              </Tooltip>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
    );
  };
}

export default withStyles(styles)(Head);
