import React, { useState, cloneElement } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { TableCell, TableRow, Checkbox, Tooltip } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
  rowWithClick: {
    cursor: 'pointer'
  },
  cell: {
    padding: '0 12px',
    '&:last-child': {
      paddingRight: 0
    }
  }
});

const Row = ({
  columns,
  values,
  onClick,
  isSelected,
  expand,
  classes,
  bulkActions
}) => {
  const [expanded, setExpanded] = useState(false);

  return (
    <>
      <TableRow
        hover
        onClick={onClick ? () => onClick(values.id) : undefined}
        role="checkbox"
        aria-checked={isSelected}
        tabIndex={-1}
        key={values.id}
        selected={isSelected}
        className={onClick ? classes.rowWithClick : undefined}
      >
        {expand && (
          <TableCell padding="none" className={classes.expandIconCell}>
            <IconButton
              className={cx(classes.expandIcon, {
                [classes.expanded]: expanded
              })}
              component="div"
              tabIndex={-1}
              aria-hidden="true"
              onClick={setExpanded}
            >
              <ExpandMoreIcon />
            </IconButton>
          </TableCell>
        )}

        {bulkActions && (
          <TableCell padding="checkbox">
            <Checkbox checked={isSelected} />
          </TableCell>
        )}

        {columns.map(({ id }) => {
          const value = values[id];
          const RenderedValue =
            typeof value === 'string' ? (
              <Tooltip title={value} placement="bottom-start">
                <span>{value}</span>
              </Tooltip>
            ) : (
              value
            );

          return (
            <TableCell align="right" key={id} classes={{ root: classes.cell }}>
              {RenderedValue}
            </TableCell>
          );
        })}
      </TableRow>

      {expand && expanded && (
        <TableRow key={`${values.id}-expand`}>
          <TableCell colSpan={1}>
            {cloneElement(expand, {
              record: {},
              basePath: '',
              resource: '',
              id: String(values.id)
            })}
          </TableCell>
        </TableRow>
      )}
    </>
  );
};

Row.propTypes = {
  columns: PropTypes.array.isRequired,
  values: PropTypes.object.isRequired,
  onClick: PropTypes.func,
  isSelected: PropTypes.bool,
  bulkActions: PropTypes.oneOfType([PropTypes.element, PropTypes.bool]),
  classes: PropTypes.array.isRequired
};

export default withStyles(styles)(Row);
