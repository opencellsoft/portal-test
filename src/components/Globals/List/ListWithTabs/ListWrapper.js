import React, { PureComponent, cloneElement } from 'react';
import PropTypes from 'prop-types';
import { Card } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import ListView from './ListView';
import Tabs from './Tabs';

const styles = () => ({
  root: {
    display: 'flex'
  },
  card: {
    position: 'relative',
    flex: '1 1 auto'
  }
});

class ListWrapper extends PureComponent {
  static propTypes = {
    tabs: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired
      })
    ).isRequired,
    selectedTab: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.array]).isRequired,
    filterValues: PropTypes.object.isRequired,
    setFilters: PropTypes.func.isRequired,
    tabField: PropTypes.string.isRequired,
    aside: PropTypes.node,
    classes: PropTypes.object.isRequired
  };

  state = {
    tabs: this.props.tabs,
    selectedTab: this.props.initialTab || this.props.tabs[0].id
  };

  componentDidMount = () => {
    const { tabField, filterValues, setFilters, initialTab } = this.props;

    setFilters({ ...filterValues, [tabField]: initialTab });
  };

  handleTab = (e, selectedTab) => {
    const { tabField, filterValues, setFilters } = this.props;

    return this.setState({ selectedTab }, () =>
      setFilters({ ...filterValues, [tabField]: selectedTab })
    );
  };

  render = () => {
    const { tabs, selectedTab } = this.state;
    const { children, aside, classes, ...rest } = this.props;

    return (
      <div className={classes.root}>
        <Card className={classes.card}>
          <ListView
            {...rest}
            tabs={
              <Tabs
                tabs={tabs}
                selectedTab={selectedTab}
                onChange={this.handleTab}
              />
            }
          >
            {children}
          </ListView>
        </Card>

        {aside && cloneElement(aside, rest)}
      </div>
    );
  };
}

export default withStyles(styles)(ListWrapper);
