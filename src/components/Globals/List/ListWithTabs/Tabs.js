import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Divider, Tab, Tabs } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
  tab: {}
});

const ListTabs = ({ tabs, selectedTab, onChange, classes }) => (
  <Fragment>
    <Tabs
      fullWidth
      centered
      value={selectedTab}
      indicatorColor="primary"
      onChange={onChange}
    >
      {tabs.map(({ id, label }) => (
        <Tab className={classes.tab} key={id} label={label} value={id} />
      ))}
    </Tabs>

    <Divider />
  </Fragment>
);

ListTabs.propTypes = {
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired
    })
  ).isRequired,
  onChange: PropTypes.func.isRequired,
  selectedTab: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ListTabs);
