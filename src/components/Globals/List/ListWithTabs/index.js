import React from 'react';
import PropTypes from 'prop-types';
import { ListController } from 'react-admin';

import ListWrapper from './ListWrapper';

const ListWithTabs = ({ children, ...rest }) => (
  <ListController {...rest}>
    {(controllerProps) => (
      <ListWrapper {...rest} {...controllerProps}>
        {children}
      </ListWrapper>
    )}
  </ListController>
);

ListWithTabs.propTypes = {
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired
    })
  ).isRequired,
  initialTab: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  tabField: PropTypes.string.isRequired
};

export default ListWithTabs;
