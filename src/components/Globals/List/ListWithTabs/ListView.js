import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import {
  ListToolbar,
  Pagination,
  BulkDeleteButton,
  BulkActionsToolbar,
  ListActions,
  getListControllerProps
} from 'react-admin';
import Title from '../../../Layout/Title';

export const styles = ({ spacing }) => ({
  root: {
    display: 'flex'
  },
  mainWrapper: {
    position: 'relative',
    flex: '1 1 auto'
  },
  actions: {
    zIndex: 2,
    display: 'flex',
    justifyContent: 'flex-end',
    flexWrap: 'wrap'
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    alignSelf: 'flex-start'
  },
  noResults: { padding: 20 }
});

const sanitizeRestProps = ({
  actions,
  basePath,
  bulkActions,
  changeListParams,
  children,
  classes,
  className,
  crudGetList,
  currentSort,
  data,
  defaultTitle,
  displayedFilters,
  exporter,
  filter,
  filterDefaultValues,
  filters,
  filterValues,
  hasCreate,
  hasEdit,
  hasList,
  hasShow,
  hideFilter,
  history,
  ids,
  initialTab,
  isLoading,
  loadedOnce,
  locale,
  location,
  match,
  onSelect,
  onToggleItem,
  onUnselectItems,
  options,
  page,
  pagination,
  params,
  permissions,
  perPage,
  push,
  query,
  refresh,
  resource,
  selectedIds,
  selectedTab,
  setFilters,
  setPage,
  setPerPage,
  setSelectedIds,
  setSort,
  showFilter,
  sort,
  tabField,
  tabs,
  theme,
  title,
  toggleItem,
  total,
  translate,
  version,
  ...rest
}) => rest;

export const ListView = ({
  // component props
  actions = <ListActions />,
  aside,
  filters,
  bulkActions, // deprecated
  bulkActionButtons = <BulkDeleteButton />,
  pagination = <Pagination />,
  // overridable by user
  children,
  className,
  classes,
  exporter,
  tabs,
  title,
  ...rest
}) => {
  const { defaultTitle, version } = rest;
  const controllerProps = getListControllerProps(rest);

  return (
    <div
      className={cx('list-page', classes.root, className)}
      {...sanitizeRestProps(rest)}
    >
      <Title title={title} defaultTitle={defaultTitle} />

      <div className={classes.mainWrapper}>
        {bulkActions !== false &&
          bulkActionButtons !== false &&
          bulkActionButtons &&
          !bulkActions && (
            <BulkActionsToolbar {...controllerProps}>
              {bulkActionButtons}
            </BulkActionsToolbar>
          )}

        {(filters || actions) && (
          <ListToolbar
            filters={filters}
            {...controllerProps}
            actions={actions}
            bulkActions={bulkActions}
            exporter={exporter}
          />
        )}

        {tabs}

        <div key={version}>
          {children &&
            React.cloneElement(children, {
              ...controllerProps,
              hasBulkActions:
                bulkActions !== false && bulkActionButtons !== false
            })}

          {pagination && React.cloneElement(pagination, controllerProps)}
        </div>
      </div>
    </div>
  );
};

ListView.propTypes = {
  actions: PropTypes.element,
  aside: PropTypes.node,
  basePath: PropTypes.string,
  bulkActions: PropTypes.oneOfType([PropTypes.bool, PropTypes.element]),
  bulkActionButtons: PropTypes.oneOfType([PropTypes.bool, PropTypes.element]),
  children: PropTypes.element,
  className: PropTypes.string,
  classes: PropTypes.object,
  currentSort: PropTypes.shape({
    field: PropTypes.string,
    order: PropTypes.string
  }),
  data: PropTypes.object,
  defaultTitle: PropTypes.string,
  displayedFilters: PropTypes.object,
  exporter: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  filterDefaultValues: PropTypes.object,
  filters: PropTypes.element,
  filterValues: PropTypes.object,
  hasCreate: PropTypes.bool,
  hideFilter: PropTypes.func,
  ids: PropTypes.array,
  isLoading: PropTypes.bool,
  onSelect: PropTypes.func,
  onToggleItem: PropTypes.func,
  onUnselectItems: PropTypes.func,
  page: PropTypes.number,
  pagination: PropTypes.oneOfType([PropTypes.bool, PropTypes.element]),
  perPage: PropTypes.number,
  refresh: PropTypes.func,
  resource: PropTypes.string,
  selectedIds: PropTypes.array,
  setFilters: PropTypes.func,
  setPage: PropTypes.func,
  setPerPage: PropTypes.func,
  setSort: PropTypes.func,
  showFilter: PropTypes.func,
  title: PropTypes.any,
  total: PropTypes.number,
  translate: PropTypes.func,
  version: PropTypes.number
};

ListView.defaultProps = {
  classes: {}
};

export default compose(
  connect(({ admin: { ui: { sidebarOpen } } }) => ({ sidebarOpen })),
  withStyles(styles)
)(ListView);
