export const STATUS_ACTIVE = 'ACTIVE';
export const STATUS_ACTIVATED = 'ACTIVE';
export const STATUS_INACTIVE = 'INACTIVE';
export const STATUS_COMPLETED = 'COMPLETED';
export const STATUS_LAUNCHED = 'LAUNCHED';
export const STATUS_RENEWAL = 'RENEWAL';
export const STATUS_VALID = 'VALID';
export const STATUS_NEW = 'NEW';
export const STATUS_VALIDATED = 'VALIDATED';

export const STATUS_OPEN = 'OPEN';
export const STATUS_BILLED = 'BILLED';
export const STATUS_RERATED = 'RERATED';

export const STATUS_INSTANTIATED = 'CREATED';
export const STATUS_PENDING = 'PENDING';
export const STATUS_AVAILABLE = 'SUSPENDED';
export const STATUS_CLOSED = 'CLOSED';
export const STATUS_CANCELED = 'CANCELED';
export const STATUS_TERMINATED = 'RESILIATED';
export const STATUS_RETIRED = 'RETIRED';
export const STATUS_OBSOLETE = 'OBSOLETE';
export const STATUS_RESILIATED = 'RESILIATED';

export const STATUS_ERROR = 'ERROR';
export const STATUS_DRAFT = 'DRAFT';
export const STATUS_REJECTED = 'REJECTED';
export const STATUS_WELL_FORMED = 'WELL_FORMED';
export const STATUS_BAD_FORMED = 'BAD_FORMED';
export const STATUS_IN_STUDY = 'IN_STUDY';
export const STATUS_IN_DESIGN = 'IN_DESIGN';
export const STATUS_IN_TEST = 'IN_TEST';

export const STATUS_MATCHING_O = 'O';
export const STATUS_MATCHING_L = 'L';
export const STATUS_MATCHING_P = 'P';
export const STATUS_MATCHING_C = 'C';
export const STATUS_MATCHING_I = 'I';
export const STATUS_MATCHING_R = 'R';
