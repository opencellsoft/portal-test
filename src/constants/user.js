export const USER_USERNAME = 'id';
export const USER_FIRSTNAME = 'firstName';
export const USER_LASTNAME = 'lastName';
export const USER_EMAIL = 'email';
export const USER_ROLES = 'roles';
export const USER_CREATED = 'createdAt';
export const USER_LAST_LOGIN = 'lastLoginDate';

export const userFields = {
  [USER_USERNAME]: 'userName',
  [USER_FIRSTNAME]: 'name.firstName',
  [USER_LASTNAME]: 'name.lastName',
  [USER_EMAIL]: 'email',
  [USER_CREATED]: 'auditable.created',
  [USER_LAST_LOGIN]: 'lastLoginDate'
};
