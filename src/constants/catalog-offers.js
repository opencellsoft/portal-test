export const CHARGE_TYPE = 'Charge_Type';

export const ONE_OFF = 'O';
export const RECURRING = 'R';
export const ONE_OFF_RECURRING = 'OR';

export const chargeTypes = {
  [ONE_OFF]: 'One off',
  [RECURRING]: 'Recurring',
  [ONE_OFF_RECURRING]: 'One off / Recurring'
};
