export const STATUS_INSTANTIATED = 'INACTIVE';
export const STATUS_ACTIVATED = 'ACTIVE';
export const STATUS_CANCELED = 'CANCELED';
export const STATUS_TERMINATED = 'TERMINATED';
export const STATUS_CLOSED = 'CLOSED';
export const STATUS_AVAILABLE = 'SUSPENDED';

// charge types
export const CHARGE_TYPE_ONE_OFF = 'One-Off';
export const CHARGE_TYPE_RECURRING = 'Recurring';
export const CHARGE_TYPE_USAGE = 'Usage';
