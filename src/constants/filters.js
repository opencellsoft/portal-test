export const SORT_ASC = 'ASC';
export const SORT_DESC = 'DESC';

export const sortBy = {
  [SORT_ASC]: 'ASCENDING',
  [SORT_DESC]: 'DESCENDING'
};
