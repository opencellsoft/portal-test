export const DASHBOARD = 'dashboard';
export const HEADER = 'header';
export const LEFT_COL = 'leftCol';
export const MAIN = 'main';
export const RIGHT_COL = 'rightCol';
export const FOOTER = 'footer';

export const CRUD_LIST = 'list';
export const CRUD_SHOW = 'show';
export const CRUD_CREATE = 'create';
export const CRUD_EDIT = 'edit';

export const SIMPLE_LIST = 'SimpleList';
export const CARD_LIST = 'CardList';
export const SIMPLE_KPI = 'SimpleKPI';
export const SIMPLE_WIDGET = 'SimpleWidget';
export const SELECT_WIDGET = 'SelectWidget';
export const ACTION_WIDGET = 'ActionWidget';
export const LIST_WIDGET = 'ListWidget';
export const CUSTOM_FIELD_WIDGET = 'CustomFieldsWidget';
export const TABLE_WIDGET = 'TableWidget';
export const DETAILED_LIST_WIDGET = 'DetailedListWidget';
export const SIMPLE_TABLE = 'SimpleTable';
export const CHART_PIE = 'PieWidget';
export const CHART_BAR = 'BarWidget';
export const CHART_LINE = 'LineWidget';
export const TIME_LINE_WIDGET = 'TimeLineWidget';
export const DURATION_WIDGET = 'DurationWidget';
export const SELECT_SUBFORM_WIDGET = 'SelectSubformWidget';
export const CUSTOM_FIELDS_FORM_WIDGET = 'CustomFieldsFormWidget';

export const ACTION_NEW = 'new';
export const ACTION_EDIT = 'edit';
export const ACTION_DELETE = 'delete';
export const ACTION_EXPORT = 'export';
export const ACTION_MODAL = 'modal';
export const ACTION_API = 'api';
export const ACTION_DOWNLOAD = 'download';
export const ACTION_INSTANTIATE_SERVICE = 'instantiate-service';
export const ACTION_ACTIVATE_SERVICE = 'activate-service';
export const ACTION_SUSPEND_SERVICE = 'suspend-service';
export const ACTION_TERMINATE_SERVICE = 'terminate-service';
export const ACTION_UPDATE_SERVICE = 'update-service';
export const ACTION_RESUME_SERVICE = 'resume-service';

export const SWITCH_BUTTON = 'switch';
export const FIELD_TEXT = 'text';
export const FIELD_LONG_TEXT = 'long-text';
export const FIELD_CODE = 'code';
export const FIELD_AUTOCOMPLETE = 'autocomplete';
export const FIELD_BOOLEAN = 'boolean';
export const FIELD_NUMBER = 'number';
export const FIELD_REFERENCE = 'reference';
export const FIELD_REFERENCE_LIST = 'reference-list';
export const FIELD_DATE = 'date';
export const FIELD_DATE_TIME = 'datetime';
export const FIELD_LIST = 'list';
export const FIELD_STATIC_LIST = 'static-list';
export const FIELD_EMAIL = 'email';
export const FIELD_BUTTON = 'button';
export const FIELD_STATUS = 'status';
export const FIELD_STATUS_ICON = 'status-icon';
export const FIELD_PASSWORD = 'password';
export const FIELD_ACTIONS = 'actions';
export const FIELD_LINK = 'link';
export const FIELD_FILE = 'file';
export const FIELD_CUSTOM_CHOICES_SELECT = 'custom-choices-select';
export const FIELD_SELECT_SUBFORM = 'select-subform';
export const FIELD_ONE_REFERENCE = 'one-reference';
export const DYNAMIC_INPUT = 'dynamic-input';

export const API_GENERIC = 'api_generic';
export const API_STANDARD = 'api_standard';

export const CUSTOM_FIELD_MULTI = 'MULTI_VALUE';
export const CUSTOM_FIELD_STRING = 'STRING';
export const CUSTOM_FIELD_BOOLEAN = 'BOOLEAN';
export const CUSTOM_FIELD_SINGLE = 'SINGLE';
export const CUSTOM_FIELD_SINGLE_VALUE = 'SINGLE_VALUE';
export const CUSTOM_FIELD_DATE = 'DATE';
export const CUSTOM_FIELD_DOUBLE = 'DOUBLE';
export const CUSTOM_FIELD_LONG = 'LONG';
export const CUSTOM_FIELD_LIST = 'LIST';
export const CUSTOM_FIELD_ENTITY = 'ENTITY';

export const CUSTOM_STORAGE_MAP = 'MAP';
export const CUSTOM_STORAGE_MATRIX = 'MATRIX';

export const FORMAT_XML = 'xml';

export const MODAL_REDUX_FORM_NAME = 'modal-form';

export const DEFAULT_API_CALL = 'default';

export const START_DATE_FIELDS = ['From_Date', 'Start_Date'];
export const END_DATE_FIELDS = ['To_Date', 'End_Date'];

export const FILE_TYPES = {
  csv: 'text/csv',
  xml: 'text/xml',
  jpeg: 'image/jpeg',
  pdf: 'application/pdf'
};
