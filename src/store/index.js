import { combineReducers, createStore, compose, applyMiddleware } from 'redux';
import { routerMiddleware, routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import createSagaMiddleware from 'redux-saga';
import { all, fork } from 'redux-saga/effects';
import createHistory from 'history/createHashHistory';
import thunk from 'redux-thunk';
import {
  adminReducer,
  adminSaga,
  defaultI18nProvider,
  i18nReducer,
  USER_LOGOUT
} from 'react-admin';

import customReducers from '../reducers';
import customProviders from '../providers';

const { NODE_ENV, REACT_APP_LOCALE = 'en' } = process.env;

export const history = createHistory();

const configureStore = ({
  dataProvider,
  authProvider,
  i18nProvider = defaultI18nProvider,
  history,
  locale = REACT_APP_LOCALE,
  initialState = {}
}) => {
  const reducer = combineReducers({
    admin: adminReducer,
    i18n: i18nReducer(locale, i18nProvider(locale)),
    form: formReducer,
    router: routerReducer,
    ...customReducers
  });

  const resettableAppReducer = (state, action) =>
    reducer(action.type !== USER_LOGOUT ? state : undefined, action);

  const saga = function* rootSaga() {
    yield all([adminSaga(dataProvider, authProvider, i18nProvider)].map(fork));
  };
  const sagaMiddleware = createSagaMiddleware();

  const store = createStore(
    resettableAppReducer,
    initialState,
    compose(
      applyMiddleware(
        sagaMiddleware,
        routerMiddleware(history),
        thunk
        // add your own middlewares here
      ),
      NODE_ENV !== 'production' &&
        typeof window !== 'undefined' &&
        window.__REDUX_DEVTOOLS_EXTENSION__
        ? window.__REDUX_DEVTOOLS_EXTENSION__({
            trace: true,
            traceLimit: 25
          }) || compose
        : (f) => f
    )
  );
  sagaMiddleware.run(saga);

  return store;
};

export default configureStore({
  history,
  ...customProviders
});
