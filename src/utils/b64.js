export const utf8ToB64 = (str) =>
  str ? window.btoa(unescape(encodeURIComponent(str))) : '';

export const b64ToUtf8 = (str) =>
  str ? decodeURIComponent(escape(window.atob(str))) : '';
