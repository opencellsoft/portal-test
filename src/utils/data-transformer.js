import { has, get, isEmpty, set } from 'lodash';

/**
 *
 * @param {array} data - Array of table rows
 * @param {array} fields - fields from configuration
 * @return {array} array with setted values by its hashTable
 */

export const transformDataByHashTable = (data = [], fields = []) => {
  const dataToTransform = [...data];
  fields.forEach((field) => {
    const source = get(field, 'source');

    if (has(field, 'hashTable')) {
      const hashTable = get(field, 'hashTable');
      const { dependsOn, ...rest } = hashTable || {};
      dataToTransform.forEach((item) => {
        const value = get(item, source);

        if (!isEmpty(dependsOn)) {
          Object.keys(dependsOn).map((key) => {
            const hasValue = get(item, key);
            if (dependsOn[key] === '!null' && hasValue) {
              const hashedValue = value && get(rest, value, value);

              return set(item, source, hashedValue);
            }

            return item;
          });
        } else {
          const processedLabel = processLabelFromValue(hashTable, value);
          set(item, source, processedLabel);
        }
      });
    }
  });

  return dataToTransform;
};

const processLabelFromValue = (hashTable, value) => {
  const matchingKey = Object.keys(hashTable).find((key) => {
    const isInversed = key.startsWith('!');
    switch (true) {
      case key.includes('NULL'):
        return isInversed ? !isEmpty(value) : isEmpty(value);
      default:
        return false;
    }
  });

  return get(hashTable, matchingKey, value);
};

export const restDataByHashTable = (data = [], fields = []) => {
  const dataToTransform = [...data];
  fields.forEach((field) => {
    const source = get(field, 'source');
    if (has(field, 'hashTable')) {
      const hashTable = get(field, 'hashTable');
      dataToTransform.forEach((item) => {
        const value = get(item, source);
        Object.keys(hashTable).forEach(
          (key) => hashTable[key] === value && set(item, source, key)
        );
      });
    }
  });

  return dataToTransform;
};
