/**
 * Returns first item of a string divided by separator
 *
 * @param {string} str
 */
export const getFirstWord = (str, sep) => {
  if (typeof str !== 'string') {
    return false;
  }

  return str.split(sep)[0];
};
