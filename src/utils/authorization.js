import { get } from 'lodash';

import dynamicAppProperties from '../dynamicAppProperties';

import { sendRequest } from '../providers/sendRequest';

const { REACT_APP_DOMAIN_URL } = dynamicAppProperties || {};

export const checkRoles = (permissions = [], roles = []) =>
  roles.some((role) => permissions.includes(role));

export const getPermissions = (user) => {
  const resourceAccess = get(user, 'resource_access', {});

  return Object.values(resourceAccess).reduce((acc, current) => {
    const { roles = [] } = current;

    return [...acc, ...roles];
  }, []);
};

export const getUserPermissions = (user) => {
  const preferredUsername = get(user, 'preferred_username');
  if (!preferredUsername) {
    return [];
  }

  const finalUrl = `${REACT_APP_DOMAIN_URL}/opencell/api/rest/user?username=${preferredUsername}`;

  return sendRequest(finalUrl).then((response) => {
    const permissions = get(response, 'user.permission') || [];

    return permissions;
  });
};

/**
 *
 * @param {array} roles - array of object, comes from resource index file
 * @param {array} permissions - permissions comes from redux-store user (keycloack)
 * @param {string} action - edit/create/etc
 * @return {bool}
 **/
export const isAllowed = (roles = [], permissions = [], action) => {
  const rolesByAction = roles.filter((item) => get(item, 'page') === action);

  return (
    rolesByAction.length === 0 ||
    (rolesByAction.length === 1 &&
      get(rolesByAction[0], 'required', []).every((role) =>
        permissions.includes(role)
      ))
  );
};
