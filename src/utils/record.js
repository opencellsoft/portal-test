import { get, isNil, partition, orderBy, sortBy, set, has } from 'lodash';
import moment from 'moment-timezone';
import { useList } from './hooks/crud';

export const extractListFromRecord = ({ record, source }) => {
  const raw = get(record, source) || [];
  const list = Array.isArray(raw) ? raw : get(raw, 'data') || [];

  return {
    list,
    total: list.length,
    ...extractVersions(raw)
  };
};

export const extractListFromFetchedResource = ({
  resourceData,
  resource,
  actualRecord = {},
  filters,
  pagination = { page: 1, perPage: 5 },
  crudGetList
}) => {
  const { data, list } = resourceData || {};
  const { ids = [], total } = list || {};

  useList({
    resource,
    actualRecord,
    filters,
    pagination,
    crudGetList,
    watchProp: [actualRecord.id, pagination.page, pagination.perPage]
  });

  return {
    list: ids.map((id) => data[id]),
    total
  };
};

export const isVersionable = (list) => {
  if (!Array.isArray(list)) {
    return false;
  }

  const [item] = list || [];
  const { valuePeriodPriority } = item || {};

  return !isNil(valuePeriodPriority);
};

export const extractVersions = (raw) => {
  if (Array.isArray(raw) || typeof raw !== 'object') {
    return {};
  }

  const { valuePeriodPriority, valuePeriodStartDate, valuePeriodEndDate } =
    raw || {};

  return {
    valuePeriodPriority,
    valuePeriodStartDate,
    valuePeriodEndDate
  };
};

/**
 * Find the latest version and set its end date to current start date
 * @param {array} fields - Array of custom fields
 * @param {string} code - custom field code
 * @return {array} - Array of custom fields

 */
export const setPreviousVersion = (fields = [], code) => {
  const [sortedFields, restFields] = splitCfsInto2ArraysByCode(fields, code);

  if (sortedFields.length <= 1) {
    return sortBy(
      [...restFields, ...sortedFields],
      ['code', 'valuePeriodPriority']
    );
  }

  const copiedFields = [...sortedFields];

  const lastStartDate = getLastVersionStartDate(fields, code);
  const previousVersion = { ...getPreviousVersion(fields, code) };

  const previousEndDate = moment
    .utc(lastStartDate)
    .local()
    .startOf('day')
    .subtract(1, 'days')
    .valueOf();

  set(previousVersion, 'valuePeriodEndDate', previousEndDate);

  copiedFields[1] = previousVersion;

  return sortBy(
    [...restFields, ...copiedFields],
    ['code', 'valuePeriodPriority']
  );
};

/**
 * Set all cf values to null and remove the latest record from array
 * @param {array} fields - Array of custom fields
 * @param {string} code - custom field code
 * @return {array} - Array of custom fields

 */
export const setFieldsValueToNull = (fields = [], code) => {
  const [sortedFields, restFields] = splitCfsInto2ArraysByCode(fields, code);

  //copy and remove first element from record
  const copiedFields = [...sortedFields.slice(1)];

  copiedFields.forEach((item) => {
    if (has(item, 'mapValue')) {
      set(item, 'mapValue', null);
    }
    if (has(item, 'stringValue')) {
      set(item, 'stringValue', null);
    }
  });

  return sortBy(
    [...restFields, ...copiedFields],
    ['code', 'valuePeriodPriority']
  );
};

/**
 * get start date of last version
 * @param {array} fields - Array of custom fields
 * @param {string} code - custom field code
 * @return {number} - start date in ms
 */
const getLastVersionStartDate = (fields = [], code) => {
  const [sortedFields] = splitCfsInto2ArraysByCode(fields, code);

  const startDate = get(sortedFields[0], 'valuePeriodStartDate', 0);

  return startDate;
};

/**
 * get cf previous version
 * @param {array} fields - Array of custom fields
 * @param {string} code - custom field code
 * @return {object} - previous version
 */
const getPreviousVersion = (fields = [], code) => {
  const [sortedFields] = splitCfsInto2ArraysByCode(fields, code);

  return sortedFields[1];
};

/**
 * split cfs array into 2 arrays by cf code
 * @param {array} fields - Array of custom fields
 * @param {string} code - custom field code
 * @return {array} - array of 2 arrays
 */
export const splitCfsInto2ArraysByCode = (fields, code) => {
  const [filteredFields, restFields] = partition(
    fields,
    (field) => field.code === code
  );

  if (filteredFields.length < 1) return fields;

  const sortedFields = orderBy(filteredFields, 'valuePeriodPriority', 'desc');

  return [sortedFields, restFields];
};

export const extractFormNestedData = (obj) => {
  const objKeys =
    typeof obj === 'object' && obj !== null ? Object.keys(obj) : [];
  const [key] = objKeys;

  if (objKeys.length && !!obj.__cf) {
    return extractFormNestedData(obj.__cf);
  }

  if (
    objKeys.length === 1 &&
    (typeof obj[key] === 'object' || key === 'create')
  ) {
    return extractFormNestedData(obj[key]);
  }

  return obj || {};
};
