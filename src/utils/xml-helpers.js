export const toArray = (iterable) => Array.from(iterable);

export const isText = ({ nodeType }) => nodeType === Node.TEXT_NODE;

export const isElement = ({ nodeType }) => nodeType === Node.ELEMENT_NODE;

export const isAttr = ({ nodeType }) => nodeType === Node.ATTRIBUTE_NODE;

export const isCDATASection = ({ nodeType }) =>
  nodeType === Node.CDATA_SECTION_NODE;

export const isIncluded = (node, exclude) => {
  if (isElement(node)) {
    return exclude.indexOf(node.tagName) === -1;
  }
  if (isAttr(node)) {
    return exclude.indexOf(node.name) === -1;
  }

  return true;
};
