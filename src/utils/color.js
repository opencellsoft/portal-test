import * as Colors from '@material-ui/core/colors';

export const extractColor = (color) => {
  const materialColor = color ? color.split('.') : [];

  return materialColor.length > 1
    ? Colors[materialColor[0]][materialColor[1]]
    : materialColor;
};
