export const downloadFile = (csv, filename, fileType) => {
  const fakeLink = document.createElement('a');

  fakeLink.style.display = 'none';
  document.body.appendChild(fakeLink);

  const blob = new Blob([csv], { type: `text/${fileType}` });

  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
    // Manage IE11+ & Edge
    window.navigator.msSaveOrOpenBlob(blob, `${filename}.${fileType}`);
  } else {
    fakeLink.setAttribute('href', URL.createObjectURL(blob));
    fakeLink.setAttribute('download', `${filename}.${fileType}`);
    fakeLink.click();
  }
};
