import { get } from 'lodash';

/**
 *
 * @param {string} gui - gui string representation from server
 * @param {string} lang - language to translate
 * @return {array} Array of gui opjects that represents gui prop name and position
 * @example
 * input: (gui = 'tab:Tarification:0;fieldGroup:Prix:0;field:0')
 *
 * output:
 *  [
 *   { tab: "Tarification", position: "0" },
 *   { fieldGroup: "Prix", position: "0" },
 *   { field: "0" }
 *  ]
 */
const transformGuiStringToObject = (gui = '', lang) => {
  const guiArr = gui.split(';').map((el) => el.split(':'));

  return guiArr.map((el) => {
    switch (el[0]) {
      case 'field':
        return {
          [el[0]]: el[1]
        };
      default:
        return {
          [el[0]]: transformName(el[1], lang),
          position: el[2] || 0
        };
    }
  });
};

/**
 *
 * @param {string} name - string of such format Pricing|ENG=Pricing|FRA=Tarification
 * @param {string} lang - langugae to translate, by default ENG
 * @return {string}
 */
const transformName = (name = '', lang = 'ENG') => {
  const nameArr = name.split('|');
  if (nameArr.length > 1) {
    const translatedItem = nameArr.forEach((item) => {
      const translationArr = item.split('=');

      return translationArr[0] === lang;
    });

    return translatedItem ? translatedItem.split('=')[1] : nameArr[0];
  }

  return nameArr[0];
};

/**
 *
 * @param {array} arr - Array of custom fields
 * @param {string} propName - Name of prop by which we're filtering
 * @param {string} lang - language to translate
 * @return {array} Sorted array of objects that contains items connected to #{propName}
 * @example
 * input: (arr = [{
 *  ...,
 *  guiPosition: "tab:Tarification:0;fieldGroup:Prix:0;field:0",
 * }], propName = 'tab')
 *
 * output:
 *  [
 *   { tab: "Tarification",
 *     position: "0",
 *     items: [{...}] ////items connected to this tab
 *   },
 *  ]
 */
const sortByProp = (arr = [], propName, lang) =>
  arr
    .reduce((acc, currentValue, index) => {
      const guiObj = transformGuiStringToObject(currentValue.guiPosition, lang);

      const filteredItem = guiObj.find((el) => !!el[propName]) || {};
      const existedObj = acc.find(
        (el) => el[propName] === filteredItem[propName]
      );

      if (existedObj) {
        existedObj.items = [...existedObj.items, currentValue];
      } else {
        acc.push({
          ...filteredItem,
          items: [currentValue]
        });
      }

      return acc;
    }, [])
    .sort(sortNumeric);

/**
 *
 * @param {string|number} a - field
 * @param {string|number} b - field
 */

const sortNumeric = (a, b) => Number(a.position) - Number(b.position);

/**
 *
 * @param {object} a - custom field
 * @param {object} b - custom field
 */
const sortByFieldPosition = (a = {}, b = {}) => {
  const firstPosition = get(
    transformGuiStringToObject(a.guiPosition).find((item) => !!item.field),
    'field'
  );
  const nextPosition = get(
    transformGuiStringToObject(b.guiPosition).find((item) => !!item.field),
    'field'
  );

  return Number(firstPosition) - Number(nextPosition);
};

/**
 *
 * @param {array} fields - Array of custom fields
 * @param {string} lang - language to translate, if no specified ENG is using by default
 * @return {array} Array of tabs that contains array of
 * fieldGroups that contains array of fields. Everything is sorted by position.
 * @example
 * input: (fields = [
 * {
 *   ...,
 *   "guiPosition": "tab:Tarification:0;fieldGroup:Configuration:1;field:0"
 * },
 * {
 *   ...
 *   "guiPosition": "tab:Tarification:0;fieldGroup:Configuration:1;field:0"
 * },
 * ...
 * ])
 *
 * output:
 * [{
 *   "tab": "Tarification",
 *   "position": "0",
 *   "items": [
 *     {
 *       "fieldGroup": "Prix",
 *       "position": "0",
 *       "items": [
 *         {
 *           ...,
 *           "guiPosition": "tab:Tarification:0;fieldGroup:Prix:0;field:0"
 *         }
 *       ]
 *     },
 *     {
 *      "fieldGroup": "Configuration",
 *       "position": "1",
 *       "items": [
 *         {
 *           ...,
 *           "guiPosition": "tab:Tarification:0;fieldGroup:Configuration:1;field:0"
 *         },
 *          ...rest items
 *      ]
 *    }
 *   ]
 * }]
 *
 */
export const sortFieldsByGui = (fields = [], lang) =>
  sortByProp(fields, 'tab').map((tab) => ({
    ...tab,
    items: sortByProp(tab.items, 'fieldGroup').map((field) => ({
      ...field,
      items: field.items.sort(sortByFieldPosition)
    }))
  }));
