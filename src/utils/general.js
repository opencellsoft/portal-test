import { isObject, reduce, merge } from 'lodash';

export const flattenKeys = (obj, path = []) =>
  !isObject(obj)
    ? { [path.join('.')]: obj }
    : reduce(
        obj,
        (cum, next, key) => merge(cum, flattenKeys(next, [...path, key])),
        {}
      );
