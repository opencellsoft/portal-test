import checkPropTypes from 'check-prop-types';

export const findBySource = (component, attr) => {
  const wrapper = component.find(`[source='${attr}']`);

  return wrapper;
};

export const checkProps = (component, expectedProps) => {
  const propsErr = checkPropTypes(
    component.propTypes,
    expectedProps,
    'props',
    component.name
  );

  return propsErr;
};
