import get from 'lodash/get';

export const handleRecordClick = ({ resource, id, onRowClick }) => {
  switch (true) {
    case typeof onRowClick === 'function':
      return onRowClick;
    // case onRowClick === 'show':
    //   return () => browserHistory.push(`/${resource}/${id}/show`);
    default:
      return () => {};
  }
};

/**
 * Helper for find resource in menu tree
 *
 * @param {string} path
 * @param {object} menu
 *
 * @return {array|undefined} - path as array of nodes to source resource or undefined if resource not found
 */
export const buildPathFromResource = (path, menu) => {
  const pathElements = path.split('/');
  const res = [];
  let curMenu = menu;

  for (let i = 1; i < pathElements.length && curMenu; i++) {
    const resource = pathElements.slice(1, i + 1).join('/');

    if (!(resource in curMenu)) {
      return undefined;
    }

    const { subItems, label } = curMenu[resource];
    res.push({
      resource,
      label
    });
    curMenu = subItems;
  }

  return res.length ? res : undefined;
};

export const getFullReference = (reference, modulesMap) =>
  Object.keys(modulesMap).find((path) =>
    // eslint-disable-next-line security/detect-non-literal-regexp
    new RegExp(`${reference}$`).test(path)
  );

export const getHomePageFromConfig = (config) =>
  get(config, 'options.routes.home', 'dashboard');

export const getSubMenu = (menu, path) => {
  const pathElements = path.split('/');

  const subItems = getSubItems(menu, pathElements.slice(0, -1).join('/'));

  if (subItems && path in subItems) {
    return subItems[path];
  }

  return undefined;
};

export const getSubItems = (menu, path) => {
  let curLevel = menu;
  if (path.length) {
    const pathElements = path.split('/');
    for (let i = 0; i < pathElements.length; i++) {
      const absPath = pathElements.slice(0, i + 1).join('/');
      if (curLevel && absPath in curLevel) {
        curLevel = curLevel[absPath].subItems;
      } else {
        return undefined;
      }
    }
  }

  return curLevel;
};

export const pathPop = (path) =>
  path
    .split('/')
    .slice(0, -1)
    .join('/');

export const getDirectoryRoutes = ({ modules, menu }) =>
  Object.values(
    Object.values(modules).reduce((acc, { resource }) => {
      const resourceElements = resource.split('/');

      if (resourceElements.length < 2) {
        return acc;
      }

      return resourceElements.slice(0, -1).reduce((acc2, path, index) => {
        const resource = resourceElements.slice(0, index + 1).join('/');

        return {
          ...acc,
          [resource]: {
            resource,
            subItems: getSubItems(menu, resource)
          }
        };
      }, {});
    }, {})
  );
