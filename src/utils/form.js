import {
  getFormSyncErrors,
  getFormAsyncErrors,
  getFormSubmitErrors
} from 'redux-form';

/**
 * get all redux form errors
 * @param {object} state
 * @param {form} form
 *
 * @return {object}
 */
export const collectErrors = (state, form) => {
  const syncErrors = getFormSyncErrors(form)(state);
  const asyncErrors = getFormAsyncErrors(form)(state);
  const submitErrors = getFormSubmitErrors(form)(state);

  return {
    ...syncErrors,
    ...asyncErrors,
    ...submitErrors
  };
};
