import { get } from 'lodash';
import { getFullReference } from '../routing';

export const getResourceFromPath = (resourcePath) => {
  const resourceArray = resourcePath.split('/');

  return resourceArray[resourceArray.length - 1];
};

/**
 * Helper MUTATING function for patching module configuration object.
 * Function find all 'resource' or 'reference' keys in all nested elements including array elements
 * try replace to path in dependency of nesting menu
 *
 *
 * @param {object} src - module configuration object
 * @param {object} modulesMap - module map with keys corresponding to the full paths to the modules
 *
 * @return {object} - updated module configuration object
 */
export const patchDeepResourcesPath = (src, modulesMap) => {
  if (src instanceof Array) {
    src.forEach((item) => patchDeepResourcesPath(item, modulesMap));
  } else if (src instanceof Object) {
    for (const key in src) {
      if (src.hasOwnProperty(key)) {
        if (
          (key === 'resource' || key === 'reference') &&
          typeof src[key] === 'string'
        ) {
          const fullPath = getFullReference(src[key], modulesMap);
          if (fullPath) {
            src[key] = fullPath;
          }
        } else {
          patchDeepResourcesPath(src[key], modulesMap);
        }
      }
    }
  }

  return src;
};

export const buildNestedResourcePath = (module) => {
  const subItemsPath = get(module, `inMenu.path`);
  const { resource } = module;

  if (!subItemsPath) {
    return module;
  }

  module.resource = `${subItemsPath}/${resource}`;

  return module;
};
