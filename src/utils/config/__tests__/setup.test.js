import { isEqual, get } from 'lodash';
import { buildNestedResourcePath } from '../setup';

describe('Config setup utils', () => {
  it('should concat sub-items path to resource name', () => {
    const { resource: transformedResource } = buildNestedResourcePath(
      mockModuleWithSubItems
    );
    const originalPath = get(mockModuleWithSubItems, `inMenu.path`);
    const { resource: initialResource } = mockModule;
    const expectedPath = `${originalPath}/${initialResource}`;

    expect(transformedResource).toEqual(expectedPath);
  });

  it('should return null as no subitems are found', () => {
    const res = buildNestedResourcePath(mockModule);

    expect(isEqual(res, mockModule)).toEqual(true);
  });
});

const mockModule = {
  resource: 'sellers',
  label: 'Sellers',
  icon: 'Beenhere',
  menuPriority: 2,
  provider: {},
  i18n: {}
};

const mockModuleWithSubItems = {
  ...mockModule,
  inMenu: {
    icon: 'Build',
    label: 'setup',
    path: 'setup'
  }
};
