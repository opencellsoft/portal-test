import { useEffect } from 'react';

import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';

import { processParams } from '../generic-render';

export const useList = ({
  resources = {},
  resource,
  actualRecord = {},
  filters = {},
  pagination = { page: 1, perPage: 5 },
  sort = { order: 'DESC' },
  crudGetList,
  watchProp = []
}) => {
  useEffect(() => {
    const listFilters = prepareFilters(filters, actualRecord);

    crudGetList(resource, pagination, sort, listFilters);
  }, watchProp);
};

const prepareFilters = (filters, actualRecord) => {
  const { code } = actualRecord;
  const hasRecord = !!code;

  if (isEmpty(filters) || isEmpty(actualRecord)) {
    return hasRecord ? { code } : {};
  }

  if (!Array.isArray(filters) || !hasRecord) {
    return processParams({ source: { record: actualRecord }, params: filters });
  }

  return filters.reduce(
    (res, field) => ({ ...res, [field]: get(actualRecord, field) }),
    {}
  );
};
