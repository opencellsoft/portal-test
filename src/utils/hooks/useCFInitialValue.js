import { useMemo, useEffect } from 'react';
import { isEmpty } from 'lodash';

import { getValueByFieldType } from '../custom-fields';

export const useCfInitialValue = ({ field, input }) => {
  const initialValue = useMemo(
    () => {
      if (isEmpty(field)) {
        return '';
      }

      return getValueByFieldType(field);
    },
    [field]
  );

  useEffect(
    () => {
      if (input.value === '' && !isEmpty(field)) {
        input.onChange(initialValue);
      }
    },
    [field, initialValue]
  );
};
