import { memoize } from 'lodash';

/**
 * Returns current origin (e.g: http://testdomain.fr or https://domain.com:8000)
 */
export const getOrigin = () =>
  `${window.location.protocol}//${window.location.host}`;

/**
 * Convert object to url params
 *
 * @param {object} data
 */
export const encodeParams = (data) =>
  Object.keys(data)
    .map((key) => `${encodeURIComponent(key)}=${encodeURIComponent(data[key])}`)
    .join('&');

/**
 * Removes unwanted fields when updating
 * (ES fields, sort) from params
 *
 * @param {object} params
 */
export const sanitizeParams = (params) => {
  if (typeof params !== 'object') {
    return {};
  }

  return Object.keys(params).reduce((res, key) => {
    switch (true) {
      case key.charAt(0) === '_':
      case key === 'sort':
        return res;
      default:
        return {
          ...res,
          [key]: params[key]
        };
    }
  }, {});
};

export const findResourceFromPathname = memoize(
  (srcPathname = '', modulesMap) =>
    Object.keys(modulesMap).find((resource) =>
      // eslint-disable-next-line security/detect-non-literal-regexp
      new RegExp(resource).test(srcPathname)
    )
);
