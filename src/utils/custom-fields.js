import moment from 'moment-timezone';
import _, {
  get,
  has,
  isEmpty,
  maxBy,
  memoize,
  sortBy,
  uniqBy,
  isArray,
  omit,
  isUndefined
} from 'lodash';

import {
  CUSTOM_FIELD_STRING,
  CUSTOM_FIELD_MULTI,
  CUSTOM_FIELD_BOOLEAN,
  CUSTOM_FIELD_DATE,
  CUSTOM_FIELD_DOUBLE,
  CUSTOM_FIELD_LONG,
  CUSTOM_FIELD_LIST,
  CUSTOM_FIELD_ENTITY,
  FIELD_TEXT,
  START_DATE_FIELDS,
  END_DATE_FIELDS,
  CUSTOM_STORAGE_MAP,
  CUSTOM_STORAGE_MATRIX,
  CUSTOM_FIELD_SINGLE_VALUE,
  CUSTOM_FIELD_SINGLE
} from '../constants/generic';

export const EDITABLE_VERSIONS_LAST = 'last';
export const EDITABLE_VERSIONS_FIRST = 'first';

const CFTypesMapping = {
  [CUSTOM_FIELD_STRING]: FIELD_TEXT
};

export const regroupTabs = memoize((fields = []) =>
  fields.reduce((res, { guiPosition, ...rest }) => {
    const [, fieldGroup] = guiPosition.match(/(?:tab:)(.*?)(?:\||:)/);

    return {
      ...res,
      [fieldGroup]: [
        ...(Array.isArray(res[fieldGroup]) ? res[fieldGroup] : []),
        rest
      ]
    };
  }, {})
);

export const extractCF = (record, fieldCode, onlyOne) => {
  if (isEmpty(record) || isEmpty(fieldCode)) {
    return null;
  }

  const customFields = get(record, 'customFields.customField', []);
  const data = customFields.filter(({ code }) => code === fieldCode);

  return onlyOne ? data[0] : data;
};

export const sortCFsByLatestVersion = (cfData = {}) => {
  const { customField: customFields = [] } = cfData;
  const uniqueCFCodes = customFields.reduce((res, { code }) => {
    if (res.findIndex((el) => el === code) < 0) {
      res.push(code);
    }

    return res;
  }, []);

  if (isEmpty(uniqueCFCodes)) {
    return undefined;
  }

  return {
    customField: uniqueCFCodes.reduce((res, uniqueCode) => {
      const currentCFs = customFields.filter(({ code }) => code === uniqueCode);

      const sortedCFs = sortBy(
        currentCFs,
        ({ valuePeriodPriority, valuePeriodStartDate }) =>
          valuePeriodPriority * -1 || valuePeriodStartDate * -1
      );

      res.push(...sortedCFs);

      return res;
    }, [])
  };
};

export const extractMatrixDefinition = (fieldsDefinition, cfCode) => {
  const { field: definitions = [] } = fieldsDefinition || {};

  const definition = definitions.find(({ code }) => code === cfCode);
  const { matrixColumn: matrixDefinition = [] } = definition || {};

  return matrixDefinition.reduce(
    (res, { code, columnUse }) => {
      if (columnUse === 'USE_KEY') {
        res.keyColumns.push(code);
      } else {
        res.valueColumns.push(code);
      }

      return res;
    },
    {
      keyColumns: [],
      valueColumns: []
    }
  );
};

export const extractMatrixDataByDefinition = (cf = {}, cfDefinition = {}) => {
  const { code: cfCode, mapValue = {} } = cf;

  const currentCFDefinition = getCFDefinitionByCode(cfDefinition, cfCode);

  if (isEmpty(currentCFDefinition)) {
    return {};
  }

  const { matrixColumn = [] } = currentCFDefinition;

  return extractMatrixData(mapValue, matrixColumn);
};

/**
 * Default schema for MAP of STRING which doesn't have schema
 */
const defaultMapMatrix = [
  { columnUse: 'USE_KEY', code: 'key', keyType: 'STRING', label: 'Key' },
  { columnUse: 'USE_VALUE', code: 'value', keyType: 'STRING', label: 'Value' }
];

export const extractMatrixData = (
  mapValue,
  matrixColumn = defaultMapMatrix
) => {
  const keyFields = getKeyFieldsFromMatrix(matrixColumn);

  const valueFields = getValueFieldsFromMatrix(matrixColumn);

  if (isEmpty(keyFields)) {
    keyFields.push({
      label: 'Key',
      type: 'text',
      code: 'key',
      source: 'key',
      columnUse: 'USE_KEY'
    });
  }

  if (isEmpty(valueFields)) {
    valueFields.push({
      label: 'Value',
      code: 'value',
      source: 'value',
      type: 'text',
      columnUse: 'USE_VALUE'
    });
  }

  const { key, ...values } = mapValue || {};

  const columns = matrixColumn.map((column) => {
    const { code, label, columnUse: use, keyType } = column || {};

    return {
      source: code,
      type: mapCFTypesToFields(keyType),
      label,
      use
    };
  });

  const data = Object.keys(values).map((key) => {
    const keyData = key.split('|');
    const { value: rawValueData = '' } = values[key];
    const valueData =
      typeof rawValueData === 'string'
        ? rawValueData.split('|')
        : [rawValueData];

    const dataWithKeys = keyFields.reduce((res, { code }, index) => {
      res[code] = keyData[index];

      return res;
    }, {});

    return valueFields.reduce((res, { code }, index) => {
      res[code] = valueData[index];

      return res;
    }, dataWithKeys);
  });

  if (isEmpty(valueFields) && isEmpty(keyFields)) {
    return {
      columns,
      data: Object.keys(values).map((key) => ({
        value: values[key].value,
        key
      }))
    };
  }

  return {
    columns,
    data
  };
};

export const mapCFTypesToFields = (keyType) =>
  CFTypesMapping[keyType] || FIELD_TEXT;

export const generateFormFieldsFromCF = (cfCode, fields, cfIndex) => ({
  rows: [
    fields.map(({ source, type, label }) => ({
      type: mapCFTypesToFields(type),
      source: `__cf.${cfCode}.${cfIndex}.${source}`,
      label
    }))
  ]
});

export const createMatrix = (
  customFields,
  cfDefinition,
  dataToInsert,
  fromDate,
  endDate
) => {
  const {
    code,
    description,
    fieldType,
    languageDescriptions,
    versionable
  } = cfDefinition;

  const { keyColumns, valueColumns } = extractMatrixDefinition(
    { field: [cfDefinition] },
    code
  );

  return {
    code,
    description,
    fieldType,
    languageDescriptions,
    mapValue: dataToInsert.reduce(
      (res, row) => {
        const rowKey = keyColumns
          .map((column, index) => row[column] || row[index])
          .join('|');

        const rowValue = valueColumns
          .map((column, index) => row[column] || row[index + keyColumns.length])
          .join('|');

        res[rowKey] = { value: rowValue };

        return res;
      },
      {
        key: {
          value: cfDefinition.matrixColumn.map(({ code }) => code).join('/')
        }
      }
    ),
    ...(versionable
      ? addMatrixVersionFields(
          customFields,
          cfDefinition,
          dataToInsert,
          fromDate,
          endDate
        )
      : {})
  };
};

const addMatrixVersionFields = (
  customFields,
  cfDefinition,
  dataToInsert,
  fromDate,
  endDate
) => {
  const { code } = cfDefinition;

  const fieldLatestVersion = getLatestCFVersion(
    { customFields: { customField: customFields } },
    code,
    true
  );

  const nextVersion = fieldLatestVersion
    ? fieldLatestVersion.valuePeriodPriority + 1
    : 1;

  const res = {
    valuePeriodPriority: nextVersion,
    valuePeriodStartDate: moment
      .utc(fromDate)
      .local()
      .startOf('day')
      .valueOf()
  };
  if (endDate) {
    res.valuePeriodEndDate = moment
      .utc(endDate)
      .local()
      .startOf('day')
      .valueOf();
  }

  return res;
};

export const getCFByVersion = (record, cfCode, version) => {
  const data = extractCF(record, cfCode) || [];

  return data.find(
    ({ valuePeriodPriority }) => valuePeriodPriority === version
  );
};

export const getLatestCFVersion = (record, cfCode) => {
  const data = extractCF(record, cfCode) || [];

  return maxBy(data, ({ valuePeriodPriority }) => valuePeriodPriority);
};

export const getAvailableCFVersions = (record, cfCode) => {
  const data = extractCF(record, cfCode) || [];
  const versions = data.map(({ valuePeriodPriority }) => valuePeriodPriority);

  return sortBy(uniqBy(versions));
};

export const clearUnusedCFs = (record, cfDefinition) => {
  const customFields = get(record, 'customFields.customField', []);
  const { field = [] } = cfDefinition || {};

  if (isEmpty(customFields)) {
    return record;
  }

  const nextCustomFields = customFields.reduce((res, cf) => {
    const { code: cfCode } = cf;
    const isCFDefined = field.find(({ code }) => code === cfCode);

    return !isEmpty(isCFDefined) ? [...res, cf] : [...res];
  }, []);

  return {
    ...record,
    customFields: { customField: nextCustomFields }
  };
};

/**
 * Sets the previous endDate value to startDate - 1 day
 * @param {*} originalData
 * @param {*} cfKeys
 * @param {*} updatedValues
 */
export const populateEndDate = (originalData, cfKeys, updatedValues) => {
  // Loop through all the keys
  cfKeys.forEach((key) => {
    // Early exit if the key doesn't exist
    if (!updatedValues[key]) {
      return;
    }

    const row = Object.keys(updatedValues[key]);
    // Read the record to update
    const { create: record } = updatedValues[key][row] || {};

    // Exit if the record is not valid
    if (!record) {
      return;
    }

    // Find the startDate field used in the config
    const startDateField = Object.keys(record).find(
      (field) => START_DATE_FIELDS.indexOf(field) > -1
    );

    // Exit if the startDateField is not valid
    if (!startDateField) {
      return;
    }

    // Substract one Day and format the date
    const formattedDate = substractDaysFromDate(record[startDateField], 1);
    const { mapValue = {} } = originalData[0];
    // Make sure its a multi-value field
    if (
      originalData[0].code === key &&
      originalData[0].fieldType === 'MULTI_VALUE' &&
      !isEmpty(mapValue)
    ) {
      // Read the key field from the map config

      const {
        key: { value: keyFields }
      } = mapValue;
      const keyFieldsArray = keyFields && keyFields.split('/');

      // Find the endDate field to use
      const endDateField = keyFieldsArray.find(
        (field) => END_DATE_FIELDS.indexOf(field) > -1
      );
      const endDateIndex = keyFieldsArray.indexOf(endDateField);

      // Read the other map keys
      const mapKeys = Object.keys(originalData[0].mapValue);

      // Override the date
      mapKeys.forEach((mapKey) => {
        const mapKeyArray = mapKey.split('|');
        updateEndDate(
          mapKeyArray,
          originalData,
          endDateIndex,
          mapKey,
          formattedDate
        );
      });
    }
  });
};

export const substractDaysFromDate = (date, days) => {
  const subsctractedDate = moment(date, 'DD/MM/YYYY')
    .subtract(days, 'days')
    .valueOf();

  return moment(subsctractedDate).format('DD/MM/YYYY');
};

export const updateEndDate = (values, dataToUpdate, index, cfKey, date) => {
  if (values.length > 1 && isEmpty(values[index])) {
    values[index] = date;
    const newKey = values.join('|');
    dataToUpdate[0].mapValue[newKey] = dataToUpdate[0].mapValue[cfKey];
    delete dataToUpdate[0].mapValue[cfKey];
  }
};

export const extractFieldsMetas = (columns = [], config = {}) => {
  const { fields = [] } = config || {};

  return fields.map(({ source, label: customLabel, ...rest }) => {
    const { label = customLabel } =
      columns.find(({ source: columnSource }) => columnSource === source) || {};

    return { ...rest, source, label };
  });
};

/**
 * Extract fields from fieldsDefinition matrixColumn
 * @param {object} fieldsDefinition
 *
 * @return {object}
 */
export const extractCFMultiValueConfig = ({ matrixColumn = [] }) => {
  const isNoKeys = matrixColumn.every(
    (item = {}) => item.columnUse === 'USE_VALUE'
  );
  const isNoValues = matrixColumn.every(
    (item = {}) => item.columnUse === 'USE_KEY'
  );

  if (isEmpty(matrixColumn) && isNoKeys) {
    matrixColumn = [
      ...matrixColumn,
      {
        code: 'key',
        columnUse: 'USE_KEY',
        keyType: 'STRING',
        label: 'key',
        position: matrixColumn.length + 1
      }
    ];
  }
  if (isNoValues) {
    matrixColumn = [
      ...matrixColumn,
      {
        code: 'value',
        columnUse: 'USE_VALUE',
        keyType: 'STRING',
        label: 'Value',
        position: matrixColumn.length + 1
      }
    ];
  }

  return {
    fields: matrixColumn.map((column) => ({
      source: column.code,
      label: column.label,
      columnUse: column.columnUse
    }))
  };
};

/**
 * Extract array of choices from fieldsDefinition
 * @param {object} fieldsDefinition
 *
 * @return {array}
 */
export const extractCustomFieldChoices = ({ listValues = {} }) =>
  Object.entries(listValues).map(([key, value]) => ({
    id: key,
    label: value
  }));

/**
 * Return field value by its type
 * @param {object} field
 * @param {boolean} isDefault
 *
 * @return {any}
 */
export const getValueByFieldType = (field = {}) => {
  switch (true) {
    case field.fieldType === CUSTOM_FIELD_ENTITY &&
      field.storageType === 'LIST':
      return get(field, 'value', []).map((item) =>
        get(item, 'value.entityReferenceValue.code')
      );
    case field.fieldType === CUSTOM_FIELD_ENTITY &&
      field.storageType === 'SINGLE':
      return get(field, 'entityReferenceValue.code', field.defaultValue);
    case field.fieldType === CUSTOM_FIELD_STRING:
      return field.stringValue || field.defaultValue;
    case field.fieldType === CUSTOM_FIELD_DOUBLE:
      return field.doubleValue || field.defaultValue;
    case field.fieldType === CUSTOM_FIELD_BOOLEAN:
      return field.booleanValue || field.defaultValue;
    case field.fieldType === CUSTOM_FIELD_LONG:
      return field.longValue || field.defaultValue;
    case field.fieldType === CUSTOM_FIELD_DATE:
      return field.dateValue || field.defaultValue;
    case field.fieldType === CUSTOM_FIELD_LIST:
      return field.stringValue || field.defaultValue;
    default:
      return field.defaultValue;
  }
};

/**
 * Return field type by its value type
 * @param {object} field
 *
 * @return {string}
 */
export const getFieldTypeByValueType = (field = {}) => {
  switch (true) {
    case has(field, 'stringValue'):
      return CUSTOM_FIELD_STRING;
    case has(field, 'doubleValue'):
      return CUSTOM_FIELD_DOUBLE;
    case has(field, 'booleanValue'):
      return CUSTOM_FIELD_BOOLEAN;
    case has(field, 'longValue'):
      return CUSTOM_FIELD_LONG;
    case has(field, 'dateValue'):
      return CUSTOM_FIELD_DATE;
    case has(field, 'mapValue'):
      return CUSTOM_FIELD_MULTI;
    default:
      return CUSTOM_FIELD_STRING;
  }
};

/**
 * Get all cfs initial values and transform it to the object *
 * @param {array} cfs
 *
 * @return {object}
 */

export const getCFsInitialValues = (cfs = []) =>
  isArray(cfs) &&
  cfs.reduce((acc, currentValue) => {
    const value = getValueByFieldType(currentValue);
    const updatedData = !!value ? { [currentValue.code]: value } : {};

    return {
      ...acc,
      customFields: {
        ...acc.customFields,
        ...updatedData
      }
    };
  }, {});

/**
 * Merge custom fields with its definitions
 * @param {array} cfs
 * @param {object} definitions
 *
 * @return {array}
 */

export const mergeCfsWithDefinitions = (cfs = [], definitions = {}) => {
  const cfsDefinitions = get(definitions, 'field', []);

  return _(cfs)
    .concat(cfsDefinitions)
    .groupBy('code')
    .map(_.spread(_.merge))
    .value();
};

export const maxValueToMaxLength = (value) => `maxLength:${value}`;

/**
 * Extract validate arr from custom field
 * @param {object} field
 * @param {boolean} isMandatory //value possible comes from subform parent
 *
 * @return {array}
 */
export const extractValidators = (field, isMandatory = false) => {
  const { maxValue, valueRequired, regExp } = field;
  const validationArr = [];
  if (!!maxValue) {
    validationArr.push(maxValueToMaxLength(maxValue));
  }

  if (isMandatory || valueRequired) {
    validationArr.push('required');
  }

  if (!!regExp) {
    validationArr.push(`regExp:${regExp}`);
  }

  return validationArr;
};

export const getKeyFields = (fieldsWithMetas = []) => {
  const keyFields = fieldsWithMetas
    .filter((field) => field.columnUse === 'USE_KEY')
    .map((field) => get(field, 'source'));

  return keyFields;
};

export const getValueFields = (fieldsWithMetas = []) => {
  const valueFields = fieldsWithMetas
    .filter((field) => field.columnUse === 'USE_VALUE')
    .map((field) => get(field, 'source'));

  return valueFields;
};

export const filterEmptyItems = (tabs, filteredFieldType = '') =>
  tabs.reduce((tabs, curTab) => {
    const groups = get(curTab, 'items', []);

    const filteredGroups = groups.reduce((groups, curGroup) => {
      const fields = get(curGroup, 'items', []);

      const filteredFields = fields.filter((field) => {
        const fieldType = get(field, 'fieldType');

        return fieldType !== filteredFieldType;
      });

      curGroup = isEmpty(filteredFields) ? omit(curGroup, 'items') : curGroup;

      return isEmpty(get(curGroup, 'items', []))
        ? groups
        : [
            ...groups,
            {
              ...curGroup
            }
          ];
    }, []);

    curTab = isEmpty(filteredGroups) ? omit(curTab, 'items') : curTab;

    return isEmpty(get(curTab, 'items', []))
      ? tabs
      : [
          ...tabs,
          {
            ...curTab
          }
        ];
  }, []);

/**
 * check if row record is already existed in CFMultiValue
 * @param {array} row
 * @param {array} value
 * @param {array} keyFields
 * @param {number} editRowIndex
 *
 * @return {boolean}
 */
export const isRowExist = (
  row = [],
  value = [],
  keyFields = [],
  editRowIndex
) =>
  isArray(value)
    ? value
        .filter((item, index) => index !== editRowIndex)
        .some((item) =>
          keyFields.every((keyField) => item[keyField] === row[keyField])
        )
    : false;

/**
 * check if value matches regexp pattern
 * @param {string | any} value
 * @param {RegExp} pattern
 *
 * @return {boolean}
 */
export const isValueMatchesPattern = (value, pattern) =>
  typeof value === 'string' && pattern.test(value);

/**
 * @param {string} key
 *
 * @return {func}
 */
const getFieldsFromMatrix = (key) => (matrixColumn = []) =>
  matrixColumn.filter(({ columnUse }) => columnUse === key);

export const getKeyFieldsFromMatrix = getFieldsFromMatrix('USE_KEY');

export const getValueFieldsFromMatrix = getFieldsFromMatrix('USE_VALUE');

export const findKeyField = (matrixColumn = []) => {
  matrixColumn.find(({ columnUse }) => columnUse === 'USE_KEY');
};

/**
 * set custom field value according to its fieldType
 * @param {object} cf
 * @param {any} value
 *
 * @return {object}
 */
export const setCFValue = (cf = {}, value) => {
  const { code, storageType } = cf;
  let { fieldType } = cf;
  if (isUndefined(fieldType)) {
    fieldType = getFieldTypeByValueType(cf);
  }

  switch (true) {
    case fieldType === CUSTOM_FIELD_STRING &&
      storageType === CUSTOM_STORAGE_MAP:
    case storageType === CUSTOM_STORAGE_MATRIX:
    case fieldType === CUSTOM_FIELD_MULTI:
      return { code, mapValue: value };
    case fieldType === CUSTOM_FIELD_STRING:
      return { code, stringValue: value };
    case fieldType === CUSTOM_FIELD_BOOLEAN:
      return { code, booleanValue: value };
    case fieldType === CUSTOM_FIELD_DATE:
      return { code, dateValue: value };
    case fieldType === CUSTOM_FIELD_DOUBLE:
      return { code, doubleValue: value };
    case fieldType === CUSTOM_FIELD_LONG:
      return { code, longValue: value };
    case fieldType === CUSTOM_FIELD_LIST:
      return { code, stringValue: value };
    case (fieldType === CUSTOM_FIELD_ENTITY &&
      storageType === CUSTOM_FIELD_SINGLE_VALUE) ||
      storageType === CUSTOM_FIELD_SINGLE:
      return { code, entityReferenceValue: value };
    case fieldType === CUSTOM_FIELD_ENTITY && storageType === CUSTOM_FIELD_LIST:
      return { code, value };

    default:
      return cf;
  }
};

/**
 * get cf definition by code
 * @param {object} cfDefinition
 * @param {string} cfCode
 *
 * @return {object}
 */
export const getCFDefinitionByCode = (cfDefinition = {}, cfCode) => {
  const { field = [] } = cfDefinition || {};

  return field.find(({ code }) => cfCode === code);
};

/**
 * serialize custom field matrix
 * @param {string} currentKey
 * @param {string} currentValue
 * @param {object} updatedValue
 * @param {object} currentCFDefinition
 * @param {boolean} isDelete
 *
 * @return {object}
 */
export const serializeCFMatrix = (
  currentKey,
  currentValue,
  updatedValue,
  currentCFDefinition,
  isDelete
) => {
  if (isEmpty(updatedValue)) {
    return isDelete ? {} : { [currentKey]: currentValue };
  }

  if (isEmpty(currentCFDefinition)) {
    return { [currentKey]: currentValue };
  }

  const { matrixColumn = [] } = currentCFDefinition;
  const keyFields = getKeyFieldsFromMatrix(matrixColumn);

  const valueFields = getValueFieldsFromMatrix(matrixColumn);

  const nextKeys = keyFields
    .map(({ code }) => {
      const val = updatedValue[code];

      return !val && typeof val === 'number' ? val : val || '';
    })
    .join('|');

  const nextValues = valueFields
    .map(({ code }) => {
      const val = updatedValue[code];

      return !val && typeof val === 'number' ? val : val || '';
    })
    .join('|');

  return { [nextKeys]: nextValues };
};

/**
 * serialize custom field matrix
 * @param {string} currentKey
 * @param {string} currentValue
 * @param {object} updatedValue
 * @param {object} currentCFDefinition
 * @param {boolean} isDelete
 *
 * @return {object}
 */
export const addCFMatrix = (data, currentCFDefinition) => {
  if (isEmpty(currentCFDefinition)) {
    return {};
  }

  const { matrixColumn = [] } = currentCFDefinition;
  const keyFields = getKeyFieldsFromMatrix(matrixColumn);

  const valueFields = getValueFieldsFromMatrix(matrixColumn);

  const key = keyFields.map(({ code }) => data[code]).join('|');
  const value = valueFields.map(({ code }) => data[code]).join('|');

  return {
    [key]: { value }
  };
};

export const addCFStringOrBoolean = (data, currentCFDefinition) => {
  if (isEmpty(currentCFDefinition)) {
    return {};
  }

  const {
    code,
    description,
    fieldType,
    languageDescriptions
  } = currentCFDefinition;
  const isBoolean = fieldType === CUSTOM_FIELD_BOOLEAN;
  const valueKey = getValueKey(fieldType);

  return {
    code,
    description,
    fieldType,
    languageDescriptions,
    [valueKey]: isBoolean ? !!data : !data ? '' : data
  };
};

const getValueKey = (fieldType) => {
  switch (fieldType) {
    case CUSTOM_FIELD_BOOLEAN:
      return 'booleanValue';
    case CUSTOM_FIELD_DATE:
      return 'dateValue';
    default:
      return 'stringValue';
  }
};

export const AddCFHandlers = {
  [CUSTOM_FIELD_MULTI]: addCFMatrix,
  [CUSTOM_FIELD_STRING]: addCFStringOrBoolean,
  [CUSTOM_FIELD_BOOLEAN]: addCFStringOrBoolean,
  [CUSTOM_FIELD_DATE]: addCFStringOrBoolean
};
