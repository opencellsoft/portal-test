import { get, has, set } from 'lodash';

const DEFAULT_TIMEOUT = 60000;

/**
 * Uses a shared object (this.cache) between instances (e.g: WithEntityCustomization)
 * to prevent firing multiple consecutive requests for the same entity
 * customization.
 *
 * @constructor {number} timeout
 */
class KeyCache {
  cache = {};

  timeout = null;

  constructor(timeout) {
    this.timeout = timeout || DEFAULT_TIMEOUT;
  }

  getPath = (namespace, key) => `${namespace}.["${key}"]`;

  /**
   * The cache is used the entity key (e.g: 'org.meveo.model.crm.Provider') and
   * is set to clear itself after one minute (ENTITY_CUSTOMIZATION_TIMEOUT).
   *
   * @param {string} key (e.g: 'org.meveo.model.crm.Provider')
   */
  setCache = ({ namespace, key, value }) => {
    const path = this.getPath(namespace, key);

    if (has(this.cache, path)) {
      const timeout = get(this.cache, `${path}.timeout`);

      if (timeout) {
        clearTimeout(timeout);
      }
    }

    set(this.cache, path, {
      value,
      timeout: setTimeout(() => {
        this.removeCache({ namespace, key });
      }, this.timeout)
    });
  };

  /**
   * Returns stored cache
   *
   * @param {string} key
   */
  getCache = ({ namespace, key }) =>
    get(this.cache, `${namespace}.["${key}"].value`);

  /**
   * Returns stored cache
   *
   * @param {string} key
   */
  hasCache = ({ namespace, key }) =>
    has(this.cache, `${namespace}.["${key}"].value`);

  /**
   * Removes timeout and key in shared object (this.cache)
   *
   * @param {string} key (e.g: 'org.meveo.model.crm.Provider')
   */
  removeCache = ({ namespace, key }) => {
    const path = this.getPath(namespace, key);
    const cache = get(this.cache, path);
    const { timeout } = cache || {};

    if (timeout) {
      clearTimeout(timeout);
    }

    delete this.cache[namespace][key];
  };

  /**
   * Removes timeout and key in shared object (this.cache)
   *
   * @param {string} namespace (e.g: 'org.meveo.model.crm.Provider')
   */
  removeNamespaceCache = ({ namespace }) => {
    const namespaceCache = get(this.cache, namespace);

    if (namespaceCache) {
      Object.keys(namespaceCache).forEach((key) =>
        this.removeCache({ namespace, key })
      );
    }

    delete this.cache[namespace];
  };
}

export default KeyCache;
