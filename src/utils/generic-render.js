import { get, has, flattenDeep, isEmpty, uniqBy, merge, assign } from 'lodash';
import moment from 'moment-timezone';
import flatten, { unflatten } from 'flat';
import store from '../store';

const reservedProperties = ['now', 'currentUser'];

export const getConfig = (data, page) =>
  get(data, `options.config.${page}`) || {};

export const getFilters = (config) => {
  const { fields, tabs, expand } = config || {};
  const { fields: expandFields, filter } = expand || {};
  const filters = [];
  const expandFilters = [];
  if (!isEmpty(expandFields) || filter) {
    expandFilters.push(filter ? expand : getFilters(expand));
  }

  if (Array.isArray(tabs)) {
    tabs.filter((tab) => filters.push(getFilters(tab)));
  }

  return expandFilters.concat(
    Array.isArray(fields)
      ? fields.filter(({ filter, type }) => filter)
      : Array.isArray(tabs)
      ? uniqBy(flattenDeep(filters), 'source')
      : []
  );
};

export const processParams = ({ target = {}, source = {}, params = {} }) => {
  if (isEmpty(params)) {
    return { ...target };
  }

  const flatParams = flatten(params);

  const processed = Object.keys(flatParams).reduce(
    (res, key) => {
      const rawValue = flatParams[key];

      const match =
        typeof rawValue === 'string' && rawValue.match(/@(.*?)(\.|$)(.*)/);
      const dynamicContainer = match && match[1];

      const propertyAliasMatch =
        // eslint-disable-next-line no-useless-escape
        typeof rawValue === 'string' && rawValue.match(/(.*?)\:(.*)/);
      const propertyAlias = propertyAliasMatch ? propertyAliasMatch[1] : key;
      const value = propertyAliasMatch ? propertyAliasMatch[2] : rawValue;

      switch (true) {
        case typeof value !== 'string':
          return { ...res, [propertyAlias]: value };
        case value === '@now':
          return { ...res, [propertyAlias]: moment().valueOf() };
        case value === '@currentUser':
          const { getState } = store;
          const { user = {} } = getState();

          return { ...res, [propertyAlias]: get(user, 'preferred_username') };
        case dynamicContainer &&
          !reservedProperties.includes(dynamicContainer) &&
          has(source, dynamicContainer):
          const dataContainer = get(source, dynamicContainer);
          const placeholder = isEmpty(match[2]) ? '' : `@${dynamicContainer}.`;

          const nextData = unflatten({
            [propertyAlias]: Array.isArray(dataContainer)
              ? dataContainer.map((element) =>
                  replaceToken(value, element, placeholder)
                )
              : replaceToken(value, dataContainer, placeholder)
          });

          const keyAlias = propertyAlias.split('.')[0];
          let latestData = get(res, keyAlias);
          latestData = latestData && latestData[0];
          const result =
            latestData &&
            Object.values(nextData).reduce((res, key) => key[0], {});

          const data = result
            ? { [keyAlias]: [assign(result, latestData)] }
            : nextData;

          return merge(res, data);
        default:
          return { ...res, [propertyAlias]: value };
      }
    },
    { ...target }
  );

  return unflatten(processed);
};

export const replaceToken = (value, object, token) => {
  const field = value.replace(token, '').replace('[]', '');

  if (field === value) {
    return object;
  }

  if (!has(object, field)) {
    throw new Error(`${field} was not found.`);
  }

  if (!object.hasOwnProperty(value.replace('[]', '').split('.')[1])) {
    return value;
  }

  return value.includes('[]') ? [get(object, field)] : get(object, field);
};

export const hydrateUrlFromParams = ({ url, params = {} }) => {
  if (isEmpty(params)) {
    return url;
  }

  return url
    .split('/')
    .map((string) => hydrateStringFromParams({ string, params }))
    .join('/');
};

export const hydrateStringFromParams = ({ string, params = {} }) => {
  const match = string.match(/@(.*?)(\.|$)(.*)/);
  const dynamicContainer = match && match[1];
  const property = match && match[3];

  if (!match || isEmpty(get(params, dynamicContainer))) {
    return string;
  }

  const path = property ? `${dynamicContainer}.${property}` : dynamicContainer;
  const value = get(params, path) || '';

  if (!has(params, path)) {
    return '';
  }

  return string.replace(`:@${path}`, value).replace(`@${path}`, value);
};

/**
 * Transform request data by transformSource(comming from action config)
 * @param {string} transformSource
 * @param {object} requestData
 * @return {requestData}
 */
export const transformBySource = (transformSource, requestData = {}) => {
  const [source, toType] =
    (!!transformSource && transformSource.split('.')) || '';

  return (
    (!!transformSource && {
      ...requestData,
      [source]:
        toType === 'string'
          ? `${get(requestData, source)}`
          : get(requestData, source)
    }) ||
    requestData
  );
};
