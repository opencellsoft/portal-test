import parseJWT from 'jwt-decode';
import { delay } from 'lodash';

import dynamicAppProperties from '../dynamicAppProperties';

import { encodeParams } from './api';

const { REACT_APP_DOMAIN_URL, REACT_APP_AUTH_URL } = dynamicAppProperties || {};

const {
  REACT_APP_CLIENT_ID,
  REACT_APP_CLIENT_SECRET,
  REACT_APP_TOKEN_REFRESH_RATE = 60000
} = process.env;

const TOKEN_STORAGE_KEY = '_tok';
const REFRESH_TOKEN_STORAGE_KEY = '_refresh_tok';

let refreshTokenIntervalID = 0;

export const setToken = (token) =>
  token && localStorage.setItem(TOKEN_STORAGE_KEY, token);

export const setRefreshToken = (refreshToken) =>
  refreshToken && localStorage.setItem(REFRESH_TOKEN_STORAGE_KEY, refreshToken);

export const getToken = () =>
  localStorage && localStorage.getItem(TOKEN_STORAGE_KEY);

export const getRefreshToken = () =>
  localStorage && localStorage.getItem(REFRESH_TOKEN_STORAGE_KEY);

export const getTokenData = () => {
  const token = getToken();

  if (!token) {
    return null;
  }

  return parseJWT(token);
};

export const unsetTokens = () => {
  localStorage.removeItem(TOKEN_STORAGE_KEY);
  localStorage.removeItem(REFRESH_TOKEN_STORAGE_KEY);

  return Promise.resolve();
};

const getFinalUrl = () =>
  REACT_APP_AUTH_URL.includes('http')
    ? REACT_APP_AUTH_URL
    : `${REACT_APP_DOMAIN_URL}${REACT_APP_AUTH_URL}`;

const refreshUserToken = () => {
  const refreshToken = getRefreshToken();
  const finalUrl = getFinalUrl();

  return fetch(finalUrl, {
    method: 'POST',
    mode: 'cors',
    body: encodeParams({
      client_id: REACT_APP_CLIENT_ID,
      client_secret: REACT_APP_CLIENT_SECRET,
      grant_type: 'refresh_token',
      refresh_token: refreshToken
    }),
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
    .then((response) => {
      if (response.status < 200 || response.status >= 300) {
        throw new Error(response.statusText);
      }

      return response.json();
    })
    .then((user) => {
      const { access_token: token, refresh_token: refreshToken } = user;
      if (!token) {
        throw new Error('auth.token_not_received');
      }
      setToken(token);
      setRefreshToken(refreshToken);
      refreshTokenIntervalID = delay(
        refreshUserToken,
        REACT_APP_TOKEN_REFRESH_RATE
      );
    })
    .catch((error) => {
      throw new Error('input.action.error');
    });
};

export const signIn = (username, password) => {
  const finalUrl = getFinalUrl();

  return fetch(finalUrl, {
    method: 'POST',
    mode: 'cors',
    body: encodeParams({
      client_id: REACT_APP_CLIENT_ID,
      client_secret: REACT_APP_CLIENT_SECRET,
      grant_type: 'password',
      username,
      password
    }),
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
    .then((response) => {
      if (response.status < 200 || response.status >= 300) {
        throw new Error(response.statusText);
      }

      return response.json();
    })
    .then((user) => {
      const { access_token: token, refresh_token: refreshToken } = user;
      if (!token) {
        throw new Error('auth.token_not_received');
      }
      setToken(token);
      setRefreshToken(refreshToken);
      delay(refreshUserToken, REACT_APP_TOKEN_REFRESH_RATE);

      return user;
    })
    .catch((error) => {
      throw new Error('ra.auth.sign_in_error');
    });
};

export const signOut = () => {
  unsetTokens();
  clearTimeout(refreshTokenIntervalID);
};
