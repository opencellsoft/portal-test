import { processParams } from '../generic-render';

const record = {
  id: 123,
  name: 'Sam',
  age: 22
};

const form = {
  id: 123,
  hobby: 'Photography'
};

const selectedItems = [
  {
    country: 'France',
    city: 'Paris'
  },
  {
    country: 'United Kingdom',
    city: 'London'
  }
];

describe('Generic Render', () => {
  describe('processParams', () => {
    describe('when processing params with @record', () => {
      const params = {
        code: 'abc',
        age: '@record.age',
        name: {
          firstname: '@record.name'
        },
        identifiers: [
          {
            id: '@record.id'
          }
        ]
      };

      it('should replace tokens correctly', () => {
        const data = processParams({
          target: record,
          source: { record },
          params
        });
        expect(data).toHaveProperty('code', 'abc');
        expect(data).toHaveProperty('age', 22);
      });

      it('should handle arrays', () => {
        const data = processParams({
          target: record,
          source: { record },
          params
        });
        const { identifiers } = data;
        expect(identifiers[0]).toHaveProperty('id', 123);
      });

      it('should handle nested objects', () => {
        const data = processParams({
          target: record,
          source: { record },
          params
        });
        const { name } = data;
        expect(name).toHaveProperty('firstname', 'Sam');
      });
    });

    describe('when processing params with @form', () => {
      const params = {
        code: 'abc',
        age: '@record.age',
        name: {
          firstname: '@record.name'
        },
        identifiers: [
          {
            id: '@record.id'
          }
        ],
        hobbies: [
          {
            name: '@form.hobby'
          }
        ]
      };

      it('should replace tokens correctly', () => {
        const data = processParams({
          record,
          source: { record, form },
          params
        });

        expect(data).toHaveProperty('code', 'abc');
        expect(data).toHaveProperty('age', 22);
      });

      it('should handle arrays', () => {
        const data = processParams({
          record,
          source: { record, form },
          params
        });
        const { identifiers, hobbies } = data;

        expect(identifiers[0]).toHaveProperty('id', 123);
        expect(hobbies[0]).toHaveProperty('name', 'Photography');
      });

      it('should handle nested objects', () => {
        const data = processParams({
          record,
          source: { record, form },
          params
        });

        const { name } = data;
        expect(name).toHaveProperty('firstname', 'Sam');
      });
    });

    describe('when processing params with @selectedItems', () => {
      const params = {
        code: 'abc',
        age: '@record.age',
        city: '@selectedItem.city',
        name: {
          firstname: '@record.name'
        },
        identifiers: [
          {
            id: '@record.id'
          }
        ],
        hobbies: [
          {
            name: '@form.hobby'
          }
        ],
        locations: '@selectedItems'
      };

      it('should replace tokens correctly', () => {
        const data = processParams({
          record,
          source: {
            record,
            form,
            selectedItem: selectedItems[0]
          },
          params
        });

        expect(data).toHaveProperty('code', 'abc');
        expect(data).toHaveProperty('age', 22);
        expect(data).toHaveProperty('city', 'Paris');
      });

      it('should handle arrays', () => {
        const data = processParams({
          record,
          source: { record, form, selectedItems },
          params
        });

        const { identifiers, hobbies } = data;

        expect(identifiers[0]).toHaveProperty('id', 123);
        expect(hobbies[0]).toHaveProperty('name', 'Photography');
      });

      it('should handle nested objects', () => {
        const data = processParams({
          record,
          source: { record, form, selectedItems },
          params
        });

        const { name } = data;
        expect(name).toHaveProperty('firstname', 'Sam');
      });

      it('should replace all the selectedItems array', () => {
        const data = processParams({
          record,
          source: { form, selectedItems },
          params
        });
        const { locations } = data;

        expect(locations.length).toEqual(2);
        expect(locations[0]).toHaveProperty('country', 'France');
        expect(locations[0]).toHaveProperty('city', 'Paris');

        expect(locations[1]).toHaveProperty('country', 'United Kingdom');
        expect(locations[1]).toHaveProperty('city', 'London');
      });
    });
  });
});
