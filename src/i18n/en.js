import englishMessages from 'ra-language-english';

export default {
  ...englishMessages,
  index: 'Home',
  search: 'Search',
  configuration: 'Configuration',
  language: 'Language',
  setup: 'Setup',
  userMenu: 'Profile',
  mediation: 'Mediation',
  CDRManagement: 'CDR Management',
  theme: {
    name: 'Theme',
    light: 'Light',
    dark: 'Dark'
  },
  currentDate: 'Now',
  weekDate: 'By week',
  monthDate: 'By month',
  dashboard: 'Dashboard',
  dashboardMessage: 'Welcome to the Opencell Portal',
  export: {
    empty: 'No elements to export'
  },
  input: {
    file: {
      upload_file: 'Upload file',
      upload: 'Upload',
      import: 'Import',
      create_version: 'Create version',
      import_version: 'Import version',
      update_version: 'Update version',
      export_version: 'Export version',
      choose_file: 'Choose file',
      file_error: 'Invalid file'
    },
    date: {
      before_now_error: 'Date should be equal or greater to now',
      before_start_date_error:
        'Must be equal or more than start date (valid from date)',
      after_end_date_error: 'Must be less than end date (valid to date)'
    },
    action: {
      success: 'Action executed successfuly',
      error: 'An error occured while executing the action'
    }
  },
  confirm: '%{label}',
  confirm_content: 'Are you sure you want to %{label}?',
  message: {
    bulk_delete_content:
      'Are you sure you want to delete this element? |||| Are you sure you want to delete these %{smart_count} elements?',
    bulk_delete_title: 'Confirm deletion',
    no_result_found: 'No result found'
  },
  status: {
    AVAILABLE: 'Available',
    CREATED: 'Instantiated',
    ACTIVE: 'Activated',
    RESILIATED: 'Terminated',
    TERMINATED: 'Terminated',
    VALIDATED: 'Validated',
    NEW: 'New',

    INACTIVE: 'Inactive',
    COMPLETED: 'Completed',
    LAUNCHED: 'Launched',
    RENEWAL: 'Renewal',
    VALID: 'Valid',
    WELL_FORMED: 'Well formed',

    DRAFT: 'Draft',
    PENDING: 'Pending',
    SUSPENDED: 'Suspended',
    CLOSED: 'Closed',
    CANCELED: 'Cancelled',
    RETIRED: 'Retired',
    OBSOLETE: 'Obsolete',

    ERROR: 'Error',
    REJECTED: 'Rejected',
    BAD_FORMED: 'Bad formed',

    IN_STUDY: 'In study',
    IN_DESIGN: 'In design',
    IN_TEST: 'In test'
  },
  action: {
    reset: 'Reset',
    instantiate: 'Instantiate'
  },
  validation: {
    unique: 'Must be unique',
    pattern: 'Value does not match regular expression "%{pattern}"',
    maxLength: 'Should be less than %{maxLength} chars',
    phone: 'Value is not a valid phone number'
  },
  auth: {
    token_not_received: 'No access token received'
  }
};
