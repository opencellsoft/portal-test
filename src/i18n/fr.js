import frenchMessages from 'ra-language-french';

export default {
  ...frenchMessages,
  index: 'Accueil',
  search: 'Rechercher',
  configuration: 'Configuration',
  language: 'Langue',
  setup: 'Configuration',
  userMenu: 'Profil',
  theme: {
    name: 'Thème',
    light: 'Clair',
    dark: 'Sombre'
  },
  currentDate: 'Date du jour',
  weekDate: 'Par semaine',
  monthDate: 'Par mois',
  dashboard: 'Tableau de bord',
  dashboardMessage: 'Bienvenue sur le portail Opencell',

  confirm: '%{label}',
  message: {
    bulk_delete_content:
      'Voulez-vous vraiment supprimer cet élément? |||| Voulez-vous vraiment supprimer ces éléments% {smart_count}?',
    bulk_delete_title: 'Confirmer la suppression',
    no_result_found: 'Aucun résultat trouvé'
  },
  confirm_content: 'Êtes-vous sûr de vouloir %{label}?',
  navigation: {
    rows_per_page: 'Lignes par page',
    previous_page: 'Page suivante',
    next_page: 'Page suivante'
  },
  export: {
    empty: "Pas d'éléments à exporter"
  },
  input: {
    file: {
      upload_file: 'Télécharger un fichier ',
      upload: 'Télécharger',
      import: 'Importer',
      choose_file: 'Choisir fichier',
      file_error: 'Fichier invalide'
    },
    date: {
      before_now_error: 'La date devrait être égale ou supérieure à maintenant',
      before_start_date_error:
        'La date devrait être égale ou supérieure à la date de début'
    },
    action: {
      success: 'Action exécutée avec succès'
    }
  },
  button: {
    reset: 'Réinitialiser',
    instantiate: 'Instancier',
    add: 'Ajouter'
  },
  status: {
    AVAILABLE: 'Disponible',
    CREATED: 'Instancié',
    ACTIVE: 'Actif',
    RESILIATED: 'Terminé',
    TERMINATED: 'Terminé',
    VALIDATED: 'Validé',
    NEW: 'Nouveau',

    OPEN: 'Ouvert',
    BILLED: 'Facturé',
    RERATED: 'Revalorisé',

    INACTIVE: 'Inactif',
    COMPLETED: 'Terminé',
    LAUNCHED: 'Lancée',
    RENEWAL: 'Renouvellement',
    VALID: 'Valide',
    WELL_FORMED: 'Correctement formatté',

    DRAFT: 'Brouillon',
    PENDING: 'En attente',
    SUSPENDED: 'Suspendu',
    CLOSED: 'Fermé',
    CANCELED: 'Annulé',
    RETIRED: 'Retiré',
    OBSOLETE: 'Obsolète',

    O: 'Ouvert',
    L: 'Assorti',
    P: 'Partiellement lettré',
    C: 'Fermé',
    I: 'Litige',
    R: 'Rejeté',

    ERROR: 'Erreur',
    REJECTED: 'Rejeté',
    BAD_FORMED: 'Mal formatté',

    IN_STUDY: 'En étude',
    IN_DESIGN: 'En conception',
    IN_TEST: 'En test'
  },
  validation: {
    unique: 'Doit être unique',
    pattern:
      'La valeur ne correspond pas à l\'expression régulière "%{pattern}"',
    maxLength: 'Doit être moins de %{maxLength} caractères',
    phone: "La valeur n'est un numéro de téléphone valide"
  },
  auth: {
    token_not_received: "Aucun token d'accès n'a été reçu"
  }
};
