export default {
  // Position
  center: {
    margin: '0 auto'
  },

  // Text
  ellipsis: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  noSelect: {
    userSelect: 'none'
  }
};
