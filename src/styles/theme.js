import { createMuiTheme } from '@material-ui/core/styles';

import defaultTheme from './defaultTheme';

export const darkTheme = createMuiTheme({
  ...defaultTheme,
  palette: {
    ...defaultTheme.palette,
    type: 'dark' // Switching the dark mode on is a single property value change.
  }
});

export const lightTheme = createMuiTheme({
  ...defaultTheme,
  palette: {
    ...defaultTheme.palette,
    background: {
      ...defaultTheme.background,
      menu: '#333c44',
      default: '#f2f2f2'
    }
  }
});
