import 'react-app-polyfill/stable';
import 'react-app-polyfill/ie11';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import JssProvider from 'react-jss/lib/JssProvider';
import moment from 'moment-timezone';

import store, { history } from './store';
import customProviders from './providers';
import * as serviceWorker from './serviceWorker';
import generateClassName from './jss';

import App from './components/App';

import './styles/index.css';

const {
  REACT_APP_TIMEZONE = 'Europe/Paris',
  REACT_APP_LOCALE = 'en'
} = process.env;
moment.tz.setDefault(REACT_APP_TIMEZONE);

ReactDOM.render(
  <Provider store={store}>
    <JssProvider generateClassName={generateClassName}>
      <App
        title="Opencell Portal"
        locale={REACT_APP_LOCALE}
        history={history}
        {...customProviders}
      />
    </JssProvider>
  </Provider>,
  document.getElementById('root')
);

serviceWorker.unregister();
