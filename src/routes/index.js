import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import get from 'lodash/get';

import Configuration from '../components/Configuration';

export default (routes = {}) => {
  const homePage = get(routes, 'home', 'dashboard');

  return [
    <Route path="/" exact>
      <Redirect to={`/${homePage}`} />
    </Route>,
    <Route path="/configuration" exact component={Configuration} />
  ];
};
