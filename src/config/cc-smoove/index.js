import menu from './menu';
import userMenu from './user-menu';

import modules from './modules';

export default {
  title: 'Customer Care',
  themeColor: '#75b818',
  modules,
  menu,
  'user-menu': userMenu,
  options: {
    routes: {
      home: 'customer-accounts'
    },
    isFirstLevelMenu: true
  }
};
