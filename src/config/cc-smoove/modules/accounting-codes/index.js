import provider from './provider.json';

export default {
  resource: 'accounting-codes',
  label: 'Accounting codes',
  icon: 'SimCard',
  provider
};
