import list from './list.json';
import show from './show.json';
import form from './form.json';
import en from './i18n/en.json';
import fr from './i18n/fr.json';
import createExtras from './create.json';
import provider from './provider.json';

const create = {
  ...form,
  ...createExtras
};

export default {
  resource: 'user-accounts',
  label: `Compte Utilisateurs`,
  icon: 'Person',
  pages: {
    list,
    show,
    create,
    edit: form
  },

  provider,
  i18n: {
    en,
    fr
  }
};
