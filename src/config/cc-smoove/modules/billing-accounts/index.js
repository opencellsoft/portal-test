import show from './show.json';
import form from './form.json';

import en from './i18n/en.json';
import fr from './i18n/fr.json';

import edit from './edit.json';

import provider from './provider.json';

export default {
  resource: 'billing-accounts',
  themeColor: '#00c8c8',
  label: 'Compte de facturation',
  icon: 'Person',
  pages: {
    show,
    create: form,
    edit: {
      ...form,
      ...edit
    }
  },
  provider,
  i18n: {
    en,
    fr
  }
};
