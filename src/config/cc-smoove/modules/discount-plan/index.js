import list from './list.json';

import en from './i18n/en.json';
import fr from './i18n/fr.json';
import provider from './provider.json';

export default {
  resource: 'discount-plan',
  label: `Discount plans`,
  // inMenu: {
  //   label: 'Setup',
  //   icon: 'Build/AttachMoney',
  //   path: 'setup'
  // },
  themeColor: '#CD853F',
  pages: {
    list
  },
  provider,
  i18n: {
    en,
    fr
  }
};
