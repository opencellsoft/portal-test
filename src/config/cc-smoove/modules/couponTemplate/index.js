import list from './list.json';
import edit from './edit.json';
import form from './form.json';
import show from './show.json';

import en from './i18n/en.json';
import fr from './i18n/fr.json';

import provider from './provider.json';
import schema from './schema.json';

export default {
  resource: 'couponTemplate',
  label: 'Promotions',
  icon: 'Grade',
  themeColor: '#2196f3',
  inMenu: true,
  menuPriority: 4,
  entity: {
    name: 'CouponTemplate',
    isCustom: true
  },
  pages: {
    list,
    create: form,
    edit: {
      ...form,
      ...edit
    },
    show
  },
  provider,
  schema,
  i18n: {
    en,
    fr
  }
};
