import list from './list.json';

import form from './form.json';
import edit from './edit.json';

import en from './i18n/en.json';
import fr from './i18n/fr.json';

import provider from './provider.json';

export default {
  resource: 'subscriptions',
  label: 'Souscriptions',
  icon: 'Subscriptions',
  themeColor: '#7c4dff',
  //inMenu: true,
  entity: 'org.meveo.model.billing.Subscription',
  menuPriority: 2,
  pages: {
    list,
    create: form,
    edit: { ...form, ...edit }
  },
  provider,
  i18n: {
    en,
    fr
  }
};
