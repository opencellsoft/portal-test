import en from './i18n/en.json';
import fr from './i18n/fr.json';

import provider from './provider.json';

export default {
  resource: 'CF_COUPON_REFERENCE',
  label: 'Coupons',
  icon: 'AttachMoney',
  themeColor: '#00bfa5',
  inMenu: false,
  provider,
  i18n: {
    en,
    fr
  }
};
