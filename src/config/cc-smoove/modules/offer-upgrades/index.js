import provider from './provider.json';

export default {
  resource: 'offer-upgrades',
  label: 'Offer upgrades',
  provider
};
