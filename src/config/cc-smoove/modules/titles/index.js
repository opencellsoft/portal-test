import list from './list.json';
import show from './show.json';
import form from './form.json';
import en from './i18n/en.json';
import fr from './i18n/fr.json';
import provider from './provider.json';

export default {
  resource: 'titles',
  label: `Titles and civilities`,
  // inMenu: {
  //   icon: 'Build',
  //   label: 'Setup',
  //   path: 'setup'
  // },
  themeColor: '#999999',
  pages: {
    list,
    show
  },
  drawer: {
    create: form,
    edit: form
  },
  provider,
  i18n: {
    en,
    fr
  }
};
