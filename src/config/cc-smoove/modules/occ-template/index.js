import provider from './provider.json';
import list from './list.json';
import fr from './i18n/fr.json';

export default {
  resource: 'occ-template',
  themeColor: '#00c853',
  icon: 'Person',
  label: 'OCC template',
  pages: {
    list
  },
  provider,
  i18n: {
    fr
  }
};
