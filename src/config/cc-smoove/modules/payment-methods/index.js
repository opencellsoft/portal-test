import fr from './i18n/fr.json';
import provider from './provider.json';

export default {
  resource: 'payment-methods',
  label: `Méthodes de paiement`,
  themeColor: '#CD853F',
  provider,
  i18n: {
    fr
  }
};
