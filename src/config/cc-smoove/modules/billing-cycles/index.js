import list from './list.json';

import en from './i18n/en.json';
import fr from './i18n/fr.json';

import provider from './provider.json';

export default {
  resource: 'billing-cycles',
  label: `Billing cycle`,
  icon: 'AttachMoney',
  themeColor: '#999999',
  // inMenu: {
  //   label: 'Setup/Billing',
  //   icon: 'Build/AttachMoney',
  //   path: 'setup/billing'
  // },
  pages: {
    list
  },
  provider,
  i18n: {
    en,
    fr
  }
};
