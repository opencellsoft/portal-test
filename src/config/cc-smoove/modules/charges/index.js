import list from './list.json';
import form from './form.json';

import en from './i18n/en.json';
import fr from './i18n/fr.json';

import provider from './provider.json';

export default {
  resource: 'charges',
  label: 'Charges',
  icon: 'Subscriptions',
  themeColor: '#7c4dff',
  entity: 'org.meveo.model.billing.Subscription',
  menuPriority: 2,
  pages: {
    list,
    create: form,
    edit: form
  },
  provider,
  i18n: {
    en,
    fr
  }
};
