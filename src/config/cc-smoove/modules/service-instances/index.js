import provider from './provider.json';

export default {
  resource: 'service-instances',
  label: 'Service instances',
  entity: 'org.meveo.model.billing.ServiceInstance',
  provider
};
