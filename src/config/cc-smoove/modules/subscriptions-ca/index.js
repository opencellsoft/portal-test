import provider from './provider.json';

export default {
  resource: 'subscriptions-ca',
  label: 'Souscriptions',
  icon: 'Subscriptions',
  themeColor: '#7c4dff',
  inMenu: false,
  entity: 'org.meveo.model.billing.Subscription',
  menuPriority: 2,
  provider
};
