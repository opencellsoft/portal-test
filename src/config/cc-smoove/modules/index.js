import accountOperations from './account-operations';
import accessPoints from './access-points';
import charges from './charges';
import creditCategories from './credit-categories';
import countries from './countries';
import customers from './customers';
import customerAccounts from './customer-accounts';
import currencies from './currencies';
import languages from './languages';
import subscriptions from './subscriptions';
import subscriptionsCA from './subscriptions-ca';
import invoices from './invoices';
import CF_COUPON_REFERENCE from './CF_COUPON_REFERENCE';
import couponTemplate from './couponTemplate';
import entityCustomization from './entity-customization';
import titles from './titles';
import userAccounts from './user-accounts';
import userAccountsSub from './user-accounts-sub';
import paymentMethods from './payment-methods';
import offers from './offers';
import offerUpgrades from './offer-upgrades';
import terminationReasons from './termination-reasons';
import discountPlan from './discount-plan';
import billingCycles from './billing-cycles';
import billingAccounts from './billing-accounts';
import serviceInstances from './service-instances';
import occTemplate from './occ-template';
import customerCategories from './customer-categories';
import sellers from './sellers';
import oneShotChargeTemplates from './one-shot-charge-templates';
import accountingCodes from './accounting-codes';
import justificatifs from './justificatifs';
import walletOperations from './wallet-operations';

export default {
  'customer-accounts': customerAccounts,
  customers,
  'access-points': accessPoints,
  'accounting-codes': accountingCodes,
  'account-operations': accountOperations,
  'billing-cycles': billingCycles,
  'credit-categories': creditCategories,
  'customer-categories': customerCategories,
  'discount-plan': discountPlan,
  charges,
  countries,
  currencies,
  justificatifs,
  languages,
  subscriptions,
  'subscriptions-ca': subscriptionsCA,
  invoices,
  offers,
  'offer-upgrades': offerUpgrades,
  'occ-template': occTemplate,
  'one-shot-charge-templates': oneShotChargeTemplates,
  CF_COUPON_REFERENCE,
  couponTemplate,
  sellers,
  'termination-reasons': terminationReasons,
  titles,
  'user-accounts': userAccounts,
  'user-accounts-sub': userAccountsSub,
  'payment-methods': paymentMethods,
  'entity-customization': entityCustomization,
  'billing-accounts': billingAccounts,
  'service-instances': serviceInstances,
  'wallet-operations': walletOperations
};
