import list from './list.json';
import form from './form.json';
import show from './show.json';
import create from './create.json';

import fr from './i18n/fr.json';

import provider from './provider.json';

export default {
  resource: 'customers',
  themeColor: '#00c853',
  icon: 'Person',
  label: 'Clients',
  inMenu: false,
  menuPriority: 1,
  pages: {
    list,
    create,
    edit: form,
    show
  },
  entity: 'org.meveo.model.crm.Customer',
  provider,
  i18n: {
    fr
  }
};
