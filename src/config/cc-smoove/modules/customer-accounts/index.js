import list from './list.json';
import form from './form.json';
import edit from './edit.json';

import fr from './i18n/fr.json';

import provider from './provider.json';

export default {
  resource: 'customer-accounts',
  themeColor: '#00c853',
  icon: 'Person',
  label: 'Clients',
  inMenu: true,
  menuPriority: 1,
  entity: 'org.meveo.model.payments.CustomerAccount',
  pages: {
    list,
    create: form,
    edit: { ...form, ...edit }
  },
  provider,
  i18n: {
    fr
  }
};
