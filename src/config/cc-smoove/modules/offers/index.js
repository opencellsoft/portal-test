import list from './list.json';
import show from './show.json';
import form from './form.json';
import en from './i18n/en.json';
import fr from './i18n/fr.json';

import provider from './provider.json';

export default {
  resource: 'offers',
  label: 'Offer templates',
  icon: 'ChromeReaderMode',
  themeColor: '#2196f3',
  pages: {
    list,
    show,
    edit: form,
    create: form
  },
  provider,
  i18n: {
    en,
    fr
  }
};
