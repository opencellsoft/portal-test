import en from './i18n/en.json';
import provider from './provider.json';

export default {
  resource: 'entity-customization',
  label: `Entity customization`,
  provider,
  i18n: {
    en
  }
};
