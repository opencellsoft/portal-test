import provider from './provider.json';

export default {
  resource: 'user-accounts-sub',
  label: `Compte Utilisateurs`,
  icon: 'Person',
  provider
};
