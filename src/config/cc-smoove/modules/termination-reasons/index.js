import list from './list.json';
import en from './i18n/en.json';
import fr from './i18n/fr.json';
import provider from './provider.json';

export default {
  resource: 'termination-reasons',
  label: `Termination reasons`,
  icon: 'Beenhere',

  themeColor: '#999999',
  pages: {
    list
  },

  provider,
  i18n: {
    en,
    fr
  }
};
