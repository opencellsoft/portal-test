import provider from './provider.json';
import list from './list.json';

export default {
  resource: 'access-points',
  themeColor: '#00c853',
  icon: 'Person',
  label: 'Access Points',
  inMenu: false,
  menuPriority: 1,
  pages: {
    list
  },
  provider
};
