import list from './list.json';
import form from './form.json';

import en from './i18n/en.json';
import fr from './i18n/fr.json';

import provider from './provider.json';

export default {
  resource: 'invoices',
  label: '',
  icon: 'FolderShared',
  themeColor: '#eb2f2f',
  //inMenu: true,
  menuPriority: 3,
  pages: {
    list,
    create: form,
    edit: form
  },
  provider,
  i18n: {
    en,
    fr
  }
};
