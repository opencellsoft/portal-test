import provider from './provider.json';
import list from './list.json';

export default {
  resource: 'one-shot-charge-templates',
  themeColor: '#00c853',
  icon: 'Person',

  pages: {
    list
  },
  provider
};
