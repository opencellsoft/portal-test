import provider from './provider.json';
import list from './list.json';
import form from './form.json';
import show from './show.json';
import edit from './edit.json';
import create from './create.json';

import fr from './i18n/fr.json';

export default {
  resource: 'account-operations',
  themeColor: '#00c853',
  icon: 'Person',
  label: 'Derniers paiements',
  // inMenu: true,
  pages: {
    list,
    create: { ...form, ...create },
    edit,
    show
  },
  provider,
  i18n: {
    fr
  }
};
