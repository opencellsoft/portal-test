import list from './list.json';

import en from './i18n/en.json';
import fr from './i18n/fr.json';

import provider from './provider.json';

export default {
  resource: 'justificatifs',
  label: 'Justificatifs',
  icon: 'Layers',
  inMenu: true,
  menuPriority: 3,
  themeColor: '#7c4dff',
  pages: {
    list
  },
  provider,
  i18n: {
    en,
    fr
  }
};
