import { teal } from '@material-ui/core/colors';
import menu from './menu';
import modules from './modules';

export default {
  menu,
  modules,
  title: 'Customer Care',
  themeColor: teal.A700
};
