export default {
  dashboard: {
    label: `dashboard`,
    icon: 'Dashboard',
    path: '/',
    extraProps: {
      exact: true
    }
  }
};
