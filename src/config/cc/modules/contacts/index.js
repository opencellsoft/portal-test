import list from './list.json';
import show from './show.json';
import form from './form.json';

import provider from './provider.json';

export default {
  resource: 'contacts',
  label: `Contacts`,
  icon: 'Contacts',
  inMenu: true,
  pages: {
    list,
    show,
    create: form,
    edit: form
  },
  provider
};
