import merge from 'lodash/merge';
import modules from './modules';

const filteredModules = Object.keys(modules).reduce(
  (res, key) => ({
    ...res,
    ...(typeof modules[key].inMenu === 'object' && modules[key].inMenu.path
      ? { [key]: modules[key] }
      : {})
  }),
  {}
);

const createSubItem = (config, label, icon, subItems, menuPriority) => {
  const obj = {
    [config]: {
      label: label || `resources.${config}.name`,
      icon,
      subItems,
      menuPriority
    }
  };

  return obj;
};

const filterModuleByKey = Object.keys(filteredModules).reduce((res, key) => {
  const { inMenu, menuPriority, label, icon } = filteredModules[key];
  const { label: inMenuLabel, icon: inMenuIcon, path } = inMenu || {};

  const inMenuLabelArr = inMenuLabel.split('/').reverse();
  const inMenuIconArr = !!inMenuIcon && inMenuIcon.split('/').reverse();

  const moduleTree = path
    .split('/')
    .reverse()
    .reduce(
      (tree, pathEl, index) =>
        createSubItem(
          pathEl,
          inMenuLabelArr[index],
          inMenuIconArr[index],
          tree,
          menuPriority
        ),
      createSubItem(key, label, icon, null, menuPriority)
    );

  return merge(res, moduleTree);
}, {});

export default filterModuleByKey;
