import menu from './menu';
import modules from './modules';

export default {
  menu,
  modules,
  title: 'MVP',
  themeColor: '#f44336'
};
