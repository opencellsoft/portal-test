import list from './list.json';
import show from './show.json';
import en from './i18n/en.json';
import fr from './i18n/fr.json';
import edit from './edit.json';
import create from './create.json';

import provider from './provider.json';
import schema from './schema.json';

export default {
  resource: 'offers',
  label: 'Offers',
  icon: 'ChromeReaderMode',
  inMenu: true,
  entity: 'org.meveo.model.catalog.OfferTemplate',
  menuPriority: 1,
  themeColor: '#2196f3',
  pages: {
    list,
    show,
    edit,
    create
  },
  provider,
  schema,
  i18n: {
    en,
    fr
  }
};
