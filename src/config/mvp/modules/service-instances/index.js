import provider from './provider.json';
import schema from './schema.json';

export default {
  resource: 'service-instances',
  label: 'Service instance',
  provider,
  schema
};
