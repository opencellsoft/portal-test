import titles from './titles';
import offers from './offers';
import entityCustomization from './entity-customization';
import customerAccounts from './customer-accounts';
import businessServiceModels from './business-service-models';
import customers from './customers';
import serviceInstances from './service-instances';
import subscriptions from './subscriptions';
import userAccounts from './user-accounts';
import sellers from './sellers';
import terminationReasons from './termination-reasons';

const config = {
  offers,
  customers,
  titles,
  'entity-customization': entityCustomization,
  'customer-accounts': customerAccounts,
  'business-service-models': businessServiceModels,
  'service-instances': serviceInstances,
  subscriptions,
  'user-accounts': userAccounts,
  sellers,
  'termination-reasons': terminationReasons
};

export default Object.keys(config)
  .sort()
  .reduce((res, key) => {
    res[key] = config[key];

    return res;
  }, {});
