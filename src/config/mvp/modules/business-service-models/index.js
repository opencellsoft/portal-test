import provider from './provider.json';

export default {
  resource: 'business-service-models',
  entity: 'org.meveo.model.catalog.ServiceTemplate',
  label: `Business service models`,
  provider
};
