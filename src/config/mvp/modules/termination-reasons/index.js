import list from './list.json';
import show from './show.json';
import form from './form.json';
import en from './i18n/en.json';

import createExtras from './create.json';
import editExtras from './edit.json';

import provider from './provider.json';

const create = {
  ...form,
  ...createExtras
};

const edit = {
  ...form,
  ...editExtras
};

export default {
  resource: 'termination-reasons',
  label: `Termination reasons`,
  icon: 'Beenhere',
  inMenu: {
    icon: 'Build',
    label: 'Build',
    path: 'setup'
  },
  themeColor: '#999999',
  pages: {
    list,
    show
  },
  drawer: {
    create,
    edit
  },
  provider,
  i18n: {
    en
  }
};
