import menu from './menu';
import modules from './modules';

export default {
  modules,
  menu,
  title: 'Admin',
  themeColor: '#f44336'
};
