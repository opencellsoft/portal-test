import provider from './provider.json';

export default {
  resource: 'cdr-call-types',
  label: 'Call types',
  inMenu: false,
  provider
};
