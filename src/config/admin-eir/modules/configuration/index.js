import provider from './provider.json';
import en from './i18n/en.json';

import list from './list.json';
import form from './form.json';

export default {
  resource: 'configuration',
  label: `Configuration`,
  icon: 'Settings',
  themeColor: '#999999',
  pages: {
    list,
    create: form,
    edit: form
  },
  provider,
  i18n: {
    en
  }
};
