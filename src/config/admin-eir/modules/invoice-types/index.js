import provider from './provider.json';
import en from './i18n/en.json';

export default {
  resource: 'invoice-types',
  provider,
  i18n: {
    en
  }
};
