import provider from './provider.json';

import en from './i18n/en';

export default {
  resource: 'notes',
  icon: 'NoteAdd',
  themeColor: '#999999',
  inMenu: false,
  menuPriority: 0,
  provider,
  i18n: {
    en
  }
};
