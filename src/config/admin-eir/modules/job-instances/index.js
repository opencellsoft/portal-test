import provider from './provider.json';
import en from './i18n/en.json';

import list from './list.json';
import form from './form.json';

export default {
  resource: 'job-instances',
  label: `Job instances`,
  inMenu: {
    icon: 'AccountBalance/CallSplit',
    label: 'Administration/Jobs',
    path: 'administration/jobs'
  },
  themeColor: '#20B2AA',
  pages: {
    list,
    create: form,
    edit: form
  },
  provider,
  i18n: {
    en
  }
};
