import provider from './provider.json';

export default {
  resource: 'payment-methods',
  label: `Payment methods`,
  provider
};
