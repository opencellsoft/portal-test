import en from './i18n/en.json';
import provider from './provider.json';

export default {
  resource: 'tariffs-plans-mapping',
  inMenu: false,
  provider,
  i18n: {
    en
  }
};
