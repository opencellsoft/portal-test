import list from './list.json';
import provider from './provider.json';
import en from './i18n/en.json';
import fr from './i18n/fr.json';

export default {
  resource: 'files-management',
  label: 'File management',
  themeColor: '#CD853F',
  inMenu: {
    label: 'Setup',
    path: 'setup'
  },
  pages: {
    list
  },
  provider,
  i18n: {
    en,
    fr
  }
};
