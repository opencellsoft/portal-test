import list from './list.json';
import show from './show.json';
import form from './form.json';

import en from './i18n/en.json';
import fr from './i18n/fr.json';
import createExtras from './create.json';
import editExtras from './edit.json';

import pageCustom from './page-custom.json';

import provider from './provider.json';
import schema from './schema.json';

const create = {
  ...form,
  ...createExtras
};

const edit = {
  ...form,
  ...editExtras
};

export default {
  resource: 'customers',
  label: `Customers`,
  themeColor: '#00c853',
  icon: 'Person',
  inMenu: {
    icon: 'Person',
    label: 'Customers',
    path: 'customers',
    color: '#00c853'
  },
  entity: 'org.meveo.model.crm.Customer',
  menuPriority: 3,
  pages: {
    list,
    show,
    create,
    edit,
    'page-custom': pageCustom
  },
  provider,
  schema,
  i18n: {
    en,
    fr
  }
};
