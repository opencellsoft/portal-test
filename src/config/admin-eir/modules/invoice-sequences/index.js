import list from './list.json';
import show from './show.json';
import form from './form.json';
import en from './i18n/en.json';

import createExtras from './create.json';
import editExtras from './edit.json';

import provider from './provider.json';

const create = {
  ...form,
  ...createExtras
};

const edit = {
  ...form,
  ...editExtras
};

export default {
  resource: 'invoice-sequences',
  label: `Invoice sequences`,
  inMenu: {
    label: 'Setup/Billing',
    icon: 'Build/AttachMoney',
    path: 'setup/billing'
  },
  themeColor: '#999999',
  pages: {
    list,
    show,
    create,
    edit
  },
  provider,
  i18n: {
    en
  }
};
