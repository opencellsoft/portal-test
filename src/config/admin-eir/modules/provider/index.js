import provider from './provider.json';

import list from './list.json';
import show from './show.json';

import en from './i18n/en.json';

export default {
  resource: 'provider',
  label: `Provider`,
  icon: 'Settings',
  entity: 'org.meveo.model.crm.Provider',
  themeColor: '#999999',
  inMenu: {
    icon: 'Build',
    label: 'Setup',
    path: 'setup'
  },
  menuPriority: 0,
  pages: {
    list,
    show
  },
  provider,
  i18n: {
    en
  }
};
