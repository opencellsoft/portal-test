import provider from './provider.json';
import en from './i18n/en.json';

export default {
  resource: 'charges',
  label: `Charges`,
  provider,
  i18n: {
    en
  }
};
