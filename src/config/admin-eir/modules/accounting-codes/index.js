import provider from './provider.json';

import en from './i18n/en.json';

import list from './list.json';
import form from './form.json';

export default {
  resource: 'accounting-codes',
  label: 'Accounting codes',
  icon: 'SimCard',
  inMenu: {
    icon: 'Business',
    label: 'Finance',
    path: 'finance'
  },
  pages: { list },
  drawer: {
    create: form,
    edit: form
  },
  provider,
  i18n: {
    en
  }
};
