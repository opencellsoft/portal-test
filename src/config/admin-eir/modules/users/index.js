import form from './form.json';
import list from './list.json';
import show from './show.json';

import en from './i18n/en.json';
import fr from './i18n/fr.json';
import provider from './provider.json';

export default {
  resource: 'users',
  label: 'Users',
  inMenu: {
    icon: 'LooksOne',
    label: 'Setup',
    path: 'setup'
  },
  pages: {
    list,
    show,
    create: form,
    edit: form
  },
  provider,
  i18n: {
    en,
    fr
  }
};
