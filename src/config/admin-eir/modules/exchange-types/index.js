import list from './list.json';
import form from './form.json';
import provider from './provider.json';

import en from './i18n/en.json';

export default {
  resource: 'exchange-types',
  label: `Exchange types`,
  icon: 'KeyboardArrowRight',
  inMenu: {
    icon: 'Build/Redeem',
    label: 'Setup/Exchange',
    path: 'setup/Exchange'
  },
  pages: {
    list
  },
  drawer: {
    edit: form,
    create: form
  },
  provider,
  i18n: {
    en
  }
};
