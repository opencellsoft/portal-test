import provider from './provider.json';
import list from './list.json';
import form from './form.json';
import en from './i18n/en.json';
import fr from './i18n/fr.json';

export default {
  resource: 'file-types',
  label: 'File types',
  themeColor: '#CD853F',
  inMenu: {
    label: 'Setup',
    path: 'setup'
  },
  pages: {
    list
  },
  drawer: {
    edit: form,
    create: form
  },
  provider,
  i18n: {
    en,
    fr
  }
};
