import provider from './provider.json';

export default {
  resource: 'v2-one-shot-charge-templates',
  provider
};
