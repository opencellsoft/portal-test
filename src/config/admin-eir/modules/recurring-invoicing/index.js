import show from './show.json';
import form from './form.json';

import createExtras from './create.json';
import editExtras from './edit.json';

import provider from './provider.json';
import en from './i18n/en.json';

const create = {
  ...form,
  ...createExtras
};

const edit = {
  ...form,
  ...editExtras
};

export default {
  resource: 'recurring-invoicing',
  label: `New billing run`,
  inMenu: {
    label: 'Setup/Billing',
    icon: 'Build/AttachMoney',
    path: 'setup/billing'
  },
  themeColor: '#00BFFF',
  pages: {
    create,
    edit,
    show
  },
  provider,
  i18n: { en }
};
