import list from './list.json';
import form from './form.json';

import edit from './edit.json';
import en from './i18n/en.json';
import fr from './i18n/fr.json';
import provider from './provider.json';

export default {
  resource: 'service-level-agreements',
  label: 'Service Level Agreements',
  inMenu: {
    label: 'SLA',
    path: 'setup'
  },
  pages: {
    list,
    edit,
    create: form
  },
  provider,
  roles: [
    {
      page: 'create',
      required: ['Discount_Manager']
    },
    {
      page: 'edit',
      required: ['Discount_Manager']
    }
  ],
  i18n: {
    en,
    fr
  }
};
