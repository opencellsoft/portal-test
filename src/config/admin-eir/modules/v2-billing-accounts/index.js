import provider from './provider.json';
import schema from './schema.json';

export default {
  resource: 'v2-billing-accounts',
  entity: 'org.meveo.model.billing.BillingAccount',
  provider,
  schema
};
