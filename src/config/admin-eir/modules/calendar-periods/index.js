import provider from './provider.json';

export default {
  resource: 'calendar-periods',
  inMenu: false,
  provider
};
