import provider from './provider.json';

export default {
  resource: 'billing-accounts-sequence',
  provider
};
