import list from './list.json';
import show from './show.json';
import form from './form.json';
import en from './i18n/en.json';

import createExtras from './create.json';
import editExtras from './edit.json';
import provider from './provider.json';

const create = {
  ...form,
  ...createExtras
};

const edit = {
  ...form,
  ...editExtras
};
export default {
  resource: 'credit-categories',
  label: `Credit categories`,
  inMenu: {
    icon: 'Build',
    label: 'Setup',
    path: 'setup'
  },
  themeColor: '#00BFFF',
  pages: {
    list,
    edit,
    show,
    create
  },
  provider,
  i18n: {
    en
  }
};
