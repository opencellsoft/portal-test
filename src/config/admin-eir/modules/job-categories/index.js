import provider from './provider.json';

export default {
  resource: 'job-categories',
  label: `Job categories`,
  provider
};
