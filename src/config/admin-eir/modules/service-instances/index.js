import provider from './provider.json';
import schema from './schema.json';

export default {
  resource: 'service-instances',
  label: 'Service instance',
  entity: 'org.meveo.model.billing.ServiceInstance',
  provider,
  schema
};
