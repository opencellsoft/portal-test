import provider from './provider.json';

export default {
  resource: 'volume-discount-aggregations',
  label: 'Volume discount aggregations',
  provider
};
