import provider from './provider.json';
import en from './i18n/en.json';

import form from './form.json';

import list from './list.json';
import show from './show.json';

export default {
  resource: 'calendar',
  label: `Calendars`,
  inMenu: {
    label: 'Setup/Billing',
    icon: 'Build/AttachMoney',
    path: 'setup/billing'
  },
  icon: 'DateRange',
  themeColor: '#9b59b6',
  pages: {
    list,
    show
  },
  drawer: {
    create: form,
    edit: form
  },
  provider,
  i18n: {
    en
  }
};
