import list from './list.json';
import show from './show.json';
import provider from './provider.json';
import schema from './schema.json';
import en from './i18n/en.json';
import fr from './i18n/fr.json';

export default {
  resource: 'orders',
  label: 'Orders',
  icon: 'CloudDownload',
  inMenu: true,
  menuPriority: 1,
  entity: 'org.meveo.model.order.Order',
  themeColor: '#ffab40',
  pages: {
    list,
    show
  },
  provider,
  schema,
  i18n: {
    en,
    fr
  }
};
