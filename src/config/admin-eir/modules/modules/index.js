import list from './list.json';
import show from './show.json';
import form from './form.json';
import en from './i18n/en.json';
import fr from './i18n/fr.json';

import createExtras from './create.json';
import editExtras from './edit.json';

import provider from './provider.json';

const create = {
  ...form,
  ...createExtras
};

const edit = {
  ...form,
  ...editExtras
};

export default {
  resource: 'modules',
  label: `Modules`,
  icon: 'PlayForWork',
  inMenu: {
    icon: 'AccountBalance',
    label: 'Administration',
    path: 'administration'
  },
  themeColor: '#00BFFF',
  pages: {
    list,
    show,
    create,
    edit
  },
  provider,
  i18n: {
    en,
    fr
  }
};
