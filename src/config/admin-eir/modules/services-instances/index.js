import list from './list.json';
import en from './i18n/en.json';
import fr from './i18n/fr.json';

import provider from './provider.json';

export default {
  resource: 'services-instances',
  label: 'Services instances',
  icon: 'CardTravel',
  inMenu: true,
  pages: { list },
  provider,
  i18n: {
    en,
    fr
  }
};
