import list from './list.json';
import show from './show.json';
import form from './form.json';
import en from './i18n/en.json';
import fr from './i18n/fr.json';

import provider from './provider.json';
import schema from './schema.json';

export default {
  resource: 'offers',
  label: 'Offers',
  inMenu: {
    icon: 'ChromeReaderMode',
    label: 'Offers',
    path: 'offers',
    color: '#00c853'
  },
  entity: 'org.meveo.model.catalog.OfferTemplate',
  menuPriority: 1,
  themeColor: '#2196f3',
  pages: {
    list,
    show,
    edit: form,
    create: form
  },
  provider,
  schema,
  i18n: {
    en,
    fr
  }
};
