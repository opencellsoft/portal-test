import provider from './provider.json';

import en from './i18n/en.json';

import list from './list.json';
import form from './form.json';
import schema from './schema.json';

export default {
  resource: 'account-operations',
  themeColor: '#CD853F',
  entity: 'org.meveo.model.payments.AccountOperation',
  pages: { list },
  drawer: {
    create: form,
    edit: form
  },
  schema,
  provider,
  i18n: {
    en
  }
};
