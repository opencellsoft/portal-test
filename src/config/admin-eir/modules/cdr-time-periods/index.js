import list from './list.json';
import form from './form.json';

import provider from './provider.json';
import en from './i18n/en.json';

export default {
  resource: 'cdr-time-periond',
  label: 'Time periods',
  inMenu: {
    label: 'CDR Rate Management',
    path: 'cdr'
  },
  menuPriority: 1000,
  pages: {
    list,
    create: form,
    edit: form
  },
  provider,
  i18n: {
    en
  }
};
