import list from './list.json';
import show from './show.json';
import form from './form.json';

import createExtras from './create.json';
import edit from './edit.json';
import en from './i18n/en.json';
import provider from './provider.json';

const create = {
  ...form,
  ...createExtras
};

export default {
  resource: 'payment-gateways',
  label: `Payment gateways`,
  inMenu: {
    icon: 'Payment',
    label: 'Payments',
    path: 'payments'
  },

  themeColor: '#f44336',
  pages: {
    list,
    show,
    create,
    edit
  },
  provider,
  i18n: {
    en
  }
};
