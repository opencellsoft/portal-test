import provider from './provider.json';
import en from './i18n/en.json';
import main from './main.json';

export default {
  resource: 'tax-pending-mapping',
  label: `Pending tax mapping`,
  icon: 'KeyboardArrowRight',
  themeColor: '#999999',
  inMenu: {
    label: 'Taxes/Tax assignment',
    path: 'taxes/tax-assignment'
  },
  provider,
  pages: {
    '': main
  },
  i18n: {
    en
  }
};
