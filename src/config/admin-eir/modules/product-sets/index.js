import provider from './provider.json';
import en from './i18n/en';

import list from './list.json';
import form from './form.json';

export default {
  resource: 'product-sets',
  label: `Product Set`,
  icon: 'List',
  themeColor: '#999999',
  inMenu: {
    icon: 'Build',
    label: 'Setup',
    path: 'setup'
  },
  pages: {
    list
  },
  drawer: {
    create: form,
    edit: form
  },
  provider,
  i18n: {
    en
  }
};
