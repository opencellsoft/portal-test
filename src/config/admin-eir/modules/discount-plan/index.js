import list from './list.json';
import show from './show.json';
import form from './form.json';

import createExtras from './create.json';
import editExtras from './edit.json';
import en from './i18n/en.json';
import provider from './provider.json';

const create = {
  ...form,
  ...createExtras
};

const edit = {
  ...form,
  ...editExtras
};

export default {
  resource: 'discount-plan',
  label: `Discount plans`,
  inMenu: {
    label: 'Setup',
    icon: 'Build/AttachMoney',
    path: 'setup'
  },
  themeColor: '#CD853F',
  pages: {
    list,
    show,
    create,
    edit
  },
  provider,
  i18n: {
    en
  }
};
