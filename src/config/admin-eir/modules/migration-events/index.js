import list from './list.json';
import form from './form.json';
import en from './i18n/en.json';
import provider from './provider.json';

const create = {
  ...form
};

const edit = {
  ...form
};

export default {
  resource: 'migration-events',
  label: `Migration events`,
  icon: 'KeyboardArrowRight',
  inMenu: {
    icon: 'Redeem',
    label: 'Setup',
    path: 'setup'
  },
  entity: {
    name: 'Migration_EVENT',
    isCustom: true
  },
  pages: {
    list
  },
  drawer: {
    create,
    edit
  },
  provider,
  i18n: {
    en
  }
};
