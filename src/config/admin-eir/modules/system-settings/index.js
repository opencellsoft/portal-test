import en from './i18n/en.json';
import fr from './i18n/fr.json';
import list from './list.json';
import form from './form.json';
import provider from './provider.json';
import createExtras from './create.json';
import show from './show.json';

const create = {
  ...form,
  ...createExtras
};
export default {
  resource: 'system-settings',
  label: `System Settings`,
  themeColor: '#559899',
  icon: 'Settings',
  inMenu: true,
  pages: {
    list,
    create,
    show
  },
  provider,
  i18n: {
    en,
    fr
  }
};
