import provider from './provider.json';
import schema from './schema.json';

import list from './list.json';
import show from './show.json';
import form from './form.json';

import en from './i18n/en.json';

export default {
  resource: 'subscriptions',
  label: 'Subscriptions',
  icon: 'Subscriptions',
  themeColor: '#7c4dff',
  entity: 'org.meveo.model.billing.Subscription',
  inMenu: true,
  menuPriority: 4,
  pages: {
    list,
    show,
    edit: form
  },
  provider,
  schema,
  i18n: {
    en
  }
};
