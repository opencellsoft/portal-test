import provider from './provider.json';

export default {
  resource: 'trading-countries',
  provider
};
