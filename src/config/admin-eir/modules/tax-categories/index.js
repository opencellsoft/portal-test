import list from './list.json';
import form from './form.json';
import createExtras from './create.json';
import editExtras from './edit.json';
import en from './i18n/en.json';
import provider from './provider.json';
import schema from './schema.json';

const create = {
  ...form,
  ...createExtras
};

const edit = {
  ...form,
  ...editExtras
};

export default {
  resource: 'tax-categories',
  label: `Tax category`,
  icon: 'KeyboardArrowRight',
  inMenu: {
    icon: 'Build/Redeem',
    label: 'Taxes',
    path: 'taxes'
  },
  entity: 'org.meveo.model.tax.TaxCategory',
  pages: {
    list
  },
  drawer: {
    create,
    edit
  },
  provider,
  schema,
  i18n: {
    en
  }
};
