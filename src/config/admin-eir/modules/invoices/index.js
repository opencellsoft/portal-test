import list from './list.json';
import en from './i18n/en.json';
import show from './show.json';

import provider from './provider.json';

export default {
  resource: 'invoices',
  label: `Invoices`,
  icon: 'FolderShared',
  themeColor: '#00bfa5',
  inMenu: true,
  menuPriority: 5,
  pages: {
    list,
    show
  },
  provider,
  i18n: {
    en
  }
};
