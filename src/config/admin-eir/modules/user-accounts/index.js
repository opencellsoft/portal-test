import list from './list.json';
import show from './show.json';
import form from './form.json';
import en from './i18n/en.json';
import fr from './i18n/fr.json';
import createExtras from './create.json';
import editExtras from './edit.json';
import provider from './provider.json';

const create = {
  ...form,
  ...createExtras
};

const edit = {
  ...form,
  ...editExtras
};

export default {
  resource: 'user-accounts',
  label: `User accounts`,
  icon: 'Person',
  inMenu: {
    icon: 'Person',
    label: 'Customers',
    path: 'customers'
  },
  entity: 'org.meveo.model.billing.UserAccount',
  pages: {
    list,
    show,
    create,
    edit
  },

  provider,
  i18n: {
    en,
    fr
  }
};
