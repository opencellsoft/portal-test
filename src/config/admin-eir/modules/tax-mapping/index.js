import provider from './provider.json';
import en from './i18n/en.json';
import list from './list.json';

export default {
  resource: 'tax-mapping',
  label: `Active tax mapping`,
  icon: 'KeyboardArrowRight',
  themeColor: '#999999',
  inMenu: {
    label: 'Taxes/Tax assignment',
    path: 'taxes/tax-assignment'
  },
  provider,
  pages: {
    list
  },
  i18n: {
    en
  }
};
