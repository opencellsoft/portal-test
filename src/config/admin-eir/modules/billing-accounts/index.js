import list from './list.json';
import show from './show.json';
import form from './form.json';
import create from './create.json';

import en from './i18n/en.json';
import fr from './i18n/fr.json';

import editExtras from './edit.json';

import provider from './provider.json';
import schema from './schema.json';

const edit = {
  ...form,
  ...editExtras
};

export default {
  resource: 'billing-accounts',
  label: `Billing accounts`,
  themeColor: '#00c8c8',
  icon: 'Person',
  inMenu: {
    icon: 'Person',
    label: 'Customers',
    path: 'customers',
    color: '#00c853'
  },
  entity: 'org.meveo.model.billing.BillingAccount',
  pages: {
    list,
    show,
    create,
    edit
  },
  provider,
  schema,
  i18n: {
    en,
    fr
  }
};
