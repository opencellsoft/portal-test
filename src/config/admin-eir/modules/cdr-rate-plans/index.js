import list from './list.json';
import form from './form.json';

import en from './i18n/en.json';
import provider from './provider.json';

export default {
  resource: 'cdr-rate-plans',
  label: 'Rate plans',
  inMenu: {
    label: 'CDR Rate Management',
    path: 'cdr'
  },
  menuPriority: 1000,
  pages: {
    list,
    create: form,
    edit: form
  },
  provider,
  i18n: {
    en
  }
};
