import provider from './provider.json';

import en from './i18n/en.json';

import list from './list.json';
import form from './form.json';

export default {
  resource: 'account-operations-types',
  label: 'Account operations types',
  inMenu: {
    label: 'Setup',
    icon: 'Business',
    path: 'setup'
  },
  themeColor: '#CD853F',
  pages: { list },
  drawer: {
    create: form,
    edit: form
  },
  provider,
  i18n: {
    en
  }
};
