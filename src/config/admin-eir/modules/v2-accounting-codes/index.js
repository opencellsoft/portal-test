import provider from './provider.json';

export default {
  resource: 'v2-accounting-codes',
  provider
};
