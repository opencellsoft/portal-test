import provider from './provider.json';
import schema from './schema.json';

export default {
  resource: 'v2-customers',
  entity: 'org.meveo.model.payments.Customers',
  schema,
  provider
};
