import provider from './provider.json';
import en from './i18n/en.json';
import list from './list.json';
import createExtras from './create.json';
import editExtras from './edit.json';
import form from './form.json';
import show from './show.json';

const create = {
  ...form,
  ...createExtras
};

const edit = {
  ...form,
  ...editExtras
};

export default {
  resource: 'invoice-sub-categories',
  label: `Invoice sub categories`,
  inMenu: {
    label: 'Setup/Billing',
    icon: 'Build/AttachMoney',
    path: 'setup/billing'
  },
  themeColor: '#999999',
  pages: {
    list,
    create,
    edit,
    show
  },
  provider,
  i18n: {
    en
  }
};
