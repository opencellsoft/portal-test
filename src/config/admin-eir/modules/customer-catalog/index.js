import show from './show.json';
import schema from './schema.json';
import en from './i18n/en.json';
import provider from './provider.json';

export default {
  resource: 'customer-catalog',
  label: `Customer Catalog`,
  inMenu: false,
  entity: 'org.meveo.model.crm.Customer',
  themeColor: '#40c4ff',
  pages: {
    show
  },
  provider,
  schema,
  i18n: {
    en
  }
};
