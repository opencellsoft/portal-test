import provider from './provider.json';

export default {
  resource: 'v2-invoice-sub-categories',
  provider
};
