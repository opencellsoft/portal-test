import list from './list.json';
import show from './show.json';
import form from './form.json';

import createExtras from './create.json';
import editExtras from './edit.json';
import en from './i18n/en.json';
import provider from './provider.json';
import schema from './schema.json';

const create = {
  ...form,
  ...createExtras
};

const edit = {
  ...form,
  ...editExtras
};

export default {
  resource: 'roles',
  label: `Roles`,
  icon: 'Portrait',
  themeColor: '#999999',
  inMenu: {
    icon: 'Build',
    label: 'Build',
    path: 'setup'
  },
  pages: {
    list,
    show,
    create,
    edit
  },
  provider,
  schema,
  i18n: {
    en
  }
};
