import provider from './provider.json';
import schema from './schema.json';

export default {
  resource: 'v2-customer-accounts',
  entity: 'org.meveo.model.payments.CustomerAccount',
  schema,
  provider
};
