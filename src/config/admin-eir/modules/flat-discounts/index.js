import list from './list.json';
import form from './form.json';

import createExtras from './create.json';
import edit from './edit.json';
import en from './i18n/en.json';
import provider from './provider.json';

const create = {
  ...form,
  ...createExtras
};

export default {
  resource: 'flat-discounts',
  label: 'Flat discounts',
  inMenu: {
    label: 'Discounts',
    path: 'discounts'
  },
  menuPriority: 2,
  pages: {
    list,
    create,
    edit
  },
  provider,
  roles: [
    {
      page: 'create',
      required: ['Discount_Manager']
    },
    {
      page: 'edit',
      required: ['Discount_Manager']
    }
  ],
  i18n: {
    en
  }
};
