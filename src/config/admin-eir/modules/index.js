import dashboard from './dashboard';
import offers from './offers';
import offerCategories from './offer-categories';
import configuration from './configuration';
import customers from './customers';
import customerCatalog from './customer-catalog';
import sellers from './sellers';
import users from './users';
import subscriptions from './subscriptions';
import services from './services';
import orders from './orders';
import ordersDate from './orders-date';
import charges from './charges';
import invoices from './invoices';
import notes from './notes';
import provider from './provider';
import entityCustomization from './entity-customization';
import taxCategories from './tax-categories';
import taxClasses from './tax-classes';
import taxManagement from './tax-rates';
import roles from './roles';
import billingAccounts from './billing-accounts';
import customerAccounts from './customer-accounts';
import userAccounts from './user-accounts';
import billingCycles from './billing-cycles';
import calendar from './calendar';
import calendarPeriods from './calendar-periods';
import invoiceTypes from './invoice-types';
import permissions from './permissions';
import accountingCodes from './accounting-codes';
import contacts from './contacts';
import paymentMethods from './payment-methods';
import invoiceCategories from './invoice-categories';
import invoiceSubCategories from './invoice-sub-categories';
import subscriberPrefixes from './subscriber-prefixes';
import productSets from './product-sets';
import scriptInstance from './script-instance';
import invoiceSequences from './invoice-sequences';
import jobInstances from './job-instances';
import jobCategories from './job-categories';
import terminationReasons from './termination-reasons';
import modules from './modules';
import countries from './countries';
import currencies from './currencies';
import languages from './languages';
import channels from './channels';
import customerBrands from './customer-brands';
import creditCategories from './credit-categories';
import customerCategories from './customer-categories';
import titles from './titles';
import ratedTransactions from './rated-transactions';
import billingRun from './recurring-invoicing';
import paymentGateways from './payment-gateways';
import paymentScheduleTemplates from './payment-schedule-templates';
import discountPlan from './discount-plan';
import accountOperationsTypes from './account-operations-types';
import accountOperations from './account-operations';
import filesManagement from './files-management';
import fileFormats from './file-formats';
import fileTypes from './file-types';
import exchangeClasses from './exchange-classes';
import exchangeTypes from './exchange-types';
import densities from './densities';
import actionQualifiers from './action-qualifiers';
import assuredFactors from './assured-factors';
import expeditedFactors from './expedited-factors';
import regions from './regions';
import serviceLevelAgreements from './service-level-agreements';
import classOfServices from './class-of-services';
import serviceInstances from './service-instances';
import serviceTypes from './service-types';
import migrationEvents from './migration-events';
import circuitTypeMapping from './circuit-type-mapping';
import billingAccountsSequence from './billing-accounts-sequence';
import tariffsPlans from './tariffs-plans';
import tariffs from './tariffs';
import pendingTariffs from './pending-tariffs';
import tariffsPlansMapping from './tariffs-plans-mapping';
import flatDiscounts from './flat-discounts';
import volumeDiscounts from './volume-discounts';
import volumeDiscountAggregations from './volume-discount-aggregations';
import v2BillingAccounts from './v2-billing-accounts';
import v2CustomerAccounts from './v2-customer-accounts';
import v2Customers from './v2-customers';
import v2Services from './v2-services';
import v2AccountingCodes from './v2-accounting-codes';
import v2InvoiceSubCategories from './v2-invoice-sub-categories';
import tradingCurrencies from './trading-currencies';
import cdrRatePlans from './cdr-rate-plans';
import cdrTimeBands from './cdr-time-bands';
import cdrTimePeriods from './cdr-time-periods';
import cdrRatePlanMapping from './cdr-rate-plan-mapping';
import cdrTrafficClasses from './cdr-traffic-classes';
import cdrTariffs from './cdr-tariffs';
import cdrCallTypes from './cdr-call-types';
import taxMapping from './tax-mapping';
import tradingCountries from './trading-countries';
import taxPendingMapping from './tax-pending-mapping';
import v2OneShotChargeTemplate from './v2-one-shot-charge-templates';

import {
  buildNestedResourcePath,
  patchDeepResourcesPath
} from '../../../utils/config/setup';

const moduleList = [
  configuration,
  dashboard,
  offers,
  offerCategories,
  customers,
  sellers,
  orders,
  ordersDate,
  users,
  provider,
  notes,
  services,
  subscriptions,
  calendar,
  calendarPeriods,
  roles,
  permissions,
  modules,
  entityCustomization,
  invoiceTypes,
  accountingCodes,
  taxCategories,
  taxClasses,
  taxManagement,
  charges,
  invoices,
  channels,
  contacts,
  languages,
  countries,
  currencies,
  customerAccounts,
  paymentMethods,
  subscriberPrefixes,
  productSets,
  customerCatalog,
  billingAccounts,
  userAccounts,
  billingCycles,
  invoiceCategories,
  invoiceSubCategories,
  scriptInstance,
  invoiceSequences,
  jobInstances,
  jobCategories,
  terminationReasons,
  customerCategories,
  customerBrands,
  creditCategories,
  titles,
  ratedTransactions,
  billingRun,
  paymentGateways,
  paymentScheduleTemplates,
  discountPlan,
  accountOperationsTypes,
  accountOperations,
  filesManagement,
  fileFormats,
  fileTypes,
  exchangeClasses,
  exchangeTypes,
  densities,
  actionQualifiers,
  assuredFactors,
  expeditedFactors,
  regions,
  serviceLevelAgreements,
  classOfServices,
  serviceInstances,
  serviceTypes,
  migrationEvents,
  circuitTypeMapping,
  billingAccountsSequence,
  pendingTariffs,
  tariffsPlans,
  tariffs,
  tariffsPlansMapping,
  flatDiscounts,
  volumeDiscounts,
  volumeDiscountAggregations,
  v2BillingAccounts,
  v2CustomerAccounts,
  v2Services,
  v2AccountingCodes,
  v2InvoiceSubCategories,
  v2Customers,
  tradingCurrencies,
  cdrRatePlans,
  cdrTimeBands,
  cdrTimePeriods,
  cdrRatePlanMapping,
  cdrTariffs,
  taxMapping,
  tradingCountries,
  taxPendingMapping,
  cdrTrafficClasses,
  cdrCallTypes,
  v2OneShotChargeTemplate
];

const config = moduleList.reduce((res, module) => {
  const updatedModule = buildNestedResourcePath(module);
  const { resource } = updatedModule;

  if (!resource) {
    throw new Error('Module without resource defined');
  }

  res[resource] = updatedModule;

  return res;
}, {});

export default Object.keys(config)
  .sort(
    (resourceNameA, resourceNameB) =>
      (resourceNameA > resourceNameB) - (resourceNameA < resourceNameB)
  )
  .reduce((res, key) => {
    res[key] = patchDeepResourcesPath(config[key], config);

    return res;
  }, {});
