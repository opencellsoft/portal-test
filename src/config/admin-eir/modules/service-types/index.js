import provider from './provider.json';
import list from './list.json';
import form from './form.json';
import en from './i18n/en.json';

const create = {
  ...form
};

const edit = {
  ...form
};

export default {
  resource: 'service-types',
  label: 'Service type',
  inMenu: {
    icon: 'Build/AttachMoney',
    label: 'Setup/Billing',
    path: 'setup/billing'
  },
  entity: {
    name: 'Service_Type',
    isCustom: true
  },
  pages: {
    list
  },
  drawer: {
    create,
    edit
  },
  provider,
  i18n: {
    en
  }
};
