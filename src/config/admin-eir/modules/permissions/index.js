import provider from './provider.json';
import schema from './schema.json';

export default {
  resource: 'permissions',
  label: `Permissions`,
  provider,
  schema
};
