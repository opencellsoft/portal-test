import en from './i18n/en';
import provider from './provider.json';
import show from './show.json';

export default {
  resource: 'pending-tariffs',
  label: 'Pending tariffs',
  inMenu: {
    icon: 'ChromeReaderMode',
    label: 'Offers',
    path: 'offers',
    color: '#00c853'
  },
  provider,
  pages: {
    '': show
  },
  i18n: {
    en
  }
};
