import provider from './provider.json';
import en from './i18n/en.json';
import fr from './i18n/fr.json';

export default {
  resource: 'offer-categories',
  label: `Offer Categories`,
  icon: 'Category',
  pages: {},
  provider,
  i18n: {
    en,
    fr
  }
};
