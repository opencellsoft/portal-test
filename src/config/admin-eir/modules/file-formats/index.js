import list from './list.json';
import en from './i18n/en.json';
import provider from './provider.json';
import form from './form.json';

export default {
  resource: 'file-formats',
  label: 'File formats',
  themeColor: '#CD853F',
  inMenu: {
    label: 'Setup',
    path: 'setup'
  },
  pages: {
    list,
    create: form,
    edit: form
  },
  drawer: {},
  provider,
  i18n: {
    en
  }
};
