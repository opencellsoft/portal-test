import provider from './provider.json';

export default {
  resource: 'script-instances',
  label: `Script instances`,

  provider
};
