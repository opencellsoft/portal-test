import list from './list.json';
import form from './form.json';
import provider from './provider';

import en from './i18n/en.json';

export default {
  resource: 'circuit-type-mapping',
  label: `Circuit type mapping`,
  icon: 'KeyboardArrowRight',
  inMenu: {
    icon: 'Build',
    label: 'Setup',
    path: 'setup'
  },
  pages: {
    list
  },
  drawer: {
    edit: form,
    create: form
  },
  provider,
  i18n: {
    en
  }
};
