import provider from './provider.json';

export default {
  resource: 'v2-services',
  label: 'Services',
  provider
};
