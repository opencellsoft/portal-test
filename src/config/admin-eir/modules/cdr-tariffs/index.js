import list from './list.json';

import en from './i18n/en.json';
import provider from './provider.json';

export default {
  resource: 'cdr-tariffs',
  label: 'Tariffs',
  inMenu: {
    label: 'CDR Rate Management',
    path: 'cdr'
  },
  menuPriority: 1000,
  pages: {
    list
  },
  provider,
  i18n: {
    en
  }
};
