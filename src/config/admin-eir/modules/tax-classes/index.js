import list from './list.json';
import form from './form.json';
import en from './i18n/en.json';
import provider from './provider.json';
import createExtras from './create.json';
import editExtras from './edit.json';

const create = {
  ...form,
  ...createExtras
};
const edit = {
  ...form,
  ...editExtras
};

export default {
  resource: 'tax-classes',
  label: `Tax class`,
  icon: 'KeyboardArrowRight',
  inMenu: {
    icon: 'Build/Redeem',
    label: 'Taxes',
    path: 'taxes'
  },
  pages: {
    list
  },
  drawer: {
    create,
    edit
  },
  provider,
  i18n: {
    en
  }
};
