import list from './list.json';
import form from './form.json';

import provider from './provider.json';

import en from './i18n/en.json';
import fr from './i18n/fr.json';

export default {
  resource: 'assured-factors',
  label: `Assured Factors`,
  icon: 'KeyboardArrowRight',
  inMenu: {
    icon: 'Build/Redeem',
    label: 'Setup',
    path: 'setup'
  },
  pages: {
    list
  },
  drawer: {
    create: form,
    edit: form
  },
  provider,
  i18n: {
    en,
    fr
  }
};
