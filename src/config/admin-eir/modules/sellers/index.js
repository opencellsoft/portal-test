import list from './list.json';
import show from './show.json';
import form from './form.json';

import createExtras from './create.json';
import editExtras from './edit.json';
import en from './i18n/en.json';
import fr from './i18n/fr.json';
import provider from './provider.json';

const create = {
  ...form,
  ...createExtras
};

const edit = {
  ...form,
  ...editExtras
};

export default {
  resource: 'sellers',
  label: `Sellers`,
  icon: 'Beenhere',
  inMenu: {
    icon: 'Build',
    label: 'setup',
    path: 'setup'
  },
  menuPriority: 2,
  themeColor: '#999999',
  pages: {
    list,
    show,
    create,
    edit
  },
  provider,
  i18n: {
    en,
    fr
  }
};
