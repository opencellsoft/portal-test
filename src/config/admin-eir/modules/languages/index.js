import list from './list.json';
import show from './show.json';
import form from './form.json';

import createExtras from './create.json';
import editExtras from './edit.json';
import provider from './provider.json';
import en from './i18n/en.json';

const create = {
  ...form,
  ...createExtras
};
const edit = {
  ...form,
  ...editExtras
};

export default {
  resource: 'languages',
  label: `Languages`,
  icon: 'Language',
  inMenu: {
    icon: 'Build',
    label: 'Setup',
    path: 'setup'
  },
  themeColor: '#999999',
  pages: {
    list,
    edit,
    show,
    create
  },
  provider,
  i18n: {
    en
  }
};
