import provider from './provider.json';

export default {
  resource: 'trading-currencies',
  provider
};
