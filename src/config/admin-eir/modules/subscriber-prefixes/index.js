import en from './i18n/en.json';
import list from './list.json';
import form from './form.json';
import provider from './provider.json';

export default {
  resource: 'subscriber-prefixes',
  label: `Subcriber prefix`,
  icon: 'Fingerprint',
  themeColor: '#999999',
  inMenu: {
    icon: 'Build',
    label: 'Setup',
    path: 'setup'
  },
  pages: {
    list
  },
  drawer: {
    create: form,
    edit: form
  },
  provider,
  i18n: {
    en
  }
};
