import list from './list.json';
import en from './i18n/en.json';
import provider from './provider.json';

export default {
  resource: 'rated-transactions',
  label: `Rated transactions`,
  inMenu: {
    label: 'Setup/Billing',
    icon: 'Build/AttachMoney',
    path: 'setup/billing'
  },
  themeColor: '#00BFFF',
  pages: {
    list
  },
  provider,
  i18n: {
    en
  }
};
