import show from './show.json';
import en from './i18n/en.json';
import fr from './i18n/fr.json';

import provider from './provider.json';

export default {
  resource: 'services',
  label: 'Services',
  icon: 'CardTravel',
  // inMenu: true,
  pages: {
    show
  },
  provider,
  i18n: {
    en,
    fr
  }
};
