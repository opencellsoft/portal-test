import admin from './admin';
import adminEIR from './admin-eir';
import cc from './cc';
import mm from './mm';
import sc from './sc';
import ccSmoove from './cc-smoove';
import mvp from './mvp';

export default {
  admin,
  'admin-eir': adminEIR,
  cc,
  mm,
  sc,
  'cc-smoove': ccSmoove,
  mvp
};
