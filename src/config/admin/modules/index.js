import dashboard from './dashboard';

const config = {
  dashboard
};

export default Object.keys(config)
  .sort()
  .reduce((res, key) => {
    res[key] = config[key];

    return res;
  }, {});
