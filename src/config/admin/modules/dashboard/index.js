import en from './i18n/en.json';
import dashboard from './dashboard.json';
import provider from './provider.json';

export default {
  resource: 'dashboard',
  label: 'Dashboard',
  icon: 'Dashboard',
  inMenu: true,
  menuPriority: 0,
  path: '/dashboard',
  pages: {
    '': dashboard
  },
  provider,
  i18n: {
    en
  }
};
