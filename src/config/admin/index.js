import menu from './menu';
import modules from './modules';

export default {
  menu,
  modules,
  title: 'Admin',
  themeColor: '#f44336'
};
