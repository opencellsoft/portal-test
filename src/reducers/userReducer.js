import { USER_SIGNIN_SUCCESS } from '../actions/user';

export default (state = {}, { type, payload }) => {
  switch (type) {
    case USER_SIGNIN_SUCCESS:
      return payload;
    default:
      return state;
  }
};
