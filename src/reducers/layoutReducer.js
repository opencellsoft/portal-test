import { OC_SET_TITLE } from '../actions/layout';

const { REACT_APP_DEFAULT_TITLE = 'Opencell Portal' } = process.env;

const initialState = {
  appTitle: REACT_APP_DEFAULT_TITLE
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case OC_SET_TITLE:
      return {
        ...state,
        appTitle: payload
      };
    default:
      return state;
  }
};
