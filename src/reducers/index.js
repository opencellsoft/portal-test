import config from './configReducer';
import layout from './layoutReducer';
import theme from './themeReducer';
import user from './userReducer';

export default { config, layout, user, theme };
