import { get } from 'lodash';

import { CHANGE_THEME } from '../actions/theme';

import { darkTheme, lightTheme } from '../styles/theme';
import config from '../config';

const { REACT_APP_KEY } = process.env;
const { modules = {}, themeColor = '#f44336' } =
  get(config, REACT_APP_KEY) || {};

const enhanceTheme = (theme) => ({
  theme: {
    ...theme,
    currentTheme: {
      ...theme.currentTheme,
      modules: Object.keys(modules).reduce((res, key) => {
        const moduleConfig = get(modules, key, {});
        const { themeColor: moduleColor, icon } = moduleConfig || {};

        return {
          ...res,
          [key]: {
            themeColor: moduleColor || themeColor,
            icon
          }
        };
      }, {})
    }
  }
});

const themes = {
  light: lightTheme,
  dark: darkTheme
};

const initialState = enhanceTheme({
  key: 'light',
  currentTheme: themes.light
});

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case CHANGE_THEME:
      return {
        ...state,
        key: payload,
        currentTheme: enhanceTheme(themes[payload])
      };
    default:
      return state;
  }
};
