import config from '../config';

const { REACT_APP_KEY = 'admin' } = process.env;

const initialState = {
  key: REACT_APP_KEY,
  config: config[REACT_APP_KEY]
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    default:
      return state;
  }
};
