import {
  email,
  required,
  number,
  regex,
  minValue,
  maxValue,
  minLength
} from 'react-admin';
import { isEmpty, get } from 'lodash';
import { memoize, getMessage, getValueToCompare } from './helpers';

export const validateEmail = [required(), email()];

export const isNull = memoize((message = 'error') => (value) =>
  value === undefined || value === null ? message : undefined
);

/**
 * Maximum length validator
 *
 * Returns an error if the value has a length higher than the parameter
 *
 * @param {integer} max
 * @param {string|function} message
 *
 *
 * borrow it, because react-admin maxLength doesn't work with numeric values
 */
export const maxLength = memoize(
  (max, message = 'ra.validation.maxLength') => (value, values, props) => {
    //have to trasnform numeric value to string here
    value = String(value);

    return !isEmpty(value) && value.length > max
      ? getMessage(message, { max }, value, values, props)
      : undefined;
  }
);

export const wildcards = memoize((message = 'error') => (value) =>
  value && value === '*' ? message : undefined
);

export const getValidatorByKey = (key, validationData, formValues) => {
  switch (true) {
    case key === 'required':
      return required();
    case key === 'email':
      return email();
    case key === 'number':
      return number();
    case key === 'minValue': {
      const { value, message } = getValueToCompare(validationData, formValues);

      return value === null ? undefined : minValue(value, message);
    }
    case key === 'maxValue': {
      const { value, message } = getValueToCompare(validationData, formValues);

      return value === null ? undefined : maxValue(value, message);
    }
    case key === 'minLength':
      return minLength(validationData.split(':')[1]);
    case key === 'maxLength':
      return maxLength(String(validationData.split(':')[1]));
    case key === 'regex':
      return regex(new RegExp(validationData.split(':')[1])); //eslint-disable-line security/detect-non-literal-regexp
    case key === 'wildcards':
      return wildcards(get(validationData, 'message'));
    case key === 'isNull':
      return isNull(get(validationData, 'message'));
    default:
      return undefined;
  }
};
