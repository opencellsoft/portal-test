import { isObject, isString, memoize as lodashMemoize, get } from 'lodash';
import moment from 'moment-timezone';

// If we define validation functions directly in JSX, it will
// result in a new function at every render, and then trigger infinite re-render.
// Hence, we memoize every built-in validator to prevent a "Maximum call stack" error.
// i borrowed it from react-admin sources
export const memoize = (fn) =>
  lodashMemoize(fn, (...args) => JSON.stringify(args));

export const getMessage = (message, messageArgs, value, values, props) =>
  typeof message === 'function'
    ? message({
        args: messageArgs,
        value,
        values,
        ...props
      })
    : props.translate(message, {
        _: message,
        ...messageArgs
      });

export const getValueToCompare = (validationData, formValues) => {
  if (isObject(validationData)) {
    const { dependsOnValue, condition, message } = validationData;

    if (condition) {
      switch (condition) {
        case 'moreThanCurrentDate': {
          const current = moment()
            .startOf('day')
            .valueOf();

          return {
            value: current,
            message
          };
        }
        default: {
          return 0;
        }
      }
    }

    const dependentValue = get(formValues, dependsOnValue, null);

    return { value: dependentValue, message };
  }

  if (isString(validationData)) {
    const validationArr = validationData.split(':');

    return { value: validationArr[1] };
  }

  return {};
};

export const findValidatorType = (validationData) => {
  const [key] =
    typeof validationData === 'string'
      ? validationData.split(':')
      : [get(validationData, 'type', '')];

  return key;
};

export const isValidatorActive = (dependenciesRules, dependentFields) =>
  Object.keys(dependenciesRules).reduce((res, key) => {
    if (
      dependenciesRules[key] === '!null' &&
      (dependentFields[key] === undefined || dependentFields[key] === null)
    ) {
      return true;
    }

    return res && dependenciesRules[key] === dependentFields[key];
  }, true);

export const getDependsOn = (validateArray) =>
  validateArray.reduce((res, el) => {
    if (typeof el === 'string') {
      return res;
    }

    const { dependsOn } = el;

    return {
      ...res,
      ...dependsOn
    };
  }, {});

export const getDependsOnValues = (validateArray) =>
  validateArray.reduce((res, el) => {
    if (typeof el === 'string') {
      return res;
    }

    const { dependsOnValue } = el;

    if (dependsOnValue) {
      res.push(dependsOnValue);
    }

    return res;
  }, []);
