import { isEmpty } from 'lodash';
import config from '../config';

import englishMessages from '../i18n/en';
import frenchMessages from '../i18n/fr';

const { REACT_APP_KEY = 'admin', REACT_APP_LOCALE = 'en' } = process.env;

export default () => {
  const locale = REACT_APP_LOCALE;

  switch (locale) {
    case 'fr':
      return {
        ...frenchMessages,
        resources: {
          ...frenchMessages.resources,
          ...extractTranslations(locale)
        }
      };
    default:
      // Always fallback on english
      return {
        ...englishMessages,
        resources: {
          ...englishMessages.resources,
          ...extractTranslations(locale)
        }
      };
  }
};

const extractTranslations = (locale) => {
  const { modules = {} } = config[REACT_APP_KEY] || {};

  return Object.keys(modules).reduce((res, key) => {
    const { i18n = {} } = modules[key];

    if (!isEmpty(i18n)) {
      res[key] = i18n[locale];
    }

    return res;
  }, {});
};
