import { get } from 'lodash';

const ERRORS = {
  CONSTRAINT_VIOLATION_EXCEPTION: 'Duplicated row'
};

export const apiErrorHandler = (error, errorContainer) => {
  if (error.status === 403) {
    return Promise.reject(error.message);
  }

  const message =
    get(ERRORS, get(error, 'body.errorCode')) ||
    get(error, `body.${errorContainer}`);

  throw message || error;
};
