import {
  AUTH_LOGIN,
  AUTH_LOGOUT,
  AUTH_CHECK,
  AUTH_ERROR,
  AUTH_GET_PERMISSIONS
  // changeLocale
} from 'react-admin';
import { isEmpty } from 'lodash';
import store from '../store';

import { signIn, signOut, getToken, getTokenData } from '../utils/auth';

import { getUserPermissions } from '../utils/authorization';

const LOGIN_URL = '#/login';

export default async (type, params) => {
  // const { dispatch, getState } = store;
  const { getState } = store;

  switch (type) {
    case AUTH_LOGIN:
      const { username, password } = params;

      return signIn(username, password).then((token) => {
        const user = getTokenData(token);
        const { locale } = user;

        console.log('User locale: ', locale);
        // if (locale) {
        //   dispatch(changeLocale(locale));
        // }
      });
    case AUTH_LOGOUT:
      await signOut();

      return Promise.resolve();
    case AUTH_CHECK:
      const token = getToken();

      if (!token && window.location.hash !== LOGIN_URL) {
        window.stop();
        window.location.hash = LOGIN_URL;
        document.location.reload();

        return Promise.reject();
      }

      return (token && Promise.resolve()) || Promise.reject();
    case AUTH_ERROR:
      const { status } = params;

      if (status === 401 || status === 403) {
        await signOut();

        return Promise.reject();
      }

      return Promise.resolve();
    case AUTH_GET_PERMISSIONS:
      const { user = {} } = getState();
      const userPermissions = getUserPermissions(user) || [];
      if (!userPermissions) {
        return Promise.reject();
      }

      return !isEmpty(userPermissions)
        ? Promise.resolve(userPermissions)
        : Promise.reject();
    default:
      return Promise.reject();
  }
};
