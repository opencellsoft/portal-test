import PolyfillHeaders from 'fetch-headers';
import { getToken } from '../utils/auth';

if (global.Headers == null) {
  global.Headers = PolyfillHeaders;
}

const defaultOptions = {
  headers: new Headers({
    Accept: 'application/json'
  })
};

const handleFiles = (res, options = {}) =>
  res.blob().then((b) => {
    const link = document.createElement('a');

    link.href = URL.createObjectURL(b);
    link.setAttribute('download', options.filename);
    link.click();
  });

/**
 * @TODO
 * Remove this file
 * Requests should be executed with a verb and a data provider
 */
export const sendRequest = (url, options = {}, fileOptions = {}) => {
  const reqOptions = { ...defaultOptions, ...options };

  const token = getToken();
  reqOptions.headers.set('Authorization', `Bearer ${token}`);

  return fetch(url, reqOptions).then((res) => {
    if (!res.ok) {
      return false;
    }

    if (fileOptions.filename) {
      return handleFiles(res, fileOptions);
    }

    return res.json();
  });
};
