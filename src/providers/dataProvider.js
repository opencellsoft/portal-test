import {
  fetchUtils,
  GET_LIST,
  GET_ONE,
  GET_MANY,
  GET_MANY_REFERENCE
} from 'react-admin';
import { get, set, isEmpty } from 'lodash';

import dynamicAppProperties from '../dynamicAppProperties';

import { getToken } from '../utils/auth';

import genericProvider from './genericProvider';

import { DEFAULT_API_CALL } from '../constants/generic';
import { RESOURCE_ENTITY_CUSTOMIZATION } from '../config/resources';
import store from '../store';
import { hydrateUrlFromParams, processParams } from '../utils/generic-render';
import KeyCache from '../utils/key-cache';
import { utf8ToB64, b64ToUtf8 } from '../utils/b64';
import { apiErrorHandler } from './apiErrorHandler';

const { REACT_APP_DOMAIN_URL } = dynamicAppProperties || {};

const { fetchJson } = fetchUtils;

const REQUEST_CACHE_TIMEOUT = 1200;
const staticCache = new KeyCache(REQUEST_CACHE_TIMEOUT);

/**
 * Query a data provider and return a promise for a response
 *
 * @param {string} type Request type, e.g GET_LIST
 * @param {string} resource Resource name, e.g. "posts"
 * @param {Object} payload Request parameters. Depends on the action type
 * @returns {Promise} the Promise for a response
 */
export default async (type, resource, params) => {
  const state = store.getState();
  const { modules } = get(state, 'config.config', {});
  const genericDefinition = get(modules, `${resource}.provider`, {});
  const entity = get(modules, `${resource}.entity`, {});

  const entityCustomizations = get(
    state,
    `admin.resources.${RESOURCE_ENTITY_CUSTOMIZATION}.data`,
    {}
  );

  const handler =
    !isEmpty(genericDefinition[type]) &&
    getGenericHandler(
      type,
      resource,
      params,
      modules,
      get(entityCustomizations, entity)
    );

  if (!handler) {
    return Promise.reject(
      new Error(`Missing handler for ${type} of ${resource}`)
    );
  }

  const handlerConfig = handler();
  const queries = Array.isArray(handlerConfig)
    ? handlerConfig
    : [handlerConfig];

  const sync =
    !isEmpty(genericDefinition[type]) && genericDefinition[type].sync;

  let res = null;
  if (sync && queries.length > 1) {
    for (const query of queries) {
      const shouldProcessUrlFromResponse = get(query, 'url').includes(':@res');
      const requestBody = get(query, 'options.body', '');
      const shouldProcessBodyFromResponse = requestBody.includes('@res');

      const processedUrl =
        shouldProcessUrlFromResponse &&
        !isEmpty(res) &&
        hydrateUrlFromParams({
          url: query.url,
          params: { res: res.data }
        });

      const processedBody =
        shouldProcessBodyFromResponse &&
        !isEmpty(res) &&
        processParams({
          params: JSON.parse(requestBody),
          source: { res: res.data }
        });

      const handler = { ...query };

      if (processedUrl) {
        handler.url = processedUrl;
      }

      if (processedBody) {
        set(handler, 'options.body', JSON.stringify(processedBody));
      }

      // eslint-disable-next-line no-await-in-loop
      res = await handleFetch({ type, resource, params, handler });
    }

    return res;
  }

  const [mainResponse] = await Promise.all(
    queries.map((handler) => handleFetch({ type, resource, params, handler }))
  );

  return mainResponse;
};

const handleFetch = ({ type, resource, params, handler }) => {
  const { url, options = {}, resolve, postRequest = {} } = handler || {};
  if (!url) {
    return Promise.reject(new Error(`Missing url for ${type} of ${resource}`));
  }

  if (!resolve) {
    return Promise.reject(
      new Error(`Missing resolver for ${type} of ${resource}`)
    );
  }

  const key = utf8ToB64(
    JSON.stringify(utf8ToB64(JSON.stringify({ type, url })))
  );
  const cacheID = { namespace: resource, key };

  switch (type) {
    case GET_ONE:
    case 'GET_ONE_CUSTOM':
    case GET_LIST:
    case GET_MANY:
    case GET_MANY_REFERENCE:
      if (staticCache.hasCache(cacheID)) {
        console.table('FROM CACHE', cacheID);

        return JSON.parse(b64ToUtf8(staticCache.getCache(cacheID)));
      }

      break;
    default:
      staticCache.removeNamespaceCache(resource);
  }

  const { headers, errorContainer = 'message', ...cleanOptions } = options;
  const defaultOptions = {
    mode: 'cors',
    headers: new Headers({
      Accept: 'application/json',
      ...headers
    })
  };

  const token = getToken();
  defaultOptions.headers.set('Authorization', `Bearer ${token}`);

  const finalUrl = url.includes('http')
    ? cleanUrl(url)
    : `${REACT_APP_DOMAIN_URL}/opencell/api/rest/${cleanUrl(url)}`;

  return fetchJson(finalUrl, {
    ...defaultOptions,
    ...cleanOptions
  })
    .then((response) => resolve(response, params))
    .then((res) => {
      if (isEmpty(postRequest)) {
        staticCache.setCache({
          ...cacheID,
          value: utf8ToB64(JSON.stringify(res))
        });

        return res;
      }

      return fetchJson(
        `${REACT_APP_DOMAIN_URL}/opencell/api/rest/${cleanUrl(
          postRequest.url
        )}`,
        {
          ...defaultOptions,
          ...postRequest.options
        }
      ).then(() => {
        staticCache.setCache({
          ...cacheID,
          value: utf8ToB64(JSON.stringify(res))
        });

        return res;
      });
    })
    .catch((error) => {
      apiErrorHandler(error, errorContainer);
    });
};

const getGenericHandler = (
  type,
  resource,
  params,
  modules,
  entityCustomizations
) => {
  const genericDefinition = modules[resource];
  const { provider, schema } = genericDefinition || {};

  if (!provider || (provider && !provider[type])) {
    return null;
  }

  const handler = genericProvider[type] || genericProvider[DEFAULT_API_CALL];

  if (!handler) {
    return null;
  }

  return () =>
    handler(params, { ...provider[type], schema }, entityCustomizations);
};

const cleanUrl = (url) => (url.endsWith('/') ? url.slice(0, -1) : url);
