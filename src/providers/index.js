import authProvider from './authProvider';
import dataProvider from './dataProvider';
import i18nProvider from './i18nProvider';

export default {
    authProvider,
    dataProvider,
    i18nProvider
};
