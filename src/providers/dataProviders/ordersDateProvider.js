import { GET_LIST, GET_MANY } from 'react-admin';
import moment from 'moment-timezone';
import get from 'lodash/get';
import uniqBy from 'lodash/uniqBy';

/**
 * List OrderDate
 *
 * @param {object} params
 */
const listOrderDate = (params) => ({
  url: `orderManagement/productOrder`,
  resolve: (params) => {
    const result = uniqBy(
      (get(params, 'json', []) || []).map(({ orderDate }) => {
        orderDate = moment(orderDate).format('DD-MM-YYYY');

        return {
          orderDate,
          id: orderDate
        };
      }),
      'id'
    );

    return {
      data: result,
      total: result.length
    };
  }
});

/**
 * List OrderDate
 *
 * @param {object} params
 */
const getManyOrderDate = () => ({
  url: `orderManagement/productOrder`,
  resolve: (params) => {
    const result = uniqBy(
      (get(params, 'json', []) || []).map(({ orderDate }) => {
        orderDate = moment(orderDate).format('DD-MM-YYYY');

        return {
          orderDate,
          id: orderDate
        };
      }),
      'id'
    );

    return {
      data: result,
      total: result.length
    };
  }
});

export default {
  [GET_LIST]: listOrderDate,
  [GET_MANY]: getManyOrderDate
};
