import {
  GET_LIST,
  GET_ONE,
  GET_MANY,
  CREATE,
  UPDATE,
  DELETE,
  DELETE_MANY
} from 'react-admin';
import { get, isEmpty } from 'lodash';
import moment from 'moment-timezone';

import queryBuilder from '../genericProvider/queryBuilder';
import {
  rebuildRecord,
  reapplySchema,
  hydrateParams,
  transformFromSchema
} from '../genericProvider/transformer';

const esEndpoints = ['filteredList/fullSearch', 'filteredList/search'];

/**
 * List Orders
 *
 * @param {object} params
 */
const listOrders = (params, providerConfig) => {
  const { url = '', params: staticParams, keyColumn = 'id', schema } =
    providerConfig || {};
  const esEnabled = esEndpoints.includes(url);
  const { query, page, perPage } = queryBuilder[GET_LIST](
    params,
    staticParams,
    { esEnabled }
  );
  const {
    filter: { state = 'Pending' }
  } = params;

  return {
    url: `orderManagement/productOrder?${query}`,
    resolve: (response) => {
      const data = get(response, 'json', [])
        .reduce(
          (res, el) =>
            el.state === state
              ? [...res, transformFromSchema(el, schema, keyColumn)]
              : res,
          []
        )
        .sort(
          ({ orderDate: orderDateA }, { orderDate: orderDateB }) =>
            -moment.utc(orderDateA).diff(moment.utc(orderDateB))
        );

      const total = data.length;
      const result = data.splice((page - 1) * perPage, perPage);

      return {
        data: result,
        total
      };
    }
  };
};

/**
 * Get Order
 *
 * @param {object} params
 */
const getOrder = ({ id }, providerConfig) => {
  const { keyColumn = 'id', schema } = providerConfig || {};

  return {
    url: `orderManagement/productOrder/${id}`,
    resolve: ({ json }) => ({
      data: transformFromSchema(json, schema, keyColumn)
    })
  };
};

/**
 * Update Order
 */
const updateOrder = (
  { data: { id, __originalData, __cf, __cfDefinition, __isDelete, ...data } },
  providerConfig
) => {
  const { url, options, schema } = providerConfig || {};
  const formattedData = !isEmpty(__cf)
    ? rebuildRecord(data, __cf, __cfDefinition, __isDelete)
    : data;

  let dataForUpdate = { ...__originalData, ...formattedData };

  if (!isEmpty(schema)) {
    dataForUpdate = reapplySchema(
      dataForUpdate,
      schema,
      __cfDefinition,
      !!__cf
    );
  }

  const { url: hydratedUrl, params: filteredParams } = hydrateParams(url);

  return {
    url: `${hydratedUrl}${filteredParams}`,
    options: {
      method: 'POST',
      body: JSON.stringify(dataForUpdate),
      ...options
    },
    resolve: () => ({ data: { ...data, id } })
  };
};

/**
 * Create Order
 *
 * @param {object} params
 */
const createOrder = ({ data }) => ({
  url: `inbound/order/place`,
  options: {
    method: 'POST',
    body: JSON.stringify(data)
  },
  headers: {
    'Content-Type': 'application/xml'
  },
  resolve: (json) => ({ data: { ...data, id: data.id } })
});

/**
 * Delete order
 *
 * @param {object} params
 */
const deleteOrder = ({ id }) => ({
  url: `orderManagement/productOrder/${id}`,
  options: {
    method: 'DELETE'
  },
  resolve: (params) => ({
    data: { ...params.data, id: params.id }
  })
});

/**
 * Delete many orders
 *
 * @param {object} params
 */
const deleteManyOrders = ({ ids }) => ({
  url: `orderManagement/productOrder/${ids}`,
  options: {
    method: 'DELETE'
  }
});

/**
 * List Orders
 *
 * @param {object} params
 */
const getManyOrders = (params, providerConfig) => {
  const { url = '', params: staticParams, keyColumn = 'id', schema } =
    providerConfig || {};
  const esEnabled = esEndpoints.includes(url);
  const { query, page, perPage } = queryBuilder[GET_MANY](
    params,
    staticParams,
    { keyColumn, esEnabled }
  );

  return {
    url: `orderManagement/productOrder?${query}`,
    resolve: (response) => {
      const data = get(response, 'json', [])
        .map((el) => transformFromSchema(el, schema, keyColumn))
        .sort(
          ({ orderDate: orderDateA }, { orderDate: orderDateB }) =>
            -moment.utc(orderDateA).diff(moment.utc(orderDateB))
        );

      const total = data.length;
      const result = data.splice((page - 1) * perPage, perPage);

      return {
        data: result,
        total
      };
    }
  };
};

export default {
  [GET_LIST]: listOrders,
  [GET_ONE]: getOrder,
  [CREATE]: createOrder,
  [UPDATE]: updateOrder,
  [DELETE]: deleteOrder,
  [DELETE_MANY]: deleteManyOrders,
  [GET_MANY]: getManyOrders
};
