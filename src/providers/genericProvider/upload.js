export const handleFilesBody = ({ file, path, fileFormat }) => {
  const formData = new FormData();

  formData.append('filename', path ? `${path}/${file.name}` : `${file.name}`);
  formData.append('uploadedFile', file);
  if (fileFormat) {
    formData.append('fileFormat', fileFormat);
  }

  return formData;
};
