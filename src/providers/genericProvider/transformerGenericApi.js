import { get, has, isEmpty, isUndefined, set, omit } from 'lodash';
import {
  extractMatrixDefinition,
  serializeCFMatrix,
  addCFMatrix
} from '../../utils/custom-fields';

import {
  getTraversableSchema,
  getProvidedValue,
  getTraversableSchemaWithChild
} from './transformer';

import {
  CUSTOM_FIELD_MULTI,
  CUSTOM_FIELD_STRING,
  CUSTOM_FIELD_BOOLEAN,
  CUSTOM_FIELD_DATE
} from '../../constants/generic';

export const extractResponse = ({
  json,
  body,
  responseContainer = '',
  nestedContainer,
  serialized,
  genericAPIEnabled,
  isFile,
  keyColumn = 'id',
  schema,
  page,
  perPage,
  isList,
  entityCustomizations
}) => {
  const rawData = json ? get(json, 'data', json) || [] : body;

  if (isFile) {
    return { data: rawData };
  }
  let formattedData = serialized ? JSON.parse(rawData) : rawData;

  if (!isList && !Array.isArray(formattedData)) {
    formattedData = nestedContainer
      ? get(formattedData, nestedContainer)
      : formattedData;

    return {
      data: transformFromSchema(
        formattedData,
        schema,
        keyColumn,
        entityCustomizations
      )
    };
  }

  let total;
  if (isList) {
    ({ total } = json);

    if (!total) {
      formattedData = [];
    }
  }

  formattedData = nestedContainer
    ? formattedData.map(({ [nestedContainer]: nestedData }) => ({
        ...nestedData
      }))
    : formattedData;

  const data =
    !isUndefined(page) &&
    !isUndefined(perPage) &&
    formattedData.length > perPage
      ? formattedData.splice((page - 1) * perPage, perPage)
      : formattedData;

  return {
    total,
    data: data.map((data) =>
      transformFromSchema(
        data,
        schema,
        !keyColumn ? 'id' : keyColumn,
        entityCustomizations
      )
    )
  };
};

/**
 * Transform an entity using a schema being provided from config
 *
 * @param {object} data
 * @param {object} schema
 */
export const transformFromSchema = (
  data,
  schema = {},
  keyColumn = 'code',
  entityCustomizations
) => {
  const { [keyColumn]: identifier, cfValues, ...rest } = data;
  const { valuesByCode } = cfValues || {};

  if (isEmpty(schema)) {
    return {
      ...rest,
      id: identifier,
      [keyColumn]: identifier,
      customFields: valuesByCode
    };
  }

  return {
    ...data,
    ...performTransform(
      {
        id: identifier
      },
      schema,
      data,
      entityCustomizations
    )
  };
};

const performTransform = (record, schema, data, entityCustomizations) => {
  Object.keys(schema).forEach((source) => {
    const target = schema[source];
    const hasData = has(data, source);

    if (hasData) {
      let possibleData = get(data, source);

      if (typeof target === 'object' && possibleData) {
        possibleData = possibleData.map((el) =>
          transformFromSchema(el, target, 'id')
        );
        data[source] = possibleData;
      } else if (target.indexOf(',') > -1) {
        const targets = target.split(',');
        targets.forEach((element) => {
          record[element] = possibleData;
        });
      } else {
        record[target] = possibleData;
      }

      return;
    }

    const isSimpleType =
      source.startsWith('CF.STRING') ||
      source.startsWith('CF.DATE') ||
      source.startsWith('CF.BOOLEAN');

    if (source.startsWith('CF.MULTI_VALUE')) {
      record[target] = readCFValueFromMap(data, source, entityCustomizations);
    } else if (isSimpleType) {
      record[target] = readCFValueFromSimpleType(data, source);
    } else if (Array.isArray(target)) {
      const [actualSource, mappedTarget] = source.split(':');

      record[actualSource] = readValuesFromArray(
        data,
        target,
        mappedTarget,
        entityCustomizations
      );
    } else if (source.includes('[]') && target.includes('[]')) {
      // @todo only handle one level of nesting for now
      const [arrayPath, nestedProperty] = source.split('[].');
      const dataContainer = get(data, arrayPath) || [];

      const cleanTarget = target.substring(arrayPath.length + '[].'.length);
      const [nextTarget, fieldToAdd] = cleanTarget.split('[]->');
      const [fieldName, fieldValue] = fieldToAdd.split(':');

      const alteredData = dataContainer.map(
        ({ [nestedProperty]: nestedData, ...rest }, index) => {
          const existingDataContainer = get(record, arrayPath) || [];
          const existingData =
            get(existingDataContainer, `[${index}].${nextTarget}`) || [];

          return {
            ...rest,
            [nextTarget]: [
              ...existingData,
              ...nestedData.map((el) => ({
                ...el,
                [fieldName]: fieldValue
              }))
            ]
          };
        }
      );

      set(record, arrayPath, alteredData);
    }
  });

  return record;
};

const formatMapField = (customField, entityCustomizations) => {
  const { value: mapValue } = customField || [];

  if (isEmpty(mapValue)) {
    return { data: [customField] };
  }

  const formattedData = Object.keys(mapValue).map((key) => {
    const value = mapValue[key] || {};
    const { keyColumns, valueColumns } = entityCustomizations || {};
    if (!value) {
      return null;
    }

    const keys = key.split('|');

    const values = value.split('|');

    const transformedValues = values.reduce(
      (res, label, index) => ({
        ...res,
        [`${valueColumns[index]}`]: values[index]
      }),
      {}
    );

    const transformedKeys = keys.reduce(
      (res, label, index) => ({
        ...res,
        [`${keyColumns[index]}`]: keys[index]
      }),
      {}
    );

    return { ...transformedKeys, ...transformedValues };
  });

  return {
    data: formattedData
  };
};

const getCustomFieldBySource = (data, source) => {
  const customFields = get(data, `cfValues.valuesByCode`, []);

  const [, , cfCode] = source.split('.');

  return get(customFields, cfCode, []);
};

/**
 * Read value from CF of type Map
 * @param {object} data
 * @param {string} source
 */
const readCFValueFromMap = (data, source, entityCustomizations) => {
  let customField = getCustomFieldBySource(data, source);
  const [, , cfCode, nestedField] = source.split('.');

  const entityData = extractMatrixDefinition(entityCustomizations, cfCode);
  customField = !Array.isArray(customField) ? [customField] : customField;

  return customField.reduce((res, field) => {
    if (!field) return null;
    const { data: fieldData } = formatMapField(field, entityData);
    if (nestedField) {
      return fieldData.find((el) => el)[nestedField];
    }

    return [...res, ...fieldData];
  }, []);
};

/**
 * Read value from a CF of common type
 * simple type - string/boolean/date
 * @param {*} data
 * @param {*} source
 */
const readCFValueFromSimpleType = (data, source) => {
  const customField = getCustomFieldBySource(data, source);

  if (!isEmpty(customField)) {
    return customField.find((el) => el).value;
  }

  return null;
};

/**
 * Read sub items
 * @param {*} data
 * @param {*} schema
 */
const readValuesFromArray = (
  data,
  schema,
  mappedTarget,
  entityCustomizations
) => {
  const items =
    (mappedTarget && mappedTarget.startsWith('CF')
      ? readCFValueFromMap(data, mappedTarget, entityCustomizations)
      : data[mappedTarget]) || [];

  return items.map((item) =>
    schema.reduce((res, definition) => {
      const [definitionKey] = Object.keys(definition);
      const childItems = definitionKey.split(':');
      const destination = definition[definitionKey];

      const hasChildren = childItems.length > 1;
      const sourceArray = definitionKey.split('.');
      const source = sourceArray[sourceArray.length - 1];

      if (hasChildren && source) {
        return addCFChildrenToParent(
          data,
          childItems,
          source,
          entityCustomizations,
          res
        );
      }

      return {
        ...res,
        [destination]: item[source]
      };
    }, {})
  );
};

const addCFChildrenToParent = (
  data,
  childItems,
  source,
  entityCustomizations,
  res
) => {
  const entityData = extractMatrixDefinition(entityCustomizations, source);
  let childData = readCFValueFromMap(
    data,
    childItems[childItems.length - 1],
    entityCustomizations
  );

  const parentKeys = entityData.keyColumns.reduce((keys, key) => {
    if (res[key]) {
      return {
        ...keys,
        [key]: res[key]
      };
    }

    return {
      ...keys
    };
  }, {});

  Object.entries(parentKeys).forEach((parentKey) => {
    const [key, value] = parentKey;
    childData = childData.filter((child) => child[key] === value);
  });

  return {
    ...res,
    [childItems[0]]: childData
  };
};

export const reapplySchema = (
  record,
  schema,
  cfDefinition,
  { hasCFsUpdated }
) => {
  if (isEmpty(cfDefinition) || hasCFsUpdated) {
    const injectedProperties = Object.values(schema);

    return omit(record, injectedProperties);
  }

  const traversableSchema = getTraversableSchema(schema);
  const expandedTraversableSchemaWithChild = getTraversableSchemaWithChild(
    traversableSchema
  );

  const customFields = get(record, 'cfValues.valuesByCode', {});

  return Object.keys(expandedTraversableSchemaWithChild).reduce(
    (res, source) =>
      updateCFValuesWithDataToUpdate(
        res,
        cfDefinition,
        expandedTraversableSchemaWithChild,
        customFields,
        source
      ),
    record
  );
};

const updateCFValuesWithDataToUpdate = (
  res,
  cfDefinition,
  fullTraversableSchema,
  customFields,
  source
) => {
  const rawTarget = fullTraversableSchema[source];

  let target = rawTarget;
  if (Array.isArray(rawTarget)) {
    [target] = source.split(':');
  }

  // if elemnt have children
  const childCfItem = Object.values(rawTarget).find(
    (elm) =>
      !Object.keys(elm)[0].startsWith('CF') && Object.keys(elm)[0].includes(':')
  );

  if (childCfItem) {
    const { [target]: value } = res || {};
    const key = Object.keys(childCfItem)[0].split(':')[0];
    const childValuesObject = value.reduce(
      (values, val) => [...values, ...val[key]],
      []
    );
    updateCFValuesWithDataToUpdate(
      { [key]: childValuesObject },
      cfDefinition,
      fullTraversableSchema,
      customFields,
      Object.keys(childCfItem)[0]
    );
  }

  const { [target]: value, ...rest } = res || {};
  const [, fieldType, cfCode, initialKey] = source.split('.');

  if (source.includes('CF.')) {
    const relevantCFIndex = customFields.hasOwnProperty(cfCode);

    const providedValue = getProvidedValue({ fieldType, value, initialKey });

    const fieldDefinition = get(cfDefinition, 'field', []).find(
      (item) => item.code === cfCode
    );

    if (relevantCFIndex) {
      customFields[cfCode] =
        fieldType === CUSTOM_FIELD_MULTI
          ? updateCFMatrix(
              customFields[cfCode],
              providedValue || {},
              fieldDefinition
            )
          : setCFValue(customFields[cfCode], providedValue);
    } else if (AddCFHandlers[fieldType]) {
      customFields[cfCode] = AddCFHandlers[fieldType](
        providedValue,
        fieldDefinition
      );
    }
  }

  if (isEmpty(customFields)) {
    return rest;
  }

  return {
    ...rest,
    cfValues: {
      valuesByCode: customFields
    }
  };
};

export const updateCFMatrix = (
  cf,
  updatedData = {},
  fieldDefinition,
  isDelete
) => {
  const { value = [] } = cf[0];
  const { create } = updatedData || {};

  const nextMatrix = [
    {
      ...cf[0],
      value: Object.keys(value).reduce(
        (res, cfKey, index) => ({
          ...res,
          ...serializeCFMatrix(
            cfKey,
            value[cfKey],
            updatedData[index],
            fieldDefinition,
            isDelete
          )
        }),
        {}
      )
    }
  ];

  return !isEmpty(create)
    ? {
        ...nextMatrix,
        value: {
          ...nextMatrix.value,
          ...AddCFHandlers[CUSTOM_FIELD_MULTI](create, fieldDefinition)
        }
      }
    : nextMatrix;
};

export const addCFSimpleType = (data, currentCFDefinition) => {
  if (isEmpty(currentCFDefinition)) {
    return {};
  }

  const { fieldType } = currentCFDefinition;
  const isBoolean = fieldType === CUSTOM_FIELD_BOOLEAN;

  return [
    {
      value: isBoolean ? !!data : !data ? '' : data
    }
  ];
};

export const AddCFHandlers = {
  [CUSTOM_FIELD_MULTI]: addCFMatrix,
  [CUSTOM_FIELD_STRING]: addCFSimpleType,
  [CUSTOM_FIELD_BOOLEAN]: addCFSimpleType,
  [CUSTOM_FIELD_DATE]: addCFSimpleType
};

const setCFValue = (cf, value) => {
  const priority = get(cf[0], 'priority', 0);

  return [{ priority, value }];
};
