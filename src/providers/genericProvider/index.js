import {
  GET_LIST,
  GET_ONE,
  GET_MANY,
  GET_MANY_REFERENCE,
  CREATE,
  UPDATE,
  DELETE,
  DELETE_MANY
} from 'react-admin';
import { stringify, parse } from 'query-string';
import { get, isEmpty, isNil, omit, merge } from 'lodash';
import { processParams } from '../../utils/generic-render';
import {
  DEFAULT_API_CALL,
  API_GENERIC,
  API_STANDARD
} from '../../constants/generic';

import queryBuilder from './queryBuilder';
import {
  formatESResponse,
  extractResponse,
  rebuildRecord,
  reapplySchema,
  hasURLPlaceholder,
  hydrateParams,
  trimObject,
  handleRenamedProperties,
  getDeletedCF,
  handleWrappedProperties,
  transformFromSchema,
  applyTransform,
  transformCustomFields,
  removeSchemaValues,
  handleRequestProperties,
  transformCustomTable,
  transformWildcardToNull,
  getDeleteRequestParams
} from './transformer';

import {
  extractResponse as extractResponseGenericApi,
  reapplySchema as reapplySchemaGenericApi
} from './transformerGenericApi';
import { handleFilesBody } from './upload';
import { clearUnusedCFs } from '../../utils/custom-fields';

const apiComponent = {
  [API_GENERIC]: ({ response, ...params }) =>
    extractResponseGenericApi({
      ...response,
      ...params
    }),
  [API_STANDARD]: ({ response, ...params }) =>
    extractResponse({
      ...response,
      ...params
    })
};

const esEndpoints = ['filteredList/fullSearch', 'filteredList/search'];
const esFilterEndpoints = 'filteredList/searchByField';
const genericAPIEndpoints = 'v2/generic';

const genericAPIPropertiesToExclude = [
  'auditable',
  'descriptionAndCode',
  'descriptionOrCode',
  'referenceCode',
  'referenceDescription',
  'version',
  'uuid',
  'cfValuesNullSafe',
  'cfAccumulatedValuesNullSafe'
];

/**
 * List Entities
 *
 * @param {object} params
 * @param {object} providerConfig
 */
const list = (params = {}, providerConfig, entityCustomizations) => {
  const {
    url = '',
    params: staticParams,
    defaultParams = {},
    responseContainer,
    totalContainer,
    nestedContainer,
    keyColumn,
    schema,
    references,
    options,
    customTableCode,
    nullToWildcard = {}
  } = providerConfig || {};

  const esEnabled = esEndpoints.includes(url);
  const genericAPIEnabled = url.includes(genericAPIEndpoints);
  const isPostRequest = get(options, 'method') === 'POST';
  const componentEnabled = genericAPIEnabled ? 'api_generic' : 'api_standard';

  const { query, page, perPage, filterQuery } = queryBuilder[GET_LIST](
    merge({ filter: { ...defaultParams } }, params),
    { ...staticParams },
    {
      ...options,
      esEnabled,
      genericAPIEnabled,
      customTableCode
    }
  );

  const resolve = apiComponent[componentEnabled];

  return {
    url:
      genericAPIEnabled || isPostRequest || customTableCode
        ? `${url}`
        : `${url}?${query}`,
    options:
      genericAPIEnabled || isPostRequest || customTableCode
        ? {
            method: 'POST',
            body: JSON.stringify(
              genericAPIEnabled || customTableCode || isPostRequest
                ? filterQuery
                : query
            ),
            ...options
          }
        : {
            method: 'GET',
            ...options
          },
    resolve: (response) =>
      esEnabled
        ? formatESResponse(response, { schema, keyColumn })
        : resolve({
            response,
            responseContainer,
            totalContainer,
            nestedContainer,
            genericAPIEnabled,
            keyColumn,
            schema,
            references,
            page,
            perPage,
            isList: true,
            entityCustomizations,
            nullToWildcard
          })
  };
};

/**
 * Get one entity
 *
 * @param {object} params
 * @param {object} providerConfig
 */
const getOne = (params = {}, providerConfig, entityCustomizations) => {
  const {
    url = '',
    responseContainer,
    nestedContainer,
    keyColumn,
    schema,
    customTableCode,
    options,
    nullToWildcard
  } = providerConfig || {};
  const { id } = params;
  const genericAPIEnabled = url.includes(genericAPIEndpoints);

  if (isNil(id)) {
    return null;
  }

  const componentEnabled = genericAPIEnabled ? 'api_generic' : 'api_standard';
  const resolve = apiComponent[componentEnabled];

  return {
    url: customTableCode ? url : `${url}${id}`,
    options:
      genericAPIEnabled || customTableCode
        ? {
            method: 'POST',
            body: JSON.stringify(customTableCode ? { filters: { id } } : {}),
            ...options
          }
        : {
            method: 'GET'
          },
    resolve: (response) => {
      const { data } = resolve({
        response,
        responseContainer,
        nestedContainer,
        keyColumn,
        genericAPIEnabled,
        schema,
        entityCustomizations,
        nullToWildcard
      });

      const finalData = customTableCode ? data[0] : data;
      const { id: key } = finalData;

      return {
        data: { ...finalData, id, key }
      };
    }
  };
};

/**
 * Get many entities
 *
 * @param {object} params
 * @param {object} providerConfig
 */
const getMany = (params = {}, providerConfig, entityCustomizations) => {
  const {
    url = '',
    params: staticParams,
    defaultParams = {},
    keyColumn,
    responseContainer,
    totalContainer,
    nestedContainer,
    schema,
    references,
    options,
    customTableCode
  } = providerConfig || {};

  const esEnabled = esEndpoints.includes(url);
  const genericAPIEnabled = url.includes(genericAPIEndpoints);
  const esFilterEnabled = url.includes(esFilterEndpoints);
  const { query, page, perPage, filterQuery } = queryBuilder[GET_MANY](
    merge({ filter: { ...defaultParams } }, params),
    staticParams,
    { keyColumn, esEnabled, genericAPIEnabled, esFilterEnabled }
  );
  const componentEnabled = genericAPIEnabled ? 'api_generic' : 'api_standard';
  const resolve = apiComponent[componentEnabled];

  return {
    url: genericAPIEnabled || customTableCode ? `${url}` : `${url}?${query}`,
    options:
      genericAPIEnabled || customTableCode
        ? {
            method: 'POST',
            body: JSON.stringify(filterQuery),
            ...options
          }
        : {
            method: 'GET'
          },
    resolve: (response) =>
      esEnabled
        ? formatESResponse(response, { schema, keyColumn })
        : resolve({
            response,
            responseContainer,
            totalContainer,
            nestedContainer,
            genericAPIEnabled,
            references,
            schema,
            page,
            perPage,
            isList: true,
            keyColumn,
            entityCustomizations
          })
  };
};

const getManyReference = (params = {}, providerConfig) => {
  const {
    url = '',
    params: staticParams,
    defaultParams = {},
    responseContainer,
    totalContainer,
    nestedContainer,
    keyColumn,
    schema,
    references,
    options
  } = providerConfig || {};
  const { id, filter } = params;
  const { __recordFilters } = filter;

  const isPost = get(options, 'method') === 'POST';
  const genericAPIEnabled = url.includes(genericAPIEndpoints);
  const { page, perPage, filterQuery } = queryBuilder[GET_MANY](
    merge({ filter: { ...defaultParams } }, params),
    { ...staticParams, ...__recordFilters },
    { keyColumn, genericAPIEnabled }
  );
  const componentEnabled = genericAPIEnabled ? 'api_generic' : 'api_standard';
  const resolve = apiComponent[componentEnabled];

  const injectId = hasURLPlaceholder(url) ? { id } : {};
  const { url: hydratedUrl, params: filteredParams } = hydrateParams(
    url,
    injectId
  );

  return {
    url: isPost ? hydratedUrl : `${hydratedUrl}${filteredParams}`,
    options: isPost
      ? {
          method: 'POST',
          body: JSON.stringify({ ...filterQuery, ...__recordFilters }),
          ...options
        }
      : {
          method: 'GET',
          ...options
        },
    resolve: (response) =>
      resolve({
        response,
        responseContainer,
        totalContainer,
        nestedContainer,
        genericAPIEnabled,
        keyColumn,
        schema,
        references,
        page,
        perPage,
        isList: true
      })
  };
};

/**
 * Create one entity
 *
 * @param {object} params
 * @param {object} providerConfig
 */
const createOne = (
  {
    data: {
      id,
      code,
      filterForm,
      __cf,
      __cfDefinition,
      __isDelete,
      __triggerValidation,
      ...data
    }
  },
  providerConfig
) => {
  const {
    url,
    options,
    bodyParams,
    appendBodyParamsToRecordValues,
    schema,
    shouldReapplySchema = true,
    excludeProperties,
    customTableCode,
    nestedBody,
    wildcardToNull
  } = providerConfig || {};
  const genericAPIEnabled = url.includes(genericAPIEndpoints);
  const identifier = id || code || '';

  const preparedData = !isEmpty(data.customFields)
    ? transformCustomFields(data, __cfDefinition)
    : data;

  const formattedData = !isEmpty(__cf)
    ? rebuildRecord({ ...data, code }, __cf, __cfDefinition, __isDelete)
    : {
        ...preparedData,
        code
      };

  const formattedDataWithBodyParams = appendBodyParamsToRecordValues
    ? {
        ...processParams({
          source: {},
          params: bodyParams
        }),
        ...formattedData
      }
    : formattedData;

  let dataForUpdate = formattedDataWithBodyParams;

  if (!isEmpty(schema)) {
    dataForUpdate = shouldReapplySchema
      ? reapplySchema(dataForUpdate, schema, __cfDefinition, {})
      : removeSchemaValues(dataForUpdate, schema);
  }

  if (Array.isArray(excludeProperties)) {
    dataForUpdate = omit(dataForUpdate, excludeProperties);
  }

  if (!isEmpty(wildcardToNull)) {
    dataForUpdate = transformWildcardToNull(dataForUpdate, wildcardToNull);
  }

  if (customTableCode) {
    dataForUpdate = transformCustomTable(
      dataForUpdate,
      customTableCode,
      nestedBody
    );
  }

  // trim the values
  dataForUpdate = trimObject(dataForUpdate);

  const defaultOptions = getDefaultOptions(
    dataForUpdate,
    bodyParams,
    !appendBodyParamsToRecordValues
  );

  const requestOptions = {
    ...defaultOptions,
    ...options
  };

  return {
    url,
    options: requestOptions,
    resolve: (res) => {
      const { id } = genericAPIEnabled ? res.json : { id: identifier.trim() };

      return { data: { ...data, id, code } };
    }
  };
};

/**
 * Update one entity
 *
 * @param {object} params
 * @param {object} providerConfig
 */
const updateOne = (
  {
    data: {
      id,
      key,
      entityType,
      filterForm,
      __cf = {},
      __cfDefinition,
      __isDelete,
      __triggerValidation,
      ...data
    }
  },
  providerConfig
) => {
  const {
    urls,
    url,
    options,
    appendBodyParamsToRecordValues,
    bodyParams,
    schema,
    shouldReapplySchema = true,
    excludeProperties,
    wrapProperties,
    renameProperties,
    keyColumn,
    customTableCode,
    nestedBody,
    wildcardToNull = {}
  } = providerConfig || {};

  const genericAPIEnabled = url && url.includes(genericAPIEndpoints);

  const preparedData = !isEmpty(data.customFields)
    ? transformCustomFields(data, __cfDefinition)
    : data;

  const formattedData = !isEmpty(__cf)
    ? rebuildRecord(preparedData, __cf, __cfDefinition, __isDelete)
    : preparedData;

  const formattedDataWithBodyParams = appendBodyParamsToRecordValues
    ? {
        ...formattedData,
        ...processParams({
          source: {},
          params: bodyParams
        })
      }
    : formattedData;

  // @todo check if we can omit __originalData
  // previously { ...__originalData, ...formattedData }
  let dataForUpdate = __cfDefinition
    ? clearUnusedCFs(formattedDataWithBodyParams, __cfDefinition) // <-
    : formattedDataWithBodyParams; // <-

  if (!isEmpty(schema)) {
    const reapplySchemaHandler = genericAPIEnabled
      ? reapplySchemaGenericApi
      : reapplySchema;
    dataForUpdate = shouldReapplySchema
      ? reapplySchemaHandler(dataForUpdate, schema, __cfDefinition, {
          hasCFsUpdated: !isEmpty(__cf),
          genericAPIEnabled
        })
      : removeSchemaValues(dataForUpdate, schema);
  }

  if (Array.isArray(excludeProperties)) {
    dataForUpdate = omit(dataForUpdate, excludeProperties);
  }

  if (Array.isArray(wrapProperties)) {
    dataForUpdate = handleWrappedProperties(dataForUpdate, wrapProperties);
  }

  if (Array.isArray(renameProperties)) {
    dataForUpdate = handleRenamedProperties(dataForUpdate, renameProperties);
  }

  if (!isEmpty(wildcardToNull)) {
    dataForUpdate = transformWildcardToNull(dataForUpdate, wildcardToNull);
  }

  if (customTableCode) {
    dataForUpdate = transformCustomTable(
      dataForUpdate,
      customTableCode,
      nestedBody,
      id
    );
  }

  // trim the values
  dataForUpdate = omit(dataForUpdate, genericAPIPropertiesToExclude);

  dataForUpdate = trimObject(dataForUpdate);

  if (urls && Array.isArray(urls)) {
    const promises = [];

    urls.forEach((url) => {
      const { method = 'PUT' } = url.options || {};
      if (method === 'POST') {
        const cfCode = Object.keys(__cf)[0];

        if (cfCode === url.params.type && __isDelete) {
          const { codeField } = url.params;
          const code = getDeletedCF(data, __cf, codeField);

          promises.push({
            url: url.path,
            options: {
              ...url.options,
              body: JSON.stringify({ code })
            },
            resolve: () => ({
              data: transformFromSchema(
                { ...dataForUpdate, id },
                schema,
                keyColumn
              )
            })
          });
        }
      } else {
        const injectId = hasURLPlaceholder(url.path) ? { id } : {};

        const { url: hydratedUrl, params: filteredParams } = hydrateParams(
          url.path,
          injectId
        );

        promises.push({
          url: `${hydratedUrl}${filteredParams}`,
          options: {
            method: 'POST',
            body: JSON.stringify(dataForUpdate),
            ...url.options
          },
          resolve: () => ({
            data: transformFromSchema(
              { ...dataForUpdate, id },
              schema,
              keyColumn
            )
          })
        });
      }
    });

    return promises;
  }

  const injectId = hasURLPlaceholder(url) ? { id } : {};
  const { url: hydratedUrl, params: filteredParams } = hydrateParams(
    url,
    injectId
  );

  const defaultOptions = getDefaultOptions(
    dataForUpdate,
    bodyParams,
    !appendBodyParamsToRecordValues
  );
  const requestOptions = {
    ...defaultOptions,
    ...options
  };

  return [
    {
      url: genericAPIEnabled
        ? `${hydratedUrl}`
        : `${hydratedUrl}/${filteredParams}`,
      options: requestOptions,
      resolve: () => ({
        data: transformFromSchema(
          { ...data, id },
          schema,
          genericAPIEnabled ? 'id' : keyColumn
        )
      })
    }
  ];
};

/**
 * Delete one entity
 *
 * @param {object} params
 * @param {object} providerConfig
 */
const deleteOne = ({ id }, providerConfig) => {
  const { urls, url, customTableCode } = providerConfig || {};
  if (urls && Array.isArray(urls)) {
    return urls.map((url) => handleDelete(id, url));
  }

  const { url: finalUrl, body: finalBody } = getDeleteRequestParams(url, id, {
    customTableCode,
    genericAPIEndpoints
  });

  return [
    {
      url: finalUrl,
      options: {
        method: 'DELETE',
        body: JSON.stringify(finalBody)
      },
      resolve: ({ data }) => ({
        data: { ...data, id }
      })
    }
  ];
};

/**
 * Delete many entities
 *
 * @param {object} params
 * @param {object} providerConfig
 */
const deleteMany = ({ ids }, providerConfig) => {
  const { urls, url, options, customTableCode } = providerConfig || {};

  if (urls && Array.isArray(urls)) {
    return urls.reduce(
      (res, url) => [
        ...res,
        ...ids.map((id) => handleDelete(id, url, () => ({ data: ids })))
      ],
      []
    );
  }

  return ids.map((id) => {
    const { url: finalUrl, body: finalBody } = getDeleteRequestParams(url, id, {
      customTableCode,
      genericAPIEndpoints
    });

    return {
      url: finalUrl,
      options: {
        method: 'DELETE',
        ...options,
        body: JSON.stringify(finalBody)
      },
      resolve: () => ({ data: ids })
    };
  });
};

/**
 *
 * @param {number} id
 * @param {object} url
 * @param {func} resolve
 */
const handleDelete = (id, url, resolve) => {
  const { method = 'DELETE' } = url.options || {};

  if (method === 'POST') {
    return {
      url: url.path,
      options: {
        ...url.options,
        body: JSON.stringify({ code: id, ...url.params })
      },
      resolve:
        resolve ||
        (({ data }) => ({
          data: { ...data, id }
        }))
    };
  }

  return {
    url: url.path.includes(genericAPIEndpoints)
      ? `${url.path}/${id}`
      : `${url.path}${id}`,
    options: {
      method: 'DELETE'
    },
    resolve:
      resolve ||
      (({ data }) => ({
        data: { ...data, id }
      }))
  };
};

/**
 * Default API Call
 *
 * @param {object} params
 * @param {object} providerConfig
 */
export const defaultAPICall = (params, providerConfig) => {
  const { url, urls, bodyParams, options, transform } = providerConfig || {};

  let requestData = applyTransform(params.data, transform);
  requestData = handleRequestProperties(requestData, providerConfig);

  const { __cfDefinition, ...rest } = requestData || {};
  if (urls && Array.isArray(urls)) {
    const promises = [];
    urls.forEach((url) => {
      const {
        bodyParams,
        options,
        customTableCode,
        preserveDataFromPreviousRequest
      } = url || {};

      let dataForUpdate = rest;
      if (customTableCode) {
        dataForUpdate = transformCustomTable(rest, customTableCode);
      }
      const defaultOptions = !isEmpty(bodyParams)
        ? getDefaultOptions(
            preserveDataFromPreviousRequest ? dataForUpdate : {},
            processParams({
              source: { record: params.record, data: params.data },
              params: bodyParams
            })
          )
        : getDefaultOptions(dataForUpdate, bodyParams);

      const requestOptions = {
        ...defaultOptions,
        ...options
      };

      const { url: hydratedUrl, params: filteredParams } =
        !url.path.includes(':@res') && hydrateParams(url.path, rest);

      const data = getDefaultCallApi(
        url,
        url.path.includes(':@res') ? url.path : hydratedUrl,
        requestOptions,
        filteredParams,
        defaultOptions
      );

      promises.push(data);
    });

    return promises;
  }

  const defaultOptions = !isEmpty(bodyParams)
    ? getDefaultOptions(
        rest,
        processParams({
          source: { record: params.record || {} },
          params: bodyParams
        })
      )
    : getDefaultOptions(rest, bodyParams);
  const requestOptions = {
    ...defaultOptions,
    ...options
  };

  const { url: hydratedUrl, params: filteredParams } = hydrateParams(
    url,
    isEmpty(rest) ? params : rest
  );

  return getDefaultCallApi(
    providerConfig,
    hydratedUrl,
    requestOptions,
    filteredParams,
    defaultOptions
  );
};

const getDefaultCallApi = (
  url,
  hydratedUrl,
  requestOptions,
  filteredParams,
  defaultOptions
) => {
  const {
    params: defaultParams = {},
    responseContainer,
    nestedContainer,
    serialized,
    isFile
  } = url || {};

  const normalizedFilteredParams =
    typeof filteredParams === 'string' ? parse(filteredParams) : filteredParams;
  const finalUrlParams =
    requestOptions.method === 'GET'
      ? stringify({
          ...defaultParams,
          ...normalizedFilteredParams
        })
      : '';

  return {
    url: `${hydratedUrl}${!!finalUrlParams ? `&${finalUrlParams}` : ``}`,
    options:
      requestOptions.method === 'GET' ? { method: 'GET' } : requestOptions,
    resolve: (data) =>
      extractResponse({
        ...data,
        responseContainer,
        nestedContainer,
        serialized,
        isFile
      })
  };
};

const getDefaultOptions = (
  data,
  bodyParams = {},
  appendBodyParamsToBody = true
) => {
  const { file = {}, ...formData } = data || {};
  const hasData = !isEmpty(data);
  const bodyParamsToAppend = appendBodyParamsToBody ? bodyParams : {};
  const hasBodyParams = !isEmpty(bodyParamsToAppend);

  const { rawFile } = file || {};
  const fileToUpload = rawFile || file;

  return {
    method: hasData ? 'POST' : 'GET',
    body:
      hasData && !!fileToUpload.name
        ? handleFilesBody({ file: fileToUpload, ...formData })
        : hasData
        ? typeof data === 'string'
          ? data
          : JSON.stringify({ ...formData, ...bodyParamsToAppend })
        : hasBodyParams
        ? JSON.stringify({ ...bodyParamsToAppend })
        : undefined
  };
};

export default {
  [GET_LIST]: list,
  [GET_ONE]: getOne,
  [GET_MANY]: getMany,
  [GET_MANY_REFERENCE]: getManyReference,
  [CREATE]: createOne,
  [UPDATE]: updateOne,
  [DELETE]: deleteOne,
  [DELETE_MANY]: deleteMany,
  [DEFAULT_API_CALL]: defaultAPICall
};
