import { GET_LIST, GET_MANY } from 'react-admin';
import queryBuilder from '../queryBuilder';

describe('Query builder', () => {
  const staticParams = {
    classnamesOrCetCodes: 'org.meveo.model.catalog.CalendarJoin',
    query: 'calendarType:PERIOD'
  };

  it('should favor static params even with the same keys for GET_LIST', () => {
    const res = queryBuilder[GET_LIST](
      {
        pagination: { page: 1, perPage: 25 },
        sort: { field: 'id', order: 'DESC' }
      },
      staticParams,
      {
        esEnabled: true,
        genericAPIEnabled: false
      }
    );

    expect(res).toEqual({
      filterQuery: '',
      page: 1,
      perPage: 25,
      query:
        'classnamesOrCetCodes=org.meveo.model.catalog.CalendarJoin&from=0&query=calendarType%3APERIOD&size=25&sortField=id&sortOrder=DESC'
    });
  });

  it('should favor static params even with the same keys for GET_MANY', () => {
    const res = queryBuilder[GET_MANY](
      {
        pagination: { page: 1, perPage: 25 },
        sort: { field: 'id', order: 'DESC' }
      },
      staticParams,
      {
        esEnabled: true,
        genericAPIEnabled: false
      }
    );

    expect(res).toEqual({
      query:
        'classnamesOrCetCodes=org.meveo.model.catalog.CalendarJoin&query=calendarType%3APERIOD'
    });
  });
});
