import { defaultAPICall } from '../index';

const providerConfigSingleUrl = {
  url: 'admin/files/upload',
  options: {
    method: 'POST'
  }
};

const providerConfigMultiUrls = {
  urls: [
    {
      path: 'admin/files/upload',
      options: {
        method: 'POST'
      }
    },
    {
      path: '../../inbound/flatFile/reprocess',
      options: {
        method: 'POST'
      }
    }
  ],
  sync: true
};

const providerConfigSingUrlWithBodyParams = {
  url: 'admin/files/upload',
  options: {
    method: 'POST'
  },
  bodyParams: {
    flatFileIds: '@record.id[]',
    fileFormat: '@record.fileFormat.code'
  }
};

const providerConfigMultiUrlsWithBodyParams = {
  urls: [
    {
      path: 'admin/files/upload',
      preserveDataFromPreviousRequest: true,
      options: {
        method: 'POST'
      },
      bodyParams: {
        flatFileIds: '@record.id[]',
        fileFormat: '@record.fileFormat.code'
      }
    },

    {
      path: '../../inbound/flatFile/reprocess',
      preserveDataFromPreviousRequest: true,
      options: {
        method: 'POST'
      },
      bodyParams: {
        flatFileIds: '@record.id[]',
        fileFormat: '@record.fileFormat.code'
      }
    }
  ],
  sync: true
};

const providerConfigMultiUrlsWithPreserveData = {
  urls: [
    {
      path: 'admin/files/upload',
      options: {
        method: 'POST'
      }
    },

    {
      path: '../../inbound/flatFile/reprocess',
      options: {
        method: 'POST'
      },
      preserveDataFromPreviousRequest: true,
      bodyParams: {
        flatFileIds: '@record.id[]',
        fileFormat: '@record.fileFormat.code'
      }
    }
  ],
  sync: true
};

const providerConfigMultiUrlsWithoutPreserveData = {
  urls: [
    {
      path: 'admin/files/upload',
      options: {
        method: 'POST'
      }
    },

    {
      path: '../../inbound/flatFile/reprocess',
      options: {
        method: 'POST'
      },
      preserveDataFromPreviousRequest: false,
      bodyParams: {
        flatFileIds: '@record.id[]',
        fileFormat: '@record.fileFormat.code'
      }
    }
  ],
  sync: true
};

const params = {
  data: {
    test: 'test'
  },
  record: {
    auditable: {
      created: 1566479204534,
      creator: 'opencell.customercare',
      lastModified: 1566479204534,
      lastUser: 'opencell.customercare'
    },
    code: 'Payment108',
    customFields: undefined,
    descriptionAndCode: 'Payment108',
    descriptionOrCode: 'Payment108',
    errorMessage: "line=1: Invalid 'payment' record at line 1",
    fileFormat: { id: 2, code: 'Payment' },
    id: 108,
    referenceCode: 'Payment108',
    status: 'BAD_FORMED',
    version: 1
  }
};

describe('Generic Provider', () => {
  describe('Default API Call', () => {
    it('should handle single url without params and bodyParams', () => {
      const res = defaultAPICall({}, providerConfigSingleUrl);

      expect(res).toEqual({
        options: { body: undefined, method: 'POST' },
        resolve: expect.any(Function),
        url: 'admin/files/upload'
      });
    });

    it('should handle single url with params', () => {
      const res = defaultAPICall(params, providerConfigSingleUrl);

      expect(res).toEqual({
        options: { body: '{"test":"test"}', method: 'POST' },
        resolve: expect.any(Function),
        url: 'admin/files/upload'
      });
    });

    it('should handle single url with bodyParams', () => {
      const params = {
        record: {
          auditable: {
            created: 1566479204534,
            creator: 'opencell.customercare',
            lastModified: 1566479204534,
            lastUser: 'opencell.customercare'
          },
          code: 'Payment108',
          customFields: undefined,
          descriptionAndCode: 'Payment108',
          descriptionOrCode: 'Payment108',
          errorMessage: "line=1: Invalid 'payment' record at line 1",
          fileFormat: { id: 2, code: 'Payment' },
          id: 108,
          referenceCode: 'Payment108',
          status: 'BAD_FORMED',
          version: 1
        }
      };

      const res = defaultAPICall(params, providerConfigSingUrlWithBodyParams);

      expect(res).toEqual({
        options: {
          body: expect.any(String),
          method: 'POST'
        },
        resolve: expect.any(Function),
        url: 'admin/files/upload'
      });

      const body = JSON.parse(res.options.body);
      expect(body).toHaveProperty('fileFormat', 'Payment');
      expect(body).toHaveProperty('flatFileIds', [108]);
    });

    it('should handle single url with bodyParams and params', () => {
      const res = defaultAPICall(params, providerConfigSingUrlWithBodyParams);

      expect(res).toEqual({
        options: {
          body: expect.any(String),
          method: 'POST'
        },
        resolve: expect.any(Function),
        url: 'admin/files/upload'
      });

      const body = JSON.parse(res.options.body);
      expect(body).toHaveProperty('fileFormat', 'Payment');
      expect(body).toHaveProperty('test', 'test');
      expect(body).toHaveProperty('flatFileIds', [108]);
    });

    it('should handle multiple urls without params and bodyParams', () => {
      const res = defaultAPICall({}, providerConfigMultiUrls);

      expect(res).toEqual([
        {
          options: { body: undefined, method: 'POST' },
          resolve: expect.any(Function),
          url: 'admin/files/upload'
        },
        {
          options: { body: undefined, method: 'POST' },
          resolve: expect.any(Function),
          url: '../../inbound/flatFile/reprocess'
        }
      ]);
    });

    it('should handle multiple urls with params', () => {
      const res = defaultAPICall(params, providerConfigMultiUrls);

      expect(res).toEqual([
        {
          options: { body: '{"test":"test"}', method: 'POST' },
          resolve: expect.any(Function),
          url: 'admin/files/upload'
        },
        {
          options: { body: '{"test":"test"}', method: 'POST' },
          resolve: expect.any(Function),
          url: '../../inbound/flatFile/reprocess'
        }
      ]);
    });

    it('should handle multiple urls with bodyParams  ', () => {
      const params = {
        record: {
          auditable: {
            created: 1566479204534,
            creator: 'opencell.customercare',
            lastModified: 1566479204534,
            lastUser: 'opencell.customercare'
          },
          code: 'Payment108',
          customFields: undefined,
          descriptionAndCode: 'Payment108',
          descriptionOrCode: 'Payment108',
          errorMessage: "line=1: Invalid 'payment' record at line 1",
          fileFormat: { id: 2, code: 'Payment' },
          id: 108,
          referenceCode: 'Payment108',
          status: 'BAD_FORMED',
          version: 1
        }
      };
      const res = defaultAPICall(params, providerConfigMultiUrlsWithBodyParams);

      expect(res).toEqual([
        {
          options: {
            body: expect.any(String),
            method: 'POST'
          },
          resolve: expect.any(Function),
          url: 'admin/files/upload'
        },
        {
          options: {
            body: expect.any(String),
            method: 'POST'
          },
          resolve: expect.any(Function),
          url: '../../inbound/flatFile/reprocess'
        }
      ]);

      const body1 = JSON.parse(res[0].options.body);
      expect(body1).toHaveProperty('fileFormat', 'Payment');
      expect(body1).toHaveProperty('flatFileIds', [108]);

      const body2 = JSON.parse(res[1].options.body);
      expect(body2).toHaveProperty('fileFormat', 'Payment');
      expect(body2).toHaveProperty('flatFileIds', [108]);
    });

    it('should handle multiple urls with bodyParams and params ', () => {
      const res = defaultAPICall(params, providerConfigMultiUrlsWithBodyParams);

      expect(res).toEqual([
        {
          options: {
            body: expect.any(String),
            method: 'POST'
          },
          resolve: expect.any(Function),
          url: 'admin/files/upload'
        },
        {
          options: {
            body: expect.any(String),
            method: 'POST'
          },
          resolve: expect.any(Function),
          url: '../../inbound/flatFile/reprocess'
        }
      ]);

      const body1 = JSON.parse(res[0].options.body);
      expect(body1).toHaveProperty('fileFormat', 'Payment');
      expect(body1).toHaveProperty('flatFileIds', [108]);
      expect(body1).toHaveProperty('test', 'test');

      const body2 = JSON.parse(res[1].options.body);
      expect(body2).toHaveProperty('fileFormat', 'Payment');
      expect(body2).toHaveProperty('flatFileIds', [108]);
      expect(body2).toHaveProperty('test', 'test');
    });

    it('should handle multiple urls one of them with (bodyParams and without params) another with (params without bodyParams)  ', () => {
      const res = defaultAPICall(
        params,
        providerConfigMultiUrlsWithoutPreserveData
      );

      expect(res).toEqual([
        {
          options: {
            body: expect.any(String),
            method: 'POST'
          },
          resolve: expect.any(Function),
          url: 'admin/files/upload'
        },
        {
          options: {
            body: expect.any(String),
            method: 'POST'
          },
          resolve: expect.any(Function),
          url: '../../inbound/flatFile/reprocess'
        }
      ]);

      const body1 = JSON.parse(res[0].options.body);
      expect(body1).toHaveProperty('test', 'test');

      const body2 = JSON.parse(res[1].options.body);
      expect(body2).toHaveProperty('fileFormat', 'Payment');
      expect(body2).toHaveProperty('flatFileIds', [108]);
    });

    it('should handle multiple urls one of them with(bodyParams and with params) another with (params without bodyParams)  ', () => {
      const res = defaultAPICall(
        params,
        providerConfigMultiUrlsWithPreserveData
      );

      expect(res).toEqual([
        {
          options: {
            body: expect.any(String),
            method: 'POST'
          },
          resolve: expect.any(Function),
          url: 'admin/files/upload'
        },
        {
          options: {
            body: expect.any(String),
            method: 'POST'
          },
          resolve: expect.any(Function),
          url: '../../inbound/flatFile/reprocess'
        }
      ]);

      const body1 = JSON.parse(res[0].options.body);
      expect(body1).toHaveProperty('test', 'test');

      const body2 = JSON.parse(res[1].options.body);
      expect(body2).toHaveProperty('fileFormat', 'Payment');
      expect(body2).toHaveProperty('flatFileIds', [108]);
      expect(body2).toHaveProperty('test', 'test');
    });

    it('should handle generic API', () => {
      const res = defaultAPICall(
        {},
        {
          url: 'v2/generic/all/FlatFile',
          options: {
            method: 'POST'
          },
          bodyParams: {
            genericFields: ['code']
          }
        }
      );

      expect(res).toEqual({
        options: {
          body: '{"genericFields":["code"]}',
          method: 'POST'
        },
        resolve: expect.any(Function),
        url: 'v2/generic/all/FlatFile'
      });
    });

    it('should handle default call API with Query', () => {
      const res = defaultAPICall(
        { id: 'Tax-Catergory' },
        {
          url: 'entityCustomization/entity/:id',
          responseContainer: 'customEntityTemplate'
        }
      );

      expect(res).toEqual({
        options: {
          method: 'GET'
        },
        resolve: expect.any(Function),
        url: 'entityCustomization/entity/Tax-Catergory'
      });
    });
  });
});
