import {
  handleWrappedProperties,
  convertReferencesToIDs
} from '../transformer';

describe('Generic Transformer', () => {
  const record = {
    id: 1,
    fileTypes: [
      {
        code: 'pmt',
        description: 'pmt',
        id: 3
      }
    ]
  };

  describe('Handle wrapped properties', () => {
    const wrappedProperties = ['fileTypes:fileTypes.serviceTemplate'];
    const res = handleWrappedProperties(record, wrappedProperties);

    it('should wrap properties if asked', () => {
      expect(res).toEqual({
        ...record,
        fileTypes: [
          {
            serviceTemplate: { ...record.fileTypes[0] }
          }
        ]
      });
    });

    it("shouldn't wrap properties twice", () => {
      const res2 = handleWrappedProperties(res, wrappedProperties);

      expect(res2).toEqual(res);
    });

    it('should leave record as is if no properties are found', () => {
      const record = { id: 1 };
      const res = handleWrappedProperties(record, wrappedProperties);

      expect(res).toEqual(record);
    });
  });

  describe('convertReferencesToIDs', () => {
    it('should convert references objects to arrays of IDs based on provided reference field', () => {
      // here reference field is -> id
      const references = ['fileTypes:id'];
      const res = convertReferencesToIDs(record, references);

      expect(res).toEqual({
        ...record,
        fileTypes: [3]
      });
    });

    it('should convert references objects to arrays of IDs with default reference field', () => {
      const references = ['fileTypes'];
      const res = convertReferencesToIDs(record, references);

      expect(res).toEqual({
        ...record,
        fileTypes: [3]
      });
    });
  });
});
