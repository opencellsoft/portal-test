import { GET_LIST, GET_MANY } from 'react-admin';
import { stringify } from 'query-string';
import { get, isEmpty, isNil, has, isString } from 'lodash';
import moment from 'moment-timezone';
import flatten, { unflatten } from 'flat';

import { sortBy } from '../../constants/filters';
import { hydrateStringFromParams } from '../../utils/generic-render';

/**
 * Build list query
 * @param {*} params
 */
const list = (params = {}, staticParams = {}, options = {}) => {
  const { page, perPage } = params.pagination || {};
  const { field = staticParams.defaultSortField || 'id', order } =
    params.sort || {};

  const {
    filter: {
      state,
      __filterParams = {},
      __recordFilters = {},
      ...queries
    } = {}
  } = params || {};
  const {
    genericFields = [],
    nestedEntities = [],
    ...remainingStaticParams
  } = staticParams;

  const {
    filters: requestFiltersParams,
    queries: remainingQueries
  } = hydrateFiltersFromQueries(queries, __filterParams);

  const {
    method,
    apiSeparator = '+',
    wildcardPrefix = '',
    queryKey = 'query',
    queryFormat = 'string',
    customTableCode,
    genericAPIEnabled,
    fields,
    wildcardPrefixes = {},
    esEnabled
  } = options;
  const isPostRequest = method === 'POST';

  const genericQueries =
    (genericAPIEnabled || customTableCode) &&
    Object.keys(queries).reduce(
      (res, key, index) =>
        key === 'q'
          ? {
              ...res,
              [`inList id`]: queries[key]
            }
          : {
              ...res,
              [key]: queries[key]
            },
      {}
    );

  let transformedField = field;

  if (fields && fields[field]) {
    transformedField = fields[field];
  }

  let filterQuery = getFilterQuery({
    genericAPIEnabled,
    genericFields,
    nestedEntities,
    remainingStaticParams,
    genericQueries,
    perPage,
    page,
    transformedField,
    order,
    customTableCode,
    wildcardPrefixes,
    wildcardPrefix,
    remainingQueries,
    queryKey,
    __recordFilters,
    __filterParams,
    esEnabled,
    apiSeparator
  });

  if (
    (
      (typeof filterQuery === 'string' &&
        filterQuery.match(/likeCriterias/g)) ||
      []
    ).length > 1
  ) {
    filterQuery = filterQuery
      .split('likeCriterias ')
      .filter((el) => el)
      .map((el) =>
        el.charAt(el.length - 1) === '+' ? el.substr(0, el.length - 1) : el
      )
      .join('');
  }

  let query;
  if (esEnabled) {
    query = {
      query: filterQuery || '*',
      ...(!isNil(perPage) ? { size: perPage } : {}),
      from: !isNil(page) && !isNil(perPage) ? (page - 1) * perPage : undefined,
      ...(!isNil(page) && !isNil(perPage)
        ? { from: (page - 1) * perPage }
        : {}),
      sortField: field === 'code' ? 'id' : field,
      sortOrder: order || 'DESC',
      ...staticParams
    };
  } else {
    query = {
      sortBy: transformedField,
      sortOrder: sortBy[order],
      ...(!isEmpty(fields) ? { fields } : {}),
      ...(!isNil(perPage) ? { limit: perPage } : {}),
      ...(!isNil(page) && !isNil(perPage)
        ? { offset: (page - 1) * perPage }
        : {}),
      ...(!isEmpty(filterQuery) && queryKey
        ? {
            [queryKey]:
              queryFormat === 'string'
                ? filterQuery
                : filterQuery.split(apiSeparator).reduce((res, token) => {
                    const [property, value] = token.split(':');

                    res[property] = value;

                    return res;
                  }, {})
          }
        : {}),
      // will ignore queryKey and add filters directly in url
      ...(!isEmpty(filterQuery) && !queryKey && queryFormat === 'string'
        ? filterQuery.split(apiSeparator).reduce((res, el) => {
            const [key, value] = el.split('=');

            res[key] = value;

            return res;
          }, {})
        : {}),
      ...staticParams,
      ...requestFiltersParams,
      ...__recordFilters
    };
  }

  query = !isPostRequest
    ? stringify(
        Object.keys(query).reduce((res, key) => {
          if (isEmpty(query[key]) && typeof query[key] !== 'number') {
            return res;
          }

          return {
            ...res,
            [key]: query[key]
          };
        }, {})
      )
    : query;

  return {
    query,
    filterQuery,
    page,
    perPage
  };
};

/**
 * Build list query
 * @param {*} params
 */
const getMany = (params, staticParams, options) => {
  const { ids = [] } = params;
  const { esEnabled, genericAPIEnabled, esFilterEnabled, keyColumn = 'code' } =
    options || {};
  const [firstEl] = ids;

  const workingSet = (Array.isArray(firstEl)
    ? ids.reduce((res, el) => [...res, ...el], [])
    : ids
  ).filter((el) => el);

  switch (true) {
    case esEnabled:
      return {
        query: stringify({
          query: workingSet
            .filter((id) => id)
            .map((id) => `${keyColumn}:${id}`)
            .join(' OR '),
          ...staticParams
        })
      };
    case genericAPIEnabled:
      const finalQuery = {
        filterQuery: {
          filters: {
            [`inList ${keyColumn}`]: workingSet
          }
        },
        ...staticParams
      };

      return finalQuery;

    case typeof workingSet[0] === 'object':
      return {
        query: stringify({
          ...workingSet[0],
          ...staticParams
        })
      };
    case esFilterEnabled:
      const fitlerTerms = `filter_terms ${keyColumn}=${workingSet
        .filter((id) => id)
        .map((id) => `${id}`)
        .join('|')}`;

      return {
        query: `${fitlerTerms}&${stringify({
          ...staticParams
        })} `
      };
    default:
      return {
        query: stringify({
          [keyColumn]: workingSet.join(','),
          ...staticParams
        })
      };
  }
};

const hydrateFilterShape = (filterShape, property, value) => {
  const flatShape = flatten(filterShape);

  if (typeof filterShape !== 'object') {
    return { [property]: filterShape.replace('@filter.value', value) };
  }

  Object.keys(flatShape).forEach((key) => {
    if (!flatShape[key].includes('@filter.value')) {
      return;
    }

    flatShape[key] = hydrateStringFromParams({
      string: flatShape[key],
      params: { filter: { value } }
    });
  });

  return unflatten(flatShape);
};

const formatFilterValue = (value, config) => {
  const { dateFormat } = config;

  switch (true) {
    case !!dateFormat:
      return moment(value).format(dateFormat);
    default:
      return value;
  }
};

const hydrateFiltersFromQueries = (values = {}, config = {}) => {
  const nextQueries = {};
  const flatValues = flatten(values);

  const nextFilters = Object.keys(flatValues).reduce((res, property) => {
    const propertyConfig = config[property] || {};
    const { container, shape } = propertyConfig;

    if (isEmpty(container) || isEmpty(shape)) {
      nextQueries[property] = flatValues[property];

      return res;
    }

    const value = formatFilterValue(flatValues[property], propertyConfig);
    const hydratedData = !isEmpty(shape)
      ? hydrateFilterShape(shape, property, value)
      : { [property]: value };

    res[container] = {
      ...res[container],
      ...hydratedData
    };

    return res;
  }, {});

  return { queries: nextQueries, filters: nextFilters };
};

const getFilterQuery = ({
  genericAPIEnabled,
  genericFields,
  nestedEntities,
  remainingStaticParams,
  genericQueries,
  perPage,
  page,
  transformedField,
  order,
  customTableCode,
  wildcardPrefixes,
  wildcardPrefix,
  remainingQueries,
  queryKey,
  __recordFilters,
  __filterParams,
  esEnabled,
  apiSeparator
}) => {
  switch (true) {
    case genericAPIEnabled: {
      return {
        genericFields,
        nestedEntities,
        filters: getFinalFilters(
          {
            ...remainingStaticParams,
            ...genericQueries,
            ...__recordFilters
          },
          { wildcardPrefixes, wildcardPrefix }
        ),
        limit: perPage,
        offset: (page - 1) * perPage,
        sortBy: transformedField,
        sortOrder: sortBy[order]
      };
    }
    case !!customTableCode: {
      return {
        filters: getFinalFilters(
          {
            ...remainingStaticParams,
            ...genericQueries,
            ...__recordFilters
          },
          { wildcardPrefixes, wildcardPrefix }
        ),
        limit: perPage,
        offset: (page - 1) * perPage,
        sortBy: transformedField,
        sortOrder: sortBy[order]
      };
    }
    default: {
      return Object.keys(remainingQueries)
        .map((key) => {
          if (
            __filterParams[key] &&
            typeof get(__filterParams[key], 'shape') === 'object'
          ) {
            return null;
          }

          const isWildcard = key === 'q';

          let filterValue = remainingQueries[key] || '';

          // if filter value is an object, we need to flatten it
          // eg: { customerAccount: { code: "CA-code" } }
          // to
          // "customerAccount.code": "CA-code"
          const isNested = !!(typeof filterValue === 'object');

          let filterKey = !isWildcard ? (queryKey ? `${key}:` : `${key}=`) : ``;
          if (isNested) {
            const flatFilter = flatten({ [key]: filterValue });
            [filterKey] = Object.keys(flatFilter);

            filterValue = flatFilter[filterKey];
            filterKey = `${filterKey}:`;
          }

          if (
            !isNested &&
            isString(filterValue) &&
            filterValue.includes('@filterKey')
          ) {
            [, filterKey] = filterValue.match(/\[(.*?)\]/);
            filterKey = `${filterKey}:`;

            [, filterValue] = filterValue.split(':');
          }

          if (!isNested && isString(filterValue) && filterValue.includes('*')) {
            if (filterValue.includes(filterKey.replace(':', ''))) {
              return `${wildcardPrefix}${filterValue}`;
            }

            return `${wildcardPrefix}${filterKey}${filterValue}`;
          }

          return filterValue
            ? esEnabled && isWildcard
              ? `${filterKey}*${filterValue}*`
              : `${filterKey}${filterValue}`
            : '';
        })
        .filter((el) => el)
        .join(apiSeparator);
    }
  }
};

const getFinalFilters = (filters, options) => {
  const { wildcardPrefixes, wildcardPrefix } = options;

  return Object.entries(filters).reduce((acc, [key, value]) => {
    const finalKey = getFilterKey(key, { wildcardPrefixes, wildcardPrefix });

    const finalValue = getFilterValue(value);

    return {
      ...acc,
      [finalKey]: finalValue
    };
  }, {});
};

const getFilterKey = (key, { wildcardPrefixes, wildcardPrefix }) => {
  switch (true) {
    case has(wildcardPrefixes, key): {
      return `${wildcardPrefixes[key]}${key}`;
    }
    case !!wildcardPrefix: {
      return `${wildcardPrefix}${key}`;
    }
    default: {
      return key;
    }
  }
};

const getFilterValue = (value) => {
  switch (value) {
    case '@currentDate': {
      const current = moment()
        .startOf('day')
        .valueOf();

      return current;
    }
    default: {
      return value;
    }
  }
};

export default {
  [GET_LIST]: list,
  [GET_MANY]: getMany
};
