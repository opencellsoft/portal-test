import {
  get,
  has,
  isEmpty,
  isUndefined,
  isNil,
  omit,
  set,
  pick,
  omitBy,
  isArray,
  orderBy
} from 'lodash';
import { stringify } from 'query-string';
import {
  extractMatrixData,
  findKeyField,
  setCFValue,
  getCFDefinitionByCode,
  serializeCFMatrix,
  AddCFHandlers,
  sortCFsByLatestVersion,
  createMatrix
} from '../../utils/custom-fields';

import {
  CUSTOM_FIELD_MULTI,
  CUSTOM_FIELD_STRING,
  CUSTOM_FIELD_LIST,
  CUSTOM_STORAGE_MAP,
  CUSTOM_STORAGE_MATRIX,
  CUSTOM_FIELD_ENTITY
} from '../../constants/generic';

export const formatESResponse = ({ json }, { schema, keyColumn = 'code' }) => {
  const { hits: rawData } = JSON.parse(json.searchResults);
  const { hits = [], total = 0 } = rawData || {};

  // eslint-disable-next-line camelcase
  const data = hits.map(({ _source: { description_sort, ...record } }) =>
    transformFromSchema(
      {
        ...record,
        id: record[keyColumn] || record._id
      },
      schema,
      keyColumn
    )
  );

  return {
    total,
    data
  };
};

export const extractResponse = ({
  json,
  body,
  responseContainer = '',
  totalContainer = 'paging.totalNumberOfRecords',
  nestedContainer,
  serialized,
  genericAPIEnabled,
  isFile,
  keyColumn,
  schema,
  references,
  page,
  perPage,
  isList,
  nullToWildcard
}) => {
  const rawData = json
    ? get(json, genericAPIEnabled ? 'data' : responseContainer) || json
    : body;

  if (isFile) {
    return { data: rawData };
  }

  let formattedData = serialized ? JSON.parse(rawData) : rawData;

  if (!isList && !Array.isArray(formattedData)) {
    formattedData = nestedContainer
      ? get(formattedData, nestedContainer)
      : formattedData;

    return {
      data: transformFromSchema(
        formattedData,
        schema,
        genericAPIEnabled && !keyColumn ? 'id' : keyColumn,
        references
      )
    };
  }

  let total;
  if (isList && !genericAPIEnabled && !Array.isArray(formattedData)) {
    formattedData = [formattedData];
    total = get(json, totalContainer, formattedData.length);
  } else if (isList && genericAPIEnabled) {
    ({ total } = json);

    if (!total) {
      formattedData = [];
    }
  } else if (isList) {
    total = get(json, totalContainer, formattedData.length);
  }

  formattedData = nestedContainer
    ? formattedData
        .filter((item) => !!item[nestedContainer])
        .map(({ [nestedContainer]: nestedData }) => ({
          ...nestedData
        }))
    : formattedData;

  let data =
    !isUndefined(page) &&
    !isUndefined(perPage) &&
    formattedData.length > perPage
      ? formattedData.splice((page - 1) * perPage, perPage)
      : formattedData;

  if (!isEmpty(nullToWildcard)) {
    data = data.map((item) => transformNullToWildcard(item, nullToWildcard));
  }

  return {
    total,
    data: data.map((data) =>
      transformFromSchema(
        data,
        schema,
        genericAPIEnabled && !keyColumn ? 'id' : keyColumn,
        references
      )
    )
  };
};

/**
 * Transform an entity using a schema being provided from config
 *
 * @param {object} data
 * @param {object} schema
 */
export const transformFromSchema = (
  data,
  schema = {},
  keyColumn = 'code',
  references = []
) => {
  const { [keyColumn]: identifier, customFields, ...rest } = data;

  if (isEmpty(schema)) {
    return {
      ...rest,
      id: identifier,
      [keyColumn]: identifier,
      customFields: sortCFsByLatestVersion(customFields)
    };
  }

  return {
    ...data,
    ...performTransform(
      {
        id: identifier,
        [keyColumn]: identifier,
        customFields: sortCFsByLatestVersion(customFields)
      },
      schema,
      data
    )
  };
};

const readCFValue = (data, source) => {
  switch (true) {
    case source.startsWith('CF.MULTI_VALUE'):
      return readCFValueFromMap(data, source);
    case source.startsWith('CF.STRING'):
      return readCFValueFromString(data, source);
    case source.startsWith('CF.DATE'):
      return readCFValueFromDate(data, source);
    case source.startsWith('CF.BOOLEAN'):
      return readCFValueFromBoolean(data, source);
    case source.startsWith('CF.DOUBLE'):
      return readCFValueFromDouble(data, source);
    default:
      return null;
  }
};

const performTransform = (record, schema, data) => {
  Object.keys(schema).forEach((source) => {
    const target = schema[source];
    const hasData = has(data, source);

    if (hasData) {
      let possibleData = get(data, source);

      if (typeof target === 'object' && possibleData) {
        possibleData = possibleData.map((el) =>
          transformFromSchema(el, target, 'id')
        );
        data[source] = possibleData;
      } else if (target.indexOf(',') > -1) {
        const targets = target.split(',');
        targets.forEach((element) => {
          record[element] = possibleData;
        });
      } else {
        record[target] = possibleData;
      }

      return;
    }

    if (source.startsWith('CF.MULTI_VALUE')) {
      record[target] = readCFValueFromMap(data, source);
    } else if (source.startsWith('CF.STRING')) {
      record[target] = readCFValueFromString(data, source);
    } else if (source.startsWith('CF.DATE')) {
      record[target] = readCFValueFromDate(data, source);
    } else if (source.startsWith('CF.BOOLEAN')) {
      record[target] = readCFValueFromBoolean(data, source);
    } else if (source.startsWith('CF.DOUBLE')) {
      record[target] = readCFValueFromDouble(data, source);
    } else if (source.startsWith('CF.LONG')) {
      record[target] = readCFValueFromLong(data, source);
    } else if (Array.isArray(target)) {
      const [actualSource, mappedTarget] = source.split(':');

      record[actualSource] = readValuesFromArray(data, target, mappedTarget);
    } else if (source.includes('[]') && target.includes('[]')) {
      // @todo only handle one level of nesting for now
      const [arrayPath, nestedProperty] = source.split('[].');
      const dataContainer = get(data, arrayPath) || [];
      const cleanTarget = target.substring(arrayPath.length + '[].'.length);
      const [nextTarget, fieldToAdd] = cleanTarget.split('[]->');

      const alteredData = dataContainer.map(
        ({ [nestedProperty]: nestedData, ...rest }, index) => {
          const existingDataContainer = get(record, arrayPath) || [];
          const existingData =
            get(existingDataContainer, `[${index}].${nextTarget}`) || [];

          return {
            ...rest,
            [nextTarget]: [
              ...existingData,
              ...nestedData.map((el) => ({
                ...el,
                ...fieldToAdd.split('|').reduce((res, item) => {
                  const [fieldName, fieldValue] = item.split(':');
                  const value = fieldValue.includes('CF.')
                    ? readCFValue(el, fieldValue)
                    : fieldValue;

                  return { ...res, [fieldName]: value };
                }, {})
              }))
            ]
          };
        }
      );

      set(record, arrayPath, alteredData);
    }
  });

  return record;
};

const formatMapField = (customField) => {
  const {
    mapValue,
    valuePeriodPriority,
    valuePeriodStartDate,
    valuePeriodEndDate
  } = customField;

  if (!mapValue) {
    return { data: [customField] };
  }

  const { key, ...rest } = mapValue || {};
  const { value: rawLabels } = key || '';
  if (!rawLabels) {
    return { data: [customField] };
  }
  const labels = rawLabels.split('/') || '';

  const formattedData = Object.keys(rest).map((key) => {
    const { value } = rest[key] || {};
    if (!value) {
      return {};
    }

    const values = [...key.split('|'), ...value.split('|')];

    return labels.reduce(
      (res, label, index) => ({ ...res, [label]: values[index] }),
      {}
    );
  });

  return {
    data: formattedData,
    ...(!isUndefined(valuePeriodPriority)
      ? {
          valuePeriodPriority,
          valuePeriodStartDate,
          valuePeriodEndDate
        }
      : {})
  };
};

/**
 * Read value from CF of type Map
 * @param {object} data
 * @param {string} source
 */
const readCFValueFromMap = (data, source) => {
  const [, , , nestedField] = source.split('.');

  let customField = getCustomFieldBySource(data, source);

  if (isEmpty(customField)) {
    return null;
  }

  customField = !Array.isArray(customField) ? [customField] : customField;

  if (nestedField) {
    const [match] = customField || [];
    const isSingleValue = !match.versionable && customField.length < 2;
    const { data: dataArray } = formatMapField(match) || {};

    const filteredData = dataArray
      .map(({ [nestedField]: field }) => field)
      .filter((el) => el);
    const value = isSingleValue ? filteredData[0] : filteredData;

    return !isEmpty(value) ? value : null;
  }

  return customField.reduce((res, field) => {
    const { data: fieldData } = formatMapField(field) || {};

    return [...res, ...fieldData];
  }, []);
};

const getCustomFieldBySource = (data, source) => {
  const customFields = get(data, `customFields.customField`) || [];

  const [, , cfCode] = source.split('.');

  let formattedCfCode = cfCode || '';
  if (formattedCfCode.indexOf('[@record/') > -1) {
    const [, rest] = formattedCfCode.split('/');
    const [attribute, staticSuffix] = rest.split(']');

    formattedCfCode = `${get(data, attribute)}${staticSuffix}`;
  }

  return customFields.find((cf) => cf.code === formattedCfCode);
};

/**
 * Read value from a CF of type String
 * @param {*} data
 * @param {*} source
 */
const readCFValueFromString = (data, source) => {
  const customField = getCustomFieldBySource(data, source);
  if (customField) {
    return customField.stringValue;
  }

  return null;
};

const readCFValueFromDouble = (data, source) => {
  const customField = getCustomFieldBySource(data, source);
  if (customField) {
    return customField.doubleValue;
  }

  return null;
};

/**
 * Read value from a CF of type Long
 * @param {*} data
 * @param {*} source
 */
const readCFValueFromLong = (data, source) => {
  const customFields = get(data, `customFields.customField`) || [];
  const cfCode = source.split('.')[2];
  const customField = customFields.find((cf) => cf.code === cfCode);

  if (customField) {
    return customField.longValue;
  }

  return null;
};

/**
 * Read value from a CF of type Date
 * @param {*} data
 * @param {*} source
 */
const readCFValueFromDate = (data, source) => {
  const customField = getCustomFieldBySource(data, source);
  if (customField) {
    return customField.dateValue;
  }

  return null;
};

/**
 * Read value from a CF of type String
 * @param {*} data
 * @param {*} source
 */
const readCFValueFromBoolean = (data, source) => {
  const customField = getCustomFieldBySource(data, source);

  if (customField) {
    return customField.booleanValue;
  }

  return null;
};

/**
 * Read sub items
 * @param {*} data
 * @param {*} schema
 */
const readValuesFromArray = (data, schema, mappedTarget) => {
  const items =
    (mappedTarget && mappedTarget.startsWith('CF')
      ? readCFValueFromMap(data, mappedTarget)
      : data[mappedTarget]) || [];

  return items.map((item) =>
    schema.reduce((res, definition) => {
      const [definitionKey] = Object.keys(definition);
      const destination = definition[definitionKey];

      const sourceArray = definitionKey.split('.');
      const source = sourceArray[sourceArray.length - 1];

      return {
        ...res,
        [destination]: item[source]
      };
    }, {})
  );
};

/**
 * Dissmis data indexes when creating CF
 *
 * @param {object} nestedData
 */
const dismissDataIndexes = (nestedData) => {
  const [index, ...rest] = Object.keys(nestedData);

  return isEmpty(rest) ? dismissDataIndexes(nestedData[index]) : nestedData;
};

/**
 * Will update or create custom fields
 *
 * @param {object} record
 * @param {object} updatedValues
 * @param {object} cfDefinition
 * @param {bool} isDelete
 */
export const rebuildRecord = (
  record,
  updatedValues,
  cfDefinition,
  isDelete
) => {
  const fields = get(record, 'customFields.customField') || [];
  const cfKeys = Object.keys(updatedValues);

  const preservedCfs = fields.filter(
    ({ code }) => !cfKeys.find((key) => key === code)
  );
  const updatedCfsData = fields.filter(({ code }) =>
    cfKeys.find((key) => key === code)
  );
  const newCfsDataKeys = cfKeys.filter(
    (cfKey) => !updatedCfsData.find(({ code }) => code === cfKey)
  );
  // const newCfsDataKeys = differenceBy(updatedCfsData, ({code}) => !cfKeys.includes(code));
  const nextNewValues = newCfsDataKeys.reduce((res, key) => {
    // Dismiss cf data indexes (only used for updates)
    // when creating new CF field object from schema
    const extractedData = dismissDataIndexes(updatedValues[key]);
    res.push(createCF(fields, key, extractedData, cfDefinition));

    return res;
  }, []);

  const nextUpdatedValues = cfKeys.reduce((res, key) => {
    const sortedVersions = sortCFsByLatestVersion({
      customField: updatedCfsData.filter(({ code }) => code === key)
    });
    const { customField: sortedFields = [] } = sortedVersions || {};

    return [
      ...res,
      ...updateCF(sortedFields, updatedValues, cfDefinition, isDelete)
    ];
  }, []);

  const nextValues = [...nextUpdatedValues, ...nextNewValues];

  return {
    ...record,
    customFields: {
      customField: [...preservedCfs, ...nextValues].filter((el) => el)
    }
  };
};

export const createCF = (customFields, cfCode, data, cfDefinition) => {
  const currentCFDefinition = getCFDefinitionByCode(cfDefinition, cfCode);

  const { fieldType = '' } = currentCFDefinition || {};

  switch (fieldType) {
    case CUSTOM_FIELD_MULTI:
      return createMatrix(customFields, currentCFDefinition, [data]);
    default:
      return null;
  }
};
/**
 * Update existing CF
 *
 * @param {array} fields
 * @param {object} updatedValues
 * @param {object} cfDefinition
 * @param {bool} isDelete
 */
export const updateCF = (fields = [], updatedValues, cfDefinition, isDelete) =>
  fields.map((currentCF, index) => {
    const { code, fieldType } = currentCF;

    if (!updatedValues[code] || !updatedValues[code][index]) {
      return currentCF;
    }

    switch (fieldType) {
      case CUSTOM_FIELD_MULTI:
        return updateCFMatrix(
          currentCF,
          updatedValues[code][index],
          cfDefinition,
          isDelete
        );
      default:
        return currentCF;
    }
  });

export const updateCFMatrix = (
  cf,
  updatedData = {},
  cfDefinition,
  isDelete
) => {
  const { code, mapValue } = cf;
  const { key, ...values } = mapValue || {};
  const { create } = updatedData || {};

  const currentCFDefinition = getCFDefinitionByCode(cfDefinition, code);

  const nextMatrix = {
    ...cf,
    mapValue: Object.keys(values).reduce(
      (res, cfKey, index) => ({
        ...res,
        ...serializeCFMatrix(
          cfKey,
          values[cfKey],
          updatedData[index],
          currentCFDefinition,
          isDelete
        )
      }),
      { key }
    )
  };

  return !isEmpty(create)
    ? {
        ...nextMatrix,
        mapValue: {
          ...nextMatrix.mapValue,
          ...AddCFHandlers[CUSTOM_FIELD_MULTI](create, currentCFDefinition)
        }
      }
    : nextMatrix;
};

export const getDeletedCF = (data, __cf, codeField) => {
  const cfCode = Object.keys(__cf)[0];
  const originalData = data.customFields.customField.find(
    (cf) => cf.code === cfCode
  );
  const existingKeys = Object.keys(originalData.mapValue);
  const newKeys = __cf[cfCode][0].map((item) => item[codeField]);

  return existingKeys.filter((key) => newKeys.indexOf(key) === -1)[0];
};

export const getTraversableSchema = (schema) =>
  Array.isArray(schema)
    ? schema.reduce((res, definition) => {
        const definitionKey = Object.keys(definition)[0];
        const value = definition[definitionKey];

        return { ...res, [definitionKey]: value };
      }, {})
    : schema;

export const getTraversableSchemaWithChild = (traversableSchema) => {
  const parentCfItem = Object.keys(traversableSchema).find(
    (elm) => !elm.startsWith('CF') && elm.includes(':')
  );

  const childCfItem = Object.values(get(traversableSchema, parentCfItem)).find(
    (elm) =>
      !Object.keys(elm)[0].startsWith('CF') && Object.keys(elm)[0].includes(':')
  );

  return {
    ...traversableSchema,
    ...childCfItem
  };
};

export const getProvidedValue = ({ fieldType, initialKey, value }) =>
  fieldType === CUSTOM_FIELD_MULTI && typeof value !== 'object'
    ? { [initialKey]: value }
    : typeof value === 'number' || typeof value === 'boolean'
    ? value
    : !value
    ? null
    : value;

export const reapplySchema = (
  record,
  schema,
  cfDefinition,
  { hasCFsUpdated }
) => {
  if (isEmpty(cfDefinition) || hasCFsUpdated) {
    const injectedProperties = Object.values(schema);

    return omit(record, injectedProperties);
  }

  const traversableSchema = getTraversableSchema(schema);

  const customFields = get(record, `customFields.customField`) || [];

  return Object.keys(traversableSchema).reduce((res, source) => {
    const rawTarget = traversableSchema[source];

    let target = rawTarget;
    if (Array.isArray(rawTarget)) {
      [target] = source.split(':');
    }

    const { [target]: value, ...rest } = res || {};
    const [, fieldType, cfCode, initialKey] = source.split('.');

    if (source.includes('CF.')) {
      const relevantCFIndex = customFields.findIndex(
        ({ code }) => code === cfCode
      );

      const providedValue = getProvidedValue({ fieldType, value, initialKey });

      const fieldDefinition = getCFDefinitionByCode(cfDefinition, cfCode);

      if (relevantCFIndex > -1) {
        customFields[relevantCFIndex] =
          fieldType === CUSTOM_FIELD_MULTI
            ? updateCFMatrix(
                customFields[relevantCFIndex],
                providedValue || {},
                cfDefinition
              )
            : setCFValue(customFields[relevantCFIndex], providedValue);
      } else if (AddCFHandlers[fieldType]) {
        const cfField = AddCFHandlers[fieldType](
          providedValue,
          fieldDefinition
        );

        if (!isEmpty(cfField)) {
          customFields.push(cfField);
        }
      }
    }

    if (
      isEmpty(customFields) ||
      (isArray(customFields) && customFields.every((field) => isEmpty(field)))
    ) {
      return rest;
    }

    return {
      ...rest,
      customFields: {
        customField: customFields
      }
    };
  }, record);
};

export const removeSchemaValues = (data, schema = {}) => {
  Object.keys(schema).forEach((source) => {
    const cfCode = source.split('.')[2];
    data = omit(data, cfCode);
  });

  return data;
};

export const hasURLPlaceholder = (url) => url.includes(':');

export const hydrateParams = (url, params = {}) => {
  const hydratedObject = Object.keys(params).reduce(
    (res, key) => {
      const { url: currentUrl, params: currentParams } = res;
      const possibleKeyIndex = currentUrl.indexOf(key);

      if (
        possibleKeyIndex > -1 &&
        currentUrl.charAt(possibleKeyIndex - 1) === ':'
      ) {
        const { [key]: excludedParam, ...rest } = currentParams;

        return {
          // eslint-disable-next-line security/detect-non-literal-regexp
          url: currentUrl.replace(new RegExp(`:${key}`, 'g'), excludedParam),
          params: rest
        };
      }

      return res;
    },
    { url, params }
  );

  return {
    url: hydratedObject.url,
    params: stringify(hydratedObject.params)
  };
};

export const clearESProperties = (data = {}, originalData = {}) =>
  Object.keys(data).reduce((res, property) => {
    if (originalData.hasOwnProperty(property)) {
      res[property] = data[property];
    }

    return res;
  }, {});

export const trimObject = (data) => {
  if ((!Array.isArray(data) && typeof data !== 'object') || isNil(data)) {
    return data;
  }

  return Object.keys(data).reduce(
    (acc, key) => {
      acc[key] =
        typeof data[key] === 'string'
          ? data[key].trim()
          : trimObject(data[key]);

      return acc;
    },
    Array.isArray(data) ? [] : {}
  );
};

export const handleNestedProperties = (data, nestedProperties) => {
  if ((!Array.isArray(data) && typeof data !== 'object') || isNil(data)) {
    return data;
  }

  return nestedProperties.reduce((acc, key) => {
    const [property, wrap] = key.split(':');
    const nesting = wrap.split('.');

    const nestedData = nesting
      .reverse()
      .reduce((res, newProperty) => ({ [newProperty]: res }), acc[property]);

    return { ...acc, ...nestedData };
  }, data);
};

export const handleWrappedProperties = (data, wrappedProperties) => {
  if ((!Array.isArray(data) && typeof data !== 'object') || isNil(data)) {
    return data;
  }

  return wrappedProperties.reduce((acc, key) => {
    const [property, aliasDefinitions] = key.split(':');
    const [alias, ...wrapping] = aliasDefinitions.split('.');

    const { [property]: value, ...rest } = acc;

    const reversedWrapping = wrapping.reverse();
    const nextValue = Array.isArray(value)
      ? value.map((el) =>
          reversedWrapping.reduce((res, wrap) => {
            if (get(res, wrap)) {
              return res;
            }

            return { [wrap]: res };
          }, el)
        )
      : value;

    rest[alias] = nextValue;

    return rest;
  }, data);
};

export const handleRenamedProperties = (data, renameProperties) => {
  if ((!Array.isArray(data) && typeof data !== 'object') || isNil(data)) {
    return data;
  }

  return renameProperties.reduce((acc, key) => {
    const [property, alias] = key.split(':');
    const { [property]: value, ...rest } = acc;

    rest[alias] = value;

    return rest;
  }, data);
};

export const handleRequestProperties = (data, providerConfig) => {
  const {
    excludeProperties,
    wrapProperties,
    renameProperties
  } = providerConfig;

  let requestData = { ...data };

  if (Array.isArray(excludeProperties)) {
    requestData = omit(requestData, excludeProperties);
  }

  if (Array.isArray(wrapProperties)) {
    requestData = handleWrappedProperties(requestData, wrapProperties);
  }

  if (Array.isArray(renameProperties)) {
    requestData = handleRenamedProperties(requestData, renameProperties);
  }

  return requestData;
};

export const applyTransform = (data, transform = {}) =>
  Object.keys(transform).reduce((res, property) => {
    const currentTransformation = transform[property];

    if (!res.hasOwnProperty(property)) {
      return res;
    }

    // eslint-disable-next-line no-new-func
    res[property] = Function(
      'res',
      'property',
      `"use strict";return (res[property]${currentTransformation})`
    )(res, property);

    return res;
  }, data);

/**
 *
 * @param {object} record
 * @param {array} references
 */
export const convertReferencesToIDs = (record = {}, references = []) => {
  const convertedReferences = references.reduce((res, referenceDefinition) => {
    const [field, referenceField = 'id'] = referenceDefinition.split(':');
    const referenceData = get(record, field) || [];

    res[field] = referenceData.map(
      ({ [referenceField]: referenceValue }) => referenceValue
    );

    return res;
  }, {});

  return {
    ...record,
    ...convertedReferences
  };
};

/**
 * Transform matrix to MapValue to send it to the server
 *
 * @param {object} cfDefinition
 * @param {array} value //matrix
 *
 * @return {object}
 */
export const transformMultiValueToMapValue = (
  cfDefinition = {},
  value = []
) => {
  if (isEmpty(value)) return {};

  const keyCode = get(
    cfDefinition.matrixColumn.find((item) => item.columnUse === 'USE_KEY'),
    'code'
  );

  return isArray(value)
    ? value.reduce(
        (acc, currentValue) => {
          const values = Object.values(currentValue).join('|');

          const currentValueKey = currentValue[keyCode];

          acc[currentValueKey] = {
            value: values
          };

          return acc;
        },
        {
          key: {
            value:
              typeof value[0] === 'object' && Object.keys(value[0]).join('/')
          }
        }
      )
    : {};
};

/**
 * Transform reference field value to ENTITY field value
 * @param {object} cfDefinition
 * @param {string} value
 *
 * @return {object}
 */
export const transformToEntityValue = (cfDefinition = {}, value) => ({
  classname: 'org.meveo.model.customEntities.CustomEntityInstance',
  classnameCode: get(cfDefinition, 'code', null),
  code: value
});

/**
 * Transform reference array field value to ENTITY list field value
 * @param {object} cfDefinition
 * @param {array} values
 *
 * @return {object}
 */
export const transformToEntityListValue = (cfDefinition = {}, values) =>
  values.map((value) => ({
    value: {
      entityReferenceValue: {
        classname: get(cfDefinition, 'entityClazz'),
        classnameCode: null,
        code: value
      }
    }
  }));

/**
 * Transform matrix to MapValue to send it to the server
 *
 * @param {object} cfDefinition
 * @param {array} value //matrix
 *
 * @return {object}
 */
export const transformMatrixToMapValue = (cfDefinition = {}, value = []) => {
  if (isEmpty(value)) return {};

  const { matrixColumn = [] } = cfDefinition;
  const keyFields = matrixColumn.filter(
    (field) => field.columnUse === 'USE_KEY'
  );

  return value.reduce(
    (acc, element) => {
      const currentKey = keyFields.map(({ code }) => element[code]).join('|');
      const currentValue = get(element, 'value', '');

      acc[currentKey] = {
        value: currentValue
      };

      return acc;
    },
    { key: { value: Object.keys(value[0]).join('/') } }
  );
};

export const transformCFMatrixToMapValue = (cfDefinition, value = []) => {
  const keyCode = get(findKeyField(cfDefinition.matrixColumn), 'code');

  return value.length >= 1
    ? value.reduce(
        (accumulator, currentValue) => {
          const values = Object.values(omit(currentValue, [keyCode])).join('|');
          const currentValueKey = currentValue[keyCode];
          accumulator[currentValueKey] = {
            value: values
          };

          return accumulator;
        },
        { key: { value: Object.keys(value[0]).join('/') } }
      )
    : null;
};

export const transformMapStringToMapValue = (value) => {
  if (isEmpty(value)) return {};

  return value.reduce((acc, currentValue) => {
    const key = get(currentValue, 'key');

    acc[key] = {
      value: get(currentValue, 'value')
    };

    return acc;
  }, {});
};

/**
 * Transform Subforms customFields values to formatted data for the server
 *
 * @param {object} subformsValues
 * @param {object} cfDefinitions
 * @param {object} {templatePath, cfOutputContainer} //config containing pathes
 *
 * @return {array}
 */
export const transformSubformCFsValues = (
  subformsValues = {},
  cfDefinitions,
  { templatePath, cfOutputContainer }
) =>
  Object.values(subformsValues).reduce((acc, current) => {
    const transformedItem = { ...current[templatePath] };

    transformedItem[cfOutputContainer] = transformCFsDataByDefinitions(
      transformedItem.customFields,
      cfDefinitions
    );
    delete transformedItem.customFields;

    return [...acc, { ...current, [templatePath]: transformedItem }];
  }, []);

/**
 * Transform customFields values to customField format for the server
 *
 * @param {object} data
 * @param {object} cfDefinitions
 *
 * @return {object}
 */
export const transformCustomFields = (data, cfDefinitions) => {
  const customFields = omit(get(data, 'customFields', {}), [
    'customField',
    'inheritedCustomField'
  ]);
  const filteredFields = omitBy(customFields, isNil);

  return {
    ...data,
    customFields: {
      customField: transformCFsDataByDefinitions(
        filteredFields,
        cfDefinitions
      ).filter((item) => has(item, 'code') && !!item.code)
    }
  };
};

/**
 * Transform customFields values to customField format by its definition
 *
 * @param {object} cfs
 * @param {object} cfDefinitions
 *
 * @return {array}
 */
const transformCFsDataByDefinitions = (cfs = {}, cfDefinitions = {}) => {
  const propertiesToInclude = get(cfDefinitions, 'includeProperties', []);

  return Object.entries(cfs).map(([cfKey, value]) => {
    const cfDefinition =
      get(cfDefinitions, 'field', []).find((el) => el.code === cfKey) || {};

    const fieldType = get(cfDefinition, 'fieldType');
    const storageType = get(cfDefinition, 'storageType');

    const updatedData = isEmpty(propertiesToInclude)
      ? { code: cfKey, fieldType, storageType }
      : pick(cfDefinition, propertiesToInclude);

    if (
      fieldType === CUSTOM_FIELD_STRING &&
      storageType === CUSTOM_STORAGE_MAP
    ) {
      value = transformMapStringToMapValue(value);
    }

    if (storageType === CUSTOM_STORAGE_MATRIX) {
      value = transformMatrixToMapValue(cfDefinition, value);
    }

    if (fieldType === CUSTOM_FIELD_MULTI) {
      value = transformMultiValueToMapValue(cfDefinition, value);
    }

    if (fieldType === CUSTOM_FIELD_ENTITY) {
      if (storageType === CUSTOM_FIELD_LIST) {
        value = transformToEntityListValue(cfDefinition, value);
      } else {
        value = transformToEntityValue(cfDefinition, value);
      }
    }

    return setCFValue(updatedData, value);
  });
};

/**
 * Remove cf matrix row(s) from the record
 *
 * @param {object} record
 * @param {object} fieldsDefinition
 * @param {object} rest //other props selectedIds, cfCode, cfIndex
 *
 * @return {object} updated record
 */
export const removeCfsFromRecord = (
  record = {},
  fieldsDefinition = {},
  { selectedIds, cfCode, cfIndex }
) => {
  const cfs = get(record, 'customFields.customField', []);
  const targetCfs = cfs.filter((cf) => cf.code === cfCode);
  const sortedCfs = orderBy(targetCfs, 'valuePeriodPriority', 'desc');

  const targetCf = sortedCfs[cfIndex];
  const targetCfData = extractMatrixData(targetCf, fieldsDefinition) || {};

  const newData = {
    ...targetCfData,
    data: targetCfData.data.filter(
      (item, i) => !selectedIds.includes(String(i))
    )
  };
  const cfDefinition = get(fieldsDefinition, 'field', []).find(
    (el) => el.code === cfCode
  );
  const transformedNewData = transformCFMatrixToMapValue(
    cfDefinition,
    newData.data
  );

  const updatedTargetCf = { ...targetCf, mapValue: transformedNewData };

  const updatedCfs = cfs.reduce((acc, current) => {
    if (
      current.code === updatedTargetCf.code &&
      current.valuePeriodPriority === updatedTargetCf.valuePeriodPriority
    ) {
      current = { ...updatedTargetCf };
    }

    return [...acc, current];
  }, []);

  set(record, 'customFields.customField', updatedCfs);

  return record;
};

/**
 * transform data to custom table format
 *
 * @param {object} data
 * @param {string} customTableCode from provider config
 * @param {number} id record id for update
 *
 * @return {object} formatted data
 */
export const transformCustomTable = (data, customTableCode, nestedBody, id) => {
  switch (true) {
    case !!nestedBody:
      return {
        customTableCode,
        [nestedBody]: {
          id,
          values: { ...data }
        }
      };
    default:
      return {
        customTableCode,
        record: [
          {
            values: id ? { ...data, id } : { ...data }
          }
        ]
      };
  }
};

/**
 * transform data according to wildcardToNull config (for CREATE, UPDATE)
 * @param {object} data
 * @param {object} wildcardToNull wildcardToNull from provider config
 *
 * @return {object} transformed data according to wildcardToNull
 */

export const transformWildcardToNull = (data, wildcardToNull = {}) =>
  Object.entries(data).reduce((acc, [key, value]) => {
    const wildcard = get(wildcardToNull, key, null);
    if (wildcard && value === wildcard) {
      return {
        ...acc,
        [key]: null
      };
    }

    return {
      ...acc,
      [key]: value
    };
  }, {});

/**
 * transform data according to nullToWildcard config (for GET_LIST, GET_ONE)
 * @param {object} data
 * @param {object} nullToWildcard nullToWildcard from provider config
 *
 * @return {object} transformed data according to nullToWildcard
 */
export const transformNullToWildcard = (data, nullToWildcard = {}) =>
  Object.entries(data).reduce((acc, [key, value]) => {
    const wildcard = get(nullToWildcard, key, null);
    if (wildcard && value === null) {
      return {
        ...acc,
        [key]: wildcard
      };
    }

    return {
      ...acc,
      [key]: value
    };
  }, {});

/**
 * get delete request url by some config props
 * @param {string} url
 * @param {string|number} id
 * @param {object} options // customTableCode, genericAPIEndpoints
 *
 * @return {string}
 */

const getDeleteRequestUrl = (
  url,
  id,
  { customTableCode, genericAPIEndpoints }
) =>
  !!customTableCode
    ? url
    : url.includes(genericAPIEndpoints)
    ? `${url}/${id}`
    : `${url}${id}`;

/**
 * get delete request body by some config props
 * @param {string|number} id
 * @param {object} options // customTableCode
 *
 * @return {object}
 */

const getDeleteRequestBody = (id, { customTableCode }) =>
  !!customTableCode
    ? {
        customTableCode,
        record: [{ id: Number(id), values: { id: Number(id) } }]
      }
    : {};

/**
 * get delete request url by some config props
 * @param {string} url
 * @param {string|number} id
 * @param {object} options // customTableCode, genericAPIEndpoints
 *
 * @return {object} // { url, body } of delete request
 */

export const getDeleteRequestParams = (
  url,
  id,
  { customTableCode, genericAPIEndpoints }
) => ({
  url: getDeleteRequestUrl(url, id, { customTableCode, genericAPIEndpoints }),
  body: getDeleteRequestBody(id, { customTableCode })
});

/**
 * update previous version data
 * @param {object} prevData
 * @param {object} newData
 * @param {object} options // newSource, prevSource
 *
 * @return {object} // updated previous version data
 */
export const updatePrevVersionData = (prevData, newData, options) => {
  const { newSource, prevSource } = options;

  const newVersionStartDate = get(newData, newSource);

  set(prevData, prevSource, newVersionStartDate);

  return prevData;
};

/**
 * configure api call for custom action
 * @param {func} dataProvider
 * @param {object} options
 *
 * @return {func}
 */
export const configureCustomActionAPICall = (
  dataProvider,
  { resource, deleteCf, record, fieldsDefinition, selectedIds, cfCode, cfIndex }
) => (requestData, verb) =>
  dataProvider(verb, resource, {
    data: {
      ...(deleteCf
        ? removeCfsFromRecord(record, fieldsDefinition, {
            selectedIds,
            cfCode,
            cfIndex
          })
        : requestData),
      __cfDefinition: fieldsDefinition
    }
  });
