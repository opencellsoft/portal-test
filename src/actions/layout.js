export const OC_SET_TITLE = 'OC_SET_TITLE';

export const setAppTitle = (title) => async (dispatch) =>
  dispatch({ type: OC_SET_TITLE, payload: title });
