export const CHANGE_THEME = 'CHANGE_THEME';

export const changeTheme = (theme) => async (dispatch) =>
  dispatch({
    type: CHANGE_THEME,
    payload: theme
  });
