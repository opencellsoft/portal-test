import { getTokenData, unsetTokens } from '../utils/auth';

export const USER_SIGNIN_SUCCESS = 'USER_SIGNIN_SUCCESS';
export const USER_SIGNOUT = 'USER_SIGNOUT';
export const USER_SIGNOUT_SUCCESS = 'USER_SIGNOUT_SUCCESS';
export const USER_SIGNOUT_FAILURE = 'USER_SIGNOUT_FAILURE';

export const signIn = () => async (dispatch) =>
  dispatch({ type: USER_SIGNIN_SUCCESS, payload: getTokenData() });

export const signOut = () => async (dispatch) => {
  dispatch({ type: USER_SIGNOUT });

  try {
    await unsetTokens();
    dispatch({ type: USER_SIGNOUT_SUCCESS });
  } catch (error) {
    dispatch({ type: USER_SIGNOUT_FAILURE, error });
  }
};

export default {
  signIn,
  signOut
};
