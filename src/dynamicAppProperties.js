const isProd = process.env.NODE_ENV === 'production';

const requiredAppProperties = ['REACT_APP_DOMAIN_URL', 'REACT_APP_AUTH_URL'];

/**
 * Will copy dynamic properties on page load
 */
const dynamicAppProperties =
  isProd && typeof window.__APP_PROPERTIES__ === 'object'
    ? { ...window.__APP_PROPERTIES__ }
    : {};

/**
 * Check if required properties are declared
 */
const finalAppProperties = requiredAppProperties.reduce(
  (res, requiredProperty) => {
    const hasProperty = dynamicAppProperties.hasOwnProperty(requiredProperty);

    // Default to process.env if property is missing
    const value = hasProperty
      ? dynamicAppProperties[requiredProperty]
      : process.env[requiredProperty];

    if (typeof value !== 'string') {
      throw new Error(
        `Missing required configuration property: ${requiredProperty}`
      );
    }

    res[requiredProperty] = value;

    return res;
  },
  {}
);

/**
 * Erase dynamic properties injection
 */
window.__APP_PROPERTIES__ = null;
delete window.__APP_PROPERTIES__;

export default finalAppProperties;
