module.exports = {
  extends: [
    'airbnb',
    'prettier',
    'prettier/react',
    'plugin:security/recommended',
    'plugin:jest/recommended'
  ],
  parser: 'babel-eslint',
  env: {
    browser: true,
    node: true
  },
  rules: {
    'no-console': 0,
    'no-debugger': 1,
    'global-require': 0,
    'func-names': 0,
    'no-shadow': 0,
    'no-use-before-define': 0,
    'no-param-reassign': 0,
    'new-cap': 0,
    camelcase: 2,
    'newline-before-return': 2,
    'no-underscore-dangle': 0,
    'no-prototype-builtins': 0,
    'no-nested-ternary': 0,
    'no-plusplus': 0,
    'no-extra-boolean-cast': 0,
    'spaced-comment': 0,
    'no-unused-vars': [
      'error',
      { vars: 'local', args: 'none', ignoreRestSiblings: true }
    ],
    'no-restricted-properties': 0,
    'no-restricted-syntax': 0,
    'no-confusing-arrow': 0,
    'no-case-declarations': 0,
    'linebreak-style': 0,
    'jsx-a11y/anchor-has-content': 0,
    'jsx-a11y/no-static-element-interactions': 0,
    'react/no-unescaped-entities': 0,
    'react/no-string-refs': 0,
    'react/sort-comp': 2,
    'react/no-multi-comp': 2,
    'react/no-danger': 0,
    'react/jsx-no-target-blank': 0,
    'react/no-array-index-key': 0,
    'react/jsx-filename-extension': 0,
    'react/require-default-props': 0,
    'react/forbid-prop-types': 0,
    'react/no-access-state-in-setstate': 0,
    'react/destructuring-assignment': 0,
    'import/no-cycle': 0,
    'import/prefer-default-export': 0,
    'import/no-named-as-default': 0,
    'import/no-extraneous-dependencies': [
      'error',
      { devDependencies: true, optionalDependencies: false }
    ],
    'security/detect-object-injection': 0,
    'security/detect-non-literal-fs-filename': 0
  },
  plugins: ['security', 'jest']
};
